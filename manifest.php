<?php

/* Application Manifest

User Session Variables list 'access_token','access_type','f_userid','l_userid','socialid','name','lastname','email','g_userid','company'
user_type			// Is user an ideator, or service_provider or an investor
access_token 		// Used for all type of data retrieval from social media platforms
access_type 		// Used to identify what social media he is logged in from
socialid 			// Facebook, Linkedin and Google Unique Id of a person
email 				// Email retrieved from google sign in, LINEDIN and FACEBOOK do not provide email
mobile				// Mobile number of the user
role 				// Role of an ideator
location 			// Location of an ideator
sector_expertise	// Sector Expertise of an ideator
student_alumni		// Tells wheather the user is student or alumni
institute			// Institute of the user
year_of_graduation 	// Year of graduation of the user
name 				// First name of the person LinkedIn and Facebook
lastname			// Last name of the person via LinkedIn
company				// Company the user belongs to
password_hash		// PHP password_hash if the user signs up through sayouideas default registration form
*/

?>