	<div class="col s12 center">
		<h3>Add a new sector</h3>
        <div class="center red-text">
            <?php echo validation_errors();?>
        </div>

	</div>

    <?php echo form_open('/admin/sectors/add'); ?>
        <div class="row custom">
        
            <div class="input-field col s12">
                <input required type="text" name="sector-name"  value="<?php echo set_value('sector-name')?>">
                <label>Sector Name</label>
                <input class="btn" type="submit" value="Add">
            </div>

        </div>
    
    </form>
    <div class="row custom">
        <h5>Sectors added by Ideators</h5>
        <ul class="collapsible" data-collapsible="expandable">

            <?php

            foreach($ideas_other_sectors as $sector)
            {
                ?>
                <li>
                    <div class="collapsible-header" style="text-transform: capitalize"><?php echo $sector['name']?></div>
                    <div class="collapsible-body" style="padding:20px"><span>Added by <a href="/users/<?php echo $sector['socialid']?>"><?php echo $sector['added_by']?></a>
                            <br>Idea: <a href="/ideas/<?php echo $sector['idea_url']?>"><?php echo $sector['idea_name']?></a>
                            <br>
                            <a class="btn green btn_add_sector" style="margin-top:15px" href="#" data-name="<?php echo $sector['name']?>">Add <?php echo $sector['name']?></a>
                        </span>
                    </div>
                </li>
                <?php
            }
        ?>
        </ul>
    </div>
    <div class="row custom">
        <h5>Sectors added by Users</h5>
        <ul class="collapsible" data-collapsible="expandable">
            <?php

            foreach($user_other_sectors as $sector)
            {
                ?>
                <li>
                    <div class="collapsible-header" style="text-transform: capitalize"><?php echo $sector['name']?></div>
                    <div class="collapsible-body" style="padding:20px"><span>Added by <a href="/users/<?php echo $sector['socialid']?>"><?php echo $sector['added_by']?></a> (<?php echo $sector['user_type']?>)
                            <br>
                            <a class="btn green btn_add_sector" style="margin-top:15px" href="#" data-name="<?php echo $sector['name']?>">Add <?php echo $sector['name']?></a>
                        </span>
                    </div>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>