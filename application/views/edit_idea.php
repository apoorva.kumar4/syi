<script>
    jQuery(document).ready(function($) {
        $(".modal-add-bullet-close").click(function () {
            setTimeout(function () {
                $(".modal-add-idea").closeModal();
                $(".lean-overlay").remove();
            });
        });
    });
</script>
<div id="modal-idea-delete" class="modal bottom-sheet">
    <div class="modal-content">
        <h4 id="delete_message_header">Delete</h4>
        <p>This will delete all the data, bullets, associated connections and this is irreversible, yes you get it!</p>
    </div>

    <div class="modal-footer">

        <a id="yes_delete" data-ideaid="<?php echo $idea['id']?>" href="#!" class="waves-effect waves-green btn red">Yes, Delete!</a>

        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Back</a>

    </div>

</div>

<div class="container">
    <div class="row card login-card z-depth-2">
    <div class="row">
        <div class="col s12 center">
            <h3>Edit Idea</h3>
            <div class="red-text">
                <?php echo validation_errors();?>
            </div>

        </div>
    </div>

    <div class="row custom">
        <div class="col s12 m6">
            <form id="uploadIdeaLogo" action="/tempimage" data-ideaid="<?php echo $idea_id?>">
                <h5>Logo</h5>
                <img id="logo-previewing" width="150px" height="150px"  src="<?php echo _f_get_idea_logo_url($idea['id'])?>">
                <div class="file-field input-field">
                    <input id="logofile" type="file" name="logofile">
                    <div class="file-path-wrapper file-path-wrapperx">
                        <input class="file-path validate" type="text" placeholder="Select an Image"">

                    </div>
                </div>
                <input type="submit" class="waves-effect waves btn" value="Save Logo">
            </form>

        </div>
        <div class="col s12 m6">
            <h5>Cover</h5>

            <script>
                jQuery(document).ready(function($){
                    $("#coverfile").change(function() {

                        var file = this.files[0];
                        var coverfile = file.type;
                        var match= ["image/jpeg","image/png","image/jpg"];
                        if(!((coverfile==match[0]) || (coverfile==match[1]) || (coverfile==match[2])))
                        {

                            Materialize.toast("Invalid image",4000)
                            return false;
                        }
                        else
                        {

                            var reader = new FileReader();
                            reader.onload = imageIsLoaded;
                            reader.readAsDataURL(this.files[0]);

                        }
                    });

                    $("#yes_delete").click(function(e){
                        e.preventDefault();
                        var ideaid=$(this).data("ideaid");
                        Materialize.toast("<span class='upload-status'>Please wait...</span>");
                        var url_pur="/api2/idea/"+ideaid;
                        $.ajax({
                            url: url_pur, // Url to which the request is send
                            type: "DELETE",             // Type of request to be send, called as method
                            contentType: false,       // The content type used when sending data to the server.
                            cache: false,             // To unable request pages to be cached
                            processData:false,        // To send DOMDocument or non processed data file it is set to false
                            success: function(data)   // A function to be called if request succeeds
                            {

                                var obj=JSON.parse(data);
                                if(obj.status=="success")
                                {
                                    Materialize.toast("Idea and its related data has been deleted successfully!",4000);
                                    setTimeout(function(){location.replace('/dashboard');},1000);
                                }
                                else{
                                    Materialize.toast("Failure"+ obj.description,4000);
                                }
                            }
                        });
                    });

                    function imageIsLoaded(e) {
                        $("#coverfile").css("color","green");
                        $('#cover-previewing').attr('src', e.target.result);
                        $('#cover-previewing').attr('width', 'fixed');
                        $('#cover-previewing').attr('height', 'auto');
                    };
                });


                jQuery(document).ready(function($){
                    $("#logofile").change(function() {

                        var file = this.files[0];
                        var coverfile = file.type;
                        var match= ["image/jpeg","image/png","image/jpg"];
                        if(!((coverfile==match[0]) || (coverfile==match[1]) || (coverfile==match[2])))
                        {

                            Materialize.toast("Invalid image",4000)
                            return false;
                        }
                        else
                        {

                            var reader = new FileReader();
                            reader.onload = imageIsLoaded;
                            reader.readAsDataURL(this.files[0]);

                        }
                    });

                    function imageIsLoaded(e) {
                        $("#logofile").css("color","green");
                        $('#logo-previewing').attr('src', e.target.result);
                        $('#logo-previewing').attr('width', 'fixed');
                        $('#logo-previewing').attr('height', 'auto');
                    };
                });

                jQuery(document).ready(function($){
                    $("#uploadIdeaLogo").on('submit',(function(e) {
                        e.preventDefault();
                        Materialize.toast("<span class='upload-status'>Uploading Idea Logo...</span>",4000);
                        var url_pur="/api/v1/ideas/"+$(this).attr('data-ideaid')+"/logo/upload";
                        var my_pare=$(this); // Saving the parent in a variable
                        $.ajax({
                            url: url_pur, // Url to which the request is send
                            type: "POST",             // Type of request to be send, called as method
                            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                            contentType: false,       // The content type used when sending data to the server.
                            cache: false,             // To unable request pages to be cached
                            processData:false,        // To send DOMDocument or non processed data file it is set to false
                            success: function(data)   // A function to be called if request succeeds
                            {

                                var obj=JSON.parse(data);
                                if(obj.status=="success")
                                {
                                    Materialize.toast("File Uploaded Successfully",4000);


                                }
                                else{
                                    Materialize.toast("Upload Failed: "+ obj.description,4000);
                                }
                            }
                        });
                    }));
                });


                jQuery(document).ready(function($){
                    $("#uploadIdeaCover").on('submit',(function(e) {
                        e.preventDefault();
                        Materialize.toast("<span class='upload-status'>Uploading Idea Cover...</span>",4000);
                        var url_pur="/api/v1/ideas/"+$(this).attr('data-ideaid')+"/cover_pic/upload";
                        var my_pare=$(this); // Saving the parent in a variable
                        $.ajax({
                            url: url_pur, // Url to which the request is send
                            type: "POST",             // Type of request to be send, called as method
                            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                            contentType: false,       // The content type used when sending data to the server.
                            cache: false,             // To unable request pages to be cached
                            processData:false,        // To send DOMDocument or non processed data file it is set to false
                            success: function(data)   // A function to be called if request succeeds
                            {

                                var obj=JSON.parse(data);
                                if(obj.status=="success")
                                {
                                    Materialize.toast("File Uploaded Successfully",4000);


                                }
                                else{
                                    Materialize.toast("Upload Failed: "+ obj.description,4000);
                                }
                            }
                        });
                    }));
                });
            </script>


            <form id="uploadIdeaCover" action="/tempimage" data-ideaid="<?php echo $idea_id?>">
                <img id="cover-previewing" class="responsive" width="455px !important" height="150px !important"  src="/api2/idea/<?php echo $idea['id']?>/cover">
                <div class="file-field input-field">
                    <input id="coverfile" type="file" name="coverfile">
                    <div class="file-path-wrapper file-path-wrapperx">
                        <input class="file-path validate" type="text" placeholder="Select an Image"">

                    </div>

                </div>
                <input type="submit" class="waves-effect waves btn" value="Save Cover">
            </form>
        </div>
    </div>


<?php echo form_open('ideas/edit/'.$idea_id); ?>
	<div class="row col s12">
		<div class="col s12 custom">

			<div class="input-field col s12 m12">
				<input type="text" class="idea-name" name="idea-name"  value="<?php echo set_value('idea-name')?set_value('idea-name'):$idea_name?>">
                <label>Name of the Idea <a href="#modal-add-idea" class="modal-trigger" tabindex="-2">?</a></label>
                <div id="modal-add-idea" class="modal modal-add-idea">
                    <div class="modal-content">
                        <h4>Idea Name</h4>
                        <p>Name by which your idea would be known eg: Expertmile - A market place for CAs and lawyers or AI based pharmaceutical product.</p>
                    </div>
                    <div class="modal-footer">
                        <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                    </div>
                </div>

			</div>

			<div class="input-field col s12">
				<input type="text" name="idea-tagline"  value="<?php echo set_value('idea-tagline')?set_value('idea-tagline'):$idea_tagline?>">
				<label>Tagline <a href="#modal-add-tag" class="modal-trigger" tabindex="-2">?</a></label>
                <div id="modal-add-tag" class="modal modal-add-idea">
                    <div class="modal-content">
                        <h4>Tagline For Iddea</h4>
                        <p>A one-liner for your Idea. Eg: Unbox Zindagi</p>
                    </div>
                    <div class="modal-footer">
                        <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                    </div>
                </div>
			</div>
			<div class="input-field col s12">
				<?php _f_dynamic_select_default_option('idea-stage',$idea_stage,'Ideation','Prototype','Launching Soon','Operational','Expansion');?>
				<label>Stage <a href="#modal-add-stage" class="modal-trigger" tabindex="-2">?</a></label>
                <div id="modal-add-stage" class="modal modal-add-idea">
                    <div class="modal-content">
                        <h4>Select current stage</h4>
                        <p>What stage is your idea: Eg - Ideation phase if no prototype is ready, or Expansion phase if you have a steady revenue and are in expansion plans</p>
                    </div>
                    <div class="modal-footer">
                        <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                    </div>
                </div>
			</div>

				<div class="input-field col s12">
					<input type="text" name="idea-no-people"  value="<?php echo set_value('idea-no-of-people')?set_value('idea-no-of-people'):$idea_no_of_people?>">
					<label> No. of people </label>
				</div>
				<div class="input-field col s12">
				<input type="text" name="idea-url"  value="<?php echo set_value('idea-url')?set_value('idea-url'):$idea_url?>">
				<label>Url</label>
				</div>
			<div class="input-field col s12">
					<textarea name="idea-description" id="textarea1" class="materialize-textarea" ><?php echo set_value('idea-description')?set_value('idea-description'):$idea_description;?></textarea>
					<label for="textarea1">Explain your Idea in brief</label>
				</div>
			<input class="btn" type="submit" value="Save Changes">
			<a class="" href="/ideas/edit/extra/<?php echo $idea_id?>">Edit More Details</a>
            <a class="btn red right click_modal1 modal-trigger" href="#modal-idea-delete" style="color:#fff !important;">Delete <?php echo $idea_name?></a>
		</div>

</form>
		<style>
			.dcover-pic{
				padding:0 !important;
			}
			.file-path-wrapperx
			{
				padding:0 !important;
			}
			#cover-previewing
			{

			}
		</style>

	</div>
    </div>
</div>
