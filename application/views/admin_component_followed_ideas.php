<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
<div class="row">
    <div class="col s12 parent" style="margin-bottom:50px;">
        <h3 class="center">Followed Ideas</h3>
	</div>
</div>
<template v-if="!_.isEmpty(ideas)">
<div id='' class="masonry">
		
		<div v-for="i in followed_ideas" class="arcsec">
			
				<div class="card white">
					<div class="card-content center-align min-height350">
						
						<img v-if="ideas[i].logo_upload_id" class="dynamic-pp card" style="display:inline-block;margin-top:0px" v-bind:src="uploads[ideas[i].logo_upload_id].complete_url" width="70px" alt="">
						<img v-else class="dynamic-pp card" style="display:inline-block;margin-top:0px" v-bind:src="'/assets/bulb.png'" width="70px" alt="Idea Logo">
						<span class="card-title"><a v-bind:href="'/ideas/'+ideas[i].unique_url" v-text="ideas[i].name"></a></span><br>
						<span class="card-idea-subtitle" v-text="'In '+ideas[i].current_phase"></span><span v-text="'with '+ideas[i].followers+' followers'"></span>
						
						<p class="left-align" style="padding: 20px" v-text="ideas[i].description"></p>
					</div>
					<div class="card-action">
						<button v-on:click="unfollowIdea(ideas[i].id)" class="btn red followidea">Unfollow Idea</button>
					</div>
				</div>
			
			
		</div>
		
</div>

</template>
<template v-else>
	<div class="row center">
	<div class="preloader-wrapper small active">
		<div class="spinner-layer spinner-blue-only">
			<div class="circle-clipper left">
				<div class="circle"></div>
			</div>
			<div class="gap-patch">
				<div class="circle"></div>
			</div>
			<div class="circle-clipper right">
				<div class="circle"></div>
			</div>
		</div>

	</div>
	</div>
</template>