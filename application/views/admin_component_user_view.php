<user-view inline-template class="" :user="user" :uploads="uploads" :users="users">
    
    <div class="app_wrapper">
        <template v-if="!_.isEmpty(user) && !_.isEmpty(users) && !_.isEmpty(uploads)">
        <div id="modal_delete_user" class="modal">
            <div class="modal-content">
                <h4>Delete {{user.first_name}} {{user.last_name}}?</h4>
                <p>This action is <span class="red-text">IRREVERSIBLE. </span> Once the user is deleted, all data associated with the user will be permanently lost.</p>
            </div>
            <div class="modal-footer">
				<a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">No</a>
				<a v-on:click="user_delete" href="#!" class="modal-action waves-effect waves-red btn red">Yes Delete</a>
            </div>
        </div>
        <div class="">
        <a class="btn-floating btn-small waves-effect waves-light back_window" @click="back_window"><i class="material-icons">arrow_back</i></a>
        	<div class="row center">
        		<div class="col s12">
        			<h3 class="admin-title">User Info</h3>
        		</div>
        	</div>
        	<div class="">
        		<div class="row view-user-cover_pic">
            
            		<div class="col s12 m3" style="margin-bottom:20px;">
                
                		<img v-if="user.profile_picture_upload_id" class="dynamic-pp card" style="display:inline-block;margin:25px" v-bind:src="uploads[user.profile_picture_upload_id].complete_url" width="200px" alt="">
                        <img v-else class="dynamic-pp card" style="display:inline-block;margin:25px" v-bind:src="user.gravatar_url" width="200px" alt="">

            		</div>
            		<div class="col s12 m6">
        	        	<h3 class="view-user-username"><span v-if="typeof user !== 'undefined'" v-text="user.first_name+' '+user.last_name"></span>
        	            
        	            <i v-if="user.verified_profile=='1'" class='material-icons' title='Verified by SaYourIdeas' style='color: #009be5 !important;font-size: 3rem !important;'>verified_user</i>
        	            <p v-if="user.user_type=='ideator'" class="user_profile-user_type">Ideator</p></h3>
                        <p v-if="user.user_type=='service_provider'" class="user_profile-user_type">Service Provider</p></h3>
                        <p v-if="user.user_type=='investor'" class="user_profile-user_type">Investor</p></h3>
            		</div>
        		</div>
        		<div class="row">
        			<div class="col s12">
        				<button v-on:click="user_unblock" v-if="user.blocked=='1'" id="reg-user-block" class="btn red"><i class='material-icons left'>lock_open</i>Unblock User</button>
                        <button v-on:click="user_block" v-else id="reg-user-block" class="btn red"><i class='material-icons left'>lock</i>Block User</button>
        				<button v-on:click="show_delete_modal" class="btn red" href="#!">Delete User</button>
                        <button v-on:click="user_verify" class="btn btn-blue" v-if="user.verified_profile!='1'">Mark as Verified</button>
                        <button v-if="user.user_type=='investor' && user.admin_approved!=='1'" v-on:click="user_approve" class="btn btn-blue">Approve Investor</button>
        			</div>
        		</div>
        		<div class="row">
        			<div class="col s12">
                        <table class="highlight ideator_table">
                            <tr>
                                <td>Mobile</td>
                                <td><span class="notranslate">{{user.mobile}}</span></td>
                            </tr>
                            <tr>
                                <td>Username</td>
                                <td><span class="notranslate">{{user.username}}</span></td>
                            </tr>
                            <tr>
                                <td>Member Since</td>
                                <td>{{user.timestamp}}</td>
                            </tr>
                            <tr>
                                <td>Role</td>
                                <td>{{user.role}}</td>
                            </tr>
                            <tr>
                                <td>Sector Expertise</td>
                                <td></td>
                            </tr>
                            <tr v-if="user.user_type=='ideator'">
                                <td>Institute</td>
                                <td><span class="notranslate" v-text="user.institute"></span></td>
                            </tr>
                            <tr v-if="user.user_type=='ideator'">
                                <td>Student/Alumni</td>
                                <td><span class="notranslate" v-text="user.student_alumni"></span></td>
                            </tr>
                            <tr v-if="user.user_type=='service_provider' || user.user_type=='investor'">
                                <td>Industry</td>
                                <td><span class="notranslate" v-text="user.industry"></span></td>
                            </tr>
                            <tr v-if="user.user_type=='service_provider' || user.user_type=='investor'">
                                <td>Company</td>
                                <td><span class="notranslate" v-text="user.company"></span></td>                      
                               
                            </tr>
                            <tr v-if="user.user_type=='service_provider' || user.user_type=='investor'">
                                <td>Working Since</td>
                                <td><span class="notranslate" v-text="user.working_since"></span></td>     
                            </tr>
                            <tr v-if="user.user_type=='investor'">
                                <td>Investment Start</td>
                                <td>{{user.investment_range_start}}</td>
                            </tr>
                            <tr v-if="user.user_type=='investor'">
                                <td>Investment End</td>
                                <td>{{user.investment_range_end}}</td>
                            </tr>
                            <tr v-if="user.user_type=='investor'">
                                <td>Professional Background</td>
                                <td>{{user.professional_background}}</td>
                            </tr>
                            <tr v-if="user.user_type=='investor'">
                                <td>Alumni Network</td>
                                <td>{{user.alumni_network}}</td>
                            </tr>
                            <tr v-if="user.user_type=='investor'">
                                <td>Investment Network</td>
                                <td>{{user.investment_network}}</td>
                            </tr>
                            <tr v-if="user.user_type=='investor'">
                                <td>Members of Organisation</td>
                                <td>{{user.members_of_organisation}}</td>
                            </tr>
                            <tr v-if="user.user_type=='investor'">
                                <td>Sample companies invested</td>
                                <td>{{user.sample_companies_invested}}</td>
                            </tr>
                            <tr v-if="user.user_type=='investor'">
                                <td>Would Like to mentor startups</td>
                                <td>{{user.would_like_to_mentor_startups}}</td>
                            </tr>
                            <tr v-if="user.user_type=='investor'">
                                <td>Currently associated with as a mentor</td>
                                <td>{{user.companies_associated_with_as_a_mentor}}</td>
                            </tr>
                            <tr v-if="user.user_type=='investor'">
                                <td>Areas of Expertise/ How do I add value</td>
                                <td>{{user.areas_of_expertise_how_do_i_add_value}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col s12">
                        <h4>Social Links</h4>
                        <table class="highlight ideator_table">
                            <tr>
                                <td>Facebook</td>
                                <td><span class="notranslate"><a target="_blank" v-bind:href="user.facebook_link">{{user.facebook_username}}</a></span></td>
                            </tr>
                            <tr>
                                <td>Twitter</td>
                                <td><span class="notranslate"><a target="_blank" v-bind:href="user.twitter_link">{{user.twitter_username}}</a></span></td>
                            </tr>
                            <tr>
                                <td>Linkedin</td>
                                <td><span class="notranslate"><a target="_blank" v-bind:href="user.linkedin_link">{{user.linkedin_username}}</a></span></td>
                            </tr>
                            <tr>
                                <td>Google+</td>
                                <td><span class="notranslate"><a target="_blank" v-bind:href="user.google_link">{{user.google_username}}</a></span></td>
                            </tr>
                        </table>
                    </div>
        		</div>
        	</div>
        </div>
        </template>
        <template v-else>
            <div class="row center">
            <div class="preloader-wrapper small active">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

            </div>
            </div>
        </template>
    
        
    </div> <!--app wrapper-->
    
</user-view> <!-- user view app-->