<style>
    main{
        background-color: #FFFFFF;
    }
</style>
<div class="container">

    <div class="row text-center">
        <div class="div_sign_up_form">

            <form method="POST" action="/forgot/chk">
                <div class="row card login-card" style="max-width:400px;margin:0px auto;">
                    <div class="col s12 text-center">
                        <h4 style="text-align:center">Enter new password</h4>

                    </div>
                    <div class="row" style="max-width:400px;margin:0px auto;">
                        <div class="input-field col s12" style="padding:0;">
                            <input name="email" type="email" required>
                            <label for="email">Email</label>
                        </div>
                        <div class="input-field col s12" style="padding:0;">
                            <input name="password" type="password" required>
                            <label for="password">New password</label>
                        </div>
                        <div class="input-field col s12" style="padding:0;">
                            <input name="password_repeat" type="password" required>
                            <label for="password_repeat">Repeat password</label>
                            <input name="code" type="hidden" value="<?php if(isset($_GET['q'])) echo $_GET['q']?>">
                        </div>
                        <input class="submit-signin btn blue fwidth" type="Submit" value="Submit"><br>
                    </div>
            </form>
        </div>
    </div>
</div>
</section>