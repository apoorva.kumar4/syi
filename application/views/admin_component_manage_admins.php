<manage-admins inline-template :admins="admins" :users="users" :uploads="uploads">
	<div class="template-wrapper">
	  <a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
		<div class="row">
			<div class="col s12 center">
	            <h3>Manage Admins</h3>
	            <p class="right"><a href="/admin/manage_admins/add" class="btn btn-blue">Add New Admin</a></p>
	        </div>
			<template v-if="!_.isEmpty(admins) && !_.isEmpty(users) && !_.isEmpty(uploads)">
				<div class="col s12">
					<ul class="collection collectionx">
						<li v-for="(a,index) in admins" class="collection-item collection-itemx avatar">
							<img v-if="users[a].profile_picture_upload_id" v-bind:src="uploads[users[a].profile_picture_upload_id].complete_url" alt="" class="circle">
							<img v-else v-bind:src="users[a].gravatar_url" alt="" class="circle">
							<span  class="title text-lighten"><a v-bind:href="'/user_info/u/'+users[a].socialid" class="text-teal" v-text="users[a].first_name"></a></span>
							<div v-if="index >= 1" class="right" style=""><span class="red-text" v-on:click="delete_admin(index)" style="cursor:pointer">Delete</span></div>
							
						</li>

					</ul>
				</div>
				<div id="modal_admin_modify" class="modal">
				<div class="modal-content">
					<h4>Remove {{users[admins[delete_user_selection]].first_name}} from Admins?</h4>
					<p>Deleting an admin will not delete the User, it will just take away the administrator privileges.</p>
				</div>
				<div class="modal-footer">
					<a class="btn red" v-on:click="delete_admin_confirm">Delete Admin</a>
				</div>
			</div>
			</template>
			<template v-else>
				<div class="row center">
				<div class="preloader-wrapper small active">
					<div class="spinner-layer spinner-blue-only">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>

				</div>
				</div>
			</template>
			
		</div>
		
	</div>
</manage-admins>