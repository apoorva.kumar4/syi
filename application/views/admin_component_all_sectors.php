<div class="row center">
      <div class="col s12 m12">
        <h3 class="admin-title">Sectors</h3>

      </div>
       <div class="row">
      <h3><a href="/admin/sectors/add" class="waves-effect waves-light btn btn1 right">Add Sector</a></h3>
    </div>
    </div>
    <div class="row">
      <div class="col  s12 m12 l12">

        <ul class="collapsible" data-collapsible="accordion">

          <?php
          foreach($sectors as $sector)
          {
            ?>
            <li>
             <div class="collapsible-header">
              <span class="title text-lighten"><?php echo "".$sector['id']?>: <?php echo $sector['name']?></span>
             </div>
             <div class="collapsible-body">
                <div class="div-action-sectorx">
                <a class="btn waves red deletesector" data-sectorid="<?php echo $sector['id']?>" >Delete</a>
                <a class="btn waves teal editsector" href="/admin/sectors/edit/<?php echo $sector['id']?>">Edit</a>
                </div>
             </div>
            </li>
            <?php
          }
          ?>
        </ul>

      </div>
    </div>