<?php
if(isset($this->session->message))
{


?>
<div class="row">
	<div class="col s12">
		<h3><?php echo $this->session->message;?></h3>
		<p><?php echo $this->session->message_description;?></p>
	</div>
</div>
<?php
$this->session->set_userdata('message',NULL);
$this->session->set_userdata('message_description',NULL);

}
else
{	
	$url= base_url();
	header("Location: $url");
}
?>

