<?php
/**
 * Created by PhpStorm.
 * User: aashayshah
 * Date: 07/10/16
 * Time: 5:32 PM
 */

?>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/fontawesome-stars-o.css">
<script src="/js/jquery.barrating.min.js"></script>

<style>
    .view-user-username{
        font-weight: 300;
        color: white;
        min-height: 200px;
        padding: 50px;
    }
    .view-user-cover_pic
    {
        background-color:#CB202D;
        box-shadow:0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 8px 0 rgba(0, 0, 0, 0.12);
    }
    .user-description
    {
        font-size:1.3rem;
        font-weight:400;

    }
</style>
{user}
<div class="row view-user-cover_pic">
    <div class="col s12 m2" style="margin-bottom:20px;">
        <img class="dynamic-pp card" style="display:inline-block;margin:25px" src="{profile_picture_url}" width="200px" alt="">
        <a class="btn-large waves-effect white" style="color:teal; margin-left: 25px;width:200px;margin-bottom:10px" href="/messages/new/{socialid}">Send a message</a>
        <a class="btn-large waves-effect white" style="color:teal; margin-left: 25px;width:200px" href="/reviews/add/{socialid}">Write a review</a>
            
    </div>
    <div class="col s12 m6">
        <h1 class="view-user-username">{first_name} {last_name}

            <?php
            if($user[0]['verified_profile']==1)
            {
                echo "<i class='material-icons' title='Verified by SaYourIdeas' style='color: #009be5 !important;font-size: 3rem !important;'>verified_user</i>";
            }
            else{

            }
            ?>
            <p class="user_profile-user_type">Service Provider</p></h1>
    </div>

</div>

<div class="row gutter-space">
    <div class="col s12">
        <div class="col s12 m6 l4 left">
            <h4>Rating</h4>
            <select id="example" name="rating" class="browser-default">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>

            </select>
            <?php
            
            $rat= round($rating['rating'],2);
            $ratc=$rating['ratings_count'];
            ?>
            <h2 class=""><?php echo $rat;?></h2>
            <span>(<?php  echo $ratc;?> reviews)</span>



        </div>

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('#example').barrating({
                    theme: 'fontawesome-stars-o',
                    initialRating:<?php echo $rat?>,
                    showValues: false,
                    readonly: true,
                });
            });
        </script>
    </div>
    <style>
        .ideator_table td,th{
            padding:5px 5px;
            font-size:1rem;
            color:#4a4a4a;
        }
    </style>
    <div class="col s12" >
        <div class="col s12 m6">
            <h4>User information</h4>
            <div class="col s12">
                <table class="highlight ideator_table">
                    <tr>
                        <td>Username</td>
                        <td><span class="notranslate">{username}</span></td>
                    </tr>
                    <?php if($user[0]['show_contact_details']=="Yes") {
                        ?>
                        <tr>
                            <td>Email</td>
                            <td><?php echo $user[0]['email']?></td>
                        </tr>
                        <tr>
                            <td>Mobile</td>
                            <td><?php echo $user[0]['mobile']?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td>Member Since</td>
                        <td><?php echo date("F j, Y", strtotime($user[0]['timestamp']));?></td>
                    </tr>
                    <tr>
                        <td>Role</td>
                        <td>{role}</td>
                    </tr>
                    <tr>
                        <td>Sector Expertise</td>
                        <td><?php echo _get_sector_name_from_id($user[0]['sector_expertise'])?></td>
                    </tr>
                    <tr>
                        <td>Industry</td>
                        <td>{industry}</td>
                    </tr>
                    <tr>
                        <td>Company</td>
                        <td>{company}</td>
                    </tr>
                    <tr>
                        <td>Working Since</td>
                        <td>{working_since}</td>
                    </tr>

                </table>
            </div>
        </div>
        <div class="col s12 m6">
            <h4>Social Links</h4>
            <div class="col s12">
                <table class="highlight ideator_table">
                    <tr>
                        <td>Facebook</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($user[0]['facebook_username'],'facebook')?>" target="_blank">{facebook_username}</a></span></td>
                    </tr>
                    <tr>
                        <td>Twitter</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($user[0]['twitter_username'],'twitter')?>" target="_blank">{twitter_username}</a></span></td>
                    </tr>
                    <tr>
                        <td>Linkedin</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($user[0]['linkedin_username'],'linkedin')?>" target="_blank">{linkedin_username}</a></span></td>
                    </tr>
                    <tr>
                        <td>Google+</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($user[0]['google_username'],'google')?>" target="_blank">{google_username}</a></span></td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>
<div class="row gutter-space">
    <div class="col s12">
        <div class="col s12 m12" >
            <h4>Services (<?php echo sizeof($services)?>)</h4>
            <?php foreach($services as $service)
            {
            ?>
            <div class="col s12 m6 l3">
                <div class="card">
                    <div class="card-content min-height250 max-height250">
                        <span class="card-title"><?php echo $service['name']?></span>
                        <p style="margin-top:15px"><?php echo $service['description']?></p>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
        </div>

    </div>
</div>
<div class="row gutter-space">
    <div class="col s12">
        <div class="col s12 m12" >
            <h4>Rate Cards (<?php echo sizeof($rate_cards)?>)</h4>
            <?php foreach($rate_cards as $ratecard)
            {
                ?>
                <div class="col s12 m6 l3 card" style="">
                    <div class="card-image">
                        <a href="<?php echo $ratecard['url']?>"><img class="responsive-img" src="<?php echo $ratecard['url']?>"></img></a>
                    </div>
                    <div class="card-action">
                        <span class="titlex"><?php echo $ratecard['name']?></span>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

    </div>
</div>
<script>
    jQuery(document).ready(function($) {
        $("#yes_delete").click(function (e) {
            e.preventDefault();
            var url_pur = "/api2/user/" + $(this).attr('data-userid');
            var pare = $(this);
            $.ajax({
                url: url_pur, // Url to which the request is send
                type: "DELETE",             // Type of request to be send, called as method
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData: false,        // To send DOMDocument or non processed data file it is set to false
                success: function (data)   // A function to be called if request succeeds
                {
                    var obj = data;
                    if (obj.status == "success") {
                        Materialize.toast("User Deleted Successfuly", 1000, 'toastgreen');
                        setTimeout("location.reload(true);", 1000);
                        history.go(-1);
                        return true;
                    }
                    else {
                        console.log(data);
                        Materialize.toast("User Delete Failed", 1000, 'toastred');
                    }
                },
                error: function (xhr) {
                    console.log(xhr);
                    Materialize.toast("User Delete Failed", 1000, 'toastred');
                }

            });
        });

        $("#reg-user-block").click(function(e) {
            e.preventDefault();
            var url=$(this).attr('href');
            var temp_par=$(this);
            $.ajax({
                url:url,
                type:"PATCH",
                beforeSend: function ()
                {
                    $("#reg-user-block").addClass("disabled");

                },
                success: function (data)
                {
                    obj=data;
                    if(obj.status=="success")
                    {
                        Materialize.toast(obj.description,4000);
                        if(obj.action=="blocked")
                        {
                            $("#reg-user-block").html("<i class='material-icons left'>lock_open</i>Unblock");
                            var url="/api2/user/"+temp_par.attr('data-socialid')+"/unblock";
                            $("#reg-user-block").attr("href",url);

                        }
                        else
                        {
                            $("#reg-user-block").html("<i class='material-icons left'>block</i>Block");
                            var url="/api2/user/"+temp_par.attr('data-socialid')+"/block";
                            $("#reg-user-block").attr("href",url);
                        }
                    }
                    else
                    {
                        Materialize.toast("An Error Occurred",4000);
                    }
                    $("#reg-user-block").removeClass("disabled");
                },
                error: function (xhr)
                {
                    Materialize.toast(xhr.responseText,4000);
                    $("#reg-user-block").removeClass("disabled");
                }
            });
        });
        $("#verify_user").click(function(e) {
            e.preventDefault();
            Materialize.toast("<span class='upload-status'>Please wait...</span>",4000);
            var url_pur="/api2/user/"+$(this).attr('data-userid')+"/verify";
            var pare=$(this);
            $.ajax({
                url: url_pur, // Url to which the request is send
                type: "PATCH",             // Type of request to be send, called as method
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {

                    var obj=data;
                    if(obj.status=="success")
                    {
                        Materialize.toast("User Approved Successfuly",1000,'toastgreen');
                        pare.fadeOut();
                        setTimeout("location.reload(true);", 1000);
                    }
                    else
                    {
                        console.log(data);
                        Materialize.toast("User Approve Failed",1000,'toastred');
                    }
                }
            });

        });
    });
</script>
{/user}
