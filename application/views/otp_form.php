<div class="container">
	<div class="row card text-center login-card" style="max-width:400px;margin:0 auto !important;">
	<form class="col s12" action="/verify_mobile/verify" method="POST">
		<div class="row text-center">
			<p style="margin:10px">We have sent a verification code to your phone, please input it below:</p>
		</div>
		<input type="hidden" name="first_name" value="{first_name}">
				<input type="hidden" name="last_name" value="{last_name}">
				<input type="hidden" name="mobile" value="{mobile}">
				<input type="hidden" name="password" value="{password}">
				
		<div class="row">
			<div class="input-field col s12">
				<input id="otp" name="otp" type="number" required>
				<label for="otp">Code here</label>
			</div>
		</div>
		<input type="submit" class="btn" type="Submit" value="Verify" />
	</form>	
	</div>
</div>
</section>