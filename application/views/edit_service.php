<div class="container">
<div class="row">
	<div class="col s12">
		<h3>Edit Service</h3>
        <div class="red-text">
            <?php echo validation_errors();?>
        </div>

	</div>
</div>
<?php echo form_open('services/edit/'.$service_id); ?>
<div class="row custom">
	
	<div class="input-field col s12">
		<input type="text" name="service-name"  value="<?php echo set_value('service-name')?set_value('service-name'):$service['name']?>">
		<label>Name of the Service</label>
	</div>
</div>
<div class="row">
<div class="input-field col s12">
          <textarea name="service-description" id="textarea1" class="materialize-textarea" value=""><?php echo set_value('service-description')?set_value('service-description'):$service['description']?></textarea>
          <label for="textarea1">Explain your Service in brief</label>
	<p>Enter a few tags for your service to be discovered by the ideators: (Max:10)</p>
	<input id="service-tags" type="hidden" name="service-tags">
	      <div class="chips chips-edit chips-service-tags-edit" style="border:none !important; margin-bottom:0px !important; box-shadow: none; !important">
		  </div>
        </div>

    <div class="input-field col s12">
    <p>Select your target phase for Idea (Maximum: 3)</p>
        <?php _f_dynamic_checkbox_phase_from_database('service-target-phases','Ideation','Prototype','Launching Soon','Operational','Expansion');?>
        
    </div>
    <div class="input-field col s12 bottom-fix-12">
        <p>Select your target sectors (Maximum: 10)</p>
        <?php _f_dynamic_checkbox_sectors_from_database('service-target-sectors');?>
        
    </div>
    <script>

        jQuery( document ).ready(function( $ ) {
            url="/api/v1/services/<?php echo $service['id']?>/tags";
            var chipdata=[];

            $.ajax({
                url:url,
                success: function (data,status)
                {
                    var obj=JSON.parse(data);
                    if(obj.status=="success")
                    {


                        $.each(obj.service_tags,function(i,val){
                            var temp={};
                            temp.tag=val.name;
                            chipdata.push(temp);
                        });
                        console.log(chipdata);
                        $('.chips-service-tags-edit').material_chip({
                            data:chipdata,
                            placeholder: 'Enter a tag',
                            secondaryPlaceholder: '+Tag'
                        });
                        $("#service-tags").val(JSON.stringify($('.chips-service-tags-edit').material_chip('data')));
                        console.log($("#service-tags").val());
                    }
                    else
                    {
                        Materialize.toast("An Error Occurred",4000);
                    }
                },
                error: function (xhr)
                {
                    console.log(xhr.responseText);

                }
            });


        });

    </script>
</div>
<input class="btn" type="submit" value="Save Changes">
</form>
</div>
    <div class="row">
</div>