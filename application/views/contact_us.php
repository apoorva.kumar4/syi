<script>
    jQuery(document).ready(function($) {
        $("#mob").keypress(function (e) {
            if (e.which == 45 || e.which == 43) {

            }
            else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //if the letter is not digit then display error and don't type anything
                    return false;
                }
                ;
            }

        });
    });
</script>

<section id="cd-google-map">
	<!-- #google-container will contain the map  -->
	<div id="google-container"></div>
	<!-- #cd-zoom-in and #zoom-out will be used to create our custom buttons for zooming-in/out -->
	<div id="cd-zoom-in"></div>
	<div id="cd-zoom-out"></div>
	
</section>
<div class="contact_us container row">
	<div class="col s12 m6 l6">
		<h4 class="contact-us" style="color:#009be5;">Come Down for Coffee</h4>
		<h5>Address: <i class="btn-clipboard tiny material-icons" data-clipboard-target="#cb_address" title="Copy to clipboard" style="color:#009be5;">content_copy</i></h5>
		
		<p id="cb_address">16, Mohanlal Mansion, Underai road,<br><br>
Opp Malad police station,<br><br>
Malad West, Mumbai - 400064, India.</p>
<h5>Telephone: </h5>
	<p>Main Office: <span id="cb_phone">+91 8080 663322</span><i class="btn-clipboard tiny material-icons" data-clipboard-target="#cb_phone" title="Copy to clipboard" style="color:#009be5;">content_copy</i></p>
<h5>Careers:</h5>
        <p>For careers please send your updated CV to <a href="mailto:info@sayourideas.com?subject=The%20subject%20of%20the%20mail" title="Email us">info@sayourideas.com</a>,<br> we are always looking for a talented and a self motivated person.</p>
	</div>
	<div class="col s12 m6 l6">
		<h4 class="contact-us" style="color:#009be5;">Send us a message</h4>
        <?php echo form_open('contact-us/submit-message');?>
  				<div class="row" style="">
                    <div class="center red-text">
                        <?php echo validation_errors();?>
                    </div>
                    <div class="input-field col s12" style="padding:0;">
  						<input name="name" type="text" value="<?php echo set_value('name');?>" required>
  						<label>Your Name*</label>
  					</div>
  					<div class="input-field col s12" style="padding:0">
  						<input name="email" type="email" value="<?php echo set_value('email');?>" required>
  						<label>Email*</label>
  					</div>
  					<div class="input-field col s12" style="padding:0">
  						<input name="mobile" pattern="^[\+0-9]*$" maxlength="16" title="Please enter valid number" id="mob" type="tel" value="<?php echo set_value('mobile');?>">
  						<label>Mobile Number</label>
  					</div>
  					<div class="input-field col s12" style="padding:0">
  						<input name="subject" type="text" required value="<?php echo set_value('subject');?>">
  						<label>Subject*</label>
  					</div>
  					<div class="input-field col s12" style="padding:0">
  						<textarea name="message" class="materialize-textarea"><?php echo set_value('message');?></textarea>
  						<label>Your thoughts here!*</label>
  					</div>
                    <div class="input-field col s12" style="padding:0">
                    <div class="g-recaptcha" data-sitekey="6LfJEwcUAAAAAC7NS279I2UeiNx_nXKn9mjA46UK"></div>
                    </div>
                    <div class="input-field col s12"  style="padding:0">
                        <input class="submit btn fwidth" type="Submit" value="Send"><br>
                    </div>
  				</div>
  		</form>
	</div>


<script src="/js/gmaps.js"></script> <!-- Resource jQuery -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwHY5R1hK5XXwTgOteUwGPBFvpyVF22lU"></script>