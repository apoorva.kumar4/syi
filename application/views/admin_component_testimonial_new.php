<add-testimonial inline-template>
	<div class="app-wrapper">
	<div class="col s12 center">
		<h3>Add a new Testimonial</h3>
	</div>
	<div class="row custom">
		<div class="file-field input-field col s12">
			<div class="btn">
				<span>Choose Profile/Logo</span>
				<input @change="profile_file_changed" type="file" name="testimonial-profile-picture">
			</div>
			<div class="file-path-wrapper">
				<input type="text" class="file-path validate">
			</div>
		</div>
		<div class="input-field col s12 m6">
			<input required type="text" v-model="first_name">
			<label>First Name</label>
		</div>
		<div class="input-field col s12 m6">
			<input required type="text" v-model="last_name">
			<label>Last Name</label>
		</div>
	</div>
	<div class="row">
		<div class="input-field col s12 m6">
			<input required type="text" v-model="company">
			<label>Company</label>
		</div>

		<div class="input-field col s12 m6">
			<input required type="text" v-model="position">
			<label>Position</label>
		</div>

		<div class="input-field col s12">
			<input required type="text" v-model="website">
			<label>Website Link</label>
		</div>

		<div class="col s12 m6">
			<p>Testimonial visible on Website?</p>
			<select v-model="active" class="browser-default">
				<option value="1" selected>Active</option>
				<option value="0">Inactive</option>
			</select>
		</div>

	</div>
	<div class="row">
		<div class="input-field col s12">
			<textarea required v-model="description" id="textarea1" class="materialize-textarea" value=""></textarea>
			<label for="textarea1">Describe the testimonial</label>
			
			</div>
		</div>
		<div class="col s12">
			<button v-on:click="submit_data" class="btn">Add Testimonial</button>
		</div>
	</div>
	</div><!-- app wrapper-->
</add-testimonial>