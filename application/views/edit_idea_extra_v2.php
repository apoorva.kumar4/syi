<script src="/js/parsley.min.js"></script>
<script>
    jQuery(document).ready(function($) {
        $(".modal-add-bullet-close").click(function () {
            setTimeout(function () {
                $(".modal-add-idea").closeModal();
                $(".lean-overlay").remove();
            });
        });
    });
</script>
<script>
    jQuery(document).ready(function($) {
        $("#more_info_sub").click(function (e) {
            e.preventDefault();
            setTimeout(function () {
                $("#prev_detail").fadeOut(400,function () {
                    $("html,body").animate({
                        scrollTop:0
                    },200);
                    $("#more_detail_extra_form").fadeIn();
                });
            });
        });

        $("#back_detail").click(function (e) {
            e.preventDefault();
            setTimeout(function () {
                $("#more_detail_extra_form").fadeOut(400,function () {
                    $("html,body").animate({
                        scrollTop:0
                    },200);
                    $("#prev_detail").fadeIn();
                });
            });
        });

        $("#form_post_idea").on('submit',(function(e) {
            e.preventDefault();
            //var $circle="<div class='spinner-layer spinner-green'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>";
            Materialize.toast("Please wait...",4000);
            var url_pur="/api2/idea/"+<?php echo $idea['id']?>;
            var form=$(this);
            $.ajax({
                url: url_pur, // Url to which the request is send
                type: "PUT",             // Type of request to be send, called as method
                data: form.serialize(), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {

                    var obj=JSON.parse(data);
                    if(obj.status=="success")
                    {

                        $("#more_detail_extra_form").fadeOut(400,function () {
                            $("html,body").animate({
                                scrollTop:0
                            },200);
                            $("#message_idea_edit_success").fadeIn();
                        });
                        //setTimeout(function(){location.replace('/dashboard');},1000);
                    }
                    else{
                        Materialize.toast("Failure"+ obj.description,4000);
                    }
                }
            });
        }));

    });

</script>
<form id="form_post_idea" action="/temp">
    <div class="container">
        <div class="row card login-card z-depth-2" id="prev_detail">
            <div class="col s12 center">
                <h3>Edit <?php echo $idea['name']?></h3>
            </div>


            <div class="row custom">
                <div class="col s12">
                    <h5 style="">Name, Tagline, Stage, Sector</h5>
                    <div class="row" style="margin:1% 5%">
                        <div class="input-field col s12">
                            <input id="name" type="text" name="name"  value="<?php echo $idea['name']?>">
                            <label data-error="Idea name doesn't quite look right">Name of the Idea * <a href="#modal-add-idea" class="modal-trigger" tabindex="-2">?</a></label>
                            <div id="modal-add-idea" class="modal modal-add-idea">
                                <div class="modal-content">
                                    <h4>Idea Name</h4>
                                    <p>Name by which your idea would be known eg: Expertmile - A market place for CAs and lawyers or AI based pharmaceutical product.</p>
                                </div>
                                <div class="modal-footer">
                                    <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                                </div>
                            </div>
                        </div>

                        <div class="input-field col s12">
                            <input type="text" name="tagline"  value="<?php echo $idea['tagline']?>">
                            <label>Tagline <a href="#modal-add-tag" class="modal-trigger" tabindex="-2">?</a></label>
                            <div id="modal-add-tag" class="modal modal-add-idea">
                                <div class="modal-content">
                                    <h4>Tagline For Iddea</h4>
                                    <p>A one-liner for your Idea. Eg: Unbox Zindagi</p>
                                </div>
                                <div class="modal-footer">
                                    <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                                </div>
                            </div>
                        </div>

<!--                        <div class="input-field col s12">-->
<!--                            <select name="stage" required>-->
<!---->
<!--                                --><?php
//                                $array_options=array('Ideation','Prototype','Launching Soon','Operational','Expansion');
//                                foreach($array_options as $value)
//                                {
//                                    if($idea['stage']==$value) {
//                                        ?>
<!--                                        <option value="--><?php //echo $value ?><!--" selected>--><?php //echo $value ?><!--</option>-->
<!--                                        --><?php
//                                    }
//                                    else
//                                    {
//                                        ?>
<!--                                        <option value="--><?php //echo $value?><!--" >--><?php //echo $value?><!--</option>-->
<!--                                        --><?php
//                                    }
//                                }
//                                ?>
<!--                            </select>-->
<!--                            <label>Select current stage * <a href="#modal-add-stage" class="modal-trigger" tabindex="-2">?</a></label>-->
<!--                            <div id="modal-add-stage" class="modal modal-add-idea">-->
<!--                                <div class="modal-content">-->
<!--                                    <h4>Select current stage</h4>-->
<!--                                    <p>What stage is your idea: Eg - Ideation phase if no prototype is ready, or Expansion phase if you have a steady revenue and are in expansion plans</p>-->
<!--                                </div>-->
<!--                                <div class="modal-footer">-->
<!--                                    <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->

                        <div class="input-field col s12">
                            <?php _f_dynamic_select_sector("sectorid",$idea['sectorid']); ?>
                            <label>Sector of the Idea (Like Agriculture, Pharmaceuticals, Technology, Banking, etc)</label>
                        </div>

                    </div>

                </div>

                <div class="col s12">
                    <h5 style="">Visibility</h5>
                    <div class="row" style="margin:1% 5%">
                        <div class="input-field col s12">
                            <p>Choose a handle name, where your idea will be visible to public (You can change this later)</p>
                            <div class="col s6" style="padding: 0;">

                                <input disabled value="https://sayourideas.com/ideas/" style="border-bottom: 0px !important;">
                            </div>
                            <div class="col s6" style="padding: 0">
                                <input id="unique_url" required type="text" name="unique_url"  value="<?php echo $idea['unique_url']?>">
                            </div>
                        </div>

                        <script>
                            jQuery(document).ready(function($){
                                $("#name").on('input',function(){
                                    var idea_name = $(this).val();
                                    idea_name = idea_name.replace(/\s+/g, '');
                                    idea_name = idea_name.replace(/\'+/g, '');
                                    idea_name = idea_name.replace(/-+/g, '');
                                    idea_name = idea_name.toLowerCase();
                                    $("#unique_url").val(idea_name);
                                });
                            });
                        </script>
                    </div>
                </div>

                <div class="col s12">
                    <h5 style="margin-top:6%">About your idea</h5>
                    <div class="row" style="margin:1% 5%">
                        <div class="input-field col s12">
                            <textarea required name="description" id="textarea1" class="materialize-textarea"><?php echo $idea['description']?></textarea>
                            <label for="textarea1">Explain your Idea in brief *</label>

                        </div>
                    </div>
                    <h5 style="margin-top:6%">Tags</h5>


                    <div class="row" style="margin:1% 5%">
                        <div class="input-field col s12">
                            

                            <input id="idea_tags" name="tags" type="hidden">
                                <div class="chips chips-edit chips-idea-tags-edit" style="border:none !important; margin-bottom:0px !important; box-shadow: none; !important">
                                </div>
                            </input>
                            

                        </div>
                    </div>
                    <script>
                                var chipdata=[];
                                var obj=JSON.parse('<?php if($idea['tags']) echo $idea['tags']; else echo "{}";?>');
                                $.each(obj,function(i,val){
                                    var temp={};
                                    temp.tag=val.tag;
                                    chipdata.push(temp);
                                });
                                console.log(chipdata);
                                $('.chips-idea-tags-edit').material_chip({
                                    data:chipdata,
                                    placeholder: 'Enter a tag',
                                    secondaryPlaceholder: 'Type tag and press enter to address'
                                });
                                $("#idea_tags").val(JSON.stringify($('.chips-idea-tags-edit').material_chip('data')));
                        
                    </script>
                    <input class="btn" type="submit" value="Next" id="more_info_sub">
                </div>

            </div>
        </div>
        <!--    more detail form-->
        <div class="col s12 card login-card"  id="more_detail_extra_form" style="display: none;">
            <div class="col s12 center">
                <h3>More Details</h3>
                <div class="center red-text">
                    <?php echo validation_errors();?>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 m12">
                    <input type="text" name="company"  value="<?php echo $idea['company']?>">
                    <label>Company Name <a href="#modal-add-comp" class="modal-trigger" tabindex="-2">?</a></label>
                    <div id="modal-add-comp" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>Company Name</h4>
                            <p>Your co. name - Apple, Microsoft, Google etc.</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="input-field col s12 m6">

                    <select name="patented" id="dt-select">
                        <?php
                        $arr_options = array('Yes','No');
                        foreach($arr_options as $option)
                        {
                            if($option == $idea['patented'])
                            {
                                ?>
                                <option id="sel" value="<?php echo $option?>"><?php echo $option?></option>
                                <?php
                            }
                            else{
                                ?>
                                <option value="<?php echo $option?>"><?php echo $option;?></option>
                                <?php
                            }
                        }

                        ?>

                    </select>


                    <label>Idea Patended? <a href="#modal-add-pat" class="modal-trigger" tabindex="-2">?</a></label>
                    <div id="modal-add-pat" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>Idea Patended?</h4>
                            <p>If you have the patent for your idea, please put the details of it which would make us easy to showcase it to the investors and corporate partners</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6" id="hidden_div">
                    <label>Patend Number</label>
                    <input name="patent_date" id="patent_date" type="text" class="tim-datepicker"  placeholder="If patented, then Patent number." style="margin-top: -8px !important" value="<?php echo $idea['patent_date']?>">
                </div>
                <!--    <script>-->
                <!--        jQuery(document).ready(function($) {-->
                <!--            $("#sel").select(function showDiv() {-->
                <!--                    document.getElementById('hidden_div').style.display = "block";-->
                <!--//                alert("1");-->
                <!--            });-->
                <!--        });-->
                <!--    </script>-->
            </div>

            <div class="row">
                <div class="input-field col s12 m6">
                    <input type="text" name="address"  value="<?php echo $idea['address']?>" required>
                    <label>Full Address of the Company * <a href="#modal-add-add" class="modal-trigger" tabindex="-2">?</a></label>
                    <div id="modal-add-add" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>Full Address of the Company</h4>
                            <p>Eg: Mumbai, India</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
                <div class="input-field col s12 m6">
                    <input type="text" name="website"  value="<?php echo $idea['website']?>">
                    <label>Website for more information about this idea <a href="#modal-add-web" class="modal-trigger" tabindex="-2">?</a></label>
                    <div id="modal-add-web" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>Website for more information about this idea</h4>
                            <p>www.sayourideas.com</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea name="achievements" id="textarea1" class="materialize-textarea"><?php echo $idea['achievements']?></textarea>
                    <label for="textarea1">Tell us about your achievements with this Idea <a href="#modal-add-abt" class="modal-trigger" tabindex="-2">?</a></label>

                    <div id="modal-add-abt" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>About your achievements with this Idea</h4>
                            <p>Eg: Won best startup idea in startup conclave or changed the way rural india used to work earlier. We recommend you to put any achievements with the proof such as link to the site or article if you have been mentioned anywhere.</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">

                    <textarea name="business_plan" id="textarea1" class="materialize-textarea" value=""><?php echo $idea['business_plan']?></textarea>
                    <label for="textarea1">Explain your Business Plan (Visbible to investors, Please be very discrete) <a href="#modal-add-explan" class="modal-trigger" tabindex="-2">?</a></label>
                    <div id="modal-add-explan" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>Business Plan</h4>
                            <p>Please note: do not give any blueprints or confidential part of your idea. We would only want to know that how have you planned your idea to be a reality and successful going ahead and what are your near term and long term goals.</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea name="future_prospects" id="textarea1" class="materialize-textarea" value=""><?php echo $idea['future_prospects']?></textarea>
                    <label for="textarea1">Future Objectives and Prospects <a href="#modal-add-obj" class="modal-trigger" tabindex="-2">?</a></label>
                    <div id="modal-add-obj" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>Future Objectives and Prospects</h4>
                            <p>Please note: do not give any blueprints or confidential part of your idea. We would only want to know that how have you planned your idea to be a reality and successful going ahead and what are your near term and long term goals.</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea name="market_size_usp" id="textarea1" class="materialize-textarea" value=""><?php echo $idea['market_size_usp']?></textarea>
                    <label for="textarea1">The market size and your USP <a href="#modal-add-usp" class="modal-trigger" tabindex="-2">?</a></label>
                    <div id="modal-add-usp" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>The market size and your USP</h4>
                            <p>
                                How are you different from your competitors and what is the size of your market? eg: We provide AI based softwares in Healthcare sector (USD 2bln industry) and our technology, way of working, business model and expertise with the team differentiates us from existing players
                            </p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea name="proof_of_concept" id="textarea1" class="materialize-textarea" value=""><?php echo $idea['proof_of_concept']?></textarea>
                    <label for="textarea1">Proof of concept <a href="#modal-add-prf" class="modal-trigger" tabindex="-2">?</a></label>
                    <div id="modal-add-prf" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>Proof of concept</h4>
                            <p>
                                How do you plan to see the market acceptance of your product? Eg: Sold 100 products in Mumbai in 2 months and getting more orders or Got a seed funding from one of the investors for 10 lakhs to work further on the product
                            </p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea name="revenue_model" id="textarea1" class="materialize-textarea" value=""><?php echo $idea['revenue_model']?></textarea>
                    <label for="textarea1">Explain your revenue model in brief <a href="#modal-add-rev" class="modal-trigger" tabindex="-2">?</a></label>
                    <div id="modal-add-rev" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>Explain your revenue model in brief</h4>
                            <p>
                                What are your revenue drivers and how do you plan to earn from your idea? Eg: subscription, product selling in large volumes, marketing revenue etc
                            </p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea name="team_overview" id="textarea1" class="materialize-textarea" value=""><?php echo $idea['team_overview']?></textarea>
                    <label for="textarea1">Give a team overview, information about your partners <a href="#modal-add-owr" tabindex="-2" class="modal-trigger">?</a></label>
                    <div id="modal-add-owr" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>Give a team overview, information about your partners</h4>
                            <p>
                                Team size, roles in brief
                            </p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea name="executive_summary" id="textarea1" class="materialize-textarea" value=""><?php echo $idea['executive_summary']?></textarea>
                    <label for="textarea1">Executive Summary <a href="#modal-add-sum" class="modal-trigger" tabindex="-2">?</a></label>
                    <div id="modal-add-sum" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>Executive Summary</h4>
                            <p>
                                Details of your founders, co-founders, mentors and other important people in the company
                            </p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 m6">
                    <input type="text" name="facebook_link"  value="<?php echo $idea['facebook_link']?>" required>
                    <label>Facebook Page for your idea *</label>
                </div>
                <div class="input-field col s12 m6">
                    <input type="text" name="youtube_link"  value="<?php echo $idea['youtube_link']?>">
                    <label>Youtube Page for your idea</label>
                </div>
                <div class="input-field col s12 m6">
                    <input type="text" name="twitter_link"  value="<?php echo $idea['twitter_link']?>">
                    <label>Twitter Page for your idea</label>
                </div>
                <div class="input-field col s12 m6">
                    <input type="text" name="linkedin_link"  value="<?php echo $idea['linkedin_link']?>" required>
                    <label>Linkedin Page for your idea *</label>
                </div>
                <div class="input-field col s12 m6">
                    <input type="text" name="pinterest_link"  value="<?php echo $idea['pinterest_link']?>">
                    <label>Pinterest Page for your idea</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 bottom-fix-12">
                    <p>Services Required at current stage of your idea <a href="#modal-add-serv" class="modal-trigger" tabindex="-2">?</a></p>
                    <div id="modal-add-serv" class="modal modal-add-idea">
                        <div class="modal-content">
                            <h4>Services Required at current stage of your idea</h4>
                            <p>
                                What are the services you required currently to move your idea ahead and make it a reality eg: Marketing, technical co-founder, market research, app developer etc.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                        </div>
                    </div>
                    <input name="services_required" class="" type="text" value="<?php echo $idea['services_required']?>" style="transition: none !important">

                    <input class="btn" type="submit" value="Done">
                    <a href="" title="Back" id="back_detail">Back</a>
                </div>
            </div>
        </div>
    </div>
    <!--    more detail form ends here-->

</form>


