<?php
require('templates/header.php');
?>
<div class="container row center">
		<div class="col s12">
			<h1 class="delete">Delete your Account</h1>
            <style>
            @media screen and (max-width:450px) {
                h1.delete {
                    font-size:3rem !important;
                }
            }
            </style>
			<p style="max-width: 500px;margin: auto;text-align: left;">Please note that this action is absolutely irreversible and once deleted we cannot restore your account. <br><br>All the data asscociated with your account will be permanently lost.</p>
            <form action="/delete_me" method="post" style="margin-top:50px">
                <input type="submit" class="btn btn-large red text-white" value="Delete Account" />
            </form>
		</div>
	</div>
</div>