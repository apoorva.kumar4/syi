<div class="container">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <h3 class="center color-teal"><i class="material-icons large">check_circle</i></h3>
            <h3 class="center">Success!</h3>
            <h5 class="center">Your event has been created successfully, go to <a href="/dashboard/events">Events Dashboard.</a></h5>
            <br><br>
        </div>
    </div>
</div> <!-- End of Container -->