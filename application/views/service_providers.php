<script src="/js/selectize.min.js"></script>
  <link rel="stylesheet" href="/css/selectize-custom.css">
  <script>
  jQuery(document).ready(function($){
    $('#select-city').selectize({
      maxOptions:4,
      openOnFocus:false,
      placeholder:"City",
      preload:false,

    });
  
  var $inputx=$("input#autocomplete-service");
  $("input#autocomplete-service").selectize({
        persist:false,
        create: function(input) {
            return {
                value: input,
                text: input,
                label: input
            }
        },
        options: [
          <?php foreach ($services as $service) {
            $name=$service['name'];
            echo "{id:1,value:'$name',label:'$name'},";
          }?>],
        valueField:'value',
        labelField:'value',
        searchField:'value',
        openOnFocus: false,
        closeAfterSelect: true,
        maxItems:1
    });
  });
  </script>
<div class="container">
	<div class="row">
		<div class="col s12 m12 l12">
			<h4 class="text-teal mtb30">Service Providers</h4>
		</div>
	</div>
    <div class="row">
        <div class="col s12" style="">
          <style>
          .submit-signin 
          {
                margin: 8px 0px;
            }
          </style>
        <form class="" method="POST" action="<?php echo base_url()?>service_providers/search">
                <div class="row" style="">
                    <div class="input-field col s12 m5" style="padding:10;">
                        
                        <input autocomplete="off" tabindex="1" class="selectized" name="service-name" type="text" required id="autocomplete-service" placeholder="Service Name"> 
                        
                    </div>
                    <div id="" class="input-field col s12 m5" style="padding:10">
                        <select name="city" tabindex="2" id="select-city" class="demo-default browser-default" placeholder="Select a city...">
                        <option value=""></option>
                      <?php foreach($cities as $city) {
                          ?>
                            <option value="<?php echo $city['name']?>"><?php echo $city['name']?></option>
                          <?php
                        }?>
                      </select>
                    </div>
                     <div class="input-field col s12 m2" style="padding:10">
                         <input tabindex="3" class="submit-signin btn fwidth" type="submit" value="Search">
                     </div>
               
                
                
                </div>
        </form>
       <script src="/js/autocomplete_cities.js"></script>
        
      </div>
    </div>
    <h5><?php if(isset($search_message)) echo $search_message?></h5>
	<div class="row">
		
		<?php foreach($service_providers as $sp)
		{

		
		?>
       		<div class="col s12 m6 l4">
                <div class="card">
                    <div class="card-content card-content-service-providers  min-height400">
                        <div>
                        <ul class="collection">
                            <li class="collection-item avatar">
                                <img src="<?php echo $sp['profile_picture_url']?>" alt="" class="circle">
                                <span class="title"><a href="/users/<?php echo $sp['username']?>" class=""><?php echo $sp['first_name']?></a></span>

                                <?php
                                if($sp['verified_profile']==1)
                                {
                                    echo "<i class='material-icons' title='Verified by SaYourIdeas' style='color: #009be5 !important;vertical-align:middle;cursor:pointer'>verified_user</i>";
                                }
                                else{

                                }
                                ?>

                                <p><?php echo $sp['location']?><br><?php echo _get_sector_name_from_id($sp['sector_expertise'])?></p>
                                <a href="#!" class="secondary-content"><i class="material-icons">grade</i><br><?php $rating=_get_rating($sp['socialid']);
                                 echo number_format($rating[0]["avg(rating)"]+0.0,1); ?></a>
                            </li>
                            </ul>
                        </div>
                       

                        <table>
                            <tr>
                                <td>Services</td>
                                <td><p class="module"><?php 
                                

                                    
                                    $services=_get_list_of_services($sp['socialid']);
                                    
                                    foreach($services as $service)
                                    {
                                        echo $service["name"]."<br>";
                                    }


                                ?></p></td>
                                

                            </tr>
                           
                            <tr>
                                <td>Rate Cards</td>
                                <td><p class="module"><?php 
                                

                                    
                                    $services=_get_list_of_rate_cards($sp['socialid']);
                                    $counter=0;
                                    foreach($services as $service)
                                    {
                                        if($counter<5)
                                        {
                                            $url=$service['url'];
                                             echo "<a class='' href='/uploads/u/$url'>".$service["name"]."</a><br>";
                                        }
                                        $counter++;
                                    }
                                    if($counter==0)
                                    {
                                        echo "N/A";
                                    }


                                ?></p></td>
                            </tr>
                        </table>
                    </div>
                </div>    
			</div>
		<?php
		}
		?>
	</div>
</div>
	
							        