<script>
jQuery(document).ready(function($){
				$("#eventcoverfile").change(function() {

					var file = this.files[0];
					var coverfile = file.type;
					var match= ["image/jpeg","image/png","image/jpg"];
					if(!((coverfile==match[0]) || (coverfile==match[1]) || (coverfile==match[2])))
					{

									Materialize.toast("Invalid image",4000);
									return false;
					}
					else
					{

									var reader = new FileReader();
									reader.onload = imageIsLoaded;
									reader.readAsDataURL(this.files[0]);

					}
				});

				function imageIsLoaded(e) {
					$("#eventcoverfile").css("color","green");
					$('#cover-previewing').attr('src', e.target.result);
					$('#cover-previewing').attr('width', '450px');
					$('#cover-previewing').attr('height', 'auto');
				};


				$("#eventlogofile").change(function() {

					var file = this.files[0];
					var coverfile = file.type;
					var match= ["image/jpeg","image/png","image/jpg"];
					if(!((coverfile==match[0]) || (coverfile==match[1]) || (coverfile==match[2])))
					{

									Materialize.toast("Invalid image",4000);
									return false;
					}
					else
					{

									var reader = new FileReader();
									reader.onload = LogoIsLoaded;
									reader.readAsDataURL(this.files[0]);

					}
				});

				function LogoIsLoaded(e) {
					$("#eventlogofile").css("color","green");
					$('#logo-previewing').attr('src', e.target.result);
					$('#logo-previewing').attr('width', '250px');
					$('#logo-previewing').attr('height', '230px');
				};

				$("#form_event_create").on('submit',function(e){
					e.preventDefault();
								$.ajax({
								url:"/api2/events",
												type:"POST",
												data: new FormData(this),
							 contentType: false,
							 cache: false,						 // To unable request pages to be cached
							 processData:false,
							 success:function(data){
											 var obj=JSON.parse(data);
											 if(obj.status==="success"){
															 $("#cont_event_create").fadeOut(400,function(){
																			 $("#cont_event_success").fadeIn();
															 });
											 }
							 },
							 error: function(data) {
										var obj= JSON.parse(data.responseText);
										Materialize.toast("Error: "+obj.description,4000);
							 }

							});
				});
});
</script>
<div class="container" id="cont_event_create">
		<div class="row card text-center login-card">
				<div class="col s12">
						<h3 class="center">Add a new Event</h3>
						<p class="materialize-red-text center">Note: Event Registrations go under an approval before they are published on the website.</p>
						

						<br><br>
				</div>
				<form id="form_event_create" class="" action="" method="POST">
				<div class="row">
						<style>
								.dcover-pic{
										padding:0 !important;
								}
								.file-path-wrapperx
								{
										padding:0 !important;
								}
								#cover-previewing
								{

								}
						</style>
						<div class="col s12">



								<div class="col s12 dcover-pic">
												<h4>Logo</h4>
												<img id="logo-previewing" width="150px" src="https://s3.ap-south-1.amazonaws.com/sayourideas-uploads/bulb.png">
												<div class="file-field input-field">
														<input id="eventlogofile" type="file" name="logofile">
														<div class="file-path-wrapper file-path-wrapperx">
																<input class="file-path validate" type="text" placeholder="Select an Image">

														</div>
												</div>

										<h4>Cover Pic</h4>
										<img id="cover-previewing" class="responsive" width="450px"  src="https://s3.ap-south-1.amazonaws.com/sayourideas-uploads/image-2.jpg">
										<div class="file-field input-field">
												<input id="eventcoverfile" type="file" name="coverfile">
												<div class="file-path-wrapper file-path-wrapperx">
														<input class="file-path validate" type="text" placeholder="Select an Image">

												</div>

										</div>
								</div>


						</div>

				</div>

				<div class="input-field col s12">
						<input type="text" name="name" required>
						<label>Name of the Event *</label>
				</div>
				<div class="input-field col s12 m12">
						<input type="text" name="description_short" required length="140">
						<label>One Line Description *</label>
				</div>
				<div class="input-field col s12 m6">
						<span>Event Start Date:</span>
						<input id="date_start" name="date_start" type="date" class="" required>
				</div>

				<div class="input-field col s12 m6">
						<span>Event End Date:</span>
						<input name="date_end" type="date" class="" placeholder="Event End Date" required>
				</div>

				<div class="input-field col s12">
						<textarea name="description_long" id="textarea1" class="materialize-textarea" required></textarea>
						<label for="textarea1">Describe your event here, be as detailed as possible *</label>
				</div>

				<div class="col s12">
						<p>Create a handle for your event so that people can visit your event page. Your event page will be https://sayourideas.com/events/your-handle-here</p>
						
				</div>
				<div class="">
            https://www.sayourideas.com/events/
            <div class="input-field inline">
                <input id="slug" required type="text" name="slug" length="32" autocomplete="off" pattern="^[a-zA-Z0-9\-]*$" class="validate">
                <label style="color:#616161;font-weight: 500;" data-success="Looks great!" data-error="Sorry that url slug is not available">event-slug</label>
            </div>
        </div>

				

				<div class="col s12">
				<input class="btn" type="submit" value="Submit">
				</div>


				</form>
		</div>
</div> <!-- End of Container -->
<div class="container" id="cont_event_success" style="display: none;">
		<div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
				<div class="col s12">
						<h3 class="center color-teal"><i class="material-icons large">check_circle</i></h3>
						<h3 class="center">Success!</h3>
						<h5 class="center">Your event has been created successfully, go to <a href="/dashboard/events">Events Dashboard.</a></h5>
						<br><br>
				</div>
		</div>
</div> <!-- End of Container -->
