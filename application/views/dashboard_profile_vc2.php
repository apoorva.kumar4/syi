<script src="/js/selectize.min.js"></script>
<link rel="stylesheet" href="/css/selectize-custom.css">
<script>
    jQuery(document).ready(function($) {
        $("#uploadImage").on('submit', (function(e) {
            e.preventDefault();
            Materialize.toast("<span class='upload-status'>Uploading...</span>", 4000);
            var url_pur = "/api2/user/profile-picture/";

            $.ajax({
                url: url_pur, // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false
                success: function(data) // A function to be called if request succeeds
                {

                    if (data.status == "success") {
                        console.log(data);
                        Materialize.toast("File Uploaded Successfully", 4000);
                        $(".dynamic-pp").attr('src',data.url);
                        $("#change-profile-modal").closeModal();
                    } else {
                        Materialize.toast("Upload Failed: " + data.description, 4000);
                    }
                }
            });
        }));
        $("#wrk_year").keypress(function (e) {
            if (e.which == 45 || e.which == 43) {

            }
            else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //if the letter is not digit then display error and don't type anything
                    return false;
                }
                ;
            }

        });
        $("#mobile").keypress(function (e) {
            if(e.which == 45 || e.which == 43)
            {

            }
            else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //if the letter is not digit then display error and don't type anything
                    return false;
                };
            }

        });

        $('.chips-receivers').on('chip.delete', function(e, chip){
            $("#receivers").val(JSON.stringify($('.chips-receivers').material_chip('data')));
            console.log($("#service-tags").val());
        });

        $('.chips-receivers').on('chip.add', function(e, chip){
            $("#receivers").val(JSON.stringify($('.chips-receivers').material_chip('data')));
            console.log($("#service-tags").val());
        });


        $("#receiversx").selectize({
            delimiter:',',
            persist:false,
            create: function(input) {
                return {
                    value: input,
                    text: input,
                    label: input
                }
            },
            options: [
                <?php
                if(isset($suggestions)) {

                    foreach($suggestions as $suggestion)
                    {
                        $id=$suggestion['id'];
                        $value=$suggestion['value'];
                        echo "{id: $id ,value: '$value',label:'$value'},";
                    }

                }
                if(isset($active) && $active !== '')
                {
                    echo "{id: 1 ,value: '$active',label:'$active'},";
                }

                ?>

            ],
            valueField:'value',
            labelField:'value',
            searchField:'value',
            preload:true,
            openOnFocus: false,
            closeAfterSelect: true,
            addPrecedence: true,
        });


        $('#form-profile-edit').on('submit', function (e) {

            e.preventDefault();
            Materialize.toast("<span class='sp_pw'>Please Wait...</span>");
            var url_pur="/api2/user";
            $.ajax({
                type: 'PUT',
                url: url_pur,
                data: $('form').serialize(),
                success: function (data) {
                    console.log(data);

                    if(data.status=="success")
                    {
                        $(".sp_pw").text("Successfully Saved");
                        setTimeout(function(){
                            $(".sp_pw").parent().fadeOut();
                        },3000);
                    }
                    else
                    {
                        if(data.status=="failure")
                        {
                            Materialize.toast("Failure: "+obj.description,4000);

                        }
                        else
                        {
                            Materialize.toast("Error: Please check your changes.",4000);
                        }

                    }
                },
                error: function(xhr)
                {
                    obj=JSON.parse(xhr.responseText);
                    $(".sp_pw").text(obj.description.capitalizeFirstLetter());
                    setTimeout(function(){
                        $(".sp_pw").parent().fadeOut();
                    },3000);
                }
            });

        });
    });

</script>
<div class="container" style="margin-bottom:70px">
    <div class="row card text-center login-card" style="max-width:750px;margin:0 auto !important;">
        <div class="row text-center">
            <div class="col s12 m4 l5">

                <div id="change-profile-modal" class="modal bottom-sheet modal95">
                    <div class="modal-content">

                        <div class="row">
                            <div class="col s12 m12 l12 ">
                                <h4>Set a new Profile Picture</h4>
                                <p>Max File upload size is 2 Mb. Recommended Resolution 250x250px.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <img id="previewing" width="250px" height="250px" src="/api2/user/<?php echo $user['socialid']?>/profile_picture">
                            </div>
                            <div class="col s12">

                                <form id="uploadImage" action="/tempimage">
                                    <div class="file-field input-field">


                                        <input id="file" type="file" name="file">

                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Select File">
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <a href="#!" id="update-image-submit" class="modal-action waves-effect waves btn green">Save</a>
                        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>


                    </div>
                </div>

                <div class="col s12" style="margin-bottom:20px;">
                    <img class="dynamic-pp card hoverable" data-userid="<?php echo $user['id']?>" style="display:inline-block;margin:25px" src="<?php echo $user['profile_picture_url']?>" width="200px" alt="">
                    <a class="waves-effect white modal-trigger" style="color:teal; margin-left: 25px;margin-bottom:10px; width:200px; display:inline-block!important;" href="#change-profile-modal">Change Profile Picture</a>

                </div>
            </div>

            <div class="col s12 m6">
                <h3 id="" class="activator grey-text text-darken-4" style="display: inline-block!important;"><?php echo $user['first_name']." ".$user['last_name']?></h3>
                <h5>Investor</h5>
                <?php _f_dynamic_pp_remove();?>
            </div>
        </div>
        <div class="col s12">
            <form id="form-profile-edit" method="POST" class="col s12">
                <div class="row">
                    <div class="input-field col s6">

                        <input id="name" name="first_name" type="text" required value="<?php echo $user['first_name']?>">
                        <label for="name">First Name</label>
                    </div>

                    <div class="input-field col s6">

                        <input id="lastname" name="last_name" type="text" required value="<?php echo $user['last_name']?>">
                        <label>Last Name</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <input name="email" type="email" required value="<?php echo $user['email']?>"><br><br>
                        <label for="">Email</label>
                    </div>

                    <div class="input-field col s6">
                        <input name="username" type="text" required value="<?php echo $user['username']?>"><br><br>
                        <label for="disabled">Username</label>
                    </div>
                    <div class="input-field col s6">
                        <input name="mobile" type="tel" maxlength="16" pattern="^[\+0-9]*$" title="Please enter valid number" id="mobile" required value="<?php echo $user['mobile']?>"><br><br>
                        <label>Mobile</label>
                    </div>
                    <div class="input-field col s6">
                        <input name="location" type="text"  value="<?php echo $user['location']?>"><br><br>
                        <label>Location</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <input name="industry" type="text"  value="<?php echo $user['industry']?>"><br><br>
                        <label>Industry</label>
                    </div>

                    <div class="input-field col s6">
                        <input class="wrk_year" name="working_since" type="text" id="wrk_year" maxlength="4" pattern="^[0-9]*$"  value="<?php echo $user['working_since']?>"><br><br>
                        <label>Working Since year: eg 2010</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <input name="company" type="text"  value="<?php echo $user['company']?>"><br><br>
                        <label>Company</label>
                    </div>
                    <div class="input-field col s6">
                        <input name="role" type="text"  value="<?php echo $user['role']?>"><br><br>
                        <label>Designation</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <input name="investment_range_start" type="text"  value="<?php echo $user['investment_range_start']?>"><br><br>
                        <label>Investment Start</label>
                    </div>

                    <div class="input-field col s6">
                        <input name="investment_range_end" type="text"  value="<?php echo $user['investment_range_end']?>"><br><br>
                        <label>Investment Upto</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <?php _f_dynamic_select_sector('sector_expertise',$user['sector_expertise']) ?>
                        <label>Sector Expertise</label>
                    </div>
                    <div class="input-field col s6">
                        <input name="sector_other" type="text" value="<?php echo $user['sector_other']?>"/> 
                        <label>If Others, please mention:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input name="professional_background" type="text"  value="<?php echo $user['professional_background']?>"><br><br>
                        <label>Professional Background</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input name="alumni_network" type="text"  value="<?php echo $user['alumni_network']?>"><br><br>
                        <label>Alumni Network</label>
                    </div>

                    <div class="input-field col s6">
                        <input name="investment_network" type="text"  value="<?php echo $user['investment_network']?>"><br><br>
                        <label>Part of Investment Network</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <input name="members_of_organisation" type="text"  value="<?php echo $user['members_of_organisation']?>"><br><br>
                        <label>Members of Organization</label>
                    </div>

                    <div class="input-field col s6">
                        <input name="sample_companies_invested" type="text"  value="<?php echo $user['sample_companies_invested']?>"><br><br>
                        <label>Sample Companies Invested in</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 m6">
                        <input type="text" name="linkedin_username"  value="<?php echo $user['linkedin_username']?>"><br><br>
                        <label>LinkedIn Link</label>
                    </div>

                    <div class="input-field col s12 m6">
                        <input type="text" name="facebook_username"  value="<?php echo $user['facebook_username']?>"><br><br>
                        <label>Facebook Username</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <input type="text" name="twitter_username" value="<?php echo $user['twitter_username']?>"><br><br>
                        <label>Twitter Username</label>
                    </div>

                    <div class="input-field col s12 m6">
                        <input type="text" name="google_username" value="<?php echo $user['google_username']?>"><br><br>
                        <label>Google+ Username</label>
                    </div>
                </div>
                <div class="input-field col s12">
                    <input id="receiversx" name="investor_tags" class="selectized" type="text" value="<?php echo $user['investor_tags']?>" style="transition: none !important" placeholder="Eg: VC, IOT, Big data, Angel investors ">
                    <label for="receiversx">Tags</label>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input name="allow_direct_contact" type="checkbox" id="test5" <?php if($user['allow_direct_contact']=="Yes") echo "checked" ?> value="Yes"/>

                        <label for="test5">Allow Startups to contact me directly</label>
                    </div>

                    <div class="input-field col s12">
                        <input name="would_like_to_mentor_startups" onchange="valueChanged()" type="checkbox" id="coupon_question" <?php if($user['would_like_to_mentor_startups']=="Yes") echo "checked" ?> value="Yes"/>
                        <label for="coupon_question">I would like to mentor the start-ups</label>
                    </div>
                </div>
                    <div class="row" style="margin-top: 5%!important;">
                        <div class="input-field col s6">
                            <input name="companies_associated_with_as_a_mentor" type="text" value="<?php echo $user['companies_associated_with_as_a_mentor']?>"><br><br>
                            <label>Companies Associated with as a Mentor</label>
                        </div>

                        <div class="input-field col s6">
                            <input name="areas_of_expertise_how_do_i_add_value" type="text" value="<?php echo $user['areas_of_expertise_how_do_i_add_value']?>"><br><br>
                            <label>Areas of Expertise/How I add value</label>
                        </div>
                    </div>
                <div class="row">
                    <div class="input-field col s12 bottom-fix-12">
                        <p>Select your target sectors (Maximum: 10)</p>

                        <?php _f_dynamic_checkbox_sectors('service-target-sectors',$user['investor_sectors'],$user['target_sector_other']);?>
                    </div>
                </div>

                <input class="btn" type="Submit" value="Save Changes"><br><br>
                
                <div class="input-field col s12">
                        <h4>Delete Account?</h4>
                        <p>To proceed with account deletion, please click <a href="/delete" class="red-text">here.</a></p>
                </div>
            </form>
        </div>
    </div>