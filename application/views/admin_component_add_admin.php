<add-admin inline-template :users="users">
	<div class="template-wrapper">
		<div class="col s12 center">
			<h3>Add new admin</h3>
			<p>Note: To add a user as an Admin, the user needs to be already present on the websie. New Admins cannot add more admins. This feature is accessible only to you.</p>

		</div>
		<template v-if="!_.isEmpty(users)">
		<div class="col s12">
			<div class="">
				<p>Type Mobile or Name</p>
				<input  v-model="mobile" type="tel" list="userlist">

				
				<datalist id="userlist">
					<option v-for="user in users" v-bind:value="user.mobile">{{user.first_name}} {{user.last_name}} ({{user.mobile}})</option>
			
				</datalist>
			<button class="btn btn-blue" v-if="mobile" v-on:click="add_admin">Add as Admin</button>
			</div>
			
			
		</div>
		</template>
		<template v-else>
			<div class="row center">
			<div class="preloader-wrapper small active">
				<div class="spinner-layer spinner-blue-only">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>

			</div>
			</div>
		</template>
	</div>
</add-admin>