<div class="row">
        <h3 class="center">Send New Message</h3>
        <form id="admin_send_new_msg" method="post">
            <div class="col s12 input-field">
                <h4>To</h4>
                
                    <p>
                    <label for="ideator">
						<input name="ideator" type="checkbox" id="ideator" value="1" />
						<span>Ideator</span>
					</label>
					</p>
                
                    <p>
                    <label for="service_provider">
						<input name="service_provider" type="checkbox" id="service_provider" value="1" />
						<span>Service Provider</span>
					</label>
                	</p>	
                
                    <p>
                    <label for="investor">
						<input name="investor" type="checkbox" id="investor" value="1" />
						<span>Investor</span>
					</label>
					</p>
                

            </div>

            <div class="col s12 input-field" style="">
                <textarea id="message" name="message" id="textarea1" class="materialize-textarea" required></textarea>
                <label for="message">Enter your message, be as precise as possible.</label>
            </div>
			<div class="col s12 input-field" style="">
            	<input class="btn" type="submit" value="Send">
			</div>
        </form>
    </div>