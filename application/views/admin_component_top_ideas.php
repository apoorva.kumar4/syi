<div class="row">
    <div class="col s12 parent" style="margin-bottom:50px;">
        <h3 class="center">Top Ideas</h3>
        <p class="center">Drag the Idea to change its rank</p>
        <div id='dragula-cont' class="row contain">
            <?php foreach($top_ideas as $idea)
            {
                //$sp=_f_get_user_from_socialid($idea['ownerid']);
                ?>
                <div class="col s12 m6 l4" data-ideaid="<?php echo $idea['id']?>">
                    <div class="card white darken-1">
                        <div class="card-content min-height100">
                            <span class="card-title"><a href="/ideas/<?php echo $idea['unique_url'] ?>"><?php echo $idea['name']?></a></span><br>
                            <span class="card-titlt"><?php echo $idea['stage']?> | <span id="placeholder_<?php echo $idea['id']?>_followers"> <?php echo $idea['followers']?> followers</span></span>
                        </div>
                        <div class="card-action">
                            <a class="btn green list-topidea margin10" data-ideaid="<?php echo $idea['id']?>" id="user-idea-top">Remove from Top</a>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>
    </div>
</div>