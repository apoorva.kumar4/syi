<?php

?>
<div class="row">
	<div class="col s12">
		<h3>Your Events<a href="<?php echo base_url()?>events/create" class="waves-effect waves-light btn btn1 right">New Event</a></h3>
	</div>
</div>
<div class="row">
	<div class="col s12">
		<!-- Modal Structure -->
		  <div id="modal1" class="modal bottom-sheet">
		    <div class="modal-content">
		      <h4 id="delete_message_header">Delete</h4>
		      <p>This will delete all the data related to this event and this is irreversible, yes you get it!</p>
		    </div>
		    <div class="modal-footer">
		      <a id="modal_delete" href="#!" class=" modal-action modal-close waves-effect waves-green btn red">Yes, Delete!</a>
			  <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Back</a>
		    </div>
		  </div>


				<?php
				if(!$events)
				{
						?> <h5>You have not created any events, click <a href="<?php echo base_url()?>events/create">Add</a></h5> <?php
				}
				else
				{?>
					<ul class="collapsible"  data-collapsible="expandable">
				<?php
				$counter=0;
				foreach($events as $event)
				{


					?>

					<li>


					<div class="collapsible-header <?php if(!$counter) echo "active"; $counter++ ?>" data-eventid="<?php echo $event['id']?>">
						<span class="collapsible-event-name" ><?php echo $event['name']?>
				    <a class='dropdown-button' data-beloworigin="true" stoppropagation="true" style="padding:0;float:right;margin:0px 0px;margin-left:20px" href='#' data-activates='dropdown<?php echo $event['id']?>'>
				        <i style="color:#424242;float:right;font-size:1rem;line-height:inherit;margin-right:0rem;" id="edit-<?php echo $event['id']?>" class="material-icons">mode_edit</i>
				    </a>

				    </span>
				    <div class="chip chipwhite" style="float:right;margin-top:5px"><?php if($event['admin_approved']) echo "Approved"; else echo "Approval Pending";?></div></span><div class="chip chipwhite" style="float:right;margin-top:5px"><?php echo $event['views']?> Views</div>

				    <!-- Dropdown Structure -->
				    <ul id='dropdown<?php echo $event['id']?>' class='dropdown-content'>
				      <li><a href="/events/<?php echo $event['slug']?>">View</a></li>
				      <li><a href="/events/edit/<?php echo $event['id']?>">Edit</a></li>
				      <li class="divider"></li>
					  <li>
						  <a class="click_modal1" href="" data-eventid="<?php echo $event['id']?>">Delete</a>
					  </li>

				    </ul>










					</div>
					<div class="collapsible-body">
                        <div class="row">
                            <div class="col s12 m3 13">
                                <h4>About</h4>
                                <p class="event-description" style="padding: 0 0 !important;"><?php echo $event['description_short']?></p>
                            </div>
                            <div class="col s12 m3 13">
                                <h4>Starts on</h4>
                                <p class="event-description" style="padding: 0 0 !important;"><?php echo date("F jS, Y", strtotime($event['date_start']))?></p>
                            </div>
                            <div class="col s12 m3 l3">
                                <h4>Ends on</h4>
                                <p class="event-description" style="padding: 0 0 !important;"><?php echo date("F jS, Y", strtotime($event['date_end']))?></p>
                            </div>
                            <div class="col s12 m6 13">
                                <h4>Brief</h4>
                                <p class="event-description" style="padding: 0 0 !important;"><?php echo $event['description_long']?></p>
                            </div>
                        </div>
					</div>
					</li>


					<?php

				}
			}
				?>
      	</div>
	</div>
</div>
