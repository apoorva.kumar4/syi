<?php
require('templates/header.php');
?>
<div class="container row center">
		<div class="col s12">
			<h1>We have deleted your account!</h1>
			<p>It will take some time for all the data to be deleted. This should usually be done in 24-72 hours.</p>
            
		</div>
	</div>
</div>