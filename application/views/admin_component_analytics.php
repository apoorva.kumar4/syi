<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
<div class="row center">
  <div class="col s12 m12">
    <h3 class="admin-title">Analytics</h3>
  </div>
</div>

<div id="row-activeusers" class="row rowx">
    <div id="embed-api-auth-container" style=""></div>
    <div id="view-selector-container" style="display:none"></div>
  <div class="col s12 m12 l4">
    <div class="">
        <div id="active-users-container"></div>
      </div>
  </div>
</div>

<div class="row card rowx">
  <div class="col  s12 m12 l4">
    <div class="">
      <h4 class="center widget-a">Top Browsers (by pageview)</h4>
      <div class="Chartjs">
          <figure class="Chartjs-figure" id="chart-3-container"></figure>
          <ol class="Chartjs-legend" id="legend-3-container"></ol>
        </div>
      </div>
  </div>
  <div class="col s12 m12 l4">
    <div class="">
      <h4 class="center widget-a">Top Countries (by sessions)</h4>
      <div class="Chartjs">
  
        <figure class="Chartjs-figure" id="chart-4-container"></figure>
          <ol class="Chartjs-legend" id="legend-4-container"></ol>
        </div>
      </div>
    </div>

    <div class="col s12 m12 l4">
      <div class="">
        <div class="Chartjs">
          <h4 class="center widget-a">Most Visited Pages (by Hits)</h4>
          <figure class="Chartjs-figure" id="chart-5-container"></figure>
              <ol class="Chartjs-legend" id="legend-5-container"></ol>
        </div>
      </div>
  </div>
</div>

<div class="row card rowx">
  <div class="col s12 m12 l12">
    <h4 class="center widget-a">This Week vs Last Week (by sessions)</h4>
    <div class="Chartjs">
  
          <figure class="Chartjs-figure" id="chart-1-container"></figure>
        <ol class="Chartjs-legend" id="legend-1-container"></ol>
    </div>
  </div>
</div>

<div class="row card rowx">
  <div class="col s12 m12 ">
    <h4 class="center widget-a">This Year vs Last Year (by users)</h4>
    <div class="Chartjs">
  
      <figure class="Chartjs-figure" id="chart-2-container"></figure>
      <ol class="Chartjs-legend" id="legend-2-container"></ol>
    </div>
  </div>
</div>