<div class="col s12 center">
		<h3>Edit sector</h3>
        <div class="center red-text">
            <?php echo validation_errors();?>
        </div>
	</div>

	<?php echo form_open('/admin/sectors/edit/'.segx(4)); ?>
	<div class="row custom">
		
		<div class="input-field col s12">
			<input required type="text" name="sector-name"  value="<?php echo set_value('sector-name')?set_value('sector-name'):$sector['name']?>">
			<label>Sector Name</label>
			<input class="btn" type="submit" value="Edit">
		</div>

	</div>

	</form>	