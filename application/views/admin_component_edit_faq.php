<div class="col s12 center">
	<h3>Edit Faq</h3>
</div>

<form id="edit_faq" method="POST">
<div class="row custom">
	<input type="hidden" name="post_id" value="<?php echo $faq['post_id']?>">
	<div class="input-field col s12">
		<input required type="text" name="post_title"  value="<?php echo $faq['post_title']?>">
		<label>Faq title</label>
	</div>
	<div class="input-field col s12">
		<textarea required type="text" name="post_desc" class="materialize-textarea"><?php echo $faq['post_desc']?></textarea>
		<label>Faq Answer</label>
		<input class="btn" type="submit" value="Save Changes">
		<a class="btn red" id="delete_faq" data-post_id="<?php echo $faq['post_id']?>">Delete</a>
		<a class="btn green" href="/admin/faq">Back</a>
	</div>

</div>

</form>