<style>
    a { text-decoration: none !important; }
    /*a:hover {color:#000;}*/
    .panel {
        border :none !important;
        box-shadow:none !important;
    }

    .faq_quetion {
        width: 100%;
        display: inline-block !important;
        color: #000 !important;
        font-weight: 400 !important;
        padding: 10px !important;
        /*background-color: #009be5 !important;*/
    }

    .faq_quetion:hover { color: #009be5!important; }

    .panel-body { border-top: none!important;}

    .panel-default>.panel-heading {
        background-color: #fff !important;
        padding: 0px !important;
    }

    .faq_quetion:hover { text-decoration: none !important; }

   .panel-collapse {
    display: none;
   } 

   .panel-title {
	font-size:1.2rem;
   }
</style>

<div class="cont" style="margin-left: 0px !important;margin-right: 0px !important;">


        <div class="row" style="margin:20px auto !important; padding:5px 20px;">
            <h3 class="center" style="margin-bottom: 40px">Frequently Asked Questions</h3>
            <div class="panel-group col s12 m10 offset-m1" id="posts">
                <div class="card faq-card">
                    <h4>For Ideators</h4>
                    <?php
                    foreach($ideator_faqs as $row)
                    {
                        ?>
                        <div class="panel panel-default" style="margin-bottom: 15px;">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                   <a class="faq_quetion" href="#<?php echo $row["post_id"]; ?>" data-toggle="collapse" data-parent="#posts"><?php echo $row["post_title"]; ?></a>
                                </h4>
                            </div>
                            <div id="<?php echo $row["post_id"]; ?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <i class="tiny material-icons" style="vertical-align: middle;">trending_flat</i> <?php echo $row["post_desc"]; ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                ?>
                </div>
            </div>

            <div class="panel-group col s12 m10 offset-m1" id="posts1">
                <div class="card faq-card">
                <h4>For Investors</h4>

                <?php
                foreach($vc_faqs as $row)
                {
                    ?>
                    <div class="panel panel-default" style="margin-bottom: 15px;">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="faq_quetion" href="#<?php echo $row["post_id"]; ?>" data-toggle="collapse" data-parent="#posts1"><?php echo $row["post_title"]; ?></a>
                            </h4>
                        </div>
                        <div id="<?php echo $row["post_id"]; ?>" class="panel-collapse collapse">
                            <div class="panel-body">
                                <i class="tiny material-icons" style="vertical-align: middle;">trending_flat</i> <?php echo $row["post_desc"]; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                </div>
            </div>

            <div class="panel-group col s12 m10 offset-m1" id="posts2">
                <div class="card faq-card">
                    <h4>For Service Providers</h4>
                    <?php
                   foreach($sp_faqs as $row)
                    {
                        ?>
                        <div class="panel panel-default" style="margin-bottom: 15px;">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="faq_quetion" href="#<?php echo $row["post_id"]; ?>" data-toggle="collapse" data-parent="#posts2"><?php echo $row["post_title"]; ?></a>
                                </h4>
                            </div>
                            <div id="<?php echo $row["post_id"]; ?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <i class="tiny material-icons" style="vertical-align: middle;">trending_flat</i> <?php echo $row["post_desc"]; ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
</div>

