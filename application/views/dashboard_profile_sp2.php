<script>
jQuery(document).ready(function($) {
	$("#uploadImage").on('submit', (function(e) {
            e.preventDefault();
            Materialize.toast("<span class='upload-status'>Uploading...</span>", 4000);
            var url_pur = "/api2/user/profile-picture/";

            $.ajax({
                url: url_pur,
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function(data)
                {

                    if (data.status == "success") {
                        console.log(data);
                        Materialize.toast("File Uploaded Successfully", 4000);
                        $(".dynamic-pp").attr('src',data.url);
                        $("#change-profile-modal").closeModal();
                    } else {
                        Materialize.toast("Upload Failed: " + data.description, 4000);
                    }
                }
            });
        }));
        
        $('#form-profile-edit').on('submit', function (e) {

            e.preventDefault();
            Materialize.toast("<span class='sp_pw'>Please Wait...</span>");
            var url_pur="/api2/user/";
            $.ajax({
                type: 'PUT',
                url: url_pur,
                data: $('form').serialize(),
                success: function (data) {
                    console.log(data);

                    if(data.status=="success")
                    {
                        $(".sp_pw").text("Successfully Saved");
                        setTimeout(function(){
                            $(".sp_pw").parent().fadeOut();
                        },3000);
                    }
                    else
                    {
                        if(data.status=="failure")
                        {
                            Materialize.toast("Failure: "+obj.description,4000);

                        }
                        else
                        {
                            Materialize.toast("Error: Please check your changes.",4000);
                        }

                    }
                },
                error: function(xhr)
                {
                    obj=JSON.parse(xhr.responseText);
                    $(".sp_pw").text(obj.description.capitalizeFirstLetter());
                    setTimeout(function(){
                        $(".sp_pw").parent().fadeOut();
                    },3000);
                }
            });

        });
    });
</script>
<div class="container" style="margin-bottom:70px">
    <div class="row card text-center login-card" style="max-width:750px;margin:0 auto !important;">
        <div class="row text-center">
            <div class="col s12 m4 l5">

                <div id="change-profile-modal" class="modal bottom-sheet modal95">
                    <div class="modal-content">

                        <div class="row">
                            <div class="col s12 m12 l12 ">
                                <h4>Set a new Profile Picture</h4>
                                <p>Max File upload size is 2 Mb. Recommended Resolution 250x250px.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <img id="previewing" width="250px" height="250px" src="<?php echo $user['profile_picture_url']?>">

                            </div>
                            <div class="col s12">

                                <form id="uploadImage" action="/tempimage">
                                    <div class="file-field input-field">


                                        <input id="file" type="file" name="file">

                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Select File">
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <a href="#!" id="update-image-submit" class="modal-action waves-effect waves btn green">Save</a>
                        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>


                    </div>
                </div>

                <div class="col s12" style="margin-bottom:20px;">
                    <img class="dynamic-pp card" data-userid="<?php echo $user['id']?>" style="display:inline-block;margin:25px" src="<?php echo $user['profile_picture_url']?>" width="200px" alt="">
                    <a class="waves-effect white modal-trigger" style="color:teal; margin-left: 25px;margin-bottom:10px; width:200px; display:inline-block!important;" href="#change-profile-modal">Change Profile Picture</a>

                </div>
            </div>
            <div class="col s12 m6">
                <h3 id="" class="activator grey-text text-darken-4" style="display: inline-block!important;"><?php echo $user['first_name']." ".$user['last_name']?></h3>
                <h5>Service Provider</h5>
                <?php _f_dynamic_pp_remove();?>
            </div>
        </div>
        <div class="col s12">
            <form id="form-profile-edit" method="POST" class="col s12">
                <div class="row">
                    <div class="input-field col s6">

                        <input id="name" name="first_name" type="text" required value="<?php echo $user['first_name']?>">
                        <label for="name">First Name</label>
                    </div>

                    <div class="input-field col s6">

                        <input id="lastname" name="last_name" type="text" required value="<?php echo $user['last_name']?>">
                        <label>Last Name</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">

                        <input name="email" type="email" value="<?php echo $user['email']?>"><br><br>
                        <label>Email</label>
                    </div>
                    <div class="input-field col s6">

                        <input name="username" type="text" required value="<?php echo $user['username']?>"><br><br>
                        <label for="disabled">Username</label>
                    </div>

                    <div class="input-field col s6">

                        <input name="mobile" id="mobile" disabled type="tel" required value="<?php echo $user['mobile']?>"><br><br>
                        <label>Mobile</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">

                        <input name="industry" type="text" value="<?php echo $user['industry']?>"><br><br>
                        <label>Industry</label>

                    </div>

                    <div class="input-field col s6">

                        <input class="wrk_date" name="working_since" type="text" maxlength="4" pattern="^[0-9]*$" id="wrk_date" value="<?php echo $user['working_since']?>"><br><br>
                        <label>Working Since year: eg 2010</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">

                        <input name="role" type="text" value="<?php echo $user['role']?>"><br><br>
                        <label>Role</label>
                    </div>

                    <div class="input-field col s6">

                        <input name="location" type="text" value="<?php echo $user['location']?>"><br><br>
                        <label>Location</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <?php _f_dynamic_select_sector('sector_expertise',$user['sector_expertise']) ?>
                        <label>Sector Expertise</label>
                    </div>
                    <div class="input-field col s6">

                        <input name="company" type="text" value="<?php echo $user['company']?>"><br><br>
                        <label>Company</label>
                    </div>
                </div>

                <div class="row">

                    <div class="input-field col s12 m6">
                        <input type="text" name="linkedin_username" value="<?php echo $user['linkedin_username']?>"><br><br>
                        <label>LinkedIn Link *</label>
                    </div>

                    <div class="input-field col s12 m6">
                        <input type="text" name="facebook_username" value="<?php echo $user['facebook_username']?>"><br><br>
                        <label>Facebook Username *</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <input type="text" name="twitter_username" value="<?php echo $user['twitter_username']?>">
                        <label>Twitter Username</label>
                    </div>

                    <div class="input-field col s12 m6">
                        <input type="text" name="google_username" value="<?php echo $user['google_username']?>">
                        <label>Google+ Username</label>
                    </div>

                    <div class="col s12">
                        <?php 
                        if($user['show_contact_details']=="Yes") {
                            $show_contact_details_state=true;
                        } else $show_contact_details_state=false;
                        ?>
                            <input id="show_contact_details" type="checkbox" name="show_contact_details" <?php if($show_contact_details_state) echo 'checked'?>>
                            <label for="show_contact_details">Keep my email and phone number publicly visible</label>
                    </div>

                </div>

                <button class="btn" type="Submit">Save Changes</button><br><br>
                <div class="input-field col s12">
                        <h4>Delete Account?</h4>
                        <p>To proceed with account deletion, please click <a href="/delete" class="red-text">here.</a></p>
                </div>
            </form>
        </div>
    </div>
