  <div class="row center back-teal" style="background-color: white">
	  <div class="container" style="">
	  	<div class="col s12 card">
		  <h4 class="" style="margin-bottom:50px;">About Us</h4>
			<div class="row  " style="float:none;margin:auto">
				<div class="col s12">
                    <?php echo html_entity_decode($youtube_link)?>
				</div>
			</div>

	        <div class="row  " style="float:none;margin:auto;">
	          <div class="col s12">
	            <div class="white darken-1">
	              <div class="card-content wysiwyg">
                      <?php echo html_entity_decode($how_we_do_it) ?>
	              </div>
	            </div>
	          </div>
			  
	          <div class="col s12">
	            <div class="white darken-1">
	              <div class="card-content wysiwyg">
                      <?php echo html_entity_decode($what_do_we_do) ?>
	              </div>
                    <br>
	            </div>
	          </div>
	        </div>
	    </div>
	  </div>
  </div>