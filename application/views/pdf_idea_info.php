<div class="row">
    <div class="twelve columns" style="text-align: center;">
        <img src="assets/logo.png" width="300px">
    </div>
</div>
<div class="row" style="padding-top: 50px !important;" >
</div>
<div class="row" style="padding-top:100px !important;">
    <div class="two columns">
        <img src="<?php echo $idea_logo?>" width="20%">
    </div>
    <div class="ten columns" style="padding-left:200px">
        <h5 style="font-weight:500"><?php echo $idea['name']?></h5>
        <p style="padding:0px;margin:0px"> <?php echo $idea['tagline']?></p>
        <p>Sector: <?php echo _get_sector_name_from_id($idea['sectorid'])?></p>
    </div>
</div>
<div class="row" style="padding-top: 300px">
    <div class="twelve columns" style="float:right">
        <table class="u-full-width">

            <tbody>
            <?php foreach($idea as $key=>$value) {
                ?>
                <tr>

                    <td><?php echo ucfirst(str_replace("_"," ",$key))?></td>
                    <td><?php echo $value?></td>

                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

