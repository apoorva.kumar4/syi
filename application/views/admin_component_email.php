<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
<div class="row">
	<h3 class="center">Send Email</h3>
	<form id="admin_send_new_email" method="post">

		<div class="col s12">
			
			<!-- <select name="from" id="from" placeholder="Email from..." required class="browser-default">
				<option value="ideators@sayourideas.com">ideators@sayourideas.com</option>
				<option value="investors@sayourideas.com">investors@sayourideas.com</option>
				<option value="serviceproviders@sayourideas.com">serviceproviders@sayourideas.com</option>
				<option value="info@sayourideas.com">info@sayourideas.com</option>
			</select> -->

	          <div class="input-field inline">
	          	<!-- <p>From:</p> -->
	            <input id="email_inline" name="from" type="text" class="validate">
	            <label for="email_inline">From:</label>
	            
	          </div>
	          @sayourideas.com
		</div>

		<div class="col s12">
			<p>To:</p>
			<input name="to" class="selectized receiversx" type="text" value="" style="transition: none !important">
		</div>

		<div class="col s12">
			<p>Cc</p>
			<input  name="cc" class="selectized receiversx" type="text" value="" style="transition: none !important">
		</div>

		<div class="col s12">
			<p>Bcc</p>
			<input  name="bcc" class="selectized receiversx" type="text" value="" style="transition: none !important">
		</div>


		<div class="col s12" style="margin-top: 30px !important;">
			<p>Subject</p>
			<input type="text" name="subject" id="subject" class="" required>
			
		</div>

		<div class="col s12 input-field" style="">
			<p>Enter your message</p>
			<textarea name="message" id="textarea1" class="materialize-textarea" required></textarea>
			
		</div>

		
		<div class="col s12 file-field input-field">
			<p>Max Attachment Size: 5MB per file. Total attachments should not exceed 25MB.</p>
			<input id="email_attachment" type="file" name="attachments[]" multiple>
			<div class="file-path-wrapper file-path-wrapperx">
				<input class="file-path validate" type="text" placeholder="Add attachments">
			</div>
		</div>

		
		
		<button class="btn" type="submit" id="email_send">Send</button>
		
	</form>
</div>