<?php
/**
 * Created by PhpStorm.
 * User: aashayshah
 * Date: 07/10/16
 * Time: 5:32 PM
 */

?>
<style>
    .view-user-username{
        font-weight: 300;
        color: white;
        min-height: 200px;
        padding: 50px;
    }
    .view-user-cover_pic
    {
        background-color:#CB202D;
        box-shadow:0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 8px 0 rgba(0, 0, 0, 0.12);
    }
    .user-description
    {
        font-size:1.3rem;
        font-weight:400;

    }
</style>

<div class="row view-user-cover_pic">
    <div class="col s12 m2" style="margin-bottom:20px;">
        <img class="dynamic-pp card hoverable" style="display:inline-block;margin:25px" src="/api2/user/<?php echo $user[0]['socialid']?>/profile_picture?d=300" width="200px" alt="">
        <a class="btn-large waves-effect white" style="color:teal; margin-left: 25px;width:200px;margin-bottom:10px" href="/messages/new/<?php echo $user[0]['socialid']?>">Send a message</a>

    </div>
    <div class="col s12 m6" >

        <h1 class="view-user-username"><?php echo $user[0]['first_name']." ".$user[0]['last_name']?>

            <?php
            if($user[0]['verified_profile']==1)
            {
                echo "<i class='material-icons' title='Verified Account' style='color: #009be5 !important;font-size: 3rem !important;'>verified_user</i>";
            }
            else{

            }
            ?>
            <p class="user_profile-user_type"><?php echo _f_get_user_type_from_socialid($user[0]['socialid'])?></p></h1>



    </div>
</div>
<div class="row" style="text-align: center">
    <div class="col s6 center" style="display: inline-block; float:none">
        <p class="user-description center">This is an administrative account owned and maintained by delegated operator of this website, for any queries send a message directly to this account and we would we happy to assist you.</p>
    </div>
</div>
