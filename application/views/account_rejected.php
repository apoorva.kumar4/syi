<div class="container">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <h3 class="center color-red"><i class="material-icons large">cancel</i></h3>
            <h3 class="center">Account Rejected</h3>
            <p class="center">Your account has been rejected by SaYourIdeas.com, please contact us on <a href="mailto:investors@sayourideas.com">investors@sayourideas.com</a> for further queries.</p>
            <br><br>
        </div>
    </div>
</div> <!-- End of Container -->