
<div class="container">

  <div class="row text-center">
    <div class="div_sign_up_form">

      <form class="" method="POST">
        <div class="row card login-card" style="max-width:400px;margin:0px auto;">
          <div class="col s12 text-center">
            <h3 style="text-align:center">Stay Tuned...</h3>
            <p>We are designing something awesome, submit your email and we will inform you.</p>
            <p class="red-text">Something Went wrong, please try again</p>

          </div>


          <div class="row" style="max-width:400px;margin:0px auto;">
            <div class="input-field col s12" style="padding:0;">

              <p class="center"><?php if(isset($errorps)) echo $errorps ?></p>
            </div>
            <div class="input-field col s12" style="padding:0;">
              <input name="email" type="email" required>
              <label>Email</label>
            </div>

            <div class="g-recaptcha" data-sitekey="6LfJEwcUAAAAAC7NS279I2UeiNx_nXKn9mjA46UK"></div>
            <input id="notify_me" class="submit-signin btn blue fwidth" type="Submit" value="Notify Me"><br>
          </div>
      </form>
    </div>
  </div>
</div>
</section>