<script src="/js/vis.js"></script>
<link rel="stylesheet" href="/css/hopscotch.css">
<script src="/js/hopscotch.min.js"></script>
<script src="/js/horizontal-timeline.js"></script>

<style>
.visualization-idea
{
	margin: 20px auto !important;
	width:98%;
}
</style>
<div id="modal-add-bullet" class="modal">
    <div class="modal-content">
      <h4>Add Bullet</h4>
      <p>Name of the bullet (Max 20 characters)</p>
      <input id="new-bullet-name" type="text">
      <p>Description of the bullet</p>
      <input id="new-bullet-description" type="text">
      <input id="new-bullet-date" type="date" class="datepicker timeline-datepicker" placeholder="dd-mm-yyyy">

    </div>
    <div class="modal-footer">
      <a href="#!" class="waves-effect waves-green btn green modal-action bullet-add modal-close">Add</a>
      <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
    </div>
  </div>

<div id="modal-update-bullet" class="modal">
    <div class="modal-content">
      <h4>Edit Bullet</h4>
      <p>Name of the bullet (Max 20 characters)</p>
      <input id="edit-bullet-name" type="text">
      <p>Description of the bullet</p>
      <input id="edit-bullet-description" type="text">
      <input id="edit-bullet-date" type="date" class="">
    </div>
    <div class="modal-footer">
      <a href="#!" class="waves-effect waves-green btn green modal-action bullet-add modal-close">Edit</a>
      <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-update-bullet-close">Close</a>
    </div>
  </div>
<script>
jQuery(document).ready(function($){

        var myCircle = Circles.create({
  id:                  'circles-1',
  radius:              40,
  value:               <?php echo $profile_completeness?>,
  maxValue:            100,
  width:               5,
  text:                function(value){return value + '%';},
  colors:              ['#F6F9FC','#1A237E'],
  duration:            200,
  wrpClass:            'circles-wrp',
  textClass:           'circles-text',
  valueStrokeClass:    'circles-valueStroke',
  maxValueStrokeClass: 'circles-maxValueStroke',
  styleWrapper:        true,
  styleText:           true
});
    
    $("#dismiss_profile_completeness").click(function(e){
        e.preventDefault();
        $("#profile_completeness").fadeOut();
    });
						
    $(".modal-update-bullet-close").click(function(){
        setTimeout(function(){$("#modal-update-bullet").closeModal();$(".lean-overlay").remove();});
    });

    $(".modal-add-bullet-close").click(function(){
        setTimeout(function(){$("#modal-add-bullet").closeModal();$(".lean-overlay").remove();});
    });

    //$('#modal-update-bullet').modal('open');
    window.ajax_get_bullet_data_json = function (ideaid)
    {
        return $.ajax({
            url: '/api/v1/ideas/'+ideaid+'/bullets/timeline',
        });
    }
    function move (timeline,percentage) {
        var range = timeline.getWindow();
        var interval = range.end - range.start;

        timeline.setWindow({
            start: range.start.valueOf() - interval * percentage,
            end:   range.end.valueOf()   - interval * percentage
        });
    }
    /**
     * Zoom the timeline a given percentage in or out
     * @param {Number} percentage   For example 0.1 (zoom out) or -0.1 (zoom in)
     */
    function zoom (timeline,percentage) {

    }
    function addx(item, callback){
        callback(item);
    }


    $(".visualization-idea").each(function(index){


        var container = $(this);
            ajax_get_bullet_data_json($(this).attr('data-ideaid')).success(function(data){
                var bullets_object=JSON.parse(data);
                var items = new vis.DataSet(bullets_object);
                var options = {
                    editable:true,
                    zoomable:true,
                    zoomKey:'ctrlKey',
                    margin: 10,
                    zoomMax:315400000000,
                    zoomMin:864000000,
                    itemsAlwaysDraggable:true,
                    min:"2000-01-01",
                    start: new Date(),
                    moment: function (date) {
                        return vis.moment(date).utcOffset('+05:30');
                    },
                    minHeight:"250px",
                    orientation: 'bottom',
                    snap: function (date, scale, step) {
                     return date;
                    },
                    onUpdate: function (item, callback) {
                        var html = item.content;
                        var description = item.description;
                        var div = document.createElement("div");
                        div.innerHTML = html;
                        var text = div.textContent || div.innerText || "";
                        $("#edit-bullet-name").val(text);
                        $("#edit-bullet-description").val(description);
                        console.log(item);
                        var dt=new Date(item.start);
                        var dt_month=dt.getMonth()+1;
                        var dt_date=dt.getDate();
                        if(dt_date<10){
                            dt_date='0'+dt_date;
                        }
                        if(dt_month<10){
                            dt_month='0'+dt_month;
                        }
                        var datey=dt.getFullYear()+"-"+dt_month+"-"+dt_date;
                        $("#edit-bullet-date").val(datey);
                        Materialize.updateTextFields();

                          $('#modal-update-bullet').openModal(
                            {
                              complete: function(){
                                setTimeout(function(){$(".lean-overlay").remove(); });
                                item.content=$("#edit-bullet-name").val();
                                item.start=$("#edit-bullet-date").val();
                                item.description=$("#edit-bullet-description").val();
                                console.log(item);
                                $("#edit-bullet-name").val("");
                                $("#edit-bullet-description").val("");
                                ajaxbulletupdate(item,callback);

                              }
                            })
                      },
                      onMove: function (item, callback) {

                                ajaxbulletmove(item,callback);

                        },

                    onAdd: function(item,callback) {

                        //debugger;
                        var dt=new Date(item.start);
                        var dt_month=dt.getMonth()+1;
                        var dt_date=dt.getDate();
                        if(dt_date<10){
                            dt_date='0'+dt_date;
                        }
                        if(dt_month<10){
                            dt_month='0'+dt_month;
                        }
                        var datey=dt.getFullYear()+"-"+dt_month+"-"+dt_date;
                        $("#new-bullet-date").val(datey);
                        Materialize.updateTextFields();

                        $('#modal-add-bullet').openModal(
                        {
                            dismissible: false,
                              complete: function(){
                                setTimeout(function(){$(".lean-overlay").remove();


                              });
                                item.content=$("#new-bullet-name").val();
                                item.start=$("#new-bullet-date").val();
                                item.description=$("#new-bullet-description").val();
                                $("#new-bullet-name").val("");
                                $("#new-bullet-description").val("");
                                ajaxbulletadd(item,callback);

                              }
                        });


                    },
                    onRemove: function(item,callback){
                        console.log(JSON.stringify(item));
                        removebullet(item,callback);

                    }



                };
                var timeline = new vis.Timeline(container[0], items, options);
                //timeline.moveTo(new Date());
                timeline.fit();
                container.children().children('.zoomIn').bind('click',function(){
                    var range = timeline.getWindow();
                    var interval = range.end - range.start;
                    var percentage= -0.2;
                    timeline.setWindow({
                        start: range.start.valueOf() - interval * percentage,
                        end:   range.end.valueOf()   + interval * percentage
                    });
                });

                container.children().children('.moveToToday').bind('click',function(){

                    timeline.moveTo(new Date());
                });

                container.children().children('.zoomOut').bind('click',function(){
                    var range = timeline.getWindow();
                    var interval = range.end - range.start;
                    var percentage= 0.2;
                    timeline.setWindow({
                        start: range.start.valueOf() - interval * percentage,
                        end:   range.end.valueOf()   + interval * percentage
                    });
                });

                container.children().children('.moveRight').bind('click',function(){
                    var range = timeline.getWindow();
                    var interval = range.end - range.start;
                    var percentage= -0.2;
                    timeline.setWindow({
                        start: range.start.valueOf() - interval * percentage,
                        end:   range.end.valueOf()   - interval * percentage
                    });
                });

                container.children().children('.moveLeft').bind('click',function(){
                    var range = timeline.getWindow();
                    var interval = range.end - range.start;
                    var percentage= 0.2;
                    timeline.setWindow({
                        start: range.start.valueOf() - interval * percentage,
                        end:   range.end.valueOf()   - interval * percentage
                    });
                });

                container.children().children('.fit').bind('click',function(){
                    timeline.fit();
                    console.log("Fit");
                });
                container.children().children('.add').bind('click',function(evt){

                    $('#modal-add-bullet').openModal(
                        {
                            dismissible: false,
                            complete: function(){
                                setTimeout(function(){$(".lean-overlay").remove();


                                });
                                var item = new Object();
                                item.content=$("#new-bullet-name").val();
                                item.start=$("#new-bullet-date").val();
                                item.description=$("#new-bullet-description").val();
                                $("#new-bullet-name").val("");
                                $("#new-bullet-description").val("");

                                ajaxbulletadd(item,redrawTimeline);


                            }
                        });
                });

                items.on('update', function (event,properties) {

                        console.log("After");
                        var adit = items.get(properties.items[0]);
                        console.log(JSON.stringify(adit));
                        console.log(JSON.stringify(event));

                        });
                items.on('remove', function (event,properties) {
                        console.log(JSON.stringify(properties));
                        //removebullet(properties.items)
                        });
                items.on('add', function (event,properties) {
                        console.log("After");
                        var adit = items.get(properties.items[0]);
                        console.log(JSON.stringify(adit));
                        console.log(JSON.stringify(event));

                        });

//									container.click(function(e){
//									    console.log("Clicked on timeline");
//                                        var props = timeline.getEventProperties(e);
//                                        console.log('object evt: %O', props);
//                                    });

                function ajaxbulletadd(item,callback)
                {
                    console.log("entered ajax bulletadd");
                    console.log(item);
                    var url="/api/v1/ideas/"+container.attr('data-ideaid')+"/bullets/add";
                    console.log(url);
                    $.ajax({
                        url:url,
                        type:'POST',
                        data:{'data':JSON.stringify(item)},
                        success: function(resp){

                            obj=JSON.parse(resp);
                            if(obj.status=="success")
                            {
                                Materialize.toast("Bullet Added Successfully",4000);
                                item.id=obj.item.id;
                                console.log("Before id;");
                                console.log(item);
                                if(callback!=null)
                                callback(item,obj.bullets);
                            }
                            else
                                Materialize.toast("Bullet creation failed",4000);
                        },
                        error: function(error)
                        {
                            Materialize.toast("An unknown error occured",4000);
                        }
                    });
                }
                function ajaxbulletupdate(item,callback)
                {
                    console.log("entered ajax bulletadd");
                    console.log(item);
                    var url="/api/v1/ideas/"+container.attr('data-ideaid')+"/bullets/"+item.id+"/updatex";
                    console.log(url);
                    $.ajax({
                        url:url,
                        type:'POST',
                        data:{'data':JSON.stringify(item)},
                        success: function(resp){

                            obj=JSON.parse(resp);
                            if(obj.status=="success")
                            {
                                Materialize.toast("Bullet Edited Successfully",4000);

                                console.log("Before id;");
                                console.log(item);
                                callback(item);
                            }
                            else
                                Materialize.toast("Bullet creation failed",4000);
                        },
                        error: function(error)
                        {
                            Materialize.toast("An unknown error occured",4000);
                        }
                    });
                }

                function ajaxbulletmove(item,callback)
                {
                    console.log("entered ajax bulletadd");
                    console.log(item);
                    var url="/api/v1/ideas/"+container.attr('data-ideaid')+"/bullets/"+item.id+"/updatex";
                    console.log(url);
                    $.ajax({
                        url:url,
                        type:'POST',
                        data:{'data':JSON.stringify(item)},
                        success: function(resp){

                            obj=JSON.parse(resp);
                            if(obj.status=="success")
                            {
                                Materialize.toast("Bullet Moved Successfully",4000);

                                console.log("Before id;");
                                console.log(item);
                                callback(item);
                            }
                            else
                                Materialize.toast("Bullet Moving failed",4000);
                        },
                        error: function(error)
                        {
                            Materialize.toast("An unknown error occured",4000);
                        }
                    });
                }

                function removebullet(item,callback)
                {
                    var url="/api/v1/ideas/"+container.attr('data-ideaid')+"/bullets/"+item.id+"/delete";

                    $.ajax({
                        url:url,
                        type:'POST',
                        data:{'data':JSON.stringify(items)},
                        success: function(resp){

                            obj=JSON.parse(resp);
                            if(obj.status=="success")
                            {
                                Materialize.toast("Bullet Deleted Successfully",4000);
                                callback(item);
                            }
                            else
                                Materialize.toast("Inbuilt Bullet cannot be deleted",4000);

                        },
                        error: function(resp){
                            consople.log(resp);
                            Materialize.toast("Inbuilt Bullet Cannot be deleted",4000);
                        }
                    });
                }
                function redrawTimeline(item,bullets)
                {
                    timeline.setItems(bullets);
                }

		timeline.fit();

            });

    });

    $(".collapsible-header").click(function(){
        //alert($(this).attr('data-ideaid'));
        localStorage.setItem('activeidea',$(this).attr('data-ideaid'));
    });

    var $activeidea=localStorage.getItem('activeidea');
    var $div_active_idea=$(".collapsible-header[data-ideaid="+$activeidea+"]");
    $div_active_idea.addClass("active");
    $div_active_idea.parent().addClass("active");
    $div_active_idea.siblings(".collapsible-body").css('display','block');

    $("#take_tour").click(function(e){
        e.preventDefault();
        start_tour();

    });

    // Start the tour!
    if(localStorage.getItem("tourtakenv2")!=<?php echo $this->session->socialid?>){
        start_tour();
        localStorage.setItem('tourtakenv2',<?php echo $this->session->socialid?>);
    }
    else{
        if(localStorage.getItem("new_idea_created")=="true"){
            start_tour();
            localStorage.setItem('new_idea_created',"false");
        }
    }
    function start_tour(){
        var tour = {
            id: "hello-hopscotch",
            steps: [
                {
                    title: "Hi!",
                    content: "Start by Adding a New Idea. After adding the Idea, resume the tour.",
                    target: "#new_idea",
                    placement: "left"
                },
                {
                    title: "Your Ideas",
                    content: "Your Ideas will appear here",
                    target: document.querySelector("#ideas-holder"),
                    placement: "top"
                },
                {
                    title: "Documents",
                    content: "You can upload Business Plan, Revenue Model etc.",
                    target: document.querySelector(".collapsible-docs"),
                    placement: "top"
                },
                {
                    title: "Visibility",
                    content: "You can toggle the visibility of your Idea. Private Ideas are only visible to you.",
                    target: document.querySelector("#switch_visibility"),
                    placement: "left"
                },
                {
                    title: "Idea",
                    content: "Your Idea has its own page, click here to see it.",
                    target: document.querySelector(".launch"),
                    placement: "left"
                },
                {
                    title: "Bullet",
                    content: "This is a bullet. It marks a milestone or an upcoming event. It helps you to keep a track of your progress.",
                    target: document.querySelector(".vis-content"),
                    placement: "top"
                },
                {
                    title: "Add a new Bullet",
                    content: "You can create new Bullets by Double Clicking on the empty space or by clicking on +",
                    target: document.querySelector(".add"),
                    placement: "top"
                },
                {
                    title: "Drag or Move",
                    content: "You can Move your bullet from one date to another",
                    target: document.querySelector(".vis-item"),
                    placement: "top"
                },
                {
                    title: "Update",
                    content: "You can edit the Bullet name by double clicking on it",
                    target: document.querySelector(".vis-item"),
                    placement: "top"
                },
                {
                    title: "Service Provider Recommendations",
                    content: "We suggest you the best Service Provider based on phase and Sector of your Idea.",
                    target: document.querySelector(".suggestes_services"),
                    placement: "top"
                }
            ],
            onStart: function() {
                // $("main").css("opacity","0.3");
            },
            onClose: function () {
                // $("main").css("opacity","1");
            },
            onEnd: function() {
                // $("main").css("opacity","1");
            }
        };

        if(window.screen.width<768) return;
        hopscotch.startTour(tour);
    }

});
</script>
<?php if($profile_completeness<100) {
?>

<div id="profile_completeness" class="row" style="max-width:750px;padding-top:20px auto;width:90%">
    <div class="row card" style=";margin:0 auto !important;background-color:#F6F9FC;padding: 10px">
        <div class="col l10 m6">
            <p>Your profile is incomplete. Complete your Profile information for better chances of being contacted by an Investor.</p>
            <p>Click <a href="/dashboard/profile">here</a> to submit details. <a href="#!" id="dismiss_profile_completeness">Dismiss</a></p>
        </div>
        <div class="col l2 m6">
            <div id="circles-1">
        </div>
    </div>
    </div>
</div>
<?php
}?>
<div class="row margin50">
	<div class="col s12">
        <h3>Ideas<a id="take_tour" class="waves-effect waves-light right hide-on-small-only" href="#">Take a tour</a></h3>
	</div>
    <div class="fixed-action-btn active">  
        <a href="<?php echo base_url()?>ideas/add" class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>
    </div>

	<div class="col s12">

		  
		  <div id="modal-edit-bullet" class="modal">
			  <div class="modal-content">
				  <h4 class="modal-edit-bullet-title"></h4>
				  <form id="form-edit-bullet-modal" action="#" method="post">
					  <div class="row">
					 	 <div class="input-field col s12">
						 	 <input id="input-bullet-name-modal" type="text" value="" name="bullet-name">
						  	<label>Bullet Title</label>
					  	</div>
				  	  </div>
					  <div class="row">
					  	<div class="input-field col s12">
						  <input id="input-bullet-description-modal" type="text" name="bullet-description">
						  <label>Description</label>
					    </div>
				  	  </div>
					  <div class="row">
					    <div class="col s12">
					    	<input id="input-bullet-start-date-modal" type="date" class="start-datepicker" placeholder="" name="bullet-start-date">
					    	<label>Date to reach milestone</label>
					    </div>
					  </div>
				  </form>
						  
						  
			  </div>
			  <div class="modal-footer">
				  <a id="action-save-edit-bullet-modal" href="#" class="modal-action waves-effect waves btn teal">Save</a>
				  <a id="action-delete-edit-bullet-modal" href="#" class="modal-action waves-effect waves btn red">Delete</a>
				  <a href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
				  
			  </div>
		  </div>


		  	
  			<link href="/css/vis.css" rel="stylesheet" type="text/css" />
		  	

				<?php
				if(!$ideas)
				{
						?> <h5>You have not added any ideas, click <a href="<?php echo base_url()?>ideas/add">Add</a></h5> <?php
				}
				else
				{?>
					<ul id="ideas-holder" class="collapsible"  data-collapsible="expandable">
				<?php
				$counter=0;
				foreach($ideas as $idea)
				{
					
					
					?>
					
					<li>

					<script>
					if(localStorage.getItem('activeidea')=="")localStorage.setItem('activeidea',"<?php echo $idea['id']?>");
					</script>
					<div id="" class="idea collapsible-header" data-ideaid="<?php echo $idea['id']?>">
						<span class="collapsible-idea-name" ><?php echo $idea['name']?></span>
                        <div class="idea-actions">
                        <a class='launch' data-beloworigin="true" style="" href="/ideas/<?php echo $idea['unique_url']?>" title="Go to Idea Profile">View Idea</a>
                        <div class="chip chipwhite" style="margin-top:5px"><?php echo $idea['followers']." followers"?></div>




<!--						<div class="chip chipwhite collapsible-docs" style="margin-top:5px"><a href="/ideas/documents/--><?php //echo $idea['id']?><!--">Add Docs</a></div>-->
                        <div id="switch_visibility" class="switch cc_visibility" style="display: inline;" data-ideaid="<?php echo $idea['id']?>">
                            <label>
                                Public
                                <input <?php if($idea['visibility']==1) echo "checked"?> type="checkbox">
                                <span class="lever"></span>

                            </label>
                        </div>

                        <a title="Share" class="sharer right modal-trigger" href="#share_idea_modal<?php echo $idea['id']?>"><i class="material-icons">link</i></a>
                        </div>
					</div>
					<div class="collapsible-body">



                                <!-- Dropdown Structure -->
                                <ul id='dropdown<?php echo $idea['id']?>' class='dropdown-content'>

                                    <li><a href="/ideas/edit/<?php echo $idea['id']?>">Edit</a></li>
                                    <li><a href="/ideas/documents/<?php echo $idea['id']?>">Docs</a></li>
                                    <li class="divider"></li>
                                    <li>
                                        <a class="click_modal1" href="" data-ideaid="<?php echo $idea['id']?>">Delete</a>
                                    </li>

                                </ul>
<!--                                <div class="" style="margin:10px;display:inline">--><?php //$temp=_f_get_idea_phase($idea['id']); echo $temp['name']?><!--</div>-->

							<div class="visualization-idea card" id="visualization-idea-<?php echo $idea['id']?>" data-ideaid="<?php echo $idea['id']?>">
								<div class="menu idea-action-desk" style="margin:0px auto;background-color: #1A237E;color:f4f4f4" >
                                    <a class='btn-flat text-white' title="Edit Idea" style="" href="/ideas/edit/<?php echo $idea['id']?>" data-activates='dropdown<?php echo $idea['id']?>'>Edit Idea</a>
                                    <a class="btn-flat collapsible-docs text-white" title="Add Documents" style="" href="/ideas/documents/<?php echo $idea['id']?>">Add Documents</a>
                                    <a class="btn-flat zoomIn text-white" title="Zoom In" data-ideaid="<?php echo $idea['id']?>"><i class="material-icons">zoom_in</i></a>
                                    <a class="btn-flat zoomOut text-white" title="Zoom Out"><i class="material-icons">zoom_out</i></a>
                                    <a class="btn-flat add text-white" title="Add Bullet"><i class="material-icons">add</i></a>
<!--							    <input type="button" class="btn-flat moveLeft" value="Move left"/>-->
<!--							    <input type="button" class="btn-flat moveRight" value="Move right"/>-->
                                    <a class="btn-flat fit text-white" title="View Fit To Timeline"><i class="material-icons">view_comfy</i></a>
                                    <a class="btn-flat moveToToday text-white" title="Today's Phase">Today</a>
                                    <a class="btn-flat moveToToday text-white" title="Add Notes" href="/ideas/notes/<?php echo $idea['id']?>">Notes</a>

							        
							    </div>
							</div>
							<div class="row">
                                <div class="col s12 suggestes_services margin10">
                                        <p class="padding10 card violet text-white">Recommended services</p>
                                        <?php
                                        $CI = & get_instance();
                                        $CI->load->model('idea');
                                        $suggested_services_next_phase=$CI->idea->get_suggested_services($idea['id']);
                                        $i=0;

                                        foreach($suggested_services_next_phase as $suggested_service) {
                                            if($i<5)
                                            ?>
                                            <div class="card col s6 m3" style="padding:10px    ">
                                            <a style='margin-top:15px' href="/services/id/<?php echo $suggested_service['id']?>"><?php
                                            echo   ucfirst($suggested_service['name'])."</a><br>";
                                            $username=_f_get_firstname_from_socialid($suggested_service['ownerid']);
                                            echo " <span class='small-text'><a href='/users/$username' class='grey-text'>By ".$username."</a></span><br></div>";

                                            $i++;
                                        }
                                        ?>
                                        </a></p>
                                </div>
							<div class="col s12 suggestes_services margin10">
									<p class="padding10 card violet text-white">Suggested services for next phase</p>
									<?php

                                    $CI = & get_instance();
                                    $CI->load->model('idea');
                                    $suggested_services_next_phase=$CI->idea->get_suggested_services_for_next_phase($idea['id']);
									$i=0;
									
									foreach($suggested_services_next_phase as $suggested_service) {
                                        if($i<5)
                                        ?>
                                        <div class="card col s6 m3" style="padding:10px    ">
                                        <a style='margin-top:15px' href="/services/id/<?php echo $suggested_service['id']?>"><?php 
                                        echo   ucfirst($suggested_service['name'])."</a><br>";
                                        $username=_f_get_firstname_from_socialid($suggested_service['ownerid']);
                                        echo " <span class='small-text'><a href='/users/$username' class='grey-text'>By ".$username."</a></span><br></div>"; 
                                        
                                        $i++; 
                                    }
									?>
									
							</div>
							</div>
					</div>
					

					</li>
                    <div id="share_idea_modal<?php echo $idea['id']?>" class="modal">
                        <div class="modal-content">
                            <h4 class="modal-edit-bullet-title">Share your idea with team members!</h4>
                            <p>You can send this link to your team members so they can view your timeline and idea details. They won't be able to do any changes to your timeline.</p>
                            <p class="btn-clipboard" id="idea_shareable_link_cb" data-clipboard-target="#idea_shareable_link_cb" ><?php echo "https://sayourideas.com/ideas/".$idea['unique_url']."?token="._f_get_idea_shareable_link($idea['id'])?></p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
                        </div>
                    </div>

					<?php
					
				}
			}
				?>
      	</div>
	</div>
</div>
<script>
    jQuery(document).ready(function($) {



    });
</script>

<script src="/js/clipboard.min.js"></script>
<script src="/js/clipboard-custom.js"></script>
