<div class="container">
	<div class="row">
		<div class="col s12 m12 l12">
			<h4 class="text-teal mtb30">Upcoming Events<?php if(_f_is_loggedin()){?><a href="<?php echo base_url()?>events/create" class="waves-effect waves-light btn btn1 right">New Event</a><?php }?></h4>
		</div>
	</div>

    <div class="card sticky-action">
      <div class="card-image waves-effect waves-block waves-light">
        <img class="activator" src="/assets/img/bulb.jpg">
      </div>
      <div class="card-content">
        <span class="card-title activator grey-text text-darken-4">WorkLoft, Bhandup (W), Mumbai on 31st July, 2016.<i class="material-icons right">more_vert</i></span>
       
      </div>
      <div class="card-reveal">
        <span class="card-title grey-text text-darken-4">Details:<i class="material-icons right">close</i></span>
        <p>SaYourIdeas and Worlzen brings to you an opportunity to meet with tech enthusiasts
who are passionate about startups! Come meet developers with skills like c, c++, c#,
php, html, css, javascript, system architecture, python, perl, ruby, andrioid & IOS.</p>
      </div>
	  <div class="card-action">
	                  <a href="#">More Details</a>
	                  <a href="#">Apply Online</a>
					 	
			          <span style="" class="addtocalendar atc-style-blue">
			  		        <var class="atc_event">
			  		            <var class="atc_date_start">2016-07-31 12:00:00</var>
			  		            <var class="atc_date_end">2016-07-31 18:00:00</var>
			  		            <var class="atc_timezone">Asia/Kolkata</var>
			  		            <var class="atc_title">Workloft, Bandra (W)</var>
			  		            <var class="atc_description">Startups Meet and co-founders match</var>
			  		            <var class="atc_location">Bandra, Mumbai</var>
			  		            <var class="atc_organizer">SaYourIdeas</var>
			  		            <var class="atc_organizer_email">kunal@sayourideas.com</var>
			  		        </var>
			  		    </span></div>
	                </div>
    </div>
	

	    
		
		
	</div>
	