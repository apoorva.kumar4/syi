<div class="container" id="message_approval_pending" style="display: none;">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <h3 class="center color-yellow"><i class="material-icons large">info</i></h3>
            <h3 class="center">Approval Pending!</h3>
            <h5 class="center">Your Message is being reviewd by our Team, it will be delivered to the Investor only if it gets approved and we will notify you. <br><a href="/dashboard">Go to Dashboard.</a></h5>
            <br><br>
        </div>
    </div>
</div> <!-- End of Container -->

<div class="" id="message_new_container">


<div class="row margin50" class="message_new_row">
	<div class="col s12">
		<h3>Send a new Message</h3>
	</div>

<script src="/js/selectize.min.js"></script>
<link rel="stylesheet" href="/css/selectize-custom.css">
<script>

jQuery(document).ready(function(){
    

    $('.chips-receivers').on('chip.delete', function(e, chip){
        $("#receivers").val(JSON.stringify($('.chips-receivers').material_chip('data')));
        console.log($("#service-tags").val());
    });

    $('.chips-receivers').on('chip.add', function(e, chip){
        $("#receivers").val(JSON.stringify($('.chips-receivers').material_chip('data')));
        console.log($("#service-tags").val());
    });

    
    $("#receiversx").selectize({
        delimiter:',',
        persist:false,
        create: function(input) {
            return {
                value: input,
                text: input,
                label: input
            }
        },
        options: [
            <?php
                if(isset($suggestions)) {
                    
                    foreach($suggestions as $suggestion) 
                    {
                        $id=$suggestion['id'];
                        $value=$suggestion['value'];
                        echo "{id: $id ,value: '$value',label:'$value'},";
                    }   
                    
                }
                if(isset($active) && $active !== '')
                {
                    echo "{id: 1 ,value: '$active',label:'$active'},";
                }
                
            ?>
            
        ],
        valueField:'value',
        labelField:'value',
        searchField:'value',
        preload:true,
        openOnFocus: false,
        closeAfterSelect: true,
        addPrecedence: true,
    });
        

	$("#sendmessage").on('submit',(function(e) {
        e.preventDefault();
        Materialize.toast("<span class='upload-status'>Please wait...</span>",4000);
        var url_pur="/api/v1/users/"+$("#profile-picture").attr('data-userid')+"/messages/send";
        $.ajax({
            url: url_pur, // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
		          var receiver_type='<?php echo _f_get_user_type_from_socialid(segx(3))?>';
                var obj={
                    sent:[],
                    notsent:[]
                };
                obj=JSON.parse(data);
                if(obj.status=="success")
                {
                    if(receiver_type=='Investor'){
			         $("#message_new_container").fadeOut(400,function(){
				        $("#message_approval_pending").fadeIn();
			         });
			
        		    } else {
        		          if('sent' in obj) 
                          {
                            var sent=obj.sent.join('<br>');
                            Materialize.toast("Message Sent Successfuly to: "+sent,100000,'toastgreen');
                          }
                          if('notsent' in obj) {
                            var notsent=obj.notsent.join('<br>');
                            Materialize.toast("Message not sent to:<br> "+notsent,100000,'toastred');
                            Materialize.toast("Ensure that you typed the correct username",100000);
                          }
                            
         		    }
                }
                else{
                	console.log(data);
                    Materialize.toast("Sending Failed: "+ obj.description,4000);
                }
            }
        });
    }));
});
        
            
            


        
</script>

	<form id="sendmessage">
    <div class="col s12 input-field">
        <h4>To</h4>
        <p>Type Username or Socialid</p>
        <input id="receiversx" name="receiversx" class="selectized" type="text" value="<?php echo $active?>" required style="transition: none !important">

    </div>
	
	<div class="col s12 input-field">
            <p for="message-body">Enter your message, be as precise as possible.</p>
	    	<textarea name="message" id="textarea1" class="materialize-textarea" required></textarea>
	    	
		</div>
        <div class="col s12 input-field">
	<input class="btn" type="submit" value="Send">
</div>
</div>
