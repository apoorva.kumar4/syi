<?php $this->load->view('templates/admin_head');?>
<div id="admin_app">
    <div class="app-wrapper">
        <div id="admin-nav" class="nav-wrapper">
            <a id="admin-menu-icon" href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
        <div class="row fwidth">
            <div class="col s12 m4 l2">
                <?php require('admin_component_nav_bar.php');?>
            </div>
            <div class="col s12 m12 l10 right">
                <?php require('admin_component_all_registrations.php');?>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin_footer');?>
