<manage-partners inline-template>
<div class="template-wrapper">
		<div id="modal_partner_delete" class="modal">
			<div class="modal-content">
				<h4>Remove Partner?</h4>
				<p>Are you sure you want to remove this partner?</p>
			</div>
			<div class="modal-footer">
				<a class="btn red" @click="do_delete_partner">Delete</a>
			</div>
		</div>
		<div class="row">
			<div class="col s12 center">
	            <h3>Manage Partners</h3>
	            <p class="right"><a href="/admin/partners/add" class="btn btn-blue">Add New Partner</a></p>
	        </div>
			<template v-if="!_.isEmpty(partners)">
				<div class="col s4" v-for="partner in partners" :key="partner.id">
					<div class="card">
						<div class="card-image">
							<img :src="partner.logo_url" />
						</div>
						<div class="card-content">
							<p>{{partner.name}}</p>
						</div>
						<div class="card-action">
							<a href="#" @click="show_delete_modal(partner.id)">Delete Partner</a>
						</div>
					</div>
				</div>
				
			</template>
			<template v-else>
				<div class="row center">
				<div class="preloader-wrapper small active">
					<div class="spinner-layer spinner-blue-only">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="gap-patch">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>

				</div>
				</div>
			</template>
			
		</div>
		
	</div>
</manage-partners>