<div class="container">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <h3 class="center color-yellow"><i class="material-icons large">error</i></h3>
            <h3 class="center">An error occurred!</h3>
            <p class="center"><?php echo $description?></p>
            <p class=""><?php echo $extras?></p>
            <br><br>
        </div>
    </div>
</div> <!-- End of Container -->