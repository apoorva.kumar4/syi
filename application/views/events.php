<style>
		.masonry {
			margin: 1.5em 0;
    padding: 0;
    -moz-column-gap: 0em;
    -webkit-column-gap: 0em;
    column-gap: 0em;
    font-size: .85em;
		}
		.arcsec {
		display: inline-block;
	    padding: 1em;
	    margin: 0;
	    width: 100%;
	    box-sizing: border-box;
	    -moz-box-sizing: border-box;
	    -webkit-box-sizing: border-box;
		}

		.arcsec .card {
			margin:0;
		}

		@media only screen and (min-width: 400px) {
    .masonry {
        -moz-column-count: 1;
        -webkit-column-count: 1;
        column-count: 1;
    }
}

@media only screen and (min-width: 700px) {
    .masonry {
        -moz-column-count: 1;
        -webkit-column-count: 1;
        column-count: 1;
    }
}

@media only screen and (min-width: 900px) {
    .masonry {
        -moz-column-count: 2;
        -webkit-column-count: 2;
        column-count: 2;
    }
}
	</style>

<div class="container">
	<div class="row">
		<div class="col s12">
			<div id="profile_completeness" class="row" style="padding-top:20px auto;">
			    <div class="row card" style=";margin:0 auto !important;background-color:#F6F9FC;padding: 10px">
			        <div class="col s12">
			            <p>Sign up for our newsletter to get updates on new Events. Click <a href="/subscribe">here</a> to subscribe.</p>
			            
			        </div>
			    </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12 m12 l12">
			<h4 class="text-teal mtb30">Upcoming Events <a class="waves-effect waves-light btn btn1 right" href="/events/create">Add Your Event</a></h4>
		</div>
	</div>
	<div class="masonry">
	<?php 
		print_events($events);
	?>
	</div>

	<div class="row">
		<div class="col s12 m12 l12">
			<h4 class="text-teal mtb30">Past Events</h4>
		</div>
	</div>
	
	<div class="masonry">
	<?php 
		print_events($pastevents);
	?>
	</div>
	</div>
	
	<?php
	function print_events($events){
		foreach($events as $event)
		{
		?>
		<div class="arcsec">
            <div class="card white">
              <div class="card-content center-align">
                <a href="/events/<?php echo $event['slug']?>"><img class="responsive-img" src="<?php echo _f_get_event_cover_url($event['id'])?>" alt=""></a>
                  <span class="card-title"><a href="/events/<?php echo $event['slug'] ?>"><?php echo $event['name']?></a>
                  </span>
              </div>
            </div>
            </div>
			<?php

		}

	}
	?>
