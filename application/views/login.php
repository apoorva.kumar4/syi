<div class="">
<div class="row text-center">
	
    <div class="div_sign_up_form">
        
        <?php 
        if(isset($message)) {
            ?>
            <p class="center">{message}</p>    
            <?php
        }
        ?>
		<form class="getin" method="POST" action="<?php echo base_url()?>oauth/sayourideas">
			<div class="row card" style="max-width:400px;margin:0px auto;padding:20px">
			<div class="col s12 text-center">
				<h3 style="text-align:center">Login</h3>
			</div>
				<div class="row" style="max-width:400px;margin:0px auto;">
					<div class="input-field col s12" style="padding:0;">
						<input tabindex=1 name="email" type="text" required>
						<label>Email or Mobile</label>
					</div>
					<div class="input-field col s12" style="padding:0">
						<input tabindex=2 name="password" type="password" required>
						<label>Password</label>
                        <a href="/forgot">Forgot Password?</a>
                        
					</div>
				
				<input class="submit-signin btn blue fwidth" type="Submit" value="Login"><br>
                <a href="/">Don't have an account? Sign up.</a>
				
				</div>
		</form>
	</div>
</div>
</div>
</section>