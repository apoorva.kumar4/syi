<div class="container">
<div class="row">
	<div class="col s12">
		<h3>Add a new bullet</h3>
        <div class="center red-text">
            <?php echo validation_errors();?>
        </div>
	</div>
</div>
<?php echo form_open('ideas/bullets/new'); ?>

<div class="row">

	<div class="input-field col s12">
		<input type="text" name="bullet-name" value="" size="50" />
		<label>Bullet Name</label>
	</div>
</div>

<div class="row">

	<div class="input-field col s12">
		<input type="text" name="bullet-description" value="" size="50" />
		<label>Description</label>
	</div>
</div>

<div class="row">
  <div class="col s12 m6">
  	<input name="bullet-start-date" type="date" class="start-datepicker" placeholder="">
  	<label>Date to reach milestone</label>
  </div>
</div>
<div class="row">
<div class="input-field col s12 m6">


<?php 
	$CI = & get_instance();
	$CI->load->model('ideator');
	$arr=$CI->ideator->_get_list_of_ideas();
	$list[0]='idea-name';
	$i=1;
	//var_dump($arr);
	foreach($arr as $idea)
	{
		$list[$i]=$idea['name'];
		$i++;
	}
	

 _f_dynamic_select_default_array($list);?>
 </div></div>
<div><input type="submit" value="Submit" class="btn"/><a class="btn red" href="/dashboard" style="margin:0px 10px">Go Back</a></div>

</form>
</div>