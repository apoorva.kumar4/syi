<style type="text/css">
	@media screen and (min-width: 768px) {
		#Subscribe {
			padding:50px 100px;
		}	
	}
	@media screen and (min-width: 1024px) {
		#Subscribe {
			padding:50px 350px;
		}	
	}

		
	
</style>
<section class="section">
	<div id="Subscribe" class="wrapper">
		<div class="row">
			<div class="col s12 card" style="padding:20px">
				<h4>Thank you!</h4>
				<p>You have been registered for newsletters and updates from us. Check your inbox to stay tuned!</p>
				
			</div>
		</div>
	</div>
</section>s