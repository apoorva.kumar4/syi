<div class="container">
<div class="row">
	<div class="col s12">
		<h3>Add a new Service</h3>
        <div class="red-text">
            <?php echo validation_errors();?>
        </div>

	</div>
</div>
<?php echo form_open('services/add'); ?>
<div class="row custom">
	
	<div class="input-field col s12">
		<input type="text" name="service-name"  value="<?php echo set_value('service-name')?>" required>
		<label>Name of the Service *</label>
	</div>
</div>
<div class="row">
	<div class="input-field col s12">
	          <textarea name="service-description" id="textarea1" class="materialize-textarea" required value="<?php echo set_value('service-description')?>"></textarea>
	          <label for="textarea1">Explain your Service in brief * (Max: 140 characters)</label>
		<p>Enter a few tags for your service to be discovered by the ideators: (Max:10) (Eg: Marketing, IT, Co-founder, App developer, etc.) </p>
		<input id="service-tags" type="hidden" name="service-tags">
		      <div class="chips chips-initial chips-service-tags" style="border:none !important; margin-bottom:0px !important; box-shadow: none; !important"></div>
	</div>
	<div class="input-field col s12">
	<p>Select your target phase for Idea (Maximum: 3)</p>
		<?php _f_dynamic_checkbox_default('service-target-phases','Ideation','Prototype','Launching Soon','Operational','Expansion');?>
		
	</div>
	<div class="input-field col s12 bottom-fix-12">
		<p>Select your target sectors (Maximum: 10)</p>
		<?php _f_dynamic_checkbox_sectors('service-target-sectors');?>
		
	</div>
</div>
<input class="btn" type="submit" value="Add">
</form>
</div>
    <div class="row">
</div>
