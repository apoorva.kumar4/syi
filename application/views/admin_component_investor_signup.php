<form id="sign_up_vc" method="POST" class="">
    <div id="registration_investor" class="" style="margin-bottom:70px;">
        <div class="row card text-center login-card" style="margin:0 auto !important;">
            <div class="row text-center">
                <h4 style="margin:20px 10px;text-align:center;">Investor Registration</h4>
            </div>
            
                <div class="input-field col s6">
                    <input id="name" name="first_name" type="text" required value="">
                    <label for="name">First Name *</label>
                </div>
                <div class="input-field col s6">
                    <input id="lastname" name="last_name" type="text" required value="">
                    <label>Last Name *</label>
                </div>
            
				
           
                <div class="input-field col s6">
                    <input name="email" required type="email" value=""><br><br>
                    <label>Email*</label>
                </div>

                <div class="input-field col s6">

                    <input name="mobile" type="tel" id="mob" value="" pattern="^[\+0-9]*$" maxlength="16" title="Please enter valid number"><span id="errmsg"></span><br><br>
                    <label>Mobile</label>
                </div>
           
				<div class="input-field col s6">
                    <input id="lastname" name="password" type="password" required value="">
                    <label>Password</label>
                </div>

            
                <div class="input-field col s6">

                    <input name="industry" type="text" value=""><br><br>
                    <label>Industry</label>

                </div>

                <div class="input-field col s6">

                    <input class="since_year" id="w_date" name="working_since" type="text" maxlength="4" pattern="^[0-9]*$" value=""><br><br>
                    <label>Working Since year: eg 2010</label>
                </div>
           
            
                <div class="input-field col s6">

                    <input name="company" type="text" value=""><br><br>
                    <label>Company</label>
                </div>

                <div class="input-field col s6">

                    <input name="role" type="text" value=""><br><br>

                    <label>Designation</label>
                </div>
           

            

                <div class="input-field col s12">
                    <p>Location</p>
                    <input type="text" list="cities" name="location" value="Mumbai City" id="select-city" class="select_city demo-default " placeholder="Select a city...">
                        
                        
                    </select>
					<datalist id="cities">
						<option v-for="city in cities" v-text="city" v-bind:value="city"></option>
                            
					</datalist>
                </div>

                <div class="col s12 m6">
					<p>Sector Expertise</p>
                    <select name='sector_expertise' id='sector' class="browser-default">
						<option v-for="(sector,index) in sectors" v-bind:value="index" v-text="sector"></option>
					</select>
                    
                </div>

                <div class="col s12 m6">
					<p>Currency</p>
                        <select name="pref_currency" id="pref_currency" class="browser-default">
                            <option id="inr" value="INR">INR</option>
                            <option id="usd" value="USD">USD</option>
                        </select>
                        
                </div>
                

                <div class="input-field col s12 m6">
                    <input name="investment_range_start" maxlength="16" pattern="^[\+0-9]*$" id="investment_range_start" type="text"><br><br>
                    <label>Investment Start</label>
                </div>
                <div class="input-field col s12 m6">

                    <input name="investment_range_end" id="investment_range_end" maxlength="16" pattern="^[\+0-9]*$" type="text" value=""><br><br>
                    <label>Investment Upto</label>
                </div>
                <div class="input-field col s12">

                    <input name="professional_background" type="text" value=""><br><br>

                    <label>Professional Background</label>
                </div>


                <div class="input-field col s12">

                    <input name="alumni_network" type="text"  value=""><br><br>

                    <label>Alumni Network</label>
                </div>
                <div class="input-field col s12">

                    <input name="investment_network" type="text"  value=""><br><br>

                    <label>Part of Investment Network</label>
                </div>
                <div class="input-field col s12">

                    <input name="members_of_organisation" type="text"  value=""><br><br>
                    <label>Members of Organisation</label>
                </div>
                <div class="input-field col s12">

                    <input name="sample_companies_invested" type="text"  value=""><br><br>
                    <label>Sample companies invested in:</label>
                </div>
            
            

                <div class="input-field col s12">
                    <input type="text" name="linkedin_username" value=""><br><br>
                    <label>LinkedIn Username / Link</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="facebook_username" value=""><br><br>
                    <label>Facebook Username / Link </label>
                </div>
                <div class="input-field col s12">
                    <input type="text" name="twitter_username" value=""><br><br>
                    <label>Twitter Username</label>
                </div>

                <div class="input-field col s12">
                    <input type="text" name="google_username" value=""><br><br>
                    <label>Google+ Username</label>
                </div>

            
                

            <div style="display: none;">
                <input name="access_type" type="hidden" value="sayourideas"><br><br>
                <input name="user_type" type="hidden" class="duser_type" value="investor">
            </div>
			<div class="input-field col s12">
            <button class="btn" type="Submit">Submit</button>
			</div>
        </div>
    </div>
</form>