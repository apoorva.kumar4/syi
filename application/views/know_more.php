<div class="section sskyblue">
    <div class="container">
      <div class="row">
          <div class="col s12">
            <h3 class="center">Organise your Startup, without difficulties.</h3>
            
          </div>
          
      </div>
      <div class="theflex">
          <div class="flex-item">
            <h4>A platform which has everything your Startup needs.</h4>
            <p>Get funding, Schedule Meetings, Talk to Investors, and much more. SaYourIdeas has everything you need to get your startup on the track without any hassles.</p>
          </div>
          <div class="flex-item">
            <h4>Submit your Idea and get connected.</h4>
            <p>Keep your documents in place, share them with Investors, and increase your PR.</p>
          </div>
          
	     </div>
       
      <div class="theflex">
          <div class="flex-item">
            <h4>Get work done with Service Providers.</h4>
            <p>We know you need the right people to get the work done. On SaYourIdeas we suggest you the best Service Providers which are specifically tailored according to your Startup's phase and sector. And the best part is you see it right on your Dashboard!</p>
          </div>
          <div class="flex-item">
            <h4>See Other Startups and how they work.</h4>
            <p>You can see other Startups, follow them and get updates from them. It's always good to be in a company.</p>
          </div>  
      </div>
      <div class="theflex">
        <div class="flex-item">
          <a href="/" class="btn btn-blue">Sign Up Now!</a>
        </div>

      </div>
    </div>
  </div>