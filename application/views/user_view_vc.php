<?php
/**
 * Created by PhpStorm.
 * User: aashayshah
 * Date: 07/10/16
 * Time: 5:32 PM
 */

?>
<style>
    .view-user-username{
        font-weight: 300;
        color: white;
        min-height: 200px;
        padding: 50px;
    }
    .view-user-cover_pic
    {
        background-color:#CB202D;
        box-shadow:0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 8px 0 rgba(0, 0, 0, 0.12);
    }
    .user-description
    {
        font-size:1.3rem;
        font-weight:400;

    }
</style>
{user}
<div class="row view-user-cover_pic">
    <div class="col s12 m2" style="margin-bottom:20px;">
        <img class="dynamic-pp card" data-userid="" style="display:inline-block;margin:25px" src="{profile_picture_url}" width="200px" alt="">
        <a class="btn-large waves-effect white" style="color:teal; margin-left: 25px;width:200px;margin-bottom:10px" href="/messages/new/{socialid}">Send a message</a>

    </div>
    <div class="col s12 m6" >

        <h1 class="view-user-username">{first_name} {last_name}
            <?php
            if($user[0]['verified_profile']==1)
            {
                echo "<i class='material-icons' id='verified_user' title='Verified by SaYourIdeas' data-userid='' style='color: #009be5 !important;font-size: 3rem !important;'>verified_user</i>";
            }
            
            ?>
            <p class="user_profile-user_type">Investor</p></h1>
    </div>
</div>
<div class="row gutter-space">
    <style>
        .ideator_table td,th{
            padding:5px 5px;
            font-size:1rem;
            color:#4a4a4a;
        }
    </style>
    <div class="col s12" >
        <div class="col s12 m6">
            <h4>User information</h4>
            <div class="col s12">
                <table class="highlight ideator_table">
                    <tr>
                        <td>Username</td>
                        <td><span class="notranslate">{username}</span></td>
                    </tr>
                    <tr>
                        <td>Member Since</td>
                        <td><?php echo date("F j, Y", strtotime($user[0]['timestamp']));?></td>
                    </tr>
                    <tr>
                        <td>Role</td>
                        <td>{role}</td>
                    </tr>
                    <tr>
                        <td>Sector Expertise</td>
                        <td><?php echo _get_sector_name_from_id($user[0]['sector_expertise'])?></td>
                    </tr>
                    
                    <tr>
                        <td>Industry</td>
                        <td>{industry}</td>
                    </tr>
                    <tr>
                        <td>Company</td>
                        <td>{company}</td>
                    </tr>
                    <tr>
                        <td>Working Since</td>
                        <td>{working_since}</td>
                    </tr>
                    <tr>
                        <td>Investment Start</td>
                        <td><?php echo _f_number_to_words($user[0]['investment_range_start'])?></td>
                    </tr>
                    <tr>
                        <td>Investment End</td>
                        <td><?php echo _f_number_to_words($user[0]['investment_range_end'])?></td>
                    </tr>
                    
                    <tr>
                        <td>Professional Background</td>
                        <td>{professional_background}</td>
                    </tr>
                        <tr>
                            <td>Alumni Network</td>
                            <td>{alumni_network}</td>
                        </tr>
                        <tr>
                            <td>Investment Network</td>
                            <td>{investment_network}</td>
                        </tr>
                        <tr>
                            <td>Members of Organisation</td>
                            <td>{members_of_organisation}</td>
                        </tr>
                        <tr>
                            <td>Sample companies invested</td>
                            <td>{sample_companies_invested}</td>
                        </tr>
                        <tr>
                            <td>Would Like to mentor startups</td>
                            <td>{would_like_to_mentor_startups}</td>
                        </tr>
                        <tr>
                            <td>Currently associated with as a mentor</td>
                            <td>{companies_associated_with_as_a_mentor}</td>
                        </tr>
                        <tr>
                            <td>Areas of Expertise/ How do I add value</td>
                            <td>{areas_of_expertise_how_do_i_add_value}</td>
                        </tr>
                </table>
            </div>
        </div>
        <div class="col s12 m6">
            <h4>Social Links</h4>
            <div class="col s12">
                <table class="highlight ideator_table">
                    <tr>
                        <td>Facebook</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($user[0]['facebook_username'],"facebook")?>" target="_blank"><?php echo $user[0]['facebook_username']?></a></span></td>
                    </tr>
                    <tr>
                        <td>Twitter</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($user[0]['twitter_username'],"twitter")?>" target="_blank"><?php echo $user[0]['twitter_username']?></a></span></td>
                    </tr>
                    <tr>
                        <td>Linkedin</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($user[0]['linkedin_username'],"linkedin")?>" target="_blank"><?php echo $user[0]['linkedin_username']?></a></span></td>
                    </tr>
                    <tr>
                        <td>Google+</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($user[0]['google_username'],"google")?>" target="_blank"><?php echo $user[0]['google_username']?></a></span></td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
</div>
{/user}
