<form id="sign_up_sp" method="POST" class="">
    <div id="registration_service_provider" style="" class="">
        <div class="row card text-center login-card" style="margin:0 auto !important;">
            <div class="row text-center" >
                <h4 style="margin:20px 10px;text-align:center;">Service Provider Registration</h4>
            </div>

            <div class="row">
                <div class="input-field col s12 m6">

                    <input id="name" name="first_name" type="text" required value="">
                    <label for="name">First Name *</label>
                </div>

                <div class="input-field col s12 m6">

                    <input id="lastname" name="last_name" type="text" required value="">
                    <label>Last Name *</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12 m6">
                    <input name="mobile" type="tel" required value="" pattern="^[\+0-9]*$" maxlength="10" title="Please enter valid number" id="mob"><span id="errmsg"></span><br><br>
                    <label>Mobile *</label>
                </div>

                <div class="input-field col s12 m6">
                    <input name="password" type="password" required><br><br>
                    <label>Password *</label>
                </div>
                
            </div>
            <div style="display: none;">
                <input name="access_type" type="hidden" value="sayourideas"><br><br>
                <input name="user_type" type="hidden" class="duser_type" value="service_provider">
            </div>
            <button class="btn" type="Submit">Submit</button>
        </div>
    </div>
</form>