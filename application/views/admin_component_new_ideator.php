
<form id="sign_up_ideator" method="POST">
    <div id="registration_ideator" class="" style="margin-bottom: 70px;">
        <div class="row card text-center login-card" style="margin:0 auto !important;margin-bottom: 100px;">
            <div class="row text-center" >
                <h3 style="margin:20px 10px;text-align:center;">Ideator Registration</h3>
                
            </div>
            <div class="row" >
                <div class="input-field col s12 m6">

                    <input id="name" name="first_name" type="text" required value="">
                    <label for="name">First Name *</label>
                </div>

                <div class="input-field col s12 m6">

                    <input id="lastname" name="last_name" type="text" required value="">
                    <label>Last Name *</label>
                </div>
            </div>

            <div class="row">
                

                <div class="input-field col s12 m6">

                    <input name="mobile" type="tel" required value="" maxlength="16" pattern="^[\+0-9]*$" title="Please enter valid number" id="mob"><span id="errmsg"></span><br><br>
                    <label>Mobile *</label>
                </div>
                <div class="input-field col s12 m6">

                    <input name="password" type="password" required value="" >
                    <label>Password*</label>
                </div>
            </div>

            <input class="btn" type="Submit" value="Submit"><br><br>
            <div style="display: none;">
                <input name="user_type" type="hidden" class="duser_type" value="ideator">
            </div>
        </div>
    </div>
</form>