<div class="container">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <h3 class="center color-yellow"><i class="material-icons large">error</i></h3>
            <h3 class="center">An error occurred!</h3>

            <p class="center">There was an error in these fields:</p>

            <?php foreach($inputs as $key=>$input)
            {
                if(is_null($input))
                {
                    echo "<p class='center'>$key</p>";
                }
            }
            ?>
            <p class="center"><a href="/sign-up">Click here to make changes</a></p>
            <br><br>
        </div>
    </div>
</div> <!-- End of Container -->