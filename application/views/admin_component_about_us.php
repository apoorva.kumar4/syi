<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
	<div class="col s12 center">
		<h3>About Us</h3>
	</div>
	<div class="row container">
		<form id="add_video">
			<h4>Video Embed Code</h4>
			<div class="card" style="padding: 20px">
				<div class="input-field">
					<textarea class="materialize-textarea" id="video_link" name="video_link"><?php echo $youtube_link?></textarea>
					
				</div>
				<input type="submit" class="btn" value="update">
			</div>

		</form>
		<form class="note-save" method="POST">
			<h4>How we do it?</h4>
			<div id="#how_we_do_it" data-note_id="1" class="myeditablediv card min-height350" style="padding: 20px">
				<?php echo html_entity_decode($how_we_do_it) ?>
			</div>
			<h4>What do we do?</h4>
			<div id="#what_do_we_do" data-note_id="2" class="myeditablediv card min-height350" style="padding: 20px">
				<?php echo html_entity_decode($what_do_we_do) ?>
			</div>  
		</form>
	</div>
