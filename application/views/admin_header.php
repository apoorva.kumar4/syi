<!DOCTYPE html>
<head>
	<title>SaYourIdeas Analytics</title>
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
    <script>
        (function(w,d,s,g,js,fs){
            g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
            js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
            js.src='https://apis.google.com/js/platform.js';
            fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
        }(window,document,'script'));
    </script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	
	<link rel="stylesheet" href="/css/chartjs-visualizations.css">
	
	<link type="text/css" rel="stylesheet" href="/css/materialize.css"  media="screen,projection"/>

    <link rel="stylesheet" href="/css/selectize-custom.css">

    <link rel="stylesheet" href="/css/custom.css">
    <script src="/js/vue.js"></script>
    
</head>
<body>
    <div id="admin_app">
        <div class="app-wrapper">
            <div id="admin-nav" class="nav-wrapper">
        		<a id="admin-menu-icon" href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        	</div>
            <div class="row fwidth">
        		<?php
                    require('admin_nav_bar.php');
                ?>
                
            
  
