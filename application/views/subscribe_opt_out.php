<style type="text/css">
	@media screen and (min-width: 768px) {
		#Subscribe {
			padding:50px 100px;
		}	
	}
	@media screen and (min-width: 1024px) {
		#Subscribe {
			padding:50px 350px;
		}	
	}

		
	
</style>
<section class="section">
	<div id="Subscribe" class="wrapper">
		<div class="row">
			<div class="col s12 card" style="padding:20px">
				<h4>Unsubscribe from all newsletters.</h4>
				<p>You will continue to receive any account related updates.</p>
				<form class="" action="/subscribe/opt_out" method="POST">
					<div class="input-field col s12">
						<input id="email" name="email" type="email" required="" />
						<label for="email">Your Email</label>
					</div>
					<div class="input-field col s12">
						<div class="g-recaptcha" data-sitekey="6LdIOjQUAAAAALRi6GT1xzV0nrg138PETl3RqLaz"></div>
					</div>
					<div class="input-field col s12">
						<input id="submit" class="btn btn-blue fwidth" type="submit" value="Unsubscribe"/>
						
					</div>
				</form>
			</div>
		</div>
	</div>
</section>