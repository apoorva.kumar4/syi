<div class="container">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <h3 class="center color-teal"><i class="material-icons large">check_circle</i></h3>
            <h3 class="center">Success!</h3>
            <h5 class="center">Your Document has been uploaded successfully, go to <a href="/ideas/documents/<?php echo $idea_id?>">Documents.</a></h5>
            <br><br>
        </div>
    </div>
</div> <!-- End of Container -->