<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
<div class="row center">
	<div class="col s12 m12">
		<h3 class="admin-title">All Ideas</h3>
		<p>Latest First</p>
	</div>
</div>
<div class="row">
	<h3>
		<a href="/admin/ideas/add" class="waves-effect waves-light btn btn1 right">New Idea</a>
		<a v-on:click="download_selected_ideas" class="btn btn1 green right" href="#">
			<i class="material-icons left">file_download</i>Download Selected as CSV
		</a>
		<a v-if="!checkedIdeas.length" class="btn btn1 blue right" v-on:click="checkAllIdeas">Select All</a>
		<a v-else class="btn btn1 blue right" v-on:click="uncheckAllIdeas">Clear Selection</a>

	</h3>
</div>


<div class="row" style="border:1px solid #ababab;padding:15px;">
	<div class="col s12 m6 l3" id="">
			<h5>Creation Date</h5>
			<input v-model="filter_date_start" name="date_start" type="date" class="">
			<input v-model="filter_date_end" name="date_end" type="date" class="">
	</div>
	<div class="col s12 m6 l3" id="">
			<h5>Sector</h5>
			
				<select v-model="filter_sector" name='sector[]' id='sector' class="browser-default">
					<option name='' value='All' selected>All</option>
					<option v-for="(sector,index) in sectors" v-bind:value="index" v-text="sector"></option>
				</select>
			
			
	</div>

	<div class="col s12 m6 l3">
			<h5>Location</h5>
			<select v-model="filter_location" name="location" id="select-city" class="demo-default browser-default" placeholder="Select a city...">
					<option value=""></option>
					<?php
					$cities=_f_get_cities();
					foreach($cities as $city) {
							?>
							<option value="<?php echo $city['name']?>"><?php echo $city['name']?></option>
							<?php
					}?>
			</select>
	</div>
	<div class="col s12 m6 l3" id="">
			<h5>Phases</h5>
			<select  v-model="filter_phase" name="stage" required id="phase" class="browser-default">
				<option value="All" selected>All</option>
				<option value="Ideation" >Ideation</option>
				<option value="Prototype" >Prototype</option>
				<option value="Launching Soon" >Launching Soon</option>
				<option value="Operational" >Operational</option>
				<option value="Expansion" >Expansion</option>
			</select>
	</div>
</div>
<div class="row" style="padding:15px;">
	<h5>Search Idea</h5>
	<div class="col s12" id="">
		<div class="input-field">
			<input type="text" autocomplete="off" id="city" v-model="filter_idea_name" class="" placeholder="Name">
		</div>
	</div>
</div>


<div class="row">
	<div class="col  s12 m12 l12">

		<ul class="collection collectionx">
			<form id="form-ideas-download" action="/admin/ideas/download" method="POST">
			 
				<template v-if="!_.isEmpty(users) && !_.isEmpty(ideas) && !_.isEmpty(uploads)">
					<li v-for="(idea,index) in recent_ideas" class="collection-item collection-itemx avatar">
						
						<label class="labelx-idea-downloads">
							<input class="filled-in" type="checkbox" v-model="checkedIdeas" v-bind:value="index" class="input-download-ideas">
							<span></span>
						</label>
						<template v-if="idea.logo_upload_id!==false">
							<img v-bind:src="uploads[idea.logo_upload_id].complete_url" alt="" class="circle">	
						</template>
						<template v-else>
							<img src="/assets/bulb.png" alt="" class="circle">	
						</template> 
						
						
						<!-- <img src="/assets/bulb.png" alt="" class="circle"> -->
						<a v-bind:href="'/ideas/'+idea.unique_url" class="text-teal title" style="width: 70%;display: inline-block;" v-text="idea.name"></a><div class="text-lighten"
						style="text-align: right;
							display: inline-block;
							float: right;
							margin-right: 0px;"><span v-text="localDate(idea.created_on)"></span><br><span v-text="idea.followers+' followers'"></span>
					</div>
						<p class="subtext subtextx">{{idea.stage}}<br>{{idea.description}}<br>
								<!-- <a href="#" class="btn red" style="color: #fff !important;margin-top: 20px !important;">Delete</a> -->
						</p>
					</li>
				</template>
				<template v-else>
				<div class="row center">
					<div class="preloader-wrapper small active">
						<div class="spinner-layer spinner-blue-only">
							<div class="circle-clipper left">
								<div class="circle"></div>
							</div>
							<div class="gap-patch">
								<div class="circle"></div>
							</div>
							<div class="circle-clipper right">
								<div class="circle"></div>
							</div>
						</div>

					</div>
					</div>
				</template>
		</ul>
	</div>
</div>
<div class="hide">
<table id="table_selected_ideas">
	<tr>
		
			<th>Id</th>
			<th>Name</th>
			<th>Stage</th>
			<th>url</th>
			<th>Description</th>
			<th>Created</th>
		
	</tr>
	<tr v-for="item in checkedIdeas" v-if="">
		<td v-text="recent_ideas[item].id"></td>
		<td v-text="recent_ideas[item].name"></td>

		<td v-if="recent_ideas[item].stage!==''" v-text="recent_ideas[item].stage"></td>
		<td v-else></td>
		
		<td v-text="'https://www.sayourideas.com/ideas/'+recent_ideas[item].unique_url"></td>
		<td v-if="recent_ideas[item].description" v-text="recent_ideas[item].description"></td>
		<td v-text="localDate(recent_ideas[item].created_on)"></td>
	</tr>
</table>
</div>