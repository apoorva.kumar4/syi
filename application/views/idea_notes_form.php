<div class="container">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <?php if(segx(3)=="add")
            {
                ?>
                <h3 class="center">Add a new Note for <?php echo $idea_name?></h3>
            <?php
            }
            else if(segx(3)=="edit") {?>
                <h3 class="center">Edit Note</h3>
            <?php
            }
            ?>
            <div class="center red-text">
                <?php echo validation_errors();?>
            </div>

        </div>
        <?php if(segx(3)=="add") {
            ?>
            <?php $var_form="/ideas/notes/add/$idea_id/do"; echo form_open($var_form);
            }
            else if(segx(3)=="edit")
            {
                ?>
                <?php
                $id=$note['id'];
                $var_form="/ideas/notes/edit/$id/do"; echo form_open($var_form);
            }
            ?>

        <div class="row custom">
            <div class="input-field col s12">
                <input type="text" name="note_name"  value="<?php echo isset($note['name'])?$note['name']:set_value('note_name')?>" required></input>
                <label for="note_content">Name of the note</label>
            </div>
            <div class="input-field col s12">
                <textarea name="note_content"  class="materialize-textarea" required><?php echo isset($note['text'])?$note['text']:set_value('note_content')?></textarea>
                <label for="note_content">Type something you want to remember</label>
            </div>

        </div>
        <input class="btn" type="submit" value="Save">
        <a class="" href="/ideas/notes/<?php echo $idea_id?>" style="margin:20px">Cancel</a>
        </form>
    </div>

</div>
<div class="row">
</div>