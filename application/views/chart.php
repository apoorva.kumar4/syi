<div id="circles-1">

	</div>

	<script>
		var myCircle = Circles.create({
  id:                  'circles-1',
  radius:              40,
  value:               43,
  maxValue:            100,
  width:               5,
  text:                function(value){return value + '%';},
  colors:              ['#F6F9FC','#1A237E'],
  duration:            200,
  wrpClass:            'circles-wrp',
  textClass:           'circles-text',
  valueStrokeClass:    'circles-valueStroke',
  maxValueStrokeClass: 'circles-maxValueStroke',
  styleWrapper:        true,
  styleText:           true
});
	</script>