<div class="col s12 m4 l2">
    
        <ul id="slide-out" class="side-nav fixed fixedc">
            <li>
                <div class="userView" style="background-color:#007ee6;">
                    <a href="#!user">
                        <img v-if="admin.profile_picture_upload_id" v-bind:src="uploads[admin.profile_picture_upload_id].complete_url" class="circle" v-bind:src="">
                    </a>
                    <span class="white-text name" v-text="admin.first_name"></span>
                    <a class="customlink" href="/admin/admin_profile"><span class="white-text email">Go to Profile</span></a>
                    <a class="customlink" href="/logout"><span class="white-text email">Logout</span></a>
                </div>
            </li>
            <li>
                <a class="waves" href="/admin/dashboard"><i class="material-icons">dashboard</i>Dashboard</a>
            </li>
            <li v-if="admin.socialid==='1'">
                <a class="waves" href="/admin/manage_admins"><i class="material-icons">assignment_ind</i>Manage Admins</a>
            </li>
            <li>
                <a class="waves" href="/admin/inactive_users"><i class="material-icons">perm_identity</i>Inactive Users</a>
            </li>
            <li>
                <a class="waves" href="/admin/recent_activity"><i class="material-icons">history</i>Recent Activity</a></li>
            <li>
                <a class="waves" href="/admin/analytics"><i class="material-icons">trending_up</i>Analytics</a>
            </li>
            <li>
                <a class="waves" href="/admin/about_us"><i class="material-icons">info_outline</i>About Us</a>
            </li>
            <li>
                <a class="waves" href="/admin/ideas"><i class="material-icons">lightbulb_outline</i>Ideas</a>
            </li>
            <li>
                <a class="waves" href="/admin/top_ideas"><i class="material-icons">star</i>Top Ideas</a>
            </li>
            <li>
                <a class="waves" href="/admin/messages"><i class="material-icons">chat</i>Messages</a>
            </li>
            <li>
                <a class="waves" href="/admin/email"><i class="material-icons">email</i>Email</a>
            </li>
            <li>
                <a class="waves" href="/admin/testimonials"><i class="material-icons">format_quote</i>Testimonials</a>
            </li>
            <li>
                <a class="waves" href="/admin/registrations/ideators"><i class="material-icons">supervisor_account</i>Ideators</a>
            </li>
            <li>
                <a class="waves" href="/admin/registrations/service_providers"><i class="material-icons">supervisor_account</i>Service Providers</a>
            </li>
            <li>
                <a class="waves" href="/admin/registrations/investors"><i class="material-icons">supervisor_account</i>Investors</a>
            </li>
            <li>
                <a class="waves" href="/admin/events"><i class="material-icons">event</i>Events</a>
            </li>
            <li>
                <a class="waves" href="/admin/faq"><i class="material-icons">record_voice_over</i>FAQ's</a>
            </li>
            <li>
                <a class="waves" href="/admin/sectors"><i class="material-icons">public</i>Sectors</a>
            </li>
        </ul>
   
</div>
