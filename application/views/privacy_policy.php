<style>
    .type_info ul li {
        list-style-type: square;
        margin-left: 5%;
    }
</style>
<div class="container">
    <div class="row" style="margin:50px auto !important; padding:5px 20px;">
        <h2 class="center">Privacy Policy</h2>
        <p>This document is an electronic record in terms of Information Technology Act, 2000 and the Rules made there under, as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures. Words note defined herein shall have the meaning ascribed to them in the Terms and Conditions available <a href="/terms">here.</a></p>
        <p>This document is published in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries guidelines) Rules, 2011 that require publishing the rules and regulations, Privacy Policy and Terms of Use for access or usage of the Website.</p>
        <p>Kunal Soni, proprietor of SaYourIdeas, having its registered office at 16, Mohanlal Mansion, Underai Road, Malad (West), Mumbai - 400064 India (hereafter referred to as "We", "Us", or "Our" and which term shall include his successors, employees, attorneys and representatives) is the owner of the Website.</p>
        <p>Your use or access of the Website and the Services are governed by this Privacy Policy in addition to the Terms and Conditions and other applicable policies as posted on the Website. </p>
        <ol>
            <li>
                <h4 class="black-text">Overview</h4>
                <p>We reserve the right, at our sole discretion, to change, modify, add or remove portions of this Privacy Policy, at any time without any prior notice. It is your responsibility to review this Privacy Policy periodically for updates / changes. Your continued use of the Website following the posting of revised Agreement in future shall mean that you accept and agree to the revisions.</p>
                <p>By providing us your Information or by accessing the Website, you hereby consent to the collection, storage, processing and transfer of any or all of your Personal Information and Non-Personal Information by Us as specified under this Privacy Policy. You further agree that such collection, use, storage and transfer of your information shall not cause any loss or wrongful gain to you or any other person.</p>
                <p>We commit to respecting your online privacy data. We further recognize your need for appropriate protection and management of any personally identifiable information (“Personal Information“) you share with us. Information that is considered to be Personal Information s includes, but is not limited to, your name, address, email address or other contact information. This Privacy Policy also applies to all Users of the Website.</p>
            </li>
            <li>
                <h4 class="black-text">Information we collect</h4>
                <ol>
                    <li type="A">
                        <h5 class="black-text">Information directly obtained from you:</h5>
                        <p>The Website gives you an option of giving us your contact information (like name, address, e-mail and telephone number) for becoming a Registered User.</p>
                        <p>Any communication between you and the Website, along with queries and posts on the Website are also included in this. </p>
                    </li>
                    <li type="A">
                        <h5 class="black-text">Information collected indirectly:</h5>
                        <p>Certain information about you is collected automatically when you access the Website. This includes, but is not limited to, your IP address, browser type and operating system, your immediate Internet history, device location, information collected through cookies etc.</p>
                        <p>We may also obtain information relating to you from third party sources such as social networking websites, databases, online marketing firms, etc. We collect only basic information such as name, e-mail address, contact number, location etc.</p>
                        <p>We also collect and store personal information provided by you from time to time on the Website. We only collect and use such information from you that we consider necessary for achieving a seamless, efficient and safe experience, customized to your needs including:</p>
                        <ol>
                            <li>To enable the provision of services opted for by you;</li>
                            <li>To show the advertisements we feel are most relevant to you;</li>
                            <li>To send the latest newsletters to you; and</li>
                            <li>To comply with applicable laws, rules and regulations</li>
                        </ol>
                        <p>Where any service requested by you involves a third party, such information as is reasonably necessary by us to carry out your service request may be shared with such third party.</p>
                        <p>Any misuse of your Account should be brought to our notice immediately on <a href="mailto:info@sayourideas.com">info@sayourideas.com</a></p>
                    </li>
                </ol>
            </li>
            <li>
                <h4 class="black-text">Manner of collecting Information</h4>
                <ol>
                    <li type="a">Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.</li>
                    <li type="a">We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law</li>
                    <li type="a">We will only retain personal information as long as necessary for the fulfillment of those purposes.</li>
                    <li type="a">We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.</li>
                    <li type="a">Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.</li>
                </ol>
            </li>
            <li id="cookies">
                <h4  class="black-text">Cookies</h4>
                <p>We use data collection devices such as “cookies” on certain pages of the Websites. “Cookies” are small files sited on your hard drive that assist us in providing customized services. We also offer certain features that are only available through the use of a “cookie”. Cookies can also help us provide information, which is targeted to your interests. Cookies may be used to identify Registered Users.</p>
                <p>Third party vendors including www.google.com (“Google”) may use cookies to serve ads based on your visits to the Website. You may visit the website of third parties and choose to opt out of the use of cookies for interest-based advertising, if the third party offers such an option. You may choose to opt-out of the DoubleClick cookie that Google and its partners use for interest-based advertising by visiting Ads Settings</p>
                <p>The website also has enabled the Google Analytics Advertising, which allows Google to collect data about Users of the Website, in addition to Google advertising cookies and anonymous identifiers. You may choose to opt out of this by downloading and installing the Google Analytics opt-out add-on here <a href="https://tools.google.com/dlpage/gaoptout/">https://tools.google.com/dlpage/gaoptout/</a></p>
            </li>
            <li>
                <h4 class="black-text">Our Use of Your Information</h4>
                <p>Your contact information is also used to contact you when necessary. We use your IP address to help diagnose problems with our server, and to administer the Website. Your IP address is also used to help identify you and to gather broad demographic information. Finally, we may use your IP address to help protect our partners and ourselves from fraud. We will continue to enhance our security procedures as new technology becomes available. We will transfer information about you if the Website is acquired by or merged with another person. In this event, we will notify you by email or by putting a prominent notice on the site before information about you is transferred and becomes subject to a different privacy policy.</p>
                <p>We may release your personal information to a third-party in order to comply with a Court Order or other similar legal procedure, or when we believe in good faith that such disclosure is necessary to comply with the law; prevent imminent physical harm or financial loss; or investigate or take action regarding illegal activities, suspected fraud, or violations of our Terms and Conditions. We may disclose personally identifiable information to parties in compliance with our <a href="/terms">Terms of Use</a> as we in our sole discretion believe necessary or appropriate in connection with an investigation of fraud, intellectual property infringement, piracy, or other unlawful activity. In such events, we may disclose name, address, country, phone number, e-mail address and such other information which we feel is necessary.</p>
            </li>
            <li>
                <h4 class="black-text">Confidentiality</h4>
                <p>You further acknowledge that the Website may contain information which is designated confidential by us and that you shall not disclose such information without our prior written consent.</p>
                <p>Your information is regarded as confidential and therefore will not be divulged to any third party, unless if legally required to do so to the appropriate authorities.</p>
                <p>We will not sell, share, or rent your personal information to any third party or use your e-mail address for unsolicited mail. Any emails sent by us will only be in connection with the provision of agreed services and products.</p>
            </li>
            <li>
                <h4 class="black-text">Our Disclosure of Your Information</h4>
                <p>Due to the existing regulatory environment, we cannot ensure that all of your private communications and other personally identifiable information will never be disclosed in ways not otherwise described in this Privacy Policy. By way of example (without limiting and foregoing), we may be forced to disclose information to the government, law enforcement agencies or third parties. Under certain circumstances, third parties may unlawfully intercept or access transmissions or private communications, or members may abuse or misuse your information that they collect from the Website. Therefore, although we use industry standard practices to protect your privacy, we do not promise, and you should not expect, that your personally identifiable information or private communications would always remain private.</p>
                <p>As a matter of policy, we do not sell or rent any personally identifiable information about you to any third party. However, the following describes some of the ways that your personally identifiable information may be disclosed:</p>
                <ol>
                    <li type="a"><strong>External Service Providers:</strong> There may be a number of services offered by external service providers that help you use our Websites. If you choose to use these optional services, and in the course of doing so, disclose information to the external service providers, and/or grant them permission to collect information about you, then their use of your information is governed by their respective privacy policies.</li>
                    <li type="a"><strong>Other Corporate Entities:</strong> We share much of our data, including personally identifiable information about you, with our parent and/or subsidiaries that are committed to serving your online needs and related services, throughout the world. Such data will be shared for the sole purpose of enhancing your browsing experience and providing our services to you. To the extent that these entities have access to your information, they will treat it at least as protectively as they treat information they obtain from their other members. It is possible that our business could be taken over or merged merge with or be acquired by another business entity. Should such a combination occur, you should expect that we would share some or all of your information in order to continue to provide the service. You will receive notice of such event (to the extent that it occurs).</li>
                    <li type="a"><strong>Law and Order:</strong>We cooperate with law enforcement inquiries, as well as other third parties to enforce laws, such as: intellectual property rights, fraud and other rights. We can (and you authorize us to) disclose any information about you to law enforcement and other government officials as we, in our sole discretion, believe necessary or appropriate, in connection with an investigation of fraud, intellectual property infringements, or other activity that is illegal or may expose us or you to legal liability.</li>
                </ol>
            </li>
            <li>
                <h4 class="black-text">Accessing and Reviewing Information</h4>
                <p>Following registration, you can review and change the information you submitted each time except your display name. If you change any information we may keep track of your old information. You can change your registration information such as: name, address, city, state, zip code, country, phone number, and profile.</p>
                <p>We will retain in our files information you have requested to remove for certain circumstances, such as to resolve disputes, troubleshoot problems and enforce our terms and conditions. Further, such prior information is never completely removed from our databases due to technical and legal constraints, including stored ‘back up’ systems. Therefore, you should not expect that all of your personally identifiable information will be completely removed from our databases in response to your requests.</p>
            </li>
            <li>
                <h4 class="black-text">Other Information Collectors</h4>
                <p>Except as otherwise expressly included in this Privacy Policy, this document only addresses the use and disclosure of information we collect from you. To the extent that you disclose your information to other parties, whether they are on the Website or on other sites throughout the Internet, different rules may apply to their use or disclosure of the information you disclose to them. To the extent that we use third party advertisers, they adhere to their own privacy policies. Since we do not control the privacy policies of the third parties, you are subject to ask questions before you disclose your personal information to others.</p>
            </li>
            <li>
                <h4 class="black-text">Security</h4>
                <p>We treat data as an asset that must be protected against loss and unauthorized access. We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission and once we receive it. However, “perfect security” does not exist on the Internet. You therefore agree that any security breaches beyond the control of our standard security procedures are at your sole risk and discretion.</p>
            </li>
            <li>
                <h4 class="black-text">Questions and Suggestions</h4>
                <p>If you have any questions or concerns regarding this privacy policy, you should contact us by sending an e-mail to <a href="mailto:info@sayourideas.com">info@sayourideas.com</a></p>
            </li>
        </ol>
    </div>
</div>

