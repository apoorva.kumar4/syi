<?php
/**
 * Created by PhpStorm.
 * User: aashayshah
 * Date: 07/10/16
 * Time: 5:32 PM
 */

?>
<?php
if(_f_is_loggedin()){
$analytics['socialid']=$this->session->socialid;
$analytics['event_category']="idea";
$analytics['event_action']="visit";
$analytics['event_label']="idea_page";
$analytics['event_value']=$idea['name'];
$analytics['event_description']="Visited Idea page for ".$idea['name'];
a_send($analytics);
}?>
<link rel="stylesheet" href="/css/hopscotch.css">
<script src="/js/hopscotch.min.js"></script>
<style>
    .view-idea-ideaname{
        font-weight: 300;
        color: white;
        min-height: 200px;
        padding: 0;
        display: table-cell;
    }

    @media screen and (max-width:768px)
    {
        .view-idea-ideaname{
            text-align: center;
        }
    }

    @media screen and (max-width:425px) {
        .view-idea-ideaname {
            display:block;
            font-size:2.5rem;
        }
        .spencer {
            text-align: center;
            display:block;
        }
    }
    .view-idea-cover_pic
    {
        background-image: url('/api2/idea/<?php echo $idea['id']?>/cover');
        background-size: contain;
        min-height: 400px;
        box-shadow:1px 1px 1px 1px #f4f4f4;
        background-repeat: no-repeat;
        background-position: center;

    }
    .idea-description
    {
        font-size:1.3rem;
        font-weight:400;

    }
</style>

<script>
    jQuery(document).ready(function($){
        $("#coverfile").change(function() {

            var file = this.files[0];
            var coverfile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((coverfile==match[0]) || (coverfile==match[1]) || (coverfile==match[2])))
            {

                Materialize.toast("Invalid image",4000)
                return false;
            }
            else
            {

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);

            }
        });

        function imageIsLoaded(e) {
            $("#coverfile").css("color","green");
            $('#cover-previewing').attr('src', e.target.result);
            $('#cover-previewing').attr('width', 'fixed');
            $('#cover-previewing').attr('height', 'auto');
        };
    });


    jQuery(document).ready(function($){
        $("#logofile").change(function() {

            var file = this.files[0];
            var coverfile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((coverfile==match[0]) || (coverfile==match[1]) || (coverfile==match[2])))
            {

                Materialize.toast("Invalid image",4000)
                return false;
            }
            else
            {

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);

            }
        });

        function imageIsLoaded(e) {
            $("#logofile").css("color","green");
            $('#logo-previewing').attr('src', e.target.result);
//            x
        };
    });

    jQuery(document).ready(function($){
        $("#uploadIdeaLogo").on('submit',(function(e) {
            e.preventDefault();
            Materialize.toast("<span class='upload-status'>Uploading Idea Logo...</span>",4000);
            var url_pur="/api/v1/ideas/"+$(this).attr('data-ideaid')+"/logo/upload";
            var my_pare=$(this); // Saving the parent in a variable
            $.ajax({
                url: url_pur, // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {

                    var obj=JSON.parse(data);
                    if(obj.status=="success")
                    {
                        Materialize.toast("File Uploaded Successfully",4000);
                        location.reload();


                    }
                    else{
                        Materialize.toast("Upload Failed: "+ obj.description,4000);
                    }
                }
            });
        }));

        $("#btn_upload_logo").click(function(e){
            e.preventDefault();
            $("#uploadIdeaLogo").submit();
        });
    });


    jQuery(document).ready(function($){
        $("#uploadIdeaCover").on('submit',(function(e) {
            e.preventDefault();
            Materialize.toast("<span class='upload-status'>Uploading Idea Cover...</span>",4000);
            var url_pur="/api/v1/ideas/"+$(this).attr('data-ideaid')+"/cover_pic/upload";
            var my_pare=$(this); // Saving the parent in a variable
            $.ajax({
                url: url_pur, // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {

                    var obj=JSON.parse(data);
                    if(obj.status=="success")
                    {
                        Materialize.toast("File Uploaded Successfully",4000);
                        location.reload();


                    }
                    else{
                        Materialize.toast("Upload Failed: "+ obj.description,4000);
                    }
                }
            });
        }));

        $("#btn_upload_cover").click(function(e){
           e.preventDefault();
           $("#uploadIdeaCover").submit();
        });

    });
</script>


<div class="row view-idea-cover_pic">
    <div class="col s12 center-align" style="margin-bottom:20px;">
        <?php
        if(($idea['ownerid']==$this->session->socialid) || _f_is_admin())
        {
            ?>
            

            <div id="edit_logo" class="modal edit_img" style="text-align: left !important;">
                <div class="modal-content">
                    <div class="col s12">
                        <form id="uploadIdeaLogo" action="/tempimage" data-ideaid="<?php echo $idea['id']?>">
                            <h5>Logo</h5>
                            <div>
                                <img id="logo-previewing" width="150px" height="150px"  src="<?php echo _f_get_idea_logo_url($idea['id'])?>">
                            </div>
                            <div class="file-field input-field">
                                <input id="logofile" type="file" name="logofile">
                                <div class="file-path-wrapper file-path-wrapperx">
                                    <input class="file-path validate" type="text" placeholder="Select an Image">

                                </div>
                            </div>

                            <div class="input-field">
                                <a id="btn_upload_logo" class="waves-effect waves btn">Upload Logo</a>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close modal-close">Close</a>
                </div>
            </div>
            <?php
        }
        ?>
        <?php
        $CI = & get_instance();
        if(_f_is_admin())
        {
        ?>

            <a class="btn green" id="idea-pdf" href="/api2/idea/<?php echo $idea['id']?>/pdf" target="_blank"><i class="material-icons left">picture_as_pdf</i>Download pdf</a>
            <?php
            $ideaid=$idea['id'];
            if($idea['top_idea']) echo "<a class='btn green mark_top' data-ideaid=$ideaid data-action='untop'>Unmark as top idea</a>";
            else
                echo "<a class='btn green mark_top' data-ideaid=$ideaid data-action='top'>Mark as top idea</a>";
            ?>
            <a href="/ideas/edit/<?php echo $ideaid?>" class="btn green" id="delete_idea" style="color: #fff !important;">Edit Idea</a>
            <a href="#modal-delete" class="btn red modal-trigger" id="delete_idea" style="color: #fff !important;">Delete</a>
            <?php
        }
        ?>
        <script>
            jQuery(document).ready(function($){
                $(".mark_top").click(function(e) {
                    e.preventDefault();
                    var pare = $(this);
                    var url_pur = "/api2/idea/"+pare.attr('data-ideaid')+"/"+pare.attr('data-action');

                    $.ajax({
                        url: url_pur, // Url to which the request is send
                        type: "PATCH",             // Type of request to be send, called as method
                        contentType: false,       // The content type used when sending data to the server.
                        cache: false,             // To unable request pages to be cached
                        processData: false,        // To send DOMDocument or non processed data file it is set to false
                        success: function (data)   // A function to be called if request succeeds
                        {
                            var obj=JSON.parse(data);

                            if(obj.status=="success")
                            {
                                if(pare.attr('data-action')==="top") {
                                    Materialize.toast("Successfully marked as top idea",1000,'toastgreen');
                                    pare.attr('data-action','untop');
                                    pare.text("Unmark as top idea");
                                }
                                else if(pare.attr('data-action')==="untop"){
                                    Materialize.toast("Removed from top idea",1000,'toastgreen');
                                    pare.attr('data-action','top');
                                    pare.text("Mark as top idea");
                                }

                            }
                            else{
                                console.log(data);
                                Materialize.toast("An error occurred",1000,'toastred');
                            }
                        }
                    });
                });

            });
        </script>

        <div id="modal-delete" class="modal" style="width: 500px;">
            <div class="modal-content">
                <h4>Want to Delete?</h4>
                <p>Idea will be deleted permanently.</p>
            </div>
            <div class="modal-footer">
                <a class="btn red right" id="yes_delete" data-ideaid="<?php echo $idea['id']?>">yes</a>
                <a href="#!" class="modal-action modal-add-bullet-close left btn modal-close">no</a>
            </div>
        </div>
    </div>

    <?php
    $shared=0;
    if(isset($_GET['token'])){
        $shared=_f_get_idea_shareable_link($idea['id'])==$_GET['token'];
    }

    if(($idea['ownerid']==$this->session->socialid) || _f_is_admin() || $shared)
    {
        ?>
        <div class="row right">
            <a href="#edit_img"  id="edit_cover_pic" title="Edit cover pic" class="waves-effect white btn modal-trigger bk-bt" style="left:92%;"><i class="material-icons">mode_edit</i></a>
            <div id="edit_img" class="modal edit_img">
                <div class="modal-content">
                    <div class="col s12">
                        <h5>Cover</h5>
                        <form id="uploadIdeaCover" action="/tempimage" data-ideaid="<?php echo $idea['id']?>">
                            <div style="width:450px !important;">
                                <img id="cover-previewing" class="responsive" style="width=450px !important;" src="/api2/idea/<?php echo $idea['id']?>/cover">
                            </div>
                            <div class="file-field input-field">
                                <input id="coverfile" type="file" name="coverfile">
                                <div class="file-path-wrapper file-path-wrapperx">
                                    <input class="file-path validate" type="text" placeholder="Select an Image"">

                                </div>

                            </div>
                            <div class="input-field">
                                <a id="btn_upload_cover" class="waves-effect waves btn">Upload Cover</a>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close modal-close">Close</a>
                </div>
            </div>
        </div>
    <?php
    }
    ?>


</div>
<style>
.spencer {vertical-align: middle;padding: 20px}

</style>
<div class="idea-information-div" style="">
    <div class="row">
        <div class="col s12">
            <h1 class="view-idea-ideaname">
            <?php
            if($CI->session->socialid==$idea['ownerid'] || _f_is_admin()) {
                $owner=1;
                ?>
                    <a href="#edit_logo"  id="edit_logo_pic" class="modal-trigger">
                    <img height="150px" width="150px" class="dynamic-pp card" style="vertical-align:middle;display:inline-block;margin-top:20px" src="<?php echo _f_get_idea_logo_url($idea['id'])?>" alt="">
                    </a>
                    <?php
            } else {
                ?>
                
                    <img height="150px" width="150px" class="dynamic-pp card" style="vertical-align:middle;display:inline-block;margin-top:20px" src="<?php echo _f_get_idea_logo_url($idea['id'])?>" alt="">


                <?php
            }
            ?>
            <span class="spencer">
            <?php echo $idea['name']?></span>
            <?php _f_dynamic_follow_button($idea['id'])?>
            </h1>
        </div>
    </div>

    <div class="row">
    <div class="col s12 m6">
        <h4>Idea Brief</h4>
        <div class="card" style="padding:20px">
            <table class="highlight">
                <tbody>
                <tr>
                    <td>Ideator</td>
                    <td><a href="/users/<?php echo $idea['ownerid']?>"><?php echo _f_get_name_from_socialid($idea['ownerid']);?></a></td>
                </tr>
                <tr>
                    <td>Stage</td>
                    <td><?php $bullet=_f_get_idea_phase($idea['id']);echo $bullet['name'];?></td>
                </tr>
                <tr>
                    <td>Started On</td>
                    <td><?php echo date('F jS, Y',strtotime($idea['start_date']))?></td>
                </tr>
                <tr>
                    <td>Tagline</td>
                    <td><?php echo $idea['tagline']?></td>
                </tr>
                <tr>
                    <td>Followers</td>
                    <td><?php echo _f_get_idea_followers($idea['id'])?></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><?php echo $idea['description']?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <?php
        if((_f_is_user_approved($CI->session->socialid)==1 && $CI->session->user_type=="investor") || _f_is_admin() || isset($_GET['token'])?_f_get_idea_shareable_link($idea['id']===$_GET['token']):0 || $idea['ownerid']==$CI->session->socialid) {
        ?>
        <div class="card" style="padding:20px">
        <table class="highlight">
            <tbody>

                        <tr>
                            <td>Address</td>
                            <td><?php echo $idea['address']?></td>
                        </tr>

                        <tr>
                            <td>Website</td>
                            <td><?php echo $idea['website']?></td>
                        </tr>

                        <tr>
                            <td>Company</td>
                            <td><?php echo $idea['company']?></td>
                        </tr>
                        <tr>
                            <td>Patented?</td>
                            <td><?php echo $idea['patented']?></td>
                        </tr>
                        <tr>
                            <td>Patent Date</td>
                            <td><?php echo $idea['patent_number']?></td>
                        </tr>
                        
                        <tr>
                            <td>Services Required</td>
                            <td><?php echo $idea['services_required']?></td>
                        </tr>
                        
                        <tr>
                            <td>Achievements</td>
                            <td><?php echo $idea['achievements']?></td>
                        </tr>
                        
                        <tr>
                            <td>Market Size/ USP</td>
                            <td><?php echo $idea['market_size_usp']?></td>
                        </tr>
                        
                        <tr>
                            <td>Business Plan</td>
                            <td><?php echo $idea['business_plan']?></td>
                        </tr>
                        
                        <tr>
                            <td>Future Prospects</td>
                            <td><?php echo $idea['future_prospects']?></td>
                        </tr>
                        
                        <tr>
                            <td>Proof of Concept</td>
                            <td><?php echo $idea['proof_of_concept']?></td>
                        </tr>
                        
                        <tr>
                            <td>Revenue Model</td>
                            <td><?php echo $idea['revenue_model']?></td>
                        </tr>
                        
                        <tr>
                            <td>Team Overview</td>
                            <td><?php echo $idea['team_overview']?></td>
                        </tr>
                        
                        <tr>
                            <td>Competitions</td>
                            <td><?php echo $idea['competitions']?></td>
                        </tr>
                        

            </tbody>
        </table>
        </div>
            <?php
        }
        ?>
        
    </div>
    <div class="col s12 m6">
            <h4>Social Links</h4>
            <div class="col s12 card" style="padding:20px;margin-top:0">
                <table class="highlight ideator_table">
                    <tr>
                        <td>Facebook</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($idea['facebook_link'],"facebook")?>" target="_blank"><?php echo $idea['facebook_link']?></a></span></td>
                    </tr>
                    <tr>
                        <td>Twitter</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($idea['twitter_link'],"twitter")?>" target="_blank"><?php echo $idea['twitter_link']?></a></span></td>
                    </tr>
                    <tr>
                        <td>Linkedin</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($idea['linkedin_link'],"linkedin")?>" target="_blank"><?php echo $idea['linkedin_link']?></a></span></td>
                    </tr>
                    <tr>
                        <td>Pinterest</td>
                        <td><span class="notranslate"><a href="<?php echo socialify_link($idea['pinterest_link'],"google")?>" target="_blank"><?php echo $idea['pinterest_link']?></a></span></td>
                    </tr>
                    <tr>
                        <td>Other Link</td>
                        <td><span class="notranslate"><a href="<?php echo $idea['link_other']?>" target="_blank"><?php echo $idea['link_other']?></a></span></td>
                    </tr>
                </table>
            </div>
        </div>
</div>
<div class="row">
    <div class="col s12">
        <h4>Team Members (<?php echo sizeof($idea['team_members'])?>)</h4>
        <?php
        foreach($idea['team_members'] as $sp)
        {
            if(!$sp['name']=="") {


                ?>

                <div class="col s12 m6 l6">
                    <div class="card white darken-1 hoverable">
                        <div class="card-content center-align min-height350">
                            <ul class="collection" style="text-align: left;">
                                <li class="collection-item avatar">
                                    <span class="title"><a
                                                href="/users/<?php if (isset($sp['socialid'])) echo $sp['socialid'] ?>"><?php echo $sp['name'] ?></a></span>
                                    <p><?php if (isset($sp['location'])) echo $sp['location'] ?>
                                        <br><?php if (isset($sp['sector_expertise'])) echo _get_sector_name_from_id($sp['sector_expertise']) ?>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>

    </div>
</div>
<script>
    jQuery(document).ready(function($){
        //$('#modal-update-bullet').modal('open');
        window.ajax_get_bullet_data_json = function (ideaid)
        {

            <?php if(isset($_GET['token'])){
                ?>
                var token="<?php echo $_GET['token']?>";
                var source_url='/api2/idea/timeline/'+ideaid+'?token='+token;
                <?php
            }
            else{
                ?>
                var token="";
                var source_url='/api2/idea/timeline/'+ideaid;
                <?php
            }
            ?>
            
            return $.ajax({
                url: source_url
            });
        }
        function move (timeline,percentage) {
            var range = timeline.getWindow();
            var interval = range.end - range.start;

            timeline.setWindow({
                start: range.start.valueOf() - interval * percentage,
                end:   range.end.valueOf()   - interval * percentage
            });
        }
        /**
         * Zoom the timeline a given percentage in or out
         * @param {Number} percentage   For example 0.1 (zoom out) or -0.1 (zoom in)
         */
        function zoom (timeline,percentage) {

        }
        function addx(item, callback){
            callback(item);
        }


        $(".visualization-idea").each(function(index){

            var container = $(this);
            ajax_get_bullet_data_json($(this).attr('data-ideaid')).success(function(data){
                var bullets_object=JSON.parse(data);
                var items = new vis.DataSet(bullets_object);
                var options = {
                    zoomable:true,
                    zoomKey:'ctrlKey',
                    margin: 10,
                    zoomMax:315400000000,
                    zoomMin:864000000,
                    itemsAlwaysDraggable:true,
                    min:"2000-01-01",
                    start: new Date(),
                    moment: function (date) {
                        return vis.moment(date).utcOffset('+05:30');
                    },
                    minHeight:"250px",
                    orientation: 'bottom',
                    snap: function (date, scale, step) {
                        return date;
                    }
                };
                var timeline = new vis.Timeline(container[0], items, options);
                container.children().children('.zoomIn').bind('click',function(){
                    var range = timeline.getWindow();
                    var interval = range.end - range.start;
                    var percentage= -0.2;
                    timeline.setWindow({
                        start: range.start.valueOf() - interval * percentage,
                        end:   range.end.valueOf()   + interval * percentage
                    });
                });
                timeline.fit();
                container.children().children('.moveToToday').bind('click',function(){

                    timeline.moveTo(new Date());

                });

                container.children().children('.zoomOut').bind('click',function(){
                    var range = timeline.getWindow();
                    var interval = range.end - range.start;
                    var percentage= 0.2;
                    timeline.setWindow({
                        start: range.start.valueOf() - interval * percentage,
                        end:   range.end.valueOf()   + interval * percentage
                    });
                });

                container.children().children('.moveRight').bind('click',function(){
                    var range = timeline.getWindow();
                    var interval = range.end - range.start;
                    var percentage= -0.2;
                    timeline.setWindow({
                        start: range.start.valueOf() - interval * percentage,
                        end:   range.end.valueOf()   - interval * percentage
                    });
                });

                container.children().children('.moveLeft').bind('click',function(){
                    var range = timeline.getWindow();
                    var interval = range.end - range.start;
                    var percentage= 0.2;
                    timeline.setWindow({
                        start: range.start.valueOf() - interval * percentage,
                        end:   range.end.valueOf()   - interval * percentage
                    });
                });

                container.children().children('.fit').bind('click',function(){
                    timeline.fit();
                    console.log("Fit");
                });

                function redrawTimeline(item,bullets)
                {
                    timeline.setItems(bullets);
                }

            });

        });

        $("#yes_delete").click(function(e){
            e.preventDefault();
            var url_pur = "/api2/idea/"+ $(this).attr('data-ideaid');
            var pare = $(this);
            $.ajax({
                url: url_pur, // Url to which the request is send
                type: "DELETE",             // Type of request to be send, called as method
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData: false,        // To send DOMDocument or non processed data file it is set to false
                success: function (data)   // A function to be called if request succeeds
                {
                    var obj=JSON.parse(data);
                    if(obj.status=="success")
                    {
                        Materialize.toast("Idea Deleted Successfuly",1000,'toastgreen');
                        setTimeout("location.reload(true);", 1000);
                        history.go(-1);
                        return true;
                    }
                    else{
                        console.log(data);
                        Materialize.toast("Idea Delete Failed",1000,'toastred');
                    }
                },
                error:function(xhr)
                {
                    console.log(xhr );
                    Materialize.toast("Idea Delete Failed",1000,'toastred');
                }

            });
        });
    });
</script>

    <link href="/css/vis.css" rel="stylesheet" type="text/css" />
<?php
$CI = & get_instance();

if((_f_is_user_approved($CI->session->socialid)==1 && $CI->session->user_type=="investor") || _f_is_admin() || isset($_GET['token'])?_f_get_idea_shareable_link($idea['id']===$_GET['token']):0 || $idea['ownerid']==$CI->session->socialid)
{
?>
    <div class="row">
        <div class="col s12">
            <h4>Idea Time Line</h4>
            <div class="visualization-idea card" id="visualization-idea-<?php echo $idea['id']?>" data-ideaid="<?php echo $idea['id']?>">
                <div class="menu idea-action-desk" style="margin:0px auto;background-color: #1A237E;color:f4f4f4" >
                    <a class="btn-flat zoomIn text-white" title="Zoom In" data-ideaid="<?php echo $idea['id']?>"><i class="material-icons">zoom_in</i></a>
                    <a class="btn-flat zoomOut text-white" title="Zoom Out"><i class="material-icons">zoom_out</i></a>
                    <a class="btn-flat fit text-white" title="View Fit To Timeline"><i class="material-icons">view_comfy</i></a>
                    <a class="btn-flat moveToToday text-white" title="Today's Phase">Today</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <h4>Idea Documents</h4>
            <?php
            $documents=$CI->idea->get_documents($idea['id']);
            foreach($documents as $document)
            {
                ?>
                <div class="col s12 m6 l3 card" style="margin:20px">
                    <div class="card-image">
                        <a href="/uploads/u/<?php echo $document['url']?>"><img class="responsive-img" alt="Download" src="/uploads/u/<?php echo $document['url']?>" onerror="this.src=''"></img></a>

                    </div>
                    <div class="card-action">
                        <span class="" style="margin:10px"><?php echo $document['upload_name']?></span>
                    </div>
                </div>
                <!--<img class="materialboxed" width="250" src="/uploads/u/<?php echo $document['url']?>">-->

                <?php
            }?>
        </div>
    </div>
    <?php
}
?>
</div>
