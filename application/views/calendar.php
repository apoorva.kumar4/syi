		<html>
		<head>
		    <!-- 1. Include style -->
		    <link href="<?php echo base_url();?>css/calendar.css" rel="stylesheet" type="text/css">
			<script src="<?php echo base_url();?>js/calendar.js"></script>
		</head>
		<body>
		    <!-- 2. Include script -->
		    

		    <!-- 3. Place event data -->
		    <span class="addtocalendar atc-style-blue">
		        <var class="atc_event">
		            <var class="atc_date_start">2016-05-04 12:00:00</var>
		            <var class="atc_date_end">2016-05-04 18:00:00</var>
		            <var class="atc_timezone">Europe/London</var>
		            <var class="atc_title">Star Wars Day Party</var>
		            <var class="atc_description">May the force be with you</var>
		            <var class="atc_location">Tatooine</var>
		            <var class="atc_organizer">Luke Skywalker</var>
		            <var class="atc_organizer_email">luke@starwars.com</var>
		        </var>
		    </span>
		</body>
		</html>