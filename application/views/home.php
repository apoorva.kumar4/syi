<link rel="stylesheet" type="text/css" href="/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="/slick/slick-theme.css"/>
<script type="text/javascript" src="/slick/slick.min.js"></script>
<div class="section sskyblue">
		<div class="container">
			<div class="row">
				<div class="col s12 m6">
					<h3 class="home-idea">An Idea without Execution is just a wish.</h3>
					<p class="card-title" style="font-size:1.2rem; letter-spacing: -0.5px; line-height: 168%;">On SaYourIdeas you can create milestones, track your work, chat with the investors and much more. Manage your start-up effortlessly with an intuitive dashboard.</p>
				</div>
				<div class="col s12 m6">
					<form id="homepage_signup" action="/verify_mobile" method="POST">
						<div class="input-field col s12 m6">
							<input name="first_name" required type="text" v-model="first_name">
							<label for="first_name">First Name</label>
						</div>
						<div class="input-field col s12 m6">
							<input name="last_name" required type="text" v-model="last_name">
							<label for="first_name">Last Name</label>
						</div>
						<div class="input-field col s12">
							<input id="input_mobile" name="mobile" required type="tel" v-model="mobile">
							<label for="first_name">Mobile</label>
							
						</div>
						<div class="input-field col s12">
							<input name="password" required type="password" v-model="password">
							<label for="password">Password</label>
							<div class="g-recaptcha" data-sitekey="6LdIOjQUAAAAALRi6GT1xzV0nrg138PETl3RqLaz"></div>
							<p class="agree" for="agree" style="color: #858585;">By Signing Up, you agree to our <a tabindex=-1 target="_blank" href="/terms">Terms</a> and <a tabindex=-1 href="/privacy-policy" target="_blank">Privacy Policy</a> including our <a href="/privacy-policy#cookies">Cookie Use Policy.</a></p>
						</div>
						
						<div class="input-field col s12">
							
							<input type="submit" name="submit" class="btn blue fwidth" value="Sign up" />
						</div>
						<div class="modal" id="modal_otp">
							<div class="modal-content">
								<h5>We have sent an OTP on your mobile, please input it below to continue:</h5>
								<div class="row">
										<div class="input-field col s6">
											<input placeholder="OTP here" type="number" class="validate" name="otp" v-model="otp">
											<button id="btn_otp_submit" type="button" class="btn blue" onclick="fn_otp_submit()">Submit</button>
										</div>
								</div>
							</div>
						</div>
					</form>          
				</div>
			</div>
			
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
					<div class="col s12">
						<h3 class="center home-organize">Organise your Startup, without difficulties.</h3>
						
					</div>
					
			</div>
			<div class="theflex">
					<div class="flex-item">
						<h4 class="home-subheadings">A platform which has everything your Startup needs.</h4>
						<p>Get funding, Schedule Meetings, Talk to Investors, and much more. SaYourIdeas has everything you need to get your startup on the track without any hassles.</p>
					</div>
					<div class="flex-item">
						<h4 class="home-subheadings">Submit your Idea and get connected.</h4>
						<p>Keep your documents in place, share them with Investors, and increase your PR.</p>
					</div>
					
			 </div>
			 
			<div class="theflex">
					<div class="flex-item">
						<h4 class="home-subheadings">Get work done with Service Providers.</h4>
						<p>We know you need the right people to get the work done. On SaYourIdeas we suggest you the best Service Providers which are specifically tailored according to your Startup's phase and sector. And the best part is you see it right on your Dashboard!</p>
					</div>
					<div class="flex-item">
						<h4 class="home-subheadings">See Other Startups and how they work.</h4>
						<p>You can see other Startups, follow them and get updates from them. It's always good to be in a company.</p>
					</div>  
			</div>

			<div class="row">
					<div class="col s12">
						<h3 class="center">How it works</h3>
						<img style="" src="https://s3.ap-south-1.amazonaws.com/sayourideas-uploads/ps_howitworks.gif" class="responsive-img">
					</div>
					
			</div>
			<div class="theflex">
					<div class="flex-item">
						<h4 class="home-subheadings">Create Your Idea Timeline</h4>
						<p>By creating your idea, you can manage your tasks and meetings, keep your schedule on track, and optionally share it with your team members.</p>
					</div>
					<div class="flex-item">
						<h4 class="home-subheadings">Pin your Tasks</h4>
						<p>You can move your tasks to a specific date, tinker it according to your targets, as well as get an overview of what needs to be done.</p>
					</div>
			 </div>
			<div class="theflex">
					<div class="flex-item">
						<h4 class="home-subheadings">Add Notes and Documents</h4>
						<p>Add important notes such as business plan, revenue model so you don't lose them. Keep your important documents attached to easily access them when you meet an investor. You can also safeguard your idea by concealing their visibility.</p>
					</div>
					<div class="flex-item">
						<h4 class="home-subheadings">Get Service Recommendations</h4>
						<p>Get service provider recommendations below your timeline depending on the phase of your ideas. Contact the service providers directly from those recommendations.</p>
					</div>
			</div>


		</div>
	</div>
	<script src="/js/selectize.min.js"></script>
	<link rel="stylesheet" href="/css/selectize-custom.css">
<div class="section sskyblue">  
<div class="container row center">
		<div class="col s12">
			<h3>Top Ideas</h3>
					<div class="row">
					<?php foreach($top_ideas as $idea)
						{
							$sp=_f_get_user_from_socialid($idea['ownerid']);
							?>
							<div class="col s12 m4">
								<div class="card white">
									<div class="card-content center-align min-height350">
									<img class="dynamic-pp card" style="display:inline-block;margin-top:0px" src="<?php echo _f_get_idea_logo_url($idea['id'])?>" width="70px" alt=""><br>
											<span class="card-title"><a href="/ideas/<?php echo $idea['unique_url'] ?>"><?php echo $idea['name']?></a></span><br>
												<p class="card-idea-subtitle">In <?php $bullet=_f_get_idea_phase($idea['id']);echo $bullet['name']?> phase with <span id="placeholder_<?php echo $idea['id']?>_followers"> <?php echo _f_get_idea_followers($idea['id'])?> followers</p></span>
											
											<p class="left-align" style="padding: 20px"><?php echo substr($idea['description'], 0,300)?>...</p>
									</div>
									
					 </div>
						</div>
							<?php
						}
					?>
						
					</div>
		</div>
	</div>
</div>
	<div id="testimonials" class="container row center">
		<h3 class="text-teal">What people say...</h3>
		<div class="row" style="">
			<div id="slick_testimonials" class="center syi-slick" data-indicators="true">
				 <?php foreach($testimonials as $testimonial) {
					?>
						<div class="slick-item">
						<div class="testimonial-wrap col s12">
							<h5 class=""><em>"<?php echo $testimonial['description']?>"</em></h5>
							<div class="cd-author">
								<img src="<?php echo $testimonial['profile_picture_url']?>" alt="" class="circle">
								<ul class="cd-author-info">
									<li><?php echo $testimonial['first_name']." ".$testimonial['last_name']?></li>
									
									<li><?php echo $testimonial['position']?><br><a href="<?php echo $testimonial['website']?>" target="_blank"><?php echo $testimonial['company']?></a></li>
								</ul>
							</div>
						</div>
						</div>
					<?php
				 }
				 ?>
			</div>
		</div>
	</div> <!-- row -->
	<div id="partners_wrapper" class="container row center">
		<h3 class="text-teal">Our Partners</h3>
		<div class="partners">
			<?php
			foreach($partners as $partner) {
				?>
				<div class="partner-holder">
					<a href="<?php echo $partner['website']?>" target="_blank">
						<img src="<?php echo $partner['logo_url']?>" />
					</a>
				</div>
				<?php
			}
			?>
				<!-- <div class="partner-holder">
					<a href="http://www.csi-india.org/" target="_blank">
						<img src="/assets/partner1.png" /> 
					</a>
				</div>
				<div class="partner-holder">
					<a href="http://www.ahventures.in/" target="_blank">
				 		<img src="/assets/partner2.png" /> 
					</a>
				</div>
				<div class="partner-holder">
					<a href="https://www.tcetmumbai.in/" target="_blank">
						<img src="/assets/partner3.png" /> 
					</a>
				</div> -->

		</div>
	</div>
<script>
	
		function fn_otp_submit(){
		console.log('submit-otp');

		var form = document.getElementById('homepage_signup');

		var http = new XMLHttpRequest();
		var url = "/api3/sign_up";

		http.open("POST",url,true);

		http.onreadystatechange = function() {
			if(http.readyState==4 && http.status==200) {
					window.location="/dashboard";
			}
		};

		http.send(new FormData(form));
		return false;
		};

	
	var form_signup = document.getElementById("homepage_signup");

	function fn_signup(evt) {
		alert("hi");
		evt.preventDefault();
		
		var http = new XMLHttpRequest();
		var url = "/api3/send_verification_code";

		http.open("POST",url,true);

		http.onreadystatechange = function() {
			if(http.readyState==4 && http.status==200) {
					console.log(http.responseText);
					obj=JSON.parse(http.responseText);
					
					if(obj.status=="success")  {
						//show modal for otp submission
						jQuery('#modal_otp').openModal();

					} 
			} else if (http.readyState==4 && http.status!=200){
						Materialize.toast("You have entered an incorrect otp, please try again!",4000);
			}
		};

		http.send(new FormData(form));
		return false;
	};

	var app= new Vue({
		el: '#homepage_signup',
		data: {
			otp:'',
			first_name:'',
			last_name:'',
			mobile:'',
			password:''
		}
	});

	$(".syi-slick").slick({
		dots:true,
		autoplay:true,
		infinite:true,
		autoplaySpeed:6000,
		fade:true,
		speed:300,
		cssEase:'ease',

	});
</script>
				
