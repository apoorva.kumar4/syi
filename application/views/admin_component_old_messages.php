    <div class="row center">
      <div class="col s12 m12">
        <h3 class="admin-title">Old Messages</h3>
        <p>Latest First</p>
      </div>
       <div class="row">
      <h3><a href="/admin/messages/" class="waves-effect waves-light btn btn1 right">Pending Messages</a><a href="/admin/messages/add" class="btn btn1 blue right">New Message</a></h3>
    </div>
    </div>
    <div class="row">
      <div class="col  s12 m12 l12">
        <table id="messages">
<thead>
<tr>
<td>From</td>
<td>To</td>
<td>Date</td>
<td>Message</td>
<td>Approve</td>
</tr>
</thead>
<tbody>
          <?php
          foreach($all_messages as $message)
          {
            ?>
            <tr>
<td><a href="/users/<?php echo $message['sender']?>"><?php echo _f_get_firstname_from_socialid($message['sender'])?></a></td>
<td><a href="/users/<?php echo $message['receiver']?>"><?php echo _f_get_firstname_from_socialid($message['receiver'])?></a></td>
<td><?php echo date('M dS, Y h:m:s a',strtotime($message['timestamp']))?></td>
<td><?php echo $message['message']?></td>
<td><?php if($message['admin_approved']==1) echo "Approved"; else echo "Rejected"?></td>
			</tr>
            <?php
          }
          ?>
			</tbody>
			</table>


      </div>
    </div>