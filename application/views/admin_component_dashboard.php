<div class="row center">
  <div class="col s12 m12">
	  <h3 class="admin-title">Dashboard</h3>
  </div>
</div>

<div class="row  rowx">

	<div class="col s6 m3 l3">
  <div class="center-align">
      
          <h4 class="center widget-a">Ideas</h4>
          <h5 class="digit" v-if="count_ideas" v-text="count_ideas"></h5>
  </div>
	</div>

	<div class="col s6 m3 l3">
	<div class="center-align">
		<h4 class="center widget-a">Ideators</h4>
		<h5 class="digit" v-if="count_ideators" v-text="count_ideators"></h5>
    </div>
	</div>
	<div class="col s6 m3 l3">
	<div class="center-align">
		
		<h4 class="center widget-a">Service Providers</h4>
		<h5 class="digit" v-if="count_service_providers" v-text="count_service_providers"></h5>
    </div>
	</div>
	<div class="col s6 m3 l3">
	<div class="center-align">
	
		<h4 class="center widget-a">Investors</h4>
		<h5 class="digit" v-if="count_investors" v-text="count_investors"></h5>
    </div>
	</div>
</div>

<div class="row">
<div class="col  s12 m12 l12">
	<h5 class="admin-title">Recent Registrations <a class="right smaller-text" href="/admin/registrations">View All</a></h5>
    <template v-if="!_.isEmpty(recent_registrations)">
		
		<ul class="collection collectionx">
      
	  
      <li v-for="user in recent_registrations" class="collection-item collection-itemx avatar">
		<img v-if="user.profile_picture_upload_id" v-bind:src="uploads[user.profile_picture_upload_id].complete_url" alt="" class="circle">
		<img v-else v-bind:src="user.gravatar_url" alt="" class="circle">
        <span class="title text-lighten"><a v-bind:href="'/user_info/u/'+user.socialid" class="text-teal" v-text="user.first_name+' '+user.last_name"></a></span>
        <div class="text-lighten" 
        style="text-align: right;
				display: inline-block;
				float: right;
				margin-right: 0px;"><span v-text="user.user_type_human"></span><br><span v-text="localDate(user.timestamp)"></span>
				
        </div>

        <p class="subtext subtextx"><span v-text="user.mobile"></span><br>
           <span v-text="user.location"></span>  		</p>
      </li>
    </ul>
		</template>	
		<template v-else>
			<div class="row center">
			<div class="preloader-wrapper small active">
				<div class="spinner-layer spinner-blue-only">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>

			</div>
			</div>
		</template>
	
  
</div>
</div>
<div class="row">
	<div class="col  s12 m12 l12">
		<h5 class="admin-title">Recent Ideas <a class="smaller-text right" href="/admin/ideas">View All</a></h5>
		<template v-if="!_.isEmpty(ideas)">
	    <ul class="collection collectionx">
		  
	      <li v-for="idea in recent_ideas_five" class="collection-item collection-itemx avatar">
	      <img v-if="idea.logo_upload_id" v-bind:src="uploads[idea.logo_upload_id].complete_url" alt="" class="circle">
	      <img v-else src="/assets/bulb.png" alt="" class="circle">

	        <a v-bind:href="'/ideas/'+idea.unique_url" class="text-teal title" v-text="idea.name"></a>
	        <div class="text-lighten" style="text-align: right; display: inline-block; float: right;margin-right: 0px;" v-text="localDate(idea.created_on)">
			</div>
	        <p class="subtext subtextx" v-text="idea.description"></p>
	        
	      </li>
		  
		  
	    </ul>
		</template>
		<template v-else>
			<div class="row center">
			<div class="preloader-wrapper small active">
				<div class="spinner-layer spinner-blue-only">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>

			</div>
			</div>
		</template>

	</div>
</div>



<div class="row">
  <div class="col  s12 m12 l12">
	  <h5 class="admin-title">Recent Events <a class="smaller-text right" href="/admin/events">View All</a></h5>
		<template v-if="!_.isEmpty(recent_events)">
	  <ul class="collection collectionx">

    <li v-for="event in recent_events" class="collection-item collection-itemx avatar">

      <img v-if="event.logo_upload_id" v-bind:src="uploads[event.logo_upload_id].complete_url" alt="" class="circle">
      <img v-else src="/assets/bulb.png" class="circle">
      <span class="title text-lighten"><a v-bind:href="'/events/'+event.slug" v-text="event.name"></a></span>
      <div class="text-lighten" style="text-align: right; display: inline-block; float: right; margin-right: 0px;">Starts {{localDate(event.date_start)}}<br>{{event.views}} Views
      </div>
      <p class="subtext subtextx">By <a v-bind:href="'/user_info/u/'+event.ownerid" class="blue-text subtext" v-text="users[event.ownerid].first_name"></a><br><span v-text="event.description_short"></span>
      </p>

    </li>

   
	  </ul>
		</template>
		<template v-else>
			<div class="row center">
			<div class="preloader-wrapper small active">
				<div class="spinner-layer spinner-blue-only">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>

			</div>
			</div>
		</template>

  </div>
</div>