<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
<div class="row center">

	<div class="col s12 m12">
		<h3 class="admin-title">All User Registrations</h3>
		<p>Latest First</p>
	</div>
    
    <div class="col s12 text-left">
      <h3>
      

      
      <a v-on:click="download_selected_users" class="btn btn1 green" href="#"><i class="material-icons left">file_download</i>Download Selected as CSV</a>

        <a v-if="!checkedUsers.length" v-on:click="checkAllUsers" class="btn btn1 blue " href="#" data-checked="false">Select All</a>
		<a v-else v-on:click="uncheckAllUsers" class="btn btn1 blue " href="#">Clear Selection</a>
		<a v-if="filter_user_type_human !== undefined" class="btn btn1" :href="'/admin/registrations/'+filter_user_type+'s/add'" v-text="'Add '+filter_user_type_human"></a>
		</h3>

    </div>
</div>

<div class="row" style="border:1px solid #ababab;padding:15px;">
					
	<div class="row">
		<div class="col s12 m6 l4" id="">
			<h5>Filter by Date</h5>
			<div class="col"><input v-model="filter_date_start" name="date_start" type="date" class="datepicker"></div>
			<div class="col"><input v-model="filter_date_end" name="date_end" type="date" class="datepicker"></div>
		</div>
		<div class="col s12 m6 l4" id="hidd_sector_filter">
			<h5>Filter by Sector</h5>
			<select v-model="filter_sector" id='sector' class="browser-default">
				<option name='' value='All' selected>All</option>
				<option v-for="(sector,index) in sectors" v-bind:value="index" v-text="sector"></option>
			</select>
		</div>

		<div class="col s12 m6 l4">
			<h5>Filter by Location</h5>
			<div class="input-field">
				<input type="text" list="cities" id="city" v-model="filter_location" class="">
				
			</div>
		
			<datalist id="cities">
				<?php
				$cities=_f_get_cities();
				foreach($cities as $city) {
					?>
					<option value="<?php echo $city['name']?>"><?php echo $city['name']?></option>
					<?php
				}?>
			</datalist>
		</div>
		<div class="col s12 m6 l4" id="">
			<h5>Filter by User Type</h5>
			<select v-model="filter_user_type" class="browser-default">
				<option name='' value='All' selected>All</option>
				<option value="ideator">Ideator</option>
				<option value="service_provider">Service Provider</option>
				<option value="investor">Investor</option>
				
			</select>
		</div>
			
	</div>
	
</div>
<div class="row" style="padding:15px;">
	<h5>Search User</h5>
	<div class="col s12 m6" id="">
		
		<div class="input-field">
			<input type="text" autocomplete="off" id="city" v-model="filter_user_name" class="" placeholder="Name">
		</div>
	</div>
	<div class="col s11 m6" id="">
		
		<div class="input-field">
			<input type="text"  id="city" v-model="filter_user_mobile" class="" placeholder="Mobile">
		</div>
	</div>
</div>
<div class="row">
	<div class="col  s12 m12 l12">
		<template v-if="!_.isEmpty(users)">
			<ul class="collection collectionx">
			<li v-for="(user,index) in recent_registrations_all" class="collection-item collection-itemx avatar">
				<label class="labelx-idea-downloads">
						<input class="filled-in" type="checkbox" v-model="checkedUsers" v-bind:value="index">
						<span></span>
					</label> 
			<img v-if="user.profile_picture_upload_id" v-bind:src="uploads[user.profile_picture_upload_id].complete_url" alt="" class="circle">
			<img v-else v-bind:src="user.gravatar_url" alt="" class="circle">
			<span class="title text-lighten"><a v-bind:href="'/user_info/u/'+user.socialid" class="text-teal" v-text="user.first_name+' '+user.last_name"></a></span>
			<div class="text-lighten" 
			style="text-align: right;
			display: inline-block;
			float: right;
			margin-right: 0px;"><span v-text="user.user_type_human"></span><br><span v-text="localDate(user.timestamp)"></span>
			
			</div>

			<p class="subtext subtextx"><span v-text="user.mobile"></span><br>
					<span v-text="user.location"></span>  		</p>
			</li>  
			</ul>      
		</template>
		<template v-else>
		<div class="row center">
			<div class="preloader-wrapper small active">
				<div class="spinner-layer spinner-blue-only">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="gap-patch">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>

			</div>
			</div>
		</template>
	</div>
</div>
<table id="table_selected_users" class="hide">
	<tr>
		<th>Socialid</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Mobile</th>
		<th>Email</th>
		<th>User type</th>
		<th>Registration</th>
		
		<th>Location</th>
	</tr>
	<tr v-for="item in checkedUsers">
		<td v-text="recent_registrations_all[item].socialid"></td>
		<td v-text="recent_registrations_all[item].first_name"></td>
		<td v-text="recent_registrations_all[item].last_name"></td>
		<td v-text="recent_registrations_all[item].mobile"></td>
		<td v-text="recent_registrations_all[item].email"></td>
		<td v-text="recent_registrations_all[item].user_type_human"></td>
		<td v-text="localDate(recent_registrations_all[item].timestamp)"></td>
		<td v-text="recent_registrations_all[item].location"></td>
	</tr>
</table>