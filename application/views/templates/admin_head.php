<!DOCTYPE html>
<head>
	<title>Admin Panel SaYourIdeas - Localhost</title>
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
    
    <link href="https://fonts.googleapis.com/icon?family=Montserrat|Material+Icons" rel="stylesheet">
	
	<link type="text/css" rel="stylesheet" href="/css/materialize-1.min.css"  media="screen,projection"/>

    
	<link rel="stylesheet" href="/css/selectize-custom.css">
	<link rel="stylesheet" href="/css/dragula.min.css">
    <link rel="stylesheet" href="/css/custom.css">
	
</head>
<body>
	<style>
		body {
			font-family: 'Montserrat', sans-serif;
		}
	</style>