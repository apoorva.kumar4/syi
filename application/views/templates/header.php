<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<title><?php echo $title?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Cache-control" content="private,max-age:3600">
	<!-- <meta content="width=device-width, initial-scale=1" name="viewport"/> -->
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> -->

	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="theme-color" content="#ffffff">

	<?php if(isset($meta_description)) {
		?>
		<meta property="og:image" content="https://sayourideas.com/assets/logo3.png">
		<meta content="<?php echo $meta_description?>" name="description"/>
		<meta property="og:description"content="<?php echo $meta_description?>"> 
		<?php
	}
	else {
	
	}
	?>
		<meta content="" name="author"/>
  <link rel="shortcut icon" href="favicon.ico"/>
	
	<link rel="stylesheet" href="/css/materialize.css"  media="screen,projection"/>
	
	<link rel="stylesheet" href="/css/custom.css">
	<link rel="stylesheet" href="/css/horizontal-timeline.css">
	<link rel="stylesheet" href="/css/testimonial-style.css"> <!-- Resource style -->
	<link rel="stylesheet" href="/css/gmaps.css">

  <link href="/css/calendar.css" rel="stylesheet" type="text/css">
  <script src="/js/circles.js"></script>
  
<script src="/js/jquery.min.js"></script>
<script src="/js/modernizr-custom.js"></script>
    <script src="/js/vis.js"></script>

<script src="/js/materialize.js"></script>
  
  
  
  <script src="/js/mousemove.js"></script>
  <script src="/js/custom.js"></script>
  
  <script src="/js/vue.js"></script>
  <script src="/js/vue-resource-1.3.5.js"></script>
  <script src="/js/lodash.js"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-90643031-1', 'auto');
        <?php if(_f_is_loggedin()){
        ?>
        ga('set','userId','<?php echo $me[0]['socialid']?>'); // Set the user Id using signed-in socialid
        <?php
        }
        ?>
        ga('send', 'pageview');


    </script>

	<!-- Start of Recaptcha -->
	<script src='https://www.google.com/recaptcha/api.js'></script>		
</head>
<body>
<main>
	<?php 
	if(_f_is_loggedin()) {


	?>
  <ul id="dropdownprof" class="dropdown-content">
      
    <li><a href="/users/<?php echo $me[0]['socialid']?>">View Profile</a></li>
    <li><a href="/dashboard/profile">Edit Profile</a></li>

    <li><a href="/dashboard">Dashboard</a></li>
    <li><a href="/dashboard/events/">Events</a></li>
    <li class="divider"></li>

    <li><a href="/logout">Logout</a></li>

  </ul>
  <?php 
	}
	?>
  <div class="">
	<nav>	
		<div class="nav-wrapper">
			<a class="brand-logo" href="/"></a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
			<ul id="" class="right hide-on-med-and-down">
				
				
				<li><a href="/startups">Startups</a></li>
				<li><a href="/events">Events</a></li>
				<li><a href="/service_providers">Service Providers</a></li>
				<li><a href="/investors">Investors</a></li>
				<li><a href="/contact-us">Contact Us</a></li>
                <li><a href="/faq">FAQ's</a></li>
				<?php if(_f_is_loggedin())
				{

					?>
					<li><a href="/inbox"><i class="material-icons tiny" id="mssg">chat</i></a></li>
					<li><a href="/notifications" style="display:inline-flex"><i class="material-icons tiny" id="notification_bell">notifications_none</i>
<div class="new-badge" style="position: relative;    left: -8px;    top: -16px;    padding: 2px 3px;    border-radius: 0;    height: 10px;    background: red;    color: black;    font-size: 13px;    font-weight: 500;"><?php $ncounts=_f_get_unread_notification_count(); if($ncounts) echo $ncounts?></div>
</a>
					</li>
		
				
				<li id="profile-picture" data-userid="<?php echo $me[0]['socialid']?>"
					style="display:inline-block"><a style="display:inline-block" class="dropdown-button" href="#!"
													data-beloworigin="true" data-activates="dropdownprof"><img
							class="dynamic-pp" id="header-pp" style="display:inline-block;float:left;margin:15px 10px"
						src="<?php echo $me[0]['profile_picture_url']?>" width="50px" alt=""
						class="circle"><i style="margin:0 !important;" class="material-icons right">arrow_drop_down</i></a>
				</li>
				<?php
				}
		        else
		        {
		        	?>
		          		<li><a href="/login">Login</a></li>
		        	<?php
		        }
		        ?>
      </ul>
			<ul id="mobile-demo" class="side-nav">
                <?php if(_f_is_loggedin()){?>
                <li>
                    <div class="userView" style="background-color:#009be5">
                        <a href="/dashboard/profile"><img class="circle" src="<?php echo $me[0]['profile_picture_url']?>"></a>
                        <span class="white-text name"><?php echo $me[0]['first_name']?></span>
                        <a class="customlink" href="/dashboard/profile"><span class="white-text email">Go to Profile</span></a>

                        <a class="customlink" href="/logout"><span class="white-text email">Logout</span></a>

                    </div>
                </li>
                    <?php
                }?>
                <li><a href="/home">Home</a></li>
				
				<li><a href="/startups">Startups</a></li>
				<li><a href="/events">Events</a></li>
				<li><a href="/service_providers">Service Providers</a></li>
				<li><a href="/investors">Investors</a></li>
				<li><a href="/contact-us">Contact Us</a></li>
                <li><a href="/faq">FAQ's</a></li>
				<?php if(_f_is_loggedin())
				{

					?>
					<li><a href="/inbox">Messages</a></li>
					<li><a href="/Dashboard">Dashboard</a>Dashboard</li>
					<?php
				} else {
				?>
				<li><a href="/login">Login</a></li>
				
				<?php
				}
				?>
			</ul>
		</div>

	</nav>
	</div>



