<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
    <div class="row center">
      <div class="col s12 m12">
        <h3 class="admin-title">All Messages</h3>
        <p>Latest First</p>
      </div>
       <div class="row">
      <h3><a href="/admin/messages/old" class="waves-effect waves-light btn btn1 right">Old Messages</a>
	  
	  <a href="/admin/messages/add" class="btn btn1 blue right">New Message</a></h3>
    </div>
    </div>
    <div class="row">
      <div class="col s12 m12 l12">
        <table id="messages">
          <thead>
            <tr>
              <td>From</td>
              <td>To</td>
              <td>Date</td>
              <td>Message</td>
              <td id="approve">Approve</td>
            </tr>
          </thead>
          <tbody>

        
          <?php
          foreach($all_messages as $message)
          {
            ?>
            <tr>
                <td><a href="/users/<?php echo $message['sender']?>"><?php echo _f_get_firstname_from_socialid($message['sender'])?></a><br><span class="user_type"><?php echo _f_get_user_type_from_socialid($message['sender'])?></span></td>
                <td><a href="/users/<?php echo $message['receiver']?>"><?php echo _f_get_firstname_from_socialid($message['receiver'])?></a><br><span class="user_type"><?php echo _f_get_user_type_from_socialid($message['receiver'])?></span></td>
<td><?php echo date('M dS, Y h:m:s a',strtotime($message['timestamp']))?></td>
<td><?php echo $message['message']?></td>
<td><a class="btn waves teal approvemessage" data-messageid=<?php echo $message['id'] ?> >Approve</a><a class="btn waves red rejectmessage" data-messageid=<?php echo $message['id'] ?> >Reject</a>
</td>
			</tr>
            <?php
          }
          ?>
			</tbody>
			</table>
      </div>
    </div>