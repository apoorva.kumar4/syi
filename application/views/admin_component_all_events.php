<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
<div class="row center">
      <div class="col s12 m12">
        <h3 class="admin-title"><?php echo $page_title?></h3>
        <p>Latest First</p>
          <a href="/events/create" class="waves-effect waves-light btn btn1 right">New Event</a>
          <!-- <a id="a-download-events" class="btn btn1 green right" href="#"><i class="material-icons left">file_download</i>Download Selected as CSV</a> -->

      </div>
    </div>
    <div class="row">
      <div class="col  s12 m12 l12">

        <ul class="collection collectionx">
            <form id="event_download" action="/admin/events/download" method="POST">
            <?php
              foreach($recent_events as $event)
              {
                ?>
                <li class="collection-item collection-itemx avatar">

                  <img src="<?php if($event['logo_upload_id']) echo $asset_path.$uploads[$event['logo_upload_id']]['url']?>" alt="" class="circle">
                  <span class="title text-lighten"><a href="/events/<?php echo $event['slug']?>"><?php echo $event['name']?></a></span>
                  <div class="text-lighten" style="text-align: right; display: inline-block; float: right; margin-right: 0px;">Event starts
                    <?php echo date("F j, Y", strtotime($event['date_start']))?><br><?php echo $event['views']. " Views"?>
                  </div>
                  <p class="subtext subtextx">By <a href="/user_info/u/<?php echo $event['ownerid']?>" class="blue-text subtext"><?php echo $users[$event['ownerid']]['first_name']?></a><br>
                    <?php echo $event['description_short'];?>

                </p>

                </li>

                <?php
              }
              ?>
            </form>
        </ul>
      </div>

      </div>
    </div>