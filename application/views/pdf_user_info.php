<div class="row">
  <div class="twelve columns" style="text-align: center;">
    <img src="assets/logo.png" width="300px">
  </div>
</div>
<div class="row" style="padding-top: 50px !important;" >
</div>
<div class="row" style="padding-top:100px !important;">
  <div class="two columns">
    <?php 

    $CI =& get_instance();
    $CI->load->model('user');
    $url= $CI->user->get_uploaded_profile_picture($userinfo['socialid']);
    ?>
    <img src="<?php echo $url?>" width="20%">
  </div>
  <div class="ten columns" style="padding-left:200px">
    <h5><?php echo $userinfo['first_name']." ".$userinfo['last_name']?></h5>
    <p style="padding:0px;margin:0px"> <?php echo $userinfo['location']?></p>
    <p><?php echo $userinfo['mobile']?></p>
  </div>
</div>
<div class="row" style="padding-top: 300px">
  <div class="twelve columns" style="float:right">
    <table class="u-full-width">

      <tbody>
      <tr>
        <td>User ID</td>
        <td><?php echo $userinfo['id']?></td>

      </tr>
      <tr>
        <td>Social ID</td>
        <td><?php echo $userinfo['socialid']?></td>

      </tr>

      <tr>
        <td>Email</td>
        <td><?php echo $userinfo['email']?></td>
      </tr>
      <tr>
        <td>Registered on</td>
        <td><?php echo $userinfo['timestamp']?></td>
      </tr>

      <tr>
        <td>Type</td>
        <td><?php echo ucfirst($userinfo['user_type'])?></td>
      </tr>
      <tr>
        <td>Registered through</td>
        <td><?php echo ucfirst($userinfo['access_type'])?></td>
      </tr>
      <tr>
        <td>Registeration Complete</td>
        <td><?php if(ucfirst($userinfo['registration_complete']=="Y")) echo "Yes"; else echo "No";?></td>
      </tr>
      <tr>
        <td>Role</td>
        <td><?php echo ucfirst($userinfo['role'])?></td>
      </tr>
      <tr>
        <td>Sector Expertise</td>
        <td><?php echo ucfirst($userinfo['sector_expertise'])?></td>
      </tr>
      </tbody>
    </table>




  </div>
</div>


