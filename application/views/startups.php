<div class="container">
	<div class="row">
		<div class="col s12 m12 l12">
			<h4 class="text-teal mtb30">All Startups <?php if(isset($filter_message)) echo "-".$filter_message?></h4>
			<p></p>
		</div>
	</div>
	<style>.filter .select-dropdown{margin:0 !important;}</style>
	<div class="row card-panel filter">
		<form method="GET" action="/startups/filter">
			<div class="row">
				<div class="col s12 m12 l5">

					<p>Filter by Phase</p>
					<select id="idea-stage" multiple name="idea-stage[]">
						
					<?php 
					$arr_phases=['All','Ideation','Prototype','Launching Soon','Operational','Expansion'];
					foreach($arr_phases as $phase){
						if(in_array($phase,$requested_phases)) {


						?>
							<option value="<?php echo $phase?>" selected><?php echo $phase?></option>
						<?php
						} else {
							?>
							<option value="<?php echo $phase?>"><?php echo $phase?></option>
							<?php
						} 	
					}

					?>
					</select>	
				</div>
				<div class="col s12 m12 l5">
					<p>Filter by Sector</p>
					
					<select multiple name='sector[]' id='sector'>
							
		
							<?php
							
							$mv_array = array(
								array("id"=>"All","name"=>"All")
							);

							$mv_array += $sectors;
							//var_dump($mvsectors);die(0);
							foreach($mv_array as $sector) {

								if(in_array($sector['id'],$requested_sectors)) {

								?>	
									<option value="<?php echo $sector['id']?>" selected><?php echo $sector['name']?></option> 
									<?php
									} else {
										?>
										<option value="<?php echo $sector['id']?>"><?php echo $sector['name']?></option>
										<?php
									}
								}
							?>
					</select>
				</div>
				<div class="col s12 m12 l2">
						<input type="submit" value="Filter" id="filter_submit" class="btn green" href="#" style="margin-top:62px">
				</div>
			</div>
		</form>
	</div>
		<script>
		jQuery(document).ready(function(){

			// $("#filter_submit").click(function(e){
			// 	e.preventDefault();
			// 	var tag="";
			// 	var res_sec="";
			// 	var total_l=$("#idea-stage option:selected").length;
			// 	var total_s=$("#sector option:selected").length;
			// 	$("#idea-stage option:selected").each(function(index){
			// 		if(index == total_l -1 )
			// 		tag += $(this).val();
			// 		else
			// 		tag += $(this).val() + "-";	
			// 	});
			// 	$("#sector option:selected").each(function(index){
			// 		if(index == total_s -1 )
			// 		res_sec += $(this).val();
			// 		else
			// 		res_sec += $(this).val() + "-";	
			// 	});
			// 	var sec=$("#sector option:selected").val();
			// 	window.location.replace("/startups/filter/phase/"+tag+"/sector/"+res_sec);
				
			// });
		});
		</script>
	<div class="row">
	<?php foreach($ideas as $idea)
		{
			$sp=_f_get_user_from_socialid($idea['ownerid']);
			?>
				<div class="col s12 m6 l6">
		          <div class="card white">
		            	<div class="card-content center-align min-height350">
		            		
                            <img class="dynamic-pp card" style="display:inline-block;margin-top:0px" src="<?php echo _f_get_idea_logo_url($idea['id'])?>" width="70px" alt=""><br>
		              		<span class="card-title"><a href="/ideas/<?php echo $idea['unique_url'] ?>"><?php echo $idea['name']?></a></span><br>
					  		<span class="card-idea-subtitle">In <?php $bullet=_f_get_idea_phase($idea['id']);echo $bullet['name']?> with <span id="placeholder_<?php echo $idea['id']?>_followers"> <?php echo _f_get_idea_followers($idea['id'])?> followers</span></span>
		              		
					  		<p class="left-align" style="padding: 20px"><?php echo substr($idea['description'],0,200)?>...</p>
		            	</div>
		            	<div class="card-action">
                            <?php
                                if(in_array($idea['id'],$followed_ideas))
                                {
                                    markup_unfollow_button($idea);

                                }
                                else
                                {
                                    markup_follow_button($idea);

                                }
                            ?>
		            	</div>
				   </div>
		        </div>
			<?php
		}
	?>
		        
	</div>
</div>
	
							        
