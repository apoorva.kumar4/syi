<script>
    jQuery(document).ready(function($) {
        $(".modal-add-bullet-close").click(function () {
            setTimeout(function () {
                $(".modal-add-idea").closeModal();
                $(".lean-overlay").remove();
            });
        });
    });
</script>


<div class="container">
<div class="col s12 card login-card" style="max-width:550px;margin:50px auto !important;">
	<div class="col s12 center">
		<h3>More Details</h3>
        <div class="center red-text">
            <?php echo validation_errors();?>
        </div>
	</div>

<?php echo form_open('ideas/edit/extra/'.$ideaid); ?>
<div class="row custom">
	
	<div class="input-field col s12">
		<!--<input type="text" name="idea-sector"  value="<?php echo set_value('sector')?set_value('sector'):$sector?>"> -->
        <?php _f_dynamic_select_sector("sector",$sector); ?>
		<label>Sector of the Idea (Like Agriculture, Pharmaceuticals, Technology, Banking, etc)</label>
	</div>
</div>
<div class="row">
	<div class="input-field col s12 m12">
		<input type="text" name="address"  value="<?php echo set_value('address')?set_value('address'):$address_of_management?>">
		<label>Full Address of the Team info
        <a href="#modal-add-add" class="modal-trigger" tabindex="-2">?</a></label>
        <div id="modal-add-add" class="modal modal-add-idea">
            <div class="modal-content">
                <h4>Full Address of the Company</h4>
                <p>Eg: Mumbai, India</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
            </div>
        </div>

    </div>
</div>
<div class="row">
	<div class="input-field col s12 m6">
        <select name="patented">
            <?php
            $arr_options = array('Yes','No');

            foreach($arr_options as $option)
            {
                if($option == $patented)
                {
                    ?>
                    <option value="<?php echo $option?>" selected><?php echo $option?></option>
                    <?php
                }
                else{
                    ?>
                    <option value="<?php echo $option?>"><?php echo $option;?></option>
                    <?php
                }
            }
            ?>
        </select>
		<label>Idea Patended?</label>
	</div>
	<div class="col s12 m6">
        <label>Date of Patent Received</label>
        <input name="patent_date" type="date" class="timeline-datepicker" placeholder="If patented, then date of patent received." value="<?php echo set_value('patent_date')?set_value('patent_date'):$patent_date?>">

	</div>
</div>
<div class="row">
	
	<div class="input-field col s12 m12">
		<input type="text" name="company"  value="<?php echo set_value('company')?set_value('company'):$company?>">
		<label>Company Name <a href="#modal-add-comp" class="modal-trigger" tabindex="-2">?</a></label>
        <div id="modal-add-comp" class="modal modal-add-idea">
            <div class="modal-content">
                <h4>Company Name</h4>
                <p>Your co. name - Apple, Microsoft, Google etc.</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class="input-field col s12 m12">
		<input type="text" name="website"  value="<?php echo set_value('website')?set_value('website'):$idea_website?>">
		<label>Website for more information about this idea <a href="#modal-add-web" class="modal-trigger" tabindex="-2">?</a></label>
        <div id="modal-add-web" class="modal modal-add-idea">
            <div class="modal-content">
                <h4>Website for more information about this idea</h4>
                <p>www.sayourideas.com</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
            </div>
        </div>
	</div>
</div>
<div class="row">
  <div class="input-field col s12">
          <textarea name="achievements" id="textarea1" class="materialize-textarea" value=""><?php echo set_value('achievements')?set_value('achievements'):$achievements?></textarea>
          <label for="textarea1">Tell us about your achievements with this Idea.</label>
        </div>
</div>
<div class="row">
  <div class="input-field col s12">
          <textarea name="business_plan" id="textarea1" class="materialize-textarea" value=""><?php echo set_value('business_plan')?set_value('business_plan'):$business_plan?></textarea>
          <label for="textarea1">Explain your Business Plan</label>
        </div>
</div>
<div class="row">
  <div class="input-field col s12">
          <textarea name="future_prospects" id="textarea1" class="materialize-textarea" value=""><?php echo set_value('future_prospects')?set_value('future_prospects'):$future_prospects?></textarea>
          <label for="textarea1">Future Objectives and Prospects</label>
        </div>
</div>
<div class="row">
  <div class="input-field col s12">
          <textarea name="proof_of_concept" id="textarea1" class="materialize-textarea" value=""><?php echo set_value('proof_of_concept')?set_value('proof_of_concept'):$proof_of_concept?></textarea>
          <label for="textarea1">Proof of concept</label>
        </div>
</div>
<div class="row">
  <div class="input-field col s12">
          <textarea name="revenue_model" id="textarea1" class="materialize-textarea" value=""><?php echo set_value('revenue_model')?set_value('revenue_model'):$revenue_model?></textarea>
          <label for="textarea1">Explain your revenue model in brief</label>
        </div>
</div>
<div class="row">
  <div class="input-field col s12">
          <textarea name="team_overview" id="textarea1" class="materialize-textarea" value=""><?php echo set_value('team_overview')?set_value('team_overview'):$team_overview?></textarea>
          <label for="textarea1">Give a team overview, information about your partners.</label>
  </div>
</div>
<div class="row">
  <div class="input-field col s12">
          <textarea name="executive_summary" id="textarea1" class="materialize-textarea" value=""><?php echo set_value('executive_summary')?set_value('executive_summary'):$executive_summary?></textarea>
          <label for="textarea1">Executive Summary</label>
  </div>
</div>
<div class="row">
	<div class="input-field col s12">
		<input type="text" name="facebook_link"  value="<?php echo set_value('facebook_link')?set_value('facebook_link'):$facebook_link?>">
		<label>Facebook Page for your idea</label>
	</div>
	<div class="input-field col s12">
		<input type="text" name="youtube_link"  value="<?php echo set_value('youtube_link')?set_value('youtube_link'):$youtube_link?>">
		<label>Youtube Page for your idea</label>
	</div>
	<div class="input-field col s12">
		<input type="text" name="twitter_link"  value="<?php echo set_value('twitter_link')?set_value('twitter_link'):$twitter_link?>">
		<label>Twitter Page for your idea</label>
	</div>
	<div class="input-field col s12">
		<input type="text" name="linkedin_link"  value="<?php echo set_value('linkedin_link')?set_value('linkedin_link'):$linkedin_link?>">
		<label>Linkedin Page for your idea</label>
	</div>
	<div class="input-field col s12">
		<input type="text" name="pinterest_link"  value="<?php echo set_value('pinterest_link')?set_value('pinterest_link'):$pinterest_link?>">
		<label>Pinterest Page for your idea</label>
	</div>
</div>
<input class="btn" type="submit" value="Done">
</form>
</div>
    <div class="row">
</div>