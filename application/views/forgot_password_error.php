<div class="container">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <h3 class="center color-yellow"><i class="material-icons large">error</i></h3>
            <h3 class="center">Oops!</h3>
            <p class="center">There was an error processing your request, please try again.</p>
            <br><br>
        </div>
    </div>
</div> <!-- End of Container -->