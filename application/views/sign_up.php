<style>
    main{
        background-color: #009be5;
    }
</style>
<script src="/js/selectize.min.js"></script>
<link rel="stylesheet" href="/css/selectize-custom.css">
<script>
    jQuery(document).ready(function($){

        $("#year_of_graduation").keypress(function (e) {
            if(e.which == 45 || e.which == 43)
            {

            }
            else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //if the letter is not digit then display error and don't type anything
                    return false;
                };
            }

        });

        $("#w_date").keypress(function (e) {
            if(e.which == 45 || e.which == 43)
            {

            }
            else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //if the letter is not digit then display error and don't type anything
                    return false;
                };
            }

        });

        $("#investment_range_start").keypress(function (e) {
            if(e.which == 45 || e.which == 43)
            {

            }
            else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //if the letter is not digit then display error and don't type anything
                    return false;
                };
            }

        });

        $("#investment_range_end").keypress(function (e) {
            if(e.which == 45 || e.which == 43)
            {

            }
            else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //if the letter is not digit then display error and don't type anything
                    return false;
                };
            }

        });

        $("#mob").keypress(function (e) {
            if(e.which == 45 || e.which == 43)
            {

            }
            else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //if the letter is not digit then display error and don't type anything
                    return false;
                };
            }

        });

        $('.chips-receivers').on('chip.delete', function(e, chip){
            $("#receivers").val(JSON.stringify($('.chips-receivers').material_chip('data')));
            console.log($("#service-tags").val());
        });

        $('.chips-receivers').on('chip.add', function(e, chip){
            $("#receivers").val(JSON.stringify($('.chips-receivers').material_chip('data')));
            console.log($("#service-tags").val());
        });


        $(".receiversx").selectize({
            delimiter:',',
            persist:false,
            create: function(input) {
                return {
                    value: input,
                    text: input,
                    label: input
                }
            },
            options: [
                <?php
                if(isset($suggestions)) {

                    foreach($suggestions as $suggestion)
                    {
                        $id=$suggestion['id'];
                        $value=$suggestion['value'];
                        echo "{id: $id ,value: '$value',label:'$value'},";
                    }

                }
                if(isset($active) && $active !== '')
                {
                    echo "{id: 1 ,value: '$active',label:'$active'},";
                }

                ?>

            ],
            valueField:'value',
            labelField:'value',
            searchField:'value',
            preload:true,
            openOnFocus: false,
            closeAfterSelect: true,
            addPrecedence: true,
        });

        $("#mob").keypress(function (e) {
            if(e.which == 45 || e.which == 43)
            {

            }
            else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //if the letter is not digit then display error and don't type anything
                    return false;
                };
            }

        });

        $('.select_city').selectize({
            maxOptions:4,
            openOnFocus:false,
            placeholder:"City",
            preload:false,

        });

    });

    $(function() {
        var tabindex = 1;
        $('input,select').each(function() {
            if (this.type != "hidden") {
                var $input = $(this);
                $input.attr("tabindex", tabindex);
                tabindex++;
            }
        });
    });
</script>

<div id="div_user_type_selector" class="row text-center" style="margin-top:40px">
    <style>
        body
        {
            overflow-y: scroll;
        }
        .login-card
        {
            transition: ease all 1s;
        }
    </style>
	<div class="div_sign_up_form">
        <div class="row card login-card" style="max-width:400px;margin:0px auto;">
            <div class="col s12 text-center">
                <h3 style="text-align:center">Sign Up</h3>
            </div>

            <div class="input-field col s12">
                <h5>Select your role</h5>
                <input class="sign_up-role" name="user_type" type="radio" value="ideator" id="ideator">
                <label for="ideator">Ideator</label><br>

                <input class="sign_up-role" name="user_type" type="radio" id="service_provider" value="service_provider">
                <label for="service_provider">Service Provider</label><br>

                <input class="sign_up-role" name="user_type" type="radio" value="investor" value="investor" id="investor">
                <label for="investor">Investor</label><br><br>
            </div>
            <style>
                .next-form{
                    display: none;
                }
            </style>
            <form id="signup_continue">
            <div class="next-form">
                <div class="col s12 text-center">
                    <div class="input-field col s12" style="padding:0;">
                        <input id="prep_email" type="email" required>
                        <label>Email</label>
                    </div>
                    <div class="input-field col s12" style="padding:0;">
                        <input id="prep_password" type="password" required>
                        <label>Password</label>
                    </div>
                    <input type="submit" class="submit-signin btn blue fwidth" value="Signup"><br>
                </div>
                <div class="col s12 text-center">
                    <p class="center">Or Sign Up using: </p>
                    <a id="social_linkedin" href="<?php echo base_url()?>social/linkedin" class="socialsign btn">LinkedIn</a>
                    <a id="social_facebook" href="<?php echo base_url()?>social/facebook" class="socialsign btn">Facebook</a>
                    <a id="social_google" href="<?php echo base_url()?>social/google" class="socialsign btn">Google</a><br>
                </div>
            </div>
            </form>
            <script>
                jQuery(document).ready(function($){
                    $(".sign_up-role").click(function(e){
                        if($(this).hasClass("initd")){

                        }
                        else
                        {
                            $(".next-form").slideDown(200);
                        }
                    });

                    $("#signup_continue").on('submit',function(e){
                        e.preventDefault();
                        var $user_type=$("input[name='user_type']:checked").val();
                        $(".duser_type").val($user_type);
                        if($user_type=="ideator")
                        {
                            var sing=$("#registration_ideator");

                            $(".demail").val($("#prep_email").val());
                            $(".dpassword").val($("#prep_password").val());

                            $("input[name='email']").val($("#prep_email").val());
                            $("input[name='password']").val($("#prep_password").val());

                            Materialize.updateTextFields();
                            sing.appendTo("#sign_up");
                            $("#div_user_type_selector").fadeOut(100,function(){
                                $("html,body").animate({
                                    scrollTop: 0
                                }, 200);
                                $(sing).fadeIn(100);
                                $("input").first().focus();

                            });
                        }
                        else if($user_type=="investor")
                        {
                            var sing=$("#registration_investor");

                            $(".demail").val($("#prep_email").val());
                            $(".dpassword").val($("#prep_password").val());
                            $("input[name='email']").val($("#prep_email").val());
                            $("input[name='password']").val($("#prep_password").val());

                            Materialize.updateTextFields();
                            sing.appendTo("#sign_up");
                            $("#div_user_type_selector").fadeOut(100,function(){
                                $("html,body").animate({
                                    scrollTop: 0
                                }, 200);
                                $(sing).fadeIn(100);
                                $("input").first().focus();

                            });
                        }
                        else if($user_type=="service_provider")
                        {
                            var sing=$("#registration_service_provider");

                            $(".demail").val($("#prep_email").val());
                            $(".dpassword").val($("#prep_password").val());
                            $("input[name='email']").val($("#prep_email").val());
                            $("input[name='password']").val($("#prep_password").val());

                            Materialize.updateTextFields();
                            sing.appendTo("#sign_up");
                            $("#div_user_type_selector").fadeOut(100,function(){
                                $("html,body").animate({
                                    scrollTop: 0
                                }, 200);
                                $(sing).fadeIn(100);
                                $("input").first().focus();

                            });
                        }
                    });

                });
            </script>
        </div>
	</div>

</div>
<form id="sign_up" method="POST" class="col s12"></form>
<div id="registration_ideator" class="container" style="margin-bottom: 70px;display:none">
    <div class="row card text-center login-card" style="max-width:550px;margin:0 auto !important;margin-bottom: 100px;">
        <div class="row text-center" >
            <h3 style="margin:20px 10px;text-align:center;">Fill the basic information</h3>

            <p class="materialize-red-text center">Note: Fields marked with * are required</p>
        </div>
        <div class="row" >
            <div class="input-field col s12 m6">

                <input id="name" name="first_name" type="text" required value="<?php echo $this->session->userdata('first_name')?>">
                <label for="name">First Name *</label>
            </div>

            <div class="input-field col s12 m6">

                <input id="lastname" name="last_name" type="text" required value="<?php echo $this->session->userdata('last_name')?>">
                <label>Last Name *</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12 m6">

                <input class="demail" disabled type="email" required value="<?php echo $this->session->userdata('email')?>"><br><br>
                <label>Email *</label>
            </div>

            <div class="input-field col s12 m6">

                <input name="mobile" type="tel" required value="<?php echo $this->session->userdata('mobile')?>" maxlength="16" pattern="^[\+0-9]*$" title="Please enter valid number" id="mob"><span id="errmsg"></span><br><br>
                <label>Mobile *</label>
            </div>
        </div>

        <div class="row">


            <div class="input-field col s12 m6">

                <input name="role" type="text" required value="<?php echo $this->session->userdata('role')?>"><br><br>
                <label>Role *</label>
            </div>
            <div class="input-field col s12 m6">
                <select name="student_alumni">
              <option value="student">Student</option>
              <option value="alumni">Alumni</option>
            </select>
                <label>Student or Alumni</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12 m6">

                <input name="institute" type="text" value="<?php echo $this->session->userdata('institute')?>"><br><br>
                <label>Institute</label>
            </div>

            <div class="input-field col s12 m6">

                <input name="year_of_graduation" type="text" maxlength="4" pattern="^[0-9]*$" id="year_of_graduation" value="<?php echo $this->session->userdata('year_of_graduation')?>"><br><br>
                <label>Year of Graduation</label>
            </div>
        </div>

        <div class="row">

            <div class="input-field col s12">

                <!--<input name="location" type="text" required value="<?php echo $this->session->userdata('location')?>"><br><br>-->

                <select tabindex="2" name="location" id="select-city" class="select_city demo-default browser-default" placeholder="Select a city...">
                    <option value=""></option>
                    <?php
                    $cities=_f_get_cities();
                    foreach($cities as $city) {
                        ?>
                        <option value="<?php echo $city['name']?>"><?php echo $city['name']?></option>
                        <?php
                    }?>
                </select>
                <!-- <label>Location *</label>-->
            </div>

            <div class="input-field col s12 m6">

                <!--<input name="sector_expertise" type="text" required value="<?php echo $this->session->userdata('sector_expertise')?>"><br><br>-->
                <?php _f_dynamic_select_sector('sector_expertise',$this->session->userdata('sector_expertise')) ?>
                <label>Sector Expertise *</label>
            </div>
            <div class="input-field col s12 m6">
                <input name="sector_other" type="text"><br><br>
                <label>If Others, please specify</label>
            </div>
        </div>
        <div class="row">

            <div class="input-field col s12">
                <input type="text" name="linkedin_username" required value="<?php echo $this->session->userdata('linkedin_username')?>"><br><br>
                <label>LinkedIn Username *</label>
            </div>

            <div class="input-field col s12">
                <input type="text" name="facebook_username" required value="<?php echo $this->session->userdata('facebook_username')?>"><br><br>
                <label>Facebook Username *</label>
            </div>
            <div class="input-field col s12">
                <input type="text" name="twitter_username" value="<?php echo $this->session->userdata('twitter_username')?>"><br><br>
                <label>Twitter Username</label>
            </div>

            <div class="input-field col s12">
                <input type="text" name="google_username" value="<?php echo $this->session->userdata('google_username')?>"><br><br>
                <label>Google+ Username</label>
            </div>

        </div>

        <input class="btn" type="Submit" value="Next"><br><br>
        <div style="display: none;">
            <input name="access_type" type="hidden" value="sayourideas"><br><br>
            <input name="email" type="hidden"><br><br>
            <input name="password" type="hidden" class="dpassword">
            <input name="user_type" type="hidden" class="duser_type">
        </div>


    </div>
</div>
<div id="registration_service_provider" style="display:none" class="container">
    <div class="row card text-center login-card" style="max-width:550px;margin:0 auto !important;">
        <div class="row text-center" >
                <h4 style="margin:20px 10px;text-align:center;">Fill the basic information</h4>
        </div>

        <div class="row col s12 m12">
                <h5>Company Logo</h5>
                <img id="logo-previewing" width="150px" height="150px"  style="float: left;" src="/assets/light-idea.png">
            <a href="#" class="btn" id="my-button" value="" style="float: left !important;margin:15px">Select Company Logo</a>
                <div class="file-field input-field center">
                    <input id="logofile" type="file" name="logofile" type="hidden">
                    <div class="file-path-wrapper file-path-wrapperx">
                        <input class="file-path validate" type="hidden">

                    </div>

                    <script>
                        $('#my-button').click(function(){
                            $('#logofile').click();
                        });
                    </script>

                    <style>
                        #logofile { visibility: hidden; }
                    </style>
                </div>

        </div>

        <div class="row rowx">
            <div class="input-field col s12 m6">

                <input id="name" name="first_name" type="text" required value="<?php echo $this->session->userdata('first_name')?>">
                <label for="name">First Name *</label>
            </div>

            <div class="input-field col s12 m6">

                <input id="lastname" name="last_name" type="text" required value="<?php echo $this->session->userdata('last_name')?>">
                <label>Last Name *</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12 m6">

                <input class="demail" type="email" required value="<?php echo $this->session->userdata('email')?>"><br><br>
                <label>Email *</label>
            </div>

            <div class="input-field col s12 m6">

                <input name="mobile" type="tel" required value="<?php echo $this->session->userdata('mobile')?>"pattern="^[\+0-9]*$" maxlength="16" title="Please enter valid number" id="mob"><span id="errmsg"></span><br><br>
                <label>Mobile *</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12 m6">

                <input name="industry" type="text" required value="<?php echo $this->session->userdata('industry')?>"><br><br>
                <label>Industry *</label>

            </div>

            <div class="input-field col s12 m6">

                <input class="wrk_year" name="working_since" type="text" maxlength="4" pattern="^[0-9]*$" id="w_date" required value="<?php echo $this->session->userdata('working_since')?>"><br><br>
                <label>Working Since year: eg 2010 *</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12 m6">

                <input name="role" type="text" required value="<?php echo $this->session->userdata('role')?>"><br><br>
                <label>Role *</label>
            </div>

            <div class="input-field col s12">

                <!-- <input name="location" type="text" required value="<?php echo $this->session->userdata('location')?>"><br><br> -->
                <select tabindex="2" name="location" id="select-city" class="select_city demo-default browser-default" placeholder="Select a city...">
                    <option value=""></option>
                    <?php
                    $cities=_f_get_cities();
                    foreach($cities as $city) {
                        ?>
                        <option value="<?php echo $city['name']?>"><?php echo $city['name']?></option>
                        <?php
                    }?>
                </select>
                <!--<label>Location</label> -->
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12 m6">

                <!-- <input name="sector_expertise" type="text" required value="<?php echo $this->session->userdata('sector_expertise')?>"><br><br> -->
                <?php _f_dynamic_select_sector('sector_expertise',$this->session->userdata('sector_expertise')) ?>
                <label>Sector Expertise *</label>

            </div>
            <div ccdlass="input-field col s12 m6">
                <input name="sector_other" type="text"><br><br>
                <label>If Others, please specify</label>
            </div>
            <div class="input-field col s12 m6">

                <input name="company" type="text" required value="<?php echo $this->session->userdata('company')?>"><br><br>
                <label>Company *</label>
            </div>
        </div>
        <div class="row">

            <div class="input-field col s12">
                <input type="text" name="linkedin_username" required value="<?php echo $this->session->userdata('linkedin_username')?>"><br><br>
                <label>LinkedIn Username or link *</label>
            </div>

            <div class="input-field col s12">
                <input type="text" name="facebook_username" required value="<?php echo $this->session->userdata('facebook_username')?>"><br><br>
                <label>Facebook Username *</label>
            </div>
            <div class="input-field col s12">
                <input type="text" name="twitter_username" value="<?php echo $this->session->userdata('twitter_username')?>"><br><br>
                <label>Twitter Username</label>
            </div>

            <div class="input-field col s12">
                <input type="text" name="google_username" value="<?php echo $this->session->userdata('google_username')?>"><br><br>
                <label>Google+ Username</label>
            </div>

            <div class="input-field col s12">
                <input type="text" name="youtube_link" value="<?php echo $this->session->userdata('youtube_link')?>"><br><br>
                <label>Vimeo or YouTube Link</label>
            </div>

        </div>
        <div style="display: none;">
            <input name="access_type" type="hidden" value="sayourideas"><br><br>
            <input name="email" type="hidden"><br><br>
            <input name="password" type="hidden" class="dpassword">
            <input name="user_type" type="hidden" class="duser_type">
        </div>
        <input class="btn" type="Submit" value="Next"><br><br>
    </div>
</div>
<div id="registration_investor" class="container" style="margin-bottom:70px;display:none">
    <div class="row card text-center login-card" style="max-width:550px;margin:0 auto !important;">
        <div class="row text-center" >
                <h4 style="margin:20px 10px;text-align:center;">Hi Investor, Fill the basic information:</h4>
                <p class="materialize-red-text center">Note: Fields marked with * are required</p>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="name" name="first_name" type="text" required value="">
                <label for="name">First Name *</label>
            </div>

            <div class="input-field col s12">
                <input id="lastname" name="last_name" type="text" required value="">
                <label for="last_name">Last Name *</label>
            </div>

            <div class="input-field col s12">
                <input name="email" type="email" required value="">
                <label for="email">Email *</label>
            </div>

            <div class="input-field col s12">
                <input id="mob" name="mobile" type="tel" required value="" pattern="^[\+0-9]*$" maxlength="16" title="Please enter valid number" id="mob"><span id="errmsg"></span>
                <label for="mobile">Mobile *</label>
            </div>

            <div class="input-field col s12">

                <input name="industry" type="text" required value="">
                <label for="industry">Industry *</label>

            </div>
            <div class="input-field col s12">

                <input class="since_year" name="working_since" type="text" maxlength="4" pattern="^[0-9]*$" id="w_date" required value="">
                <label>Working Since year: eg 2010 *</label>
            </div>

            <div class="input-field col s6">

                <input name="company" type="text" required value="">
                <label>Company *</label>
            </div>

            <div class="input-field col s6">

                <input name="role" type="text" required value="">

                <label>Designation *</label>
            </div>


            <div class="input-field col s12">
                <select tabindex="2" name="location" id="select-city" class="select_city demo-default browser-default" placeholder="Select a city...">
                    <option value=""></option>
                    <?php
                    $cities=_f_get_cities();
                    foreach($cities as $city) {
                        ?>
                        <option value="<?php echo $city['name']?>"><?php echo $city['name']?></option>
                        <?php
                    }?>
                </select>
            </div>

            <div class="input-field col s12 m6">
                <?php _f_dynamic_select_sector('sector_expertise',$this->session->userdata('sector_expertise')) ?>
                <label>Sector Expertise *</label>
            </div>

            <div class="input-field col s12 m6">
                <input type="text" name="sector_other">
                <label for="sector_other">If Others, please specify.</label>
            </div>

            <div class="input-field col s12 m2">
                <div class="select-wrapper"><span class="caret">▼</span>
                    <select name="currency" id="pref_currency" class="initialized" tabindex="12">
                        <option id="inr" value="INR">INR</option>
                        <option id="usd" value="USD">USD</option>
                    </select>
                    <label>Currency</label>
                </div>
            </div>

            <div class="input-field col s12 m5">

                <input name="investment_range_start" id="investment_range_start" maxlength="16" pattern="^[\+0-9]*$" id="investment_range_start" type="text" required value="<?php echo $this->session->userdata('investment_range_start')?>"><br><br>
                <label>Investment Start *</label>
            </div>
            <div class="input-field col s12 m5">

                <input name="investment_range_end" id="investment_range_end" maxlength="16" pattern="^[\+0-9]*$" type="text" required value="<?php echo $this->session->userdata('investment_range_end')?>"><br><br>
                <label>Investment Upto *</label>
            </div>
            <div class="input-field col s12">

                <input name="professional_background" type="text" required value="">

                <label>Professional Background *</label>
            </div>


            <div class="input-field col s12">

                <input name="alumni_network" type="text"  value="">

                <label>Alumni Network</label>
            </div>
            <div class="input-field col s12">

                <input name="investment_network" type="text"  value="">

                <label>Part of Investment Network</label>
            </div>
            <div class="input-field col s12">

                <input name="members_of_organisation" type="text"  value="">
                <label>Members of Organisation</label>
            </div>
            <div class="input-field col s12">

                <input name="sample_companies_invested" type="text"  value="">
                <label>Sample companies invested in:</label>
            </div>
            <div class="input-field col s12">
                <input type="text" name="linkedin_username" required value="">
                <label>LinkedIn Username / Link *</label>
            </div>

            <div class="input-field col s12">
                <input type="text" name="facebook_username" required value="">
                <label>Facebook Username / Link *</label>
            </div>
            <div class="input-field col s12">
                <input type="text" name="twitter_username" value="">
                <label>Twitter Username</label>
            </div>

            <div class="input-field col s12">
                <input type="text" name="google_username" value="">
                <label>Google+ Username</label>
            </div>

            <div class="input-field col s12">
                <input name="investor_tags" class="selectized receiversx" type="text" value="" style="transition: none !important" placeholder="Eg: VC, IOT, Big data, Angel investors ">
                <label for="receiversx">Tags</label>
            </div>

            <div class="input-field col s12">
                <input name="allow_direct_contact" type="checkbox" id="test5" value="1" />
                <label for="test5">Allow Startups to contact me directly</label>
            </div>

            <div class="input-field col s12">
                <input name="would_like_to_mentor_startups" onchange="valueChanged()" type="checkbox" id="coupon_question" value="1"/>
                <label for="coupon_question">I would like to mentor the start-ups</label>
            </div>

            <div class="row hidd-field col s12" id="hidd" style="display: none !important;">

                <script type="text/javascript">
                    function valueChanged()
                    {
                        if($('#coupon_question').is(":checked")){
                            var markup_html= "<div class='input-field col s12'> <label for='test511'>Currently mentoring any startups?</label><br><br><div class='row'> <div class='input-field col s4'> <input id='yes' name='currently_mentoring_any_startups' type='radio' value='1'> <label for='yes'>Yes</label> </div> <div class='input-field col s4'> <input id='No' name='currently_mentoring_any_startups' type='radio' value='0'/> <label for='No'>No</label> </div> </div> </div> <div class='input-field col s12'> <input type='text' name='companies_associated_with_as_a_mentor'><br><br> <label>Companies Associated with as a Mentor?</label> </div> <div class='input-field col s12'> <input type='text' name='areas_of_expertise_how_do_i_add_value'><br><br> <label>Areas of Expertise/How I add value </label> </div>";
                            $(".hidd-field").append(markup_html);
                            $(".hidd-field").slideDown();
                        }

                        else{
                            $("#hidd").slideUp(400,function () {
                                $("#hidd").empty();
                            });

                        }


                    }
                </script>


                <script>

                </script>
            </div>

            <div class="input-field col s12 bottom-fix-12">
                <p>Select your target sectors (Maximum: 10)</p>
                <?php _f_dynamic_checkbox_sectors('service-target-sectors');?>
            </div>

            <div class="input-field col s12" style="margin-top: 10% !important;">
                <input id="specify_other" name="specify_other" class="selectized receiversx" type="text" maxlength="10" placeholder="Maximum: 10" style="transition: none !important">
                <label for="specify_other">Specify if Others</label>
            </div>

            <div style="display: none;">
                <input name="access_type" type="hidden" value="sayourideas"><br><br>
                <input name="email" type="hidden"><br><br>
                <input name="password" type="hidden" class="dpassword">
                <input name="user_type" type="hidden" class="duser_type">
            </div>

            <div class="input-field col s12">
                <input class="btn" type="Submit" value="Next"><br><br>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($){
        $("#logofile").change(function() {

            var file = this.files[0];
            var coverfile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((coverfile==match[0]) || (coverfile==match[1]) || (coverfile==match[2])))
            {

                Materialize.toast("Invalid image",4000)
                return false;
            }
            else
            {

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);

            }
        });

        function imageIsLoaded(e) {
            $("#logofile").css("color","green");
            $('#logo-previewing').attr('src', e.target.result);
            $('#logo-previewing').attr('width', '150px');
            $('#logo-previewing').attr('height', '150px');
        };

        $("#sign_up").on('submit',function(e){
           e.preventDefault();
           Materialize.toast("<span class='xhr_pw'>Please Wait...</span>");

           $.ajax({
               type:"POST",
               url:"/api2/"+$(".duser_type").val()+"s",
               data:new FormData(this),
               contentType:false,
               processData:false,
               success: function(data)
               {
                   if(data.status=="success")
                   {

                       $(".xhr_pw").text("Sign Up Success");
                       $(".xhr_pw").parent().addClass("toastgreen");
                       location.reload();

                   }
                   else
                   {

                       $(".xhr_pw").text(data.reponseText.description);
                       $(".xhr_pw").parent().addClass("toastred");
                   }
               },
               error: function(xhr)
               {
                   var obj= JSON.parse(xhr.responseText);
                   $(".xhr_pw").text(obj.description);
                   $(".xhr_pw").parent().addClass("toastred");
               }
           });


        });

        $("#social_facebook").click(function(e)
        {
            e.preventDefault();
            var signup_window=window.open("/social/facebook","_black","height=600,width=1024,status=no,toolbar=no,menubar=no,location=no,addressbar=no,scrollbars=yes");


        });

        $("#social_google").click(function(e)
        {
            e.preventDefault();
            var signup_window=window.open("/social/google","_black","height=600,width=1024,status=no,toolbar=no,menubar=no,location=no,addressbar=no,scrollbars=yes");


        });

        $("#social_linkedin").click(function(e)
        {
            e.preventDefault();
            var signup_window=window.open("/social/linkedin","_black","height=600,width=1024,status=no,toolbar=no,menubar=no,location=no,addressbar=no,scrollbars=yes");


        });


    });
    function social_make(data)
    {
        // We have received the social data; plug it into the signup form
        jQuery(document).ready(function($){
            if(data.registered){
                location.reload();
            }
            $("#prep_email").val(data.email);
            $("input[name='access_type']").val(data.access_type);
            $("input[name='first_name']").val(data.first_name);
            $("input[name='last_name']").val(data.last_name);

            $("#signup_continue").submit();
        });

    }
</script>