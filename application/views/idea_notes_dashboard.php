<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    jQuery(document).ready(function($){
        var save_note = function()
        {
            var data = {'note_content':$(".myeditablediv").html()};
            var jqxhr= $.post('/api/v1/ideas/<?php echo $idea_id?>/notes/<?php echo $note[0]['id']?>/do', data);

            jqxhr.done(function(result){
                if(result)
                {

                    console.log(result);
                    var obj= JSON.parse(result);
                    if(obj.status==="success")
                    {
                        Materialize.toast('Saved!',4000);
                        //location.reload();
                    }
                    else if(obj.status==="refresh")
                    {
                        location.reload();
                    }
                    else
                        console.log(result);

                }
                else
                {
                    alert("Noresult");
                }


            });
        };
        tinymce.init({
            selector: '.myeditablediv',
            menubar: '',
            toolbar: 'save | bold | italic | underline | strikethrough | alignleft | aligncenter | alignright | alignjustify | styleselect | formatselect | cut | copy | paste | bullist | numlist | outdent | indent | undo | redo | removeformat',
            inline: true,
            plugins: 'save',
            save_onsavecallback: save_note
        });


    });

</script>
<style>
    .mce-edit-focus {
         outline: 0px dotted #333 !important;
    }
</style>
<div class="container">
<div class="row" style="">
    <div class="col s12 marginauto floatnone">
        <h3 class="">Notes for <?php echo $idea_name?><a href="/dashboard" class="linkfix right ">Dashboard</a></h3>
    </div>
</div>
<div class="row">
    <form class="note-save" method="POST">
	    <div data-note_id="<?php echo $note[0]['id']?>" class="myeditablediv card min-height350" style="padding: 20px"><?php echo html_entity_decode($note[0]['text'])?></div>
    </form>
</div>
</div>
