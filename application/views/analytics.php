<?php
	
	require_once 'vendor/autoload.php';
	
	$scopes=array('https://www.googleapis.com/auth/analytics.readonly','https://www.googleapis.com/auth/analytics');
	
	$client_email='sayourideas-a@sayourideasindia.iam.gserviceaccount.com';
	
	
	$client = new Google_Client();
	
	$private_key = file_get_contents('sayourideas-aa75df1d7b66.p12');
	
	//$client->setAuthConfigFile('sayourideas-c4789daa5b07.json');
	
	
	
	$credentials = new Google_Auth_AssertionCredentials(
	    $client_email,
	    $scopes,
	    $private_key
		);
	
	
	$client->setAssertionCredentials($credentials);
	
	if ($client->getAuth()->isAccessTokenExpired()) 
	{
	  $client->getAuth()->refreshTokenWithAssertion();
	}
	
	$access_token=$client->getAccessToken();
	
	$acc=json_decode($access_token,true);
	
	$accessme=$acc['access_token'];
	
	
	
	
?>
<!DOCTYPE html>
<html>
<head>
  <title>Embed API Demo</title>
</head>
<body>

<script>
(function(w,d,s,g,js,fs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
}(window,document,'script'));
</script>
<!-- Step 1: Create the containing elements. -->

<div id="chart-1-container"></div>
<div id="chart-2-container"></div>
<div id="view-selector-container" style="display:none"></div>
<div id="main-chart-container"></div>
<div id="breakdown-chart-container"></div>

<script>
gapi.analytics.ready(function() {

	gapi.analytics.auth.authorize({
	    'serverAuth': {
			'access_token': '<?php echo $accessme;?>'
	    }
	  });
	
  //var CLIENT_ID = '1051696419271-mapnkb8opr631koc79l5b30r9g479o75.apps.googleusercontent.com';

    var CLIENT_ID='1051696419271-1sg1leiv6te2od96rf6a6pguhs8tpeim.apps.googleusercontent.com';

  gapi.analytics.auth.authorize({
    container: 'auth-button',
    clientid: CLIENT_ID,
  });

  var viewSelector = new gapi.analytics.ViewSelector({
     container: 'view-selector-container'
   });

   // Render the view selector to the page.
   viewSelector.execute();
    
 
  var dataChart1 = new gapi.analytics.googleCharts.DataChart({
      query: {
        'ids': 'ga:138506858', // <-- Replace with the ids value for your view.
        'start-date': '7daysAgo',
        'end-date': 'today',
        'metrics': 'ga:sessions,ga:users',
        'dimensions': 'ga:date'
      },
      chart: {
        'container': 'chart-1-container',
        'type': 'LINE',
        'options': {
          'width': '50%'
        }
      }
    });
    dataChart1.execute();
	
	
	
	
	var dataChart2 = new gapi.analytics.googleCharts.DataChart({
	    query: {
	      'ids': 'ga:138506858', // <-- Replace with the ids value for your view.
	      'start-date': '7daysAgo',
	      'end-date': 'today',
	      'metrics': 'ga:pageviews',
	      'dimensions': 'ga:pagePathLevel1',
	      'sort': '-ga:pageviews',
	      'filters': 'ga:pagePathLevel1!=/',
	      'max-results': 7
	    },
	    chart: {
	      'container': 'chart-2-container',
	      'type': 'PIE',
	      'options': {
	        'width': '50%',
	        'pieHole': 4/9,
	      }
	    }
	  });
	  dataChart2.execute();
	  
  	var mainChart = new gapi.analytics.googleCharts.DataChart({
		query: {
			  'ids': 'ga:138506858',
		      'dimensions': 'ga:browser',
		      'metrics': 'ga:sessions',
		      'sort': '-ga:sessions',
		      'max-results': '6'
		    },
		    chart: {
		      type: 'TABLE',
		      container: 'main-chart-container',
		      options: {
		        width: '50%'
		      }
		    }
  	  });
  	  mainChart.execute();
	  
	  var breakdownChart = new gapi.analytics.googleCharts.DataChart({
	      query: {
	        'dimensions': 'ga:date',
	        'metrics': 'ga:sessions',
	        'start-date': '7daysAgo',
	        'end-date': 'yesterday',
	      },
	      chart: {
	        type: 'LINE',
	        container: 'breakdown-chart-container',
	        options: {
	          width: '50%'
	        }
	      }
	    });
		breakdownChart.execute();
		
		
		viewSelector.on('change', function(ids) {
		    var options = {query: {ids: ids}};

		    // Clean up any event listeners registered on the main chart before
		    // rendering a new one.
		    if (mainChartRowClickListener) {
		      google.visualization.events.removeListener(mainChartRowClickListener);
		    }

		    mainChart.set(options).execute();
		    breakdownChart.set(options);

		    // Only render the breakdown chart if a browser filter has been set.
		    if (breakdownChart.get().query.filters) breakdownChart.execute();
		  });
		  
		 var mainChartRowClickListener;
		 
	    mainChart.on('success', function(response) {

	       var chart = response.chart;
	       var dataTable = response.dataTable;

	       // Store a reference to this listener so it can be cleaned up later.
	       mainChartRowClickListener = google.visualization.events
	           .addListener(chart, 'select', function(event) {

	         // When you unselect a row, the "select" event still fires
	         // but the selection is empty. Ignore that case.
	         if (!chart.getSelection().length) return;

	         var row =  chart.getSelection()[0].row;
	         var browser =  dataTable.getValue(row, 0);
	         var options = {
	           query: {
	             filters: 'ga:browser==' + browser
	           },
	           chart: {
	             options: {
	               title: browser
	             }
	           }
	         };

	         breakdownChart.set(options).execute();
	       });
	     });
	  
	  
		  
});
</script>
</body>
</html>