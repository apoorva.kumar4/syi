 <div class="container">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <h3>Edit Event</h3>
            <div class="red-text">
                <?php echo validation_errors();?>
            </div>
            <br>
        </div>
        <form id="form_event_create" class="" action="" method="POST">
            <div class="input-field">
                <input type="hidden" name="id" value="<?php echo segx(3) ?>">
            </div>
        <div class="input-field col s12">
            <input type="text" name="name"  value="<?php echo set_value('event-name')?set_value('event-name'):$event['name']?>">
            <label>Name of the Event</label>
        </div>
        <div class="input-field col s12 m12">
            <input type="text" name="description_short"  value="<?php echo set_value('event-description_short')?set_value('event-description_short'):$event['description_short']?>">
            <label>Short Description</label>
        </div>

        <div class="col s12 m6">
            <input name="date_start" type="date" class="timeline-datepicker" placeholder="Event Start Date" value="<?php echo set_value('event-date_start')?set_value('event-date_start'):$event['date_start']?>">

        </div>

        <div class="col s12 m6">
            <input name="date_end" type="date" class="timeline-datepicker" placeholder="Event End Date" value="<?php echo set_value('event-date_end')?set_value('event-date_end'):$event['date_end']?>">

        </div>

        <div class="input-field col s12">
            <input type="text" name="slug"  value="<?php echo set_value('event-slug')?set_value('event-slug'):$event['slug']?>">
            <label>A unique slug which will be visible to public on https://sayourideas.com/events/slug</label>
        </div>

        <div class="input-field col s12">
            <textarea name="description_long" id="textarea1" class="materialize-textarea" value=""><?php echo set_value('event-description')?set_value('event-description'):$event['description_long']?></textarea>
            <label for="textarea1">Describe your event here, be as detailed as possible.</label>
        </div>

        <div class="col s12">
            <input class="btn" type="submit" value="Save Changes">
        </div>

        </form>
    </div>

     <script>
         jQuery(document).ready(function($){
             $("#form_event_create").on('submit',function(e){
                 e.preventDefault();
                 $.ajax({
                     url:"/api2/event/"+<?php echo segx(3)?>,
                     type:"PUT",
                     data: $(this).serialize(),
                     contentType: false,
                     cache: false,             // To unable request pages to be cached
                     processData:false,
                     success:function(data){
                         var obj=data;
                         if(obj.status==="success"){
                             Materialize.toast("Changes saved successfully",4000,'toastgreen');
                         }
                     },
                     error:function(xhr) {
                         Materialize.toast("Error:"+xhr.responseText.description);

                     }

                 });
             });
         });
     </script>



   
