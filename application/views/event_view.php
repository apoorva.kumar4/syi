<!-- PHOTOSWIPE -->
<!-- Core CSS file -->
<link rel="stylesheet" href="/css/photoswipe/photoswipe.css">

<!-- Skin CSS file (styling of UI - buttons, caption, etc.)
		 In the folder of skin CSS file there are also:
		 - .png and .svg icons sprite,
		 - preloader.gif (for browsers that do not support CSS animations) -->
<link rel="stylesheet" href="/css/photoswipe/default-skin.css">

<!-- Core JS file -->
<script src="/js/photoswipe/photoswipe.min.js"></script>

<!-- UI JS file -->
<script src="/js/photoswipe/photoswipe-ui-default.min.js"></script>
<script>
jQuery(document).ready(function($){
	$("#admin_approve_event").click(function(e){
    	event_id=e.target.dataset.eventId;
		$.ajax({
			url:'/api2/event/'+event_id+'/approve',
			type:'PATCH',
			success:function(response){
				$(e.target).fadeOut();
				Materialize.toast('Event Approved Successfully',4000);

			},
			error:function(response){
				
				Materialize.toast('There was an unkown error, please try again!',4000);
			}
		})
	});

	$("#admin_delete_event").click(function(e){
    	event_id=e.target.dataset.eventId;

		$.ajax({
			url:'/api2/event/'+event_id,
			type:'DELETE',
			success:function(response){
				Materialize.toast('Event Deleted Successfully');
			},
			error:function(response){
				
				Materialize.toast('There was an unkown error, please try again!',4000);
			}
		})
	});
				$("#eventcoverfile").change(function() {
								var file = this.files[0];
								var coverfile = file.type;
								var match= ["image/jpeg","image/png","image/jpg"];
								if(!((coverfile==match[0]) || (coverfile==match[1]) || (coverfile==match[2])))
								{

												Materialize.toast("Invalid image",4000)
																return false;
								}
								else
								{

												var reader = new FileReader();
												reader.onload = event_cover_loaded;
												reader.readAsDataURL(this.files[0]);

								}
				});

				function event_cover_loaded(e) {
								$("#eventcoverfile").css("color","green");
								$('#cover-previewing').attr('src', e.target.result);
								$('#cover-previewing').attr('width', 'fixed');
								$('#cover-previewing').attr('height', 'auto');
				};

				$("#eventlogofile").change(function() {

								var file = this.files[0];
								var coverfile = file.type;
								var match= ["image/jpeg","image/png","image/jpg"];
								if(!((coverfile==match[0]) || (coverfile==match[1]) || (coverfile==match[2])))
								{

												Materialize.toast("Invalid image",4000)
																return false;
								}
								else
								{

												var reader = new FileReader();
												reader.onload = event_logo_loaded;
												reader.readAsDataURL(this.files[0]);

								}
				});
				
				$("#input_gallery").change(function() {

								var file = this.files[0];
								var file_type = file.type;
								var match= ["image/jpeg","image/png","image/jpg"];
								if(!((file_type==match[0]) || (file_type==match[1]) || (file_type==match[2])))
								{

												Materialize.toast("Invalid image",4000)
																return false;
								}
								else
								{

												var reader = new FileReader();
												reader.onload = event_gallery_image_loaded;
												reader.readAsDataURL(this.files[0]);

								}
				});

				function event_logo_loaded(e) {
								$("#eventlogofile").css("color","green");
								$('#logo-previewing').attr('src', e.target.result);
								$('#logo-previewing').attr('width', '250px');
								$('#logo-previewing').attr('height', '230px');
				};
				
				function event_gallery_image_loaded(e) {
								$("#input_gallery").css("color","green");
								$('#gallery-previewing').attr('src', e.target.result);
								$('#gallery-previewing').attr('width', '250px');
								$('#gallery-previewing').attr('height', 'auto');
				};
				$("#uploadEventLogo").on('submit',(function(e) {
								e.preventDefault();
								var $event_id=$(this).attr('data-eventid');
								Materialize.toast("<span class='upload-status'>Uploading Event Logo...</span>",4000);
								var url_pur="/api2/event/"+$(this).attr('data-eventid')+"/logo";
								var randomId = new Date().getTime();
								var my_pare=$(this); // Saving the parent in a variable
								$.ajax({
								url: url_pur, // Url to which the request is send
												type: "POST",             // Type of request to be send, called as method
												data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
								contentType: false,       // The content type used when sending data to the server.
								cache: false,             // To unable request pages to be cached
								processData:false,        // To send DOMDocument or non processed data file it is set to false
								success: function(data)   // A function to be called if request succeeds
								{

												var obj=JSON.parse(data);
												if(obj.status=="success")
												{
																Materialize.toast("File Uploaded Successfully",4000);
																$(".dynamic-pp").attr('src','/api2/event/'+$event_id+'/logo?random='+randomId);
																$("#edit_logo").closeModal();
												}
												else{
																Materialize.toast("Upload Failed: "+ obj.description,4000);
												}
								}
								});
				}));

				$("#btn_event_upload_logo").click(function(e){
								e.preventDefault();
								$("#uploadEventLogo").submit();
				});

				$("#uploadEventCover").on('submit',(function(e) {
								e.preventDefault();
								var $event_id=$(this).attr('data-eventid');
								Materialize.toast("<span class='upload-status'>Uploading Event Cover...</span>",4000);
								var url_pur="/api2/event/"+$(this).attr('data-eventid')+"/cover";
								var my_pare=$(this); // Saving the parent in a variable
								var randomId = new Date().getTime();
								$.ajax({
								url: url_pur, // Url to which the request is send
												type: "POST",             // Type of request to be send, called as method
												data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
								contentType: false,       // The content type used when sending data to the server.
								cache: false,             // To unable request pages to be cached
								processData:false,        // To send DOMDocument or non processed data file it is set to false
								success: function(data)   // A function to be called if request succeeds
								{

												var obj=JSON.parse(data);
												if(obj.status=="success")
												{
																Materialize.toast("File Uploaded Successfully",4000);
																$(".view-event-cover_pic").css('background-image','url(/api2/event/'+$event_id+'/cover?random='+randomId);
																$("#edit_img").closeModal();
												}
												else{
																Materialize.toast("Upload Failed: "+ obj.description,4000);
												}
								}
								});
				}));

				$("#btn_event_upload_cover").click(function(e){
								e.preventDefault();
								$("#uploadEventCover").submit();
				});

				

				$("#form_uploadgallery").on('submit',function(e){
								e.preventDefault();
								var url="/api2/event/"+$(this).attr('data-eventid') + "/gallery";
								$.ajax({
								url:url,
												type:"POST",
												data: new FormData(this),
								contentType: false,       // The content type used when sending data to the server.
								cache: false,             // To unable request pages to be cached
								processData:false,
								success:function(data)
								{
												Materialize.toast("Images were uploaded successfully",4000);
												console.log(data)
								},
								error:function(xhr)
								{
												Materialize.toast("An error occured",3000);
								}
								});
				});

				$("#push_event").click(function () {
								$("#modal-push-event").openModal();
				});

});
</script>

<style>
		.view-event-eventname{
				font-weight: 300;
				color: white;
				min-height: 200px;
				padding: 0;
				display: table-cell;

		}

		@media screen and (max-width:425px) {
				.view-event-eventname {
						display:block;
						font-size:2.5rem;
						text-align: center;
				}
				.spencer {
						text-align: center;
						display:block;
				}
		}

		.view-event-cover_pic
		{
				background-image: url('<?php echo _f_get_event_cover_url($event['id'])?>');
				background-size: contain;
				background-repeat: no-repeat;
				background-position: 50%;
				min-height: 500px;
				background-color: #f6f9fc;
		}
		.event-description
		{
				font-size:1.3rem;
				font-weight:300;

		}
</style>

<div class="row view-event-cover_pic">
		<div class="col s12 center-align">


<?php
$CI =& get_instance();
$owner=0;
if($CI->session->socialid==$event['ownerid'])
{
				$owner=1;
?>


								<div id="edit_logo" class="modal edit_img" style="text-align: left !important;">
										<div class="modal-content">
												<div class="col s12">
														<form id="uploadEventLogo" action="/tempimage" data-eventid="<?php echo $event['id']?>">
																<h5>Logo</h5>
																<div>
																		<img id="logo-previewing" width="150px" height="150px"  src="<?php echo _f_get_event_logo_url($event['id'])?>">
																</div>
																<div class="file-field input-field">
																		<input id="eventlogofile" type="file" name="logofile">
																		<div class="file-path-wrapper file-path-wrapperx">
																				<input class="file-path validate" type="text" placeholder="Select an Image">
																		</div>
																</div>

																<div class="input-field">
																		<a id="btn_event_upload_logo" class="waves-effect waves btn">Save Logo</a>
																</div>
														</form>

												</div>
										</div>
										<div class="modal-footer">
												<a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close modal-close">Close</a>
										</div>
								</div>

<?php

}
else {

}
?>
<?php if(_f_is_admin()) {
?>	
								<a id="admin_delete_event" class="btn-large waves-effect white" style="color:teal; margin-left: 25px;width:200px;margin-bottom:10px" href="#" data-event-id="<?php echo $event['id']?>">Delete Event</a>

								<?php if($event['admin_approved']==0) {
									?>
									<a id="admin_approve_event" class="btn-large waves-effect white" style="color:teal; margin-left: 25px;width:200px;margin-bottom:10px" href="#" data-event-id="<?php echo $event['id']?>">Approve Event</a>	
									<?php
								}
								?>
								<a class="btn-large waves-effect white" style="color:teal; margin-left: 25px;width:200px;margin-bottom:10px" href="/messages/new/<?php echo $event['ownerid']?>">Send message</a>
								
								<a href="#modal-push-event" class="btn-large waves-effect white" style="color:teal; margin-left: 25px;width:200px;margin-bottom:10px" id="push_event">Push Events</a>
<?php
}?>

<?php
				if($CI->session->socialid==$event['ownerid'] || _f_is_admin())
				{
?>
						<a class="btn-large waves-effect white upload_event_gallery modal-trigger" style="color:teal; margin-left: 25px;width:200px;margin-bottom:10px" href="#upload_gallery" data-event_id="<?php echo $event['id']?>">Upload images for gallery</a>
						<div id="upload_gallery" class="modal edit_img" style="text-align: left !important;">
								<div class="modal-content">
										<div class="col s12">
												<form id="form_uploadgallery" action="/tempimage" data-eventid="<?php echo $event['id']?>">
														<h5>Add Image to Gallery</h5>
														<div>
																<img id="gallery-previewing" height="150px"  src="">
														</div>
														<div class="file-field input-field">
																<input id="input_gallery" type="file" name="images[]" multiple>
																<div class="file-path-wrapper file-path-wrapperx">
																		<input class="file-path validate" type="text" placeholder="Select an Image">
																</div>
														</div>

														<div class="input-field">
																<button type="submit" class="waves-effect waves btn">Upload</button>
														</div>
												</form>

										</div>
								</div>
								<div class="modal-footer">
										<a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close modal-close">Close</a>
								</div>
						</div>
<?php
				}
?>
				</div>

						<div class="row right">


<?php
if($owner)
{
?>
												<a href="#edit_img"  id="edit_cover_pic" title="Edit cover pic" class="waves-effect white btn modal-trigger bk-bt" style="left:92%;"><i class="material-icons">mode_edit</i></a>
<?php
}
else
{

}

?>

								<div id="edit_img" class="modal edit_img">
										<div class="modal-content">
												<div class="col s12">
														<h5>Cover</h5>
														<form id="uploadEventCover" action="/tempimage" data-eventid="<?php echo $event['id']?>">
																<div style="width:450px !important;">
																		<img id="cover-previewing" class="responsive" style="width=450px !important;" src="<?php echo _f_get_event_cover_url($event['id'])?>">
																</div>
																<div class="file-field input-field">
																		<input id="eventcoverfile" type="file" name="coverfile">
																		<div class="file-path-wrapper file-path-wrapperx">
																				<input class="file-path validate" type="text" placeholder="Select an Image">

																		</div>

																</div>
																<div class="input-field">
																		<a id="btn_event_upload_cover" class="waves-effect waves btn">Save Cover</a>
																</div>

														</form>
												</div>
										</div>
										<div class="modal-footer">
												<a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close modal-close">Close</a>
										</div>
								</div>
						</div>
</div>
</div>
<div id="modal-push-event" class="modal">
		<div class="row">
				<div class="col s12">

						<form id="admin_send_new_event" method="post">
								<div class="col s12 input-field">
										<h4 style="">Events Push To</h4>
										<div class="row">
												<div class="input-field col s12 l4">
														<input name="ideator" type="checkbox" id="ideator" value="1" />
														<label for="ideator">Ideator</label>
												</div>
												<div class="input-field col s12 l4">
														<input name="service_provider" type="checkbox" id="service_provider" value="1" />
														<label for="service_provider">Service Provider</label>
												</div>
												<div class="input-field col s12 l4">
														<input name="investor" type="checkbox" id="investor" value="1" />
														<label for="investor">Investor</label>
												</div>
										</div>
								</div>
								<div class="col s12 input-field" style="margin-top: 30px !important;">
										<textarea name="message" id="textarea1" class="materialize-textarea" required></textarea>
										<label for="message-body">Description about event</label>
								</div>
								<input class="btn right" type="submit" value="Send">
						</form>
				</div>
		</div>
</div>
<style>
.spencer {vertical-align: middle;padding: 20px}

</style>
<div style="margin:0 50px">
		<div class="row">
		<div class="col s12">

		<h1 class="view-event-eventname">
<?php
if($CI->session->socialid==$event['ownerid']) {
				$owner=1;
?>
								<a href="#edit_logo"  id="edit_logo_pic" class="modal-trigger">
								<img height="150px" width="150px" class="dynamic-pp card" style="vertical-align:middle;display:inline-block;margin-top:20px" src="<?php echo _f_get_event_logo_url($event['id'])?>" alt="">
								</a>
<?php
} else {
?>

								<img height="150px" width="150px" class="dynamic-pp card" style="vertical-align:middle;display:inline-block;margin-top:20px" src="<?php echo _f_get_event_logo_url($event['id'])?>" alt="">
<?php
}
?>
		<span class="spencer">
		<?php echo $event['name']?></span></h1>
		</div>
		</div>

<div class="row">
		<div class="col s12 m6 l3">
				<h4>Start Date</h4>
				<p class="event-description"><?php echo date("F jS, Y", strtotime($event['date_start']));?></p>
		</div>
		<div class="col s12 m6 l3">
				<h4>End Date</h4>
				<p class="event-description"><?php echo date("F jS, Y", strtotime($event['date_end']));?></p>
		</div>
		<div class="col s12 m6 l3">
				<h4>Tagline</h4>
				<p class="event-description"><?php echo $event['description_short']?></p>
		</div>
		<div class="col s12 m6 l3">
				<h4>Gallery</h4>
				<a class="event-description view_event_gallery" href="#" data-event_id="<?php echo $event['id']?>">View Gallery</a>
		</div>
		<div class="col s12 m6 l6">
				<h4>Description</h4>
				<p class="event-description"><?php echo $event['description_long']?></p>
		</div>


</div>
</div>
<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

		<!-- Background of PhotoSwipe.
				 It's a separate element as animating opacity is faster than rgba(). -->
		<div class="pswp__bg"></div>

		<!-- Slides wrapper with overflow:hidden. -->
		<div class="pswp__scroll-wrap">

				<!-- Container that holds slides.
						PhotoSwipe keeps only 3 of them in the DOM to save memory.
						Don't modify these 3 pswp__item elements, data is added later on. -->
				<div class="pswp__container">
						<div class="pswp__item"></div>
						<div class="pswp__item"></div>
						<div class="pswp__item"></div>
				</div>

				<!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
				<div class="pswp__ui pswp__ui--hidden">

						<div class="pswp__top-bar">

								<!--  Controls are self-explanatory. Order can be changed. -->

								<div class="pswp__counter"></div>

								<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

								<button class="pswp__button pswp__button--share" title="Share"></button>

								<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

								<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

								<!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
								<!-- element will get class pswp__preloader--active when preloader is running -->
								<div class="pswp__preloader">
										<div class="pswp__preloader__icn">
												<div class="pswp__preloader__cut">
														<div class="pswp__preloader__donut"></div>
												</div>
										</div>
								</div>
						</div>

						<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
								<div class="pswp__share-tooltip"></div>
						</div>

						<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
						</button>

						<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
						</button>

						<div class="pswp__caption">
								<div class="pswp__caption__center"></div>
						</div>

				</div>

		</div>

</div>

<style>
</style>
