<div class="container">
<div class="row after-fide">
	<div class="col s12">
	<?php
	$fname=_f_get_firstname_from_socialid(segx(3));
	?>
		<h3>Submit your review for <?php echo $fname?> (<a href="/users/<?php echo segx(3)?>"><?php echo _get_username_from_socialid(segx(3))?></a>)</h3>
	</div>
</div>
<script src="/js/selectize.min.js"></script>
<link rel="stylesheet" href="/css/selectize-custom.css">
<script>

jQuery(document).ready(function(){
        
	$("#submitreview").on('submit',(function(e) {
        e.preventDefault();
        Materialize.toast("<span class='upload-status'>Submitting your review...</span>",4000);
        var url_pur="/api/v1/users/"+$("#profile-picture").attr('data-userid')+"/reviews/add/<?php echo segx(3)?>";
        console.log($("#example").val());
        $.ajax({
            url: url_pur, // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {

                var obj=JSON.parse(data);
                if(obj.status=="success")
                {
                    
                    Materialize.toast("Review Submitted Successfully",4000,'toastgreen');
                    $(".after-fide").fadeOut(300,function(){
                    	debugger;
                    	$(".after-side").fadeIn();
                    });

                    
                }
                else{
                	console.log(data);
                    Materialize.toast("Review Submission Failed: "+ obj.description,4000);
                }
            }
        });
    }));
});
        
            
            


        
</script>
<link rel="stylesheet" href="/css/fontawesome-stars-o.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<script src="/js/jquery.barrating.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
      $('#example').barrating({
        theme: 'fontawesome-stars-o',
        showValues: false,
        showSelectedRating: true
      });
   });
</script>
<form id="submitreview" class="after-fide">
<div class="row after-fide">
	
    <div class="col s12 input-field">
        <h4>Rating</h4>
        <select id="example" name="rating" class="browser-default">
		  <option value="1">1</option>
		  <option value="2">2</option>
		  <option value="3">3</option>
		  <option value="4">4</option>
		  <option value="5">5</option>
		</select>
    </div>
	<br><br>
</div>
<div class="row after-fide">
	<div class="col s12 input-field">
    	<textarea name="review" id="textarea1" class="materialize-textarea" required></textarea>
    	<label for="message-body">Write your review here...</label>
	    	
	</div>
	<div class="col s12 input-field">
		<input class="btn" type="submit" value="Submit">
	</div>
</div>
</form>
<style>
.after-side
{
	display: none;
}
</style>
<div class="row after-side" style="">
	<div class="col s12">
	<h2>Thank you!</h2>
		<h3>Your review has been submitted successfully!</h3>
	</div>
</div>