<admin-profile inline-template :users="users" :uploads="uploads" :socialid="my_socialid" v-on:profile_picture_uploaded="profilePictureUploaded" v-on:profile_updated="profile_updated" :first-name="users[my_socialid].first_name" :last-name="users[my_socialid].last_name" :username="users[my_socialid].username">
    <div class="row">
        <div id="change-profile-modal" class="modal bottom-sheet modal95">
            <form id="uploadImage" action="/tempimage" v-on:submit="upload_logo">
                <div class="modal-content">

                    <div class="row">
                        <div class="col s12 m12 l12 ">
                            <h4>Set a new Profile Picture</h4>
                            <p>Max File upload size is 2 Mb. Recommended Resolution 250x250px.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <img id="previewing" width="250px" height="250px" v-bind:src="new_profile_picture_url">

                        </div>
                        <div class="col s12">

                            
                                <div class="file-field input-field">


                                    <input @change="filechanged" id="file" type="file" name="file">

                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Select File">
                                    </div>
                                </div>
                            

                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" id="update-image-submit" class="modal-action waves-effect waves btn green">Upload</button>    
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>


                </div>
            </form>
        </div>
        <div class="col s12 center">
            <h3>Your Profile</h3>
        </div>
        <div class="col s12 m4 center">
			<img v-if="users[socialid].profile_picture_upload_id" class="dynamic-pp card"  style="" v-bind:src="uploads[users[socialid].profile_picture_upload_id].complete_url" width="200px" alt="">
			<img v-else class="dynamic-pp card"  style="" v-bind:src="users[socialid].gravatar_url" width="200px" alt="">
			<p><a class="waves-effect white" style="" v-on:click="open_profile_picture_modal" href="#">Change Profile Picture</a></p>
        </div>

        <div class="col s12 m8">
            
                <h4 style="">Basic Details</h4>
                <div class="col s12">
                    <div class="input-field">
						<p for="first_name">First Name</p>
						<input id="first_name" name="first_name" type="text" v-model="firstName" autocomplete="off">
                        
                    </div>
                    <div class="input-field">
						<p for="last_name">Last Name</p>
						<input id="last_name" name="last_name" type="text" v-model="lastName" autocomplete="off">
                        
                    </div>
                    <div class="input-field">
						<p for="user_name">Username</p>
						<input id="username" name="username" type="text" v-model="username" autocomplete="off">
                        
                    </div>
                    <button v-on:click="update_profile" class="submit-signin btn blue">Save Profile</button>
                </div>
                <h4 style="">Change Password</h4>
                <div class="col s12">

                    <div class="input-field">
						<p for="current_password">Current password</p>
						<input  v-model="current_password" id="curr_password" type="password" required>
                        
                    </div>
                    <div class="input-field">
						<p for="new_password">New password</p>
						<input  v-model="new_password" id="new_password" type="password" required>
                        
                    </div>
                    <div class="input-field" >
						<p for="repeat_password">Repeat password</p>
						<input  v-model="repeat_password" id="repeat_password" type="password" required>
                        
                    </div>
                    <button v-on:click="change_password" class="submit-signin btn blue" id="submit_password">Change Password</button>
                </div>
            
        </div>
    </div>
</admin-profile>