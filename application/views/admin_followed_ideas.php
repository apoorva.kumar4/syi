<?php $this->load->view('templates/admin_head');?>
<style>
        .masonry {
            margin: 1.5em 0;
    padding: 0;
    -moz-column-gap: 0em;
    -webkit-column-gap: 0em;
    column-gap: 0em;
    font-size: .85em;
        }
        .arcsec {
        display: inline-block;
        padding: 1em;
        margin: 0;
        width: 100%;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        }

        .arcsec .card {
            margin:0;
        }

        @media only screen and (min-width: 400px) {
    .masonry {
        -moz-column-count: 1;
        -webkit-column-count: 1;
        column-count: 1;
    }
}

@media only screen and (min-width: 700px) {
    .masonry {
        -moz-column-count: 1;
        -webkit-column-count: 1;
        column-count: 1;
    }
}

@media only screen and (min-width: 900px) {
    .masonry {
        -moz-column-count: 3;
        -webkit-column-count: 3;
        column-count: 3;
    }
}
</style>
<div id="admin_app">
    <div class="app-wrapper">
        <div id="admin-nav" class="nav-wrapper">
            <a id="admin-menu-icon" href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
        <div class="row fwidth">
            <div class="col s12 m4 l2">
                <?php require('admin_component_nav_bar.php');?>
            </div>
            <div class="col s12 m12 l10 right">
                <?php require('admin_component_followed_ideas.php');?>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin_footer');?>