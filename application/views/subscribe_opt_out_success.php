<style type="text/css">
	@media screen and (min-width: 768px) {
		#Subscribe {
			padding:50px 100px;
		}	
	}
	@media screen and (min-width: 1024px) {
		#Subscribe {
			padding:50px 350px;
		}	
	}

		
	
</style>
<section class="section">
	<div id="Subscribe" class="wrapper">
		<div class="row">
			<div class="col s12 card" style="padding:20px">
				<h4>We are sorry to see you go!</h4>
				<p>In case you change your mind, please click <a href="/Subscribe">here</a> to opt-in again.</p>
				
			</div>
		</div>
	</div>
</section>