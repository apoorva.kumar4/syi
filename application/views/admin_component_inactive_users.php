<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
   
        <div class="row center">
            <div class="col s12 m12">
                <h3 class="admin-title"><?php echo $title?></h3>
				<p>Users who have not performed any activity since last 3 months</p>
            </div>
        </div>
		<div class="row">
			<div class="input-field col s12 m6 right">
					<a v-on:click="download_inactive_users" class="btn green right" href="#" style="margin:0 10px"> <i class="material-icons left">file_download</i>Download as CSV</a>
                </div>
		</div>
        
        <div class="col  s12 m12 l12">
            <!--      <p id="filter-dewscription" class="description">Fetched --><?php //echo count($recent_registrations) ?><!-- records: From --><?php //echo date('m/d/Y',strtotime($recent_registrations[0]['timestamp']))?><!-- to --><?php //echo date('m/d/Y',strtotime(end($recent_registrations)['timestamp']))?><!--</p>-->
            <ul class="collection collectionx">
                
                    <?php
                    if(!count($inactive_users)==0) {


                        foreach ($inactive_users as $user) {
                            ?>
                            <li class="collection-item collection-itemx avatar">
                                <img
                                    src="<?php echo $user['gravatar_url'] ?>"
                                    alt="" class="circle">
                                <span class="title text-lighten"><a href="/user_info/u/<?php echo $user['socialid'] ?>"
                                                                    
                                                                    class="text-teal"><?php echo $user['first_name'] ?> <?php echo $user['last_name'] ?></a></span>
                                <div class="text-lighten"
                                     style="text-align: right;
                display: inline-block;
                float: right;
                margin-right: 0px;">
                                    <?php
                                    if ($user['user_type'] == "investor")
                                        echo "Investor";
                                    else if ($user['user_type'] == "service_provider")
                                        echo "Service Provider";
                                    else if ($user['user_type'] == "ideator")
                                        echo "Ideator";

                                    ?> <br><?php echo date("F jS, Y g:i A", $user['last_active']) ?>
                                </div>
                                <p class="subtext subtextx">
                                    <a href="mailto:<?php echo $user['email']?>" target="_blank"><?php echo $user['email']?></a><br>
                                    <?php
                                    if($user['facebook_username']!="")
                                    {
                                        ?>
                                        <a target=_blank href="<?php echo socialify_link($user['facebook_username'],"facebook")?>"><i class="fa fa-facebook-official"></i></a>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if($user['twitter_username']!="")
                                    {
                                        ?>
                                        <a target=_blank href="<?php echo socialify_link($user['twitter_username'],"twitter")?>"><i class="fa fa-twitter"></i></a>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if($user['linkedin_username']!="")
                                    {
                                        ?>
                                        <a target=_blank href="<?php echo socialify_link($user['linkedin_username'],"linkedin")?>"><i class="fa fa-linkedin-square"></i></a>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if($user['google_username']!="")
                                    {
                                        ?>
                                        <a target=_blank href="<?php echo socialify_link($user['google_username'],"google")?>"><i class="fa fa-google-plus-official"></i></a>
                                        <?php
                                    }
                                    ?>

                                    <?php
                                    if($user['mobile']!="")
                                    {
                                        ?>
                                        <a target=_blank href="tel:<?php echo $user['mobile']?>"><i class="fa fa-phone"></i></a>
                                        <?php
                                    }
                                    ?>

                                </p>
                            </li>
                            <?php
                        }
                    }
                    ?>
            </ul>
        </div>
<div class="hidden-metadata" style="display:none">
	<table id="table_inactive_users">
	<thead>
		<tr>
			<td>User ID</td>
			<td>Name</td>
			<td>User Type</td>
			<td>Registration Date</td>
            <td>Facebook Url</td>
            <td>Linkedin Url</td>
            <td>Twitter Url</td>
            <td>Google Url</td>
            <td>Mobile</td>
		</tr>
	</thead>
	<tbody>
		<?php
                        foreach($inactive_users as $user)
                        {
							echo "<tr>";
								
								echo "<td>".$user['socialid']."</td>";
								echo "<td>".$user['first_name']." ".$user['last_name']."</td>";
								
								echo "<td>".$user['user_type']."</td>";
								echo "<td>".date("F jS, Y g:i A", strtotime($user['timestamp']))."</td>";
                                echo "<td>".$user['facebook_username']."</td>";
                                echo "<td>".$user['linkedin_username']."</td>";
                                echo "<td>".$user['twitter_username']."</td>";
                                echo "<td>".$user['google_username']."</td>";
                                echo "<td>".$user['mobile']."</td>";
							echo '</tr>';
                            
                        }
                        ?>
	</tbody>
	</table>
</div>