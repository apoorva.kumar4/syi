<div class="row margin50">
    <div class="col s12 marginauto floatnone">
		<h3>Notifications</h3>
	</div>

	<div class="col s12 floatnone marginauto" style="">

		<style>
		.btn-inbox-message-action {
    		margin: 10px 25px;
		}
		</style>

<ul class="collapsible" data-collapsible="expandable">
	<?php
	foreach($notifications as $notification)
	{
		$human_date=date("F jS, Y",strtotime($notification['datetime']));

		?>
		<li>
			
      		<div class="collapsible-header"><i class="material-icons">chat</i><?php echo $notification['name']?><span class="hide-on-small-only	"style="float:right"><?php echo $human_date?></span> </div>
      		<div class="collapsible-body"><?php echo $notification['description']?>
      			<div class="btn-inbox-message-action">
      			</div>
      			
      		</div>

    	</li>
		<?php

	}
	?>
</ul>
</div>
</div>
