<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
<div class="row">
	<h3 class="center">Bulk Email</h3>
	<form id="admin_send_bulk_email" method="post">

		<div class="col s12">
			<div class="input-field inline">
	          	<!-- <p>From:</p> -->
	            <input id="email_inline" name="from" type="text" class="validate">
	            <label for="email_inline">From:</label>
	            
	          </div>
	          @sayourideas.com
		</div>

		<div class="col s12">
				<p>Select Lists</p>
				<p>
				<label>
					<input type="checkbox" class="filled-in" name="list_ideators" />
					<span v-text="'Ideators ('+ num_ideators+')'"></span>
				</label>
				</p>
				<p>
				<label>
					<input type="checkbox" class="filled-in" name="list_service_providers" />
					<span v-text="'Service Providers ('+ num_service_providers+')'"></span>
				</label>
				</p>
				<p>
				<label>
					<input type="checkbox" class="filled-in" name="list_investors" />
					<span v-text="'Investors ('+ num_investors+')'"></span>
				</label>
				</p>
				<p>
				<label>
					<input type="checkbox" class="filled-in" name="list_inactive_users" />
					<span v-text="'Inactive Users ('+ inactive_users_socialids.length+')'"></span>
				</label>
				</p>
				<!-- <select multiple name="lists[]" class="browser-default">
					<option value="ideators">Ideators</option>
					<option value="service_providers">Service Providers</option>
					<option value="investors">Investors</option>
					<option value="inactive_users">Inactive Users</option>

				</select> -->
				
		</div>

		<div class="col s12" style="margin-top: 30px !important;">
			<p>Subject</p>
			<input type="text" name="subject" id="subject" class="" required>
			
		</div>

		<div class="col s12 input-field" style="">
			<p>Enter your message</p>
			<textarea name="message" id="textarea1" class="materialize-textarea" required></textarea>
			
		</div>
		
		<button class="btn" type="submit" id="email_send">Send</button>
		
	</form>
</div>