<link rel="stylesheet" href="/css/hopscotch.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/fontawesome-stars-o.css">
<div id="app_dashboard_sp">
	<div class="app-wrapper">
		<div class="row margin50">
			<div class="col s12">
				<h3>Your Rating<a href="/reviews" class="waves-effect waves-light btn btn1 right">See All Reviews</a><a id="take_tour" class="waves-effect waves-light right hide-on-small-only" href="#">Take a tour</a></h3>
				<h4 class=""  id="rev"><?php $rating=_get_rating($this->session->socialid); $rat= round($rating[0]['avg(rating)'],2); echo $rat;?> of <?php $ratc=$rating[0]['count']; echo $ratc;?> reviews</h4>

				<select id="example" name="rating" class="browser-default">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>
			</div>
		</div>
		<div class="row margin50">
			<div class="col s12">
			<h3>Your Services<a href="<?php echo base_url()?>services/add" class="waves-effect waves-light btn btn1 right" id="add_serv">Add New Service</a></h3>
			</div>
		</div>
		<div class="row margin50">
			<?php
			if(!$services)
			{
			?>
					<div class="col s12">
					<h5>You have not added any services, click <a href="<?php echo base_url()?>services/add">Add</a></h5>
					</div>
			<?php
			}
			else
			{?>
			<?php
			$counter=0;
			foreach($services as $service)
			{


			?>
					<div class="col s12 m6 l4">
						<div class="card">
						<div class="card-content">
							<span class="card-title"><?php echo $service['name']?></span>
							<p><?php echo $service['description']?></p>
						</div>
						<div class="card-action">
							<a href="/services/edit/<?php echo $service['id']?>">Edit</a>
						</div>
						</div>
					</div>
			<?php

			}
			}
			?>
		</div>
		<div class="row margin50">
			<div class="col s12">
				<h3>Price Cards<a href="<?php echo base_url()?>rate_cards/add" class="waves-effect waves-light btn btn1 right" id="add_card">Add New Price Card</a></h3>
				
					<template v-if="!_.isEmpty(rate_cards)">
					<div class="row">
						<div v-for="(rate_card,index) in rate_cards" class="col s12 m6 l3 card" style="margin:20px">
							<div class="card-image">
								<a v-bind:href="rate_card.url" style="display:inline-block;text-align:center;">
									<template v-if="rate_card.loading_pdf">
										<div v-bind:data-loading="rate_card.loading_pdf" class="preloader-wrapper small active">
											<div class="spinner-layer spinner-green-only">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div><div class="gap-patch">
												<div class="circle"></div>
											</div><div class="circle-clipper right">
												<div class="circle"></div>
											</div>
											</div>
										</div>
									</template>
									<canvas style="max-width:250px" ref="canvas" v-bind:data-rate-card-id="rate_card.id" v-bind:data-pdf-url="rate_card.url" v-show="get_file_extension(rate_card.url)=='pdf'" class="canvas-rate-card"></canvas>
									<img v-show="get_file_extension(rate_card.url)!='pdf'" class="responsive-img" v-bind:src="rate_card.url" />

								</a>
							</div>
							<div class="card-action">
								<p class="titlex" v-text="rate_card.name"></p>
								<button class="btn red" v-on:click="delete_rate_card(index)" v-bind:data-ratecardid="rate_card.id">Delete</button>
							</div>
						</div>
					</div>
					</template>
			</div>
		</div>
		<div class="row margin50">
			<div class="col s12">
			<h3>Suggested Ideas:</h3>
			</div>
			
			<div class="" style="display:flex;flex-wrap: wrap;width: 100%;">

			<?php foreach($ideas as $idea)
			{
				$sp=_f_get_user_from_socialid($idea['ownerid']);
			?>
					<div class="suggested-idea">
					<div class="card white darken-1">
						<div class="card-content center-align min-height450">
						<img class="dynamic-pp card" style="display:inline-block;margin-top:0px" src="<?php echo _f_get_idea_logo_url($idea['id'])?>" width="50px" alt=""><br>
						<span class="card-title"><a href="/ideas/<?php echo $idea['unique_url'] ?>"><?php echo $idea['name']?></a></span><br>
						<span class="card-titlt"><?php $bullet=_f_get_idea_phase($idea['id']);echo $bullet['name']?> | <span id="placeholder_<?php echo $idea['id']?>_followers"> <?php echo _f_get_idea_followers($idea['id'])?> followers</span></span>
						<p class="left-align"><strong>Description:</strong></p>
						<p class="left-align"><?php echo $idea['description']?></p>
						</div>
						<div class="card-action">

						<?php _f_dynamic_follow_button($idea['id'])?>

						</div>
					</div>
					</div>

			<?php

			}

			?>
			</div>
		</div>
	</div>
</div>
<script src="/js/hopscotch.min.js"></script>
<script src="/js/jquery.barrating.min.js"></script>
<script src="/js/pdf.min.js"></script>
<script>
var app_dashboard_sp = new Vue({
	el:"#app_dashboard_sp",
	data:{
		rate_cards:[],
		pdfs:[],
		canvas_elements:{},
		template_loaded:0,
	},
	mounted:function() {
		var instance=this;
		this.$http.get('/api2/rate_cards').then(response=>{
			
			instance.rate_cards=response.data.data;
			for(var i=0;i<instance.rate_cards.length;i++) {
				
				if(instance.get_file_extension(instance.rate_cards[i].url)==="pdf")
				{
					//instance.rate_cards[i].loading_pdf=true;
					instance.rate_cards[i].pdfTask= PDFJS.getDocument(instance.rate_cards[i].url);
					
				}
				

			}

			instance.init_pdf_previews();
			

		},response=>{
			Materialize.toast("There was an error loading Rate Cards!",3000);
		});

		
	},
	methods:{
		delete_rate_card:function(index) {
			
			var data = new FormData();
			data.append("id",this.rate_cards[index].id);

			this.$http.post('/api3/delete_rate_card/',data).then(response=>{
				Materialize.toast("Rate Card Removed Successfully",4000);
				var rate_cards = this.rate_cards;
				rate_cards.splice(index,1);
				this.rate_cards=rate_cards;
			});
		},
		get_file_extension:function (filename) {
			//debugger;
			return filename.split('.').pop().toLowerCase();
		},
		init_pdf_previews:function() {
			var canvas_elements = document.querySelectorAll('.canvas-rate-card');
			var scale = 0.4;
    		
			
    		
			for(let i=0;i<this.rate_cards.length;i++) {
				var instance=this;
				
				if(this.rate_cards[i].hasOwnProperty('pdfTask')) {
					

					this.rate_cards[i].pdfTask.promise.then(function(pdf){
						//app_dashboard_sp.rate_cards[i].loading_pdf=false;
					
						var pageNumber = 1;
						pdf.getPage(pageNumber).then(function(page) {
							console.log('Page loaded');
							var viewport = page.getViewport(scale);
							
							var canvas = instance.$refs.canvas[i];
							var context = canvas.getContext('2d');
							canvas.height = viewport.height;
							//canvas.width = viewport.width;
							

							// Render PDF page into canvas context
							var renderContext = {
								canvasContext: context,
								viewport: viewport
							};
							var renderTask = page.render(renderContext);
							renderTask.then(function () {
								console.log('Page rendered');
								
							});
						});
					});
				}
				
			}
			
		}
	},
	updated:function() {
		
		
	}
});
	jQuery( document ).ready(function($) {
		$('#example').barrating({
		theme: 'fontawesome-stars-o',
			initialRating:<?php echo $rat?>,
			showValues: false,
			readonly: true,
		});

		

		var tour = {
			id: "hello-hopscotch",
			steps: [
					{
						title: "See Your Reviews",
							content: "Here you can see reviews given by others to you",
							target: "#rev",
							placement: "top"
					},
					{
						title: "New Service",
							content: "You can add new services here",
							target: document.querySelector("#add_serv"),
								placement: "left"
					},
					{
						title: "Add price card",
							content: "You can add new price card here (PDF,JPEG,PNG)",
							target: document.querySelector("#add_card"),
								placement: "left"
					},
					{
						title: "Messages",
							content: "You can send the messages to others(ideators and service providers).",
							target: document.querySelector("#mssg"),
								placement: "left"
					},
					{
						title: "Notifications",
							content: "You can simply get notifications here.",
							target: document.querySelector("#notification_bell"),
								placement: "left"
					}
					],
			onStart: function() {
				$("main").css("opacity","0.3");
			},
			onEnd: function() {
				$("main").css("opacity","1");
			},
			onClose: function () {
				$("main").css("opacity","1");
			}
		};

		if(localStorage.getItem("tourtaken")!=<?php echo $this->session->socialid?>){
			hopscotch.startTour(tour);
			localStorage.setItem('tourtaken',<?php echo $this->session->socialid?>);
		}
		else{
		}

		// Start the tour!
		$("#take_tour").click(function(e){
			e.preventDefault();
			hopscotch.startTour(tour);
		});

	});
</script>