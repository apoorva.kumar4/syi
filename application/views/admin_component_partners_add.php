<manage-partners-add inline-template>
<div class="template-wrapper">
    <div class="row">
        <div class="col s12 center">
            <h3>Add new Partner</h3>
        </div>
        <div class="col s12">
            <div class="input-field">
                <p>Partner Name</p>
                <input id="partner_name" v-model="partner.name" required/>
            </div>
        </div>
        <div class="col s12 center">
            <div class="file-field input-field">
                <div class="btn">
                    <span>Logo</span>
                    <input type="file" ref="logo_file" @change="file_changed"/>
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="input-field">
                <p>Partner Website</p>
                <input id="partner_website" v-model="partner.website" required/>
            </div>
        </div>
        <div class="col s12">
            <a class="waves-effect waves-light btn" @click="submit">Submit</a>
            </div>
        </div>
    </div>
</div>
</manage-partners-add>