<div class="container">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <h3 class="center color-yellow"><i class="material-icons large">error</i></h3>
            <h3 class="center">Account Approval Pending!</h3>
            <p class="center">Your account is under approval, we will contact you shortly. You can contact us on <a href="mailto:investors@sayourideas.com">investors@sayourideas.com</a> for any queries.</p>
            <br><br>
        </div>
    </div>
</div> <!-- End of Container -->