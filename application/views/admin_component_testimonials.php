<testimonials inline-template>
<div class="app-wrapper">
<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
<div class="row center">
	<div class="col s12 m12">
        <h3 class="admin-title">Testimonials</h3>
        
      </div>
       <div class="row">
      <h3><a href="/admin/testimonials/add" class="waves-effect waves-light btn btn1 right">Add Testimonial</a></h3>
    </div>
</div>
<div class="row">
  <div class="col  s12 m12 l12">

    <ul class="collection collectionx">

      
        <li v-for="(testimonial,index) in testimonials" class="collection-item collection-itemx avatar">
         <img v-bind:src="testimonial.profile_picture_url" alt="" class="circle">
          <span class="title text-lighten" v-text="testimonial.first_name"></span>
          <p style="margin:20px" v-text="testimonial.description"></p>
          <div class="div-action-testimonial">
            <a v-on:click="delete_testimonial(index)" class="btn waves red" id="admin_delete_event">Delete</a>
            <a v-if="testimonial.active==='0'" v-on:click="activate_testimonial(index)" class="btn waves green">Activate</a>
            <a v-else v-on:click="deactivate_testimonial(index)" class="btn waves red">Deactivate</a>

          </div>
        </li>
    </ul>
  </div>
</div>
</div>
</testimonials>