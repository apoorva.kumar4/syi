<style type="text/css">
	@media screen and (min-width: 768px) {
		#Subscribe {
			padding:50px 100px;
		}	
	}
	@media screen and (min-width: 1024px) {
		#Subscribe {
			padding:50px 350px;
		}	
	}

		
	
</style>
<section class="section">
	<div id="Subscribe" class="wrapper">
		<div class="row">
			<div class="col s12 card" style="padding:20px">
				<h4>Subscribe to get the latest updates from SaYourIdeas.</h4>
				<p>We will never spam you or share your email. Do read our <a href="/privacy_policy">Privacy Policy</a> for more details. Also you can <a href="/subscribe/opt_out">opt-out</a> at any time.</p>
				<form class="" action="/subscribe" method="POST">
					<div class="input-field col s12">
						<input id="email" name="email" type="email" required="" />
						<label for="email">Your Email</label>
					</div>
					<div class="input-field col s12">
						<div class="g-recaptcha" data-sitekey="6LdIOjQUAAAAALRi6GT1xzV0nrg138PETl3RqLaz"></div>
					</div>
					<div class="input-field col s12">
						<input id="submit" class="btn btn-blue fwidth" type="submit" value="Sign me up for newsletter!"/>
						
					</div>
				</form>
			</div>
		</div>
	</div>
</section>