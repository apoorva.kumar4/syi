<div class="container">
<div class="row">
	<div class="col s12">
		<h3>Add a new Price Card</h3>
        <div class="center red-text">
            <?php echo validation_errors();?>
        </div>
	</div>
</div>
<form id="uploadRateCard" action="">
<div class="row custom">
	
	<div class="input-field col s12">
		<input type="text" name="ratecard-name"  value="<?php echo set_value('rate_card-name')?>" required>
		<label>Name of the Price Card</label>
	</div>
	<div class="file-field input-field col s12">
		<input id="rate" type="file" name="rate-card" required>
		<p>PDF, JPEG, PNG files can be uploaded.</p>
		<div class="file-path-wrapper">
			<input class="file-path validate" type="text" placeholder="Click to select a file">
        </div>
    </div>
</div>
<input class="btn" type="submit" value="Add">
</form>
</div>
    <div class="row">
</div>