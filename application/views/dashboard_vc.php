<style>
        .masonry {
            margin: 1.5em 0;
    padding: 0;
    -moz-column-gap: 0em;
    -webkit-column-gap: 0em;
    column-gap: 0em;
    font-size: .85em;
        }
        .arcsec {
        display: inline-block;
        padding: 1em;
        margin: 0;
        width: 100%;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        }

        .arcsec .card {
            margin:0;
        }

        @media only screen and (min-width: 400px) {
    .masonry {
        -moz-column-count: 1;
        -webkit-column-count: 1;
        column-count: 1;
    }
}

@media only screen and (min-width: 700px) {
    .masonry {
        -moz-column-count: 1;
        -webkit-column-count: 1;
        column-count: 1;
    }
}

@media only screen and (min-width: 900px) {
    .masonry {
        -moz-column-count: 3;
        -webkit-column-count: 3;
        column-count: 3;
    }
}
    </style>
<link rel="stylesheet" href="/css/hopscotch.css">
<script src="/js/hopscotch.min.js"></script>
<script>
    jQuery(document).ready(function($) {
        var tour = {
            id: "hello-hopscotch",
            steps: [
                {
                    title: "Hi!",
                    content: "Here are some short listed ideas,Your shortlisted ideas will come over here",
                    target: "#sh_idea",
                    placement: "bottom"
                },
                {
                    title: "Recommendations",
                    content: "Recommended ideas for you will come over here",
                    target: document.querySelector("#recm_sec"),
                    placement: "top"
                },
                {
                    title: "Messages",
                    content: "You can send the messages to others(ideators,service provider,investor).",
                    target: document.querySelector("#mssg"),
                    placement: "left"
                },
                {
                    title: "Notifications",
                    content: "You can simply get notifications here.",
                    target: document.querySelector("#notification_bell"),
                    placement: "left"
                }

            ],
            onStart: function() {
                $("main").css("opacity","0.3");
            },
            onEnd: function() {
                $("main").css("opacity","1");
            },
            onClose: function () {
                $("main").css("opacity","1");
            }

        };

        if(localStorage.getItem("tourtaken")!=<?php echo $this->session->socialid?>){
            hopscotch.startTour(tour);
            localStorage.setItem('tourtaken',<?php echo $this->session->socialid?>);
        }
        else{
        }

        // Start the tour!
        $("#take_tour").click(function(e){
            e.preventDefault();
            hopscotch.startTour(tour);
        });


    });
</script>



<div class="row margin50">
    <div class="col s12">
        <h3 id="sh_idea">Shortlisted Ideas<a id="take_tour" class="waves-effect waves-light right hide-on-small-only" href="#">Take a tour</a></h3>

        <div class="row">
            <?php foreach($ideas as $idea)
            {
                $sp=_f_get_user_from_socialid($idea['ownerid']);
                ?>
                <div class="col s12 m6 l3">
                    <div class="card white darken-1">
                        <div class="card-content center-align min-height350">
                            
                            <img class="dynamic-pp card hoverable" style="display:inline-block;margin-top:0px" src="<?php echo _f_get_idea_logo_url($idea['id'])?>" width="50px" alt=""><br>
                            <span class="card-title"><a href="/ideas/<?php echo $idea['unique_url'] ?>"><?php echo $idea['name']?></a></span><br>
                            <span class="card-titlt"><?php $bullet=_f_get_idea_phase($idea['id']);echo $bullet['name']?> | <span id="placeholder_<?php echo $idea['id']?>_followers"> <?php echo _f_get_idea_followers($idea['id'])?> followers</span></span>
                            <p class="left-align"><strong>Description:</strong></p>
                            <p class="left-align"><?php echo $idea['description']?></p>
                        </div>
                        <div class="card-action">

                            <?php _f_dynamic_follow_button($idea['id'])?>

                        </div>
                    </div>
                </div>

                <?php

            }

            ?>
        </div>

        </div>

    </div>

</div>
<div class="margin50">
    <div class="col s12">
        <h3 id="recm_sec">Recommendations for you:</h3>
    </div>
    
        <div class="masonry">
        <?php foreach($sug_ideas as $idea)
        {
            
            ?>
                <div class="arcsec">
                    <div class="card white darken-1">
                        <div class="card-content center-align">
                            
                            <img class="dynamic-pp card" style="display:inline-block;margin-top:0px" src="<?php echo _f_get_idea_logo_url($idea['id'])?>" width="50px" alt=""><br>
                            <span class="card-title"><a href="/ideas/<?php echo $idea['unique_url'] ?>"><?php echo $idea['name']?></a></span><br>
                            <span class="card-titlt"><?php $bullet=_f_get_idea_phase($idea['id']);echo $bullet['name']?> | <span id="placeholder_<?php echo $idea['id']?>_followers"> <?php echo _f_get_idea_followers($idea['id'])?> followers</span></span>
                            <p class="left-align"><strong>Description:</strong></p>
                            <p class="left-align"><?php echo $idea['description']?></p>
                        </div>
                        <div class="card-action">

                            <?php _f_dynamic_follow_button($idea['id'])?>

                        </div>
                    </div>
                </div>
            

            <?php

        }

        ?>
        </div>

</div>
