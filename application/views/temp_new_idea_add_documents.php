<!--    add new document section-->
            <div class="col s12">
                <h5 style="">Your Documents</h5>
                <div class="input-field">
                    <input type="hidden" type="text" name="no_of_people"  value="">
                    <p style="">You can add your Documents like Business Plan, Excel Sheets, Revenue model, Etc.</p>
                </div>
                <div class="document_adding_form">
                    <div class="document-area" style="">
                            <p style="color:#616161;font-weight: 500;">Business Plan <a href="#modal-add-business" class="modal-trigger"><i class="material-icons">info</i></a></p>

                            <div class="file-field input-field">
                            
                                <input type="file" name="document_business_plan">
                                <div class="file-path-wrapper">
                                    <input  class="file-path validate" type="text" placeholder="Select file">
                                </div>

                            </div>
                            <p style="color:#616161;font-weight: 500;">Financials <a href="#modal-add-fin" class="modal-trigger"><i class="material-icons">info</i></a></p>
                            <div class="file-field input-field">
                                
                                <input type="file" name="document_financials">
                                <div class="file-path-wrapper">
                                    <input  class="file-path validate" type="text" placeholder="Select file">
                                </div>
                            </div>
                            <p style="color:#616161;font-weight: 500;">Executive Summary <a href="#modal-add-summary" class="modal-trigger"><i class="material-icons">info</i></a></p>
                            <div class="file-field input-field">
                                <input type="file" name="document_business_executive_summary">
                                <div class="file-path-wrapper">
                                    <input  class="file-path validate" type="text" placeholder="Select file">
                                </div>
                            </div>
                            <p style="color:#616161;font-weight: 500;">Achievements / Compliments</p>
                            <div class="file-field input-field">
                                
                                <input type="file" name="document_achievements">
                                <div class="file-path-wrapper">
                                    <input  class="file-path validate" type="text" placeholder="Select file">
                                </div>
                            </div>
                            <p style="color:#616161;font-weight: 500;">Any Extra (Brochures, Patents, Certifications etc) <a href="#modal-add-extra" class="modal-trigger"><i class="material-icons">info</i></a></p>
                            <div class="file-field input-field">
                                
                                <input type="file" name="document_extra">
                                <div class="file-path-wrapper">
                                    <input  class="file-path validate" type="text" placeholder="Select file">
                                </div>
                            </div>
                        
                    </div>
                </div>
               
            </div>
            <!--    add document section ends here-->