<user-activity inline-template :users="users" :uploads="uploads">
    <div class="app-wrapper">
        <a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
        <div class="row center">
            <div class="col s12 m12">
                <h3 class="admin-title">Recent Activity by all Users</h3>
                <p>Latest First</p>
            </div>
            <div class="row">
                <div class="input-field col s12 m3">
                    <p style="text-align:left">Date Start</p>
                    <input v-model="date_start" type="date" format="yyyy-mm-dd" placeholder="Date Start"/><br><br>
                    
                </div>
                <div class="input-field col s12 m3">
                    <p style="text-align:left">Date End</p>
                    <input v-model="date_end" type="date" format="yyyy-mm-dd" placeholder="Date End"/>
                </div>
			    <div class="input-field col s12 m6 right">
					<a v-on:click="download" class="btn green right" href="#" style="margin:0 10px"> <i class="material-icons left">file_download</i>Download as CSV</a>
                </div>
		    </div>
        </div>
    
        <!-- <div class="row" style="border:1px solid #f4f4f4;padding:15px;">    
            <div class="col s12" id="">
                <h5>Filter by Date</h5>
                <div class="col"><input id="filter_date_start" name="date_start" type="date" class="datepicker"></div>
                <div class="col"><input id="filter_date_end" name="date_end" type="date" class="datepicker"></div>
                <div class="col right">
                    <button type="button" class="btn waves-green green right" id="filter_submit">Filter</button>
                    <button id="a-download-activity" v-on:click="download_as_csv" class="btn green right" style="margin:0 10px"> <i class="material-icons left">file_download</i>Download as CSV</button>
                </div>
            </div>
        </div> -->
        <template v-if="!_.isEmpty(activity) && !_.isEmpty(users) && !_.isEmpty(uploads)">
            <div class="col  s12 m12 l12">
                <ul class="collection collectionx">
                    <li class="collection-item collection-itemx avatar" v-for="act in activity">
                        <img v-if="users[act.socialid].profile_picture_upload_id" v-bind:src="uploads[users[act.socialid].profile_picture_upload_id].complete_url" alt="" class="circle">
                        <img v-else v-bind:src="users[act.socialid].gravatar_url" alt="" class="circle">
                        <span class="title text-lighten"><a v-bind:href="'/user_info/u/'+act.socialid" class="text-teal" v-text="users[act.socialid].first_name"></a></span>
                        <div class="text-lighten" style="text-align: right;display: inline-block;float: right;margin-right: 0px;"><span v-text="users[act.socialid].user_type_human"></span><br>
                                <span v-text="localDate(act.timestamp)"></span>
                                
                            </div>
                            <p class="subtext subtextx" v-text="act.description"><br></p>
                    </li>
                    
                </ul>
            </div>
            <table id="table_activity" class="hide">
                <tr>
                    <th>Socialid</th>
                    <th>Name</th>
                    <th>Activity Description</th>
                    <th>Date and Time</th>
                </tr>
                <tr v-for="act in activity">
                    <td v-text="act.socialid"></td>
                    <td v-text="users[act.socialid].first_name +' '+ users[act.socialid].last_name"></td>
                    <td v-text="act.description"></td>
                    <td v-text="localDate(act.timestamp)"></td>
                </tr>
            </table>
        </template>
        <template v-else>
            <div class="row center">
            <div class="preloader-wrapper small active">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

            </div>
            </div>
        </template>
        
    </div> <!--app wrapper -->
</user-activity>