<style>
	td,th {
		border:1px solid #e0e0e0;
		border-left: none;
		border-right:none;
		padding:10px 5px;
	}
</style>
<div class="container">
<h3 class="">Select your Account type:</h3>
<blockquote class=""><span class="red-text"> Note: Investor Accounts go through an approval, only after which they can use the website.</span></blockquote>
<form id="" action="/dashboard" method="POST">
	<div class="row">
		<div class="col s12 m6 l4">
			<div class="card">
				<div class="card-content">
					<input id="checkbox_ideator" class="with-gap" type="radio" value="ideator" name="select_user_type">
					<label for="checkbox_ideator"></label>
					<h4>Ideator</h4>
					<table>
						<tr><td>Create your own Idea Timeline.</td></tr>
						<tr><td>Connect with Investors, Mentors &amp; Service Providers.</td></tr>
						<tr><td>Get recommendation at various stages of your idea.</td></tr>
						<tr><td>Get updates on various startup events near you.</td></tr>
						<tr><td>Much more..</td></tr>
					</table>
				</div>
			</div>
		</div>
		<div class="col s12 m6 l4">
			<div class="card">
				<div class="card-content">
					<input id="checkbox_sp" class="with-gap" type="radio" value="service_provider" name="select_user_type">
					<label for="checkbox_sp"></label>
					<h4>Service Provider</h4>
					<table>
						<tr><td>Display your best services &amp; Upload your brochures.</td></tr>
						<tr><td>Get recommendations of startups requiring your services.</td></tr>
						<tr><td>Connect to the ideators.</td></tr>
						<tr><td>Get updates on various startup events near you.</td></tr>
						<tr><td>Much more..</td></tr>
						
					</table>
				</div>
			</div>
		</div>
		<div class="col s12 m6 l4">
			<div class="card">
				<div class="card-content">
					<input id="checkbox_vc" class="with-gap" type="radio" value="investor" name="select_user_type">
					<label for="checkbox_vc"></label>
					<h4>Investor</h4>
					<table>
						
						<tr><td>Shortlist the ideas.</td></tr>
						<tr><td>Connect with ideators, Service Providers at your discrete.</td></tr>
						<tr><td>Get suggestion on the best ideas as per your preferences.</td></tr>
						<tr><td>Get updates on various startup events near you.</td></tr>
						<tr><td>Much more..</td></li>
					</table>
				</div>
			</div>
		</div>
		<div class="col s12">
			<input type="submit" value="Submit" class="btn">
		</div>
	</div>

</form>
</div>