<a class="btn-floating btn-small waves-effect waves-light back_window"><i class="material-icons">arrow_back</i></a>
	<div class="row center">
	  <div class="col s12 m12">
			<h3 class="admin-title">Faq's</h3>
		
	  </div>
	   <div class="row">
	  	<h3><a href="/admin/faq/add" class="waves-effect waves-light btn btn1 right">Add New Faq</a></h3>
		</div>
	</div>
	<div class="row">
	  <div class="col  s12 m12 l12">
	  <h4>Ideator Faqs</h4>
		<ul class="collapsible">

		  <?php
		  foreach($ideator_faqs as $testimonial)
		  {
			?>
			<li >
			
			  <div class="collapsible-header"><?php echo $testimonial['post_title']?></div>
			  <div class="collapsible-body">
			  
			  <p style=""><?php echo $testimonial['post_desc']?></p>
			  <p>
				  
			   <a class="btn waves green" data-testimonialid="<?php echo $testimonial['post_id']?>" href="/admin/faq/edit/<?php echo $testimonial['post_id']?>">Edit</a>
			   
			   </p>
			  

			  </div>
	
			</li>
			<?php
		  }
		  ?>
		</ul>
	  </div>
	</div>

	<div class="row">
	  <div class="col  s12 m12 l12">
	  <h4>Service Provider Faqs</h4>
		<ul class="collapsible">

		  <?php
		  foreach($service_provider_faqs as $testimonial)
		  {
			?>
			<li >
			
			  <div class="collapsible-header"><?php echo $testimonial['post_title']?></div>
			  <div class="collapsible-body">
			  
			  <p style=""><?php echo $testimonial['post_desc']?></p>
			  <p>
				  
			   <a class="btn waves green" data-testimonialid="<?php echo $testimonial['post_id']?>" href="/admin/faq/edit/<?php echo $testimonial['post_id']?>">Edit</a>
			   
			   </p>
			  

			  </div>
	
			</li>
			<?php
		  }
		  ?>
		</ul>
	  </div>
	</div>

	<div class="row">
	  <div class="col  s12 m12 l12">
	  <h4>Investor Faqs</h4>
		<ul class="collapsible">

		  <?php
		  foreach($investor_faqs as $testimonial)
		  {
			?>
			<li >
			
			  <div class="collapsible-header"><?php echo $testimonial['post_title']?></div>
			  <div class="collapsible-body">
			  
			  <p style=""><?php echo $testimonial['post_desc']?></p>
			  <p>
				  
			   <a class="btn waves green" data-testimonialid="<?php echo $testimonial['post_id']?>" href="/admin/faq/edit/<?php echo $testimonial['post_id']?>">Edit</a>
			   
			   </p>
			  

			  </div>
	
			</li>
			<?php
		  }
		  ?>
		</ul>
	  </div>
	</div>