<div class="row">
    <div class="col s12">
        <h3>Documents for <?php echo $idea_name?><a href="/ideas/documents/upload/<?php echo $idea_id;?>" class="waves-effect waves-light btn btn1 right">Upload New</a>   <a href="/dashboard" class="linkfix right">Idea Dashboard</a></h3>
        <div class="row">
            <?php
            foreach($documents as $document)
            {
                ?>
                <div class="col s12 m6 l3 card" style="margin:20px">
                    <div class="card-image">
                        <a href="/uploads/u/<?php echo $document['url']?>"><img class="responsive-img" alt="Download" src="/uploads/u/<?php echo $document['url']?>" onerror="this.src=''"></img></a>

                    </div>
                    <div class="card-action">
                        <span class="" style="margin:10px"><?php echo $document['upload_name']?></span>
                        <a class="delete-document" href="#" data-documentid="<?php echo $document['id']?>">Delete</a>
                    </div>
                </div>
                <!--<img class="materialboxed" width="250" src="/uploads/u/<?php echo $document['url']?>">-->

                <?php
            }
            ?>
            <script>
                jQuery( document ).ready(function($) {
                    $(".delete-document").click(function(e){
                        e.preventDefault();
                        var flag=$(this);
                        var documentid=$(this).attr("data-documentid");
                        $.ajax({
                            url: '/api2/idea/document/'+documentid, // Url to which the request is send
                            type: "DELETE",             // Type of request to be send, called as method
                            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                            contentType: false,       // The content type used when sending data to the server.
                            cache: false,             // To unable request pages to be cached
                            processData:false,        // To send DOMDocument or non processed data file it is set to false
                            success: function(data)   // A function to be called if request succeeds
                            {
                                var obj=JSON.parse(data);
                                if(obj.status=="success")
                                {
                                    Materialize.toast("Document Deleted Successfully",4000);
                                    flag.parent().parent().fadeOut();
                                }
                                else{
                                    Materialize.toast("Document Deletion Failed: "+ obj.description,4000);
                                }
                            }
                        });
                    });

                });


            </script>
        </div>
    </div>
</div>
