<div class="col s12 center">
	<h3>Add Faq</h3>
</div>

<form id="add_faq" method="POST">
<div class="row custom">
	<div class=" col s12">
		<p>For User</p>
		<select required name="user_type" class="browser-default">
		<option value="ideator">Ideator</option>
		<option value="service_provider">Service Provider</option>
		<option value="investor">Investor</option>
		</select>
		
	</div>
	<div class="input-field col s12">
		<p>Faq title</p>
		<input required type="text" name="post_title">
		
	</div>
	
	<div class="input-field col s12">
		<p>Faq Answer</p>
		<textarea required type="text" name="post_desc" class="materialize-textarea"></textarea>
		
		
		
	</div>
	<div class="input-field col s12">
		<input class="btn" type="submit" value="Add">
		<a class="btn green" href="/admin/faq">Back</a>
	</div>
</div>

</form>