<script src="/js/parsley.min.js"></script>
<script src="/js/selectize.min.js"></script>
<link rel="stylesheet" href="/css/selectize-custom.css">
<style>
.input-field label {
    color: #616161;
    left:0 !important;
}
</style>
<div id="app_new_idea" class="container">
    <div id="modal-add-tag" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Tagline For Iddea</h4>
            <p>A one-liner for your Idea. Eg: Unbox Zindagi</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-idea" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Idea Name</h4>
            <p>Name by which your idea would be known eg: Expertmile - A market place for CAs and lawyers or AI based pharmaceutical product.</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-stage" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Current stage of idea</h4>
            <p>What stage is your idea: Eg - Ideation phase if no prototype is ready, or Expansion phase if you have a steady revenue and are in expansion plans.</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-expidea" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>One or two liners of the company</h4>
            <p>Where did you get this idea? What were the pain points you identified and what is a solution? Please be clear and precise as this would be the first thing about your idea to be seen by the investors and the corporate partners</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-plan" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Start of timeline</h4>
            <p>It’s the approximate date when you had got the idea and started exploring it further, we recommend it to be realistic as this would be seen by the investors and corporate cos - eg: 12th December 2010</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-exp" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>By when do you see your business idea would  be in expansion phase?</h4>
            <p>It’s the date when you want your idea to be up and running successfully, we recommend you to be realistic in this considering current phase of your idea as it would be seen by investors and corporate companies.</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-explan" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Explain your Business Plan</h4>
            <p>Do not give any blueprints or confidential part of your idea. We would only want to know that how have you planned your idea to be a reality and successful going ahead and what are your near term and long term goals.</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-usp" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>The market size and your USP</h4>
            <p>
                How are you different from your competitors and what is the size of your market? eg: We provide AI based softwares in Healthcare sector (USD 2bln industry) and our technology, way of working, business model and expertise with the team differentiates us from existing players.
            </p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-prf" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Proof of concept</h4>
            <p>
                How do you plan to see the market acceptance of your product? Eg: Sold 100 products in Mumbai in 2 months and getting more orders or Got a seed funding from one of the investors for 10 lakhs to work further on the product.
            </p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-rev" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Explain your revenue model in brief</h4>
            <p>
                What are your revenue drivers and how do you plan to earn from your idea? Eg: subscription, product selling in large volumes, marketing revenue etc.
            </p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-obj" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Future Objectives and Prospects</h4>
            <p>What are your future plans - near term and long term goals in detail; eg: Short term goal: expanding in 3 major cities with similar product range and..., Long term goal: Expanding the product range in the same field and further expanding to other countries like...</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-abt" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>About your achievements with this Idea</h4>
            <p>Eg: Won best startup idea in startup conclave or changed the way rural india used to work earlier. We recommend you to put any achievements with the proof such as link to the site or article if you have been mentioned anywhere.</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-pat" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Idea patented?</h4>
            <p>If you have the patent for your idea, please put the details of it which would make us easy to showcase it to the investors and corporate partners.</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-serv" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Services Required at current stage of your idea</h4>
            <p>
                What are the services you required currently to move your idea ahead and make it a reality eg: Marketing, technical co-founder, market research, app developer etc.
            </p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-comp" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Company Name</h4>
            <p>Your co. name - Apple, Microsoft, Google etc.</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-add" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Full Address of the Company</h4>
            <p>Eg: Mumbai, India</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-web" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Website for more information about this idea</h4>
            <p>www.sayourideas.com</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-sum" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Team Overview</h4>
            <p>
                Details of your founders, co-founders, mentors and other important people in the company.
                Team size, roles in brief: Eg: We are team of 4 - one CTO, one CEO and two sales  people working since 5 months
            </p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-business" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Business Plan</h4>
            <p>A business deck of your company to be seen by the investors and corporate partners. (If you do not have a business deck, please feel free to <a href="<?php echo base_url()?>contact-us">contact us</a> for free consultation).</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-fin" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Financials</h4>
            <p>Detailed financial model or basic financials of the company detailing the past, current and future financial plans (If you do not have a financial plans in place, please feel free to contact us for free consultation).</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-summary" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Executive Summary</h4>
            <p>Details of your founders, co-founders, mentors and other important people in the company.</p>                                </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
        </div>
    </div>
    <div id="modal-add-extra" class="modal modal-add-idea">
        <div class="modal-content">
            <h4>Any Extra</h4>
            <p>Eg.Brochure, certification, patent approval etc. </p>
            <div class="modal-footer">
                <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
            </div>
        </div>
    </div>

    <form id="form_post_idea" action="/temp">

        <div class="" id="prev_detail">
            <div class="row">
                <div class="col s12">
                    <h3>Add a new Idea</h3>	
                    <div class="errors materialize-red-text" style="">
                        <div class="red-text">
                            Note: Fields in Red are required. Please do not upload any blue prints or confidential documents of your ideas/innovations.
                        </div>
                    </div>
            	</div>

                <div class="col s12 m6">
                    <h5>Logo</h5>
                    <img id="logo-previewing" class="responsive" width="150px" src="https://s3.ap-south-1.amazonaws.com/sayourideas-uploads/bulb.png" style="">
                    <div class="file-field input-field">
                        <div class="btn">
                            <span>Select Logo</span>
                            <input id="logofile" type="file" name="logofile">
                        </div>
                        <div class="file-path-wrapper" style="visibility: hidden">
                            <input class="file-path validate" type="text" placeholder="Upload one or more files">
                        </div>
                    </div>
                </div>
                <div class="col s12 m6">
                    <h5>Cover</h5>
                    <img id="cover-previewing" class="responsive" src="https://s3.ap-south-1.amazonaws.com/sayourideas-uploads/image-2.jpg" style="width:100%;">
                    <div class="file-field input-field">
                        <div class="btn">
                            <span>Select Cover</span>
                            <input id="coverfile" type="file" name="coverfile">
                        </div>
                        <div class="file-path-wrapper" style="visibility: hidden">
                            <input class="file-path validate" type="text" placeholder="Upload one or more files">
                        </div>
                    </div>
                </div>
                <div class="col s12">
                    <?php if(_f_is_admin()) {
                        ?>
                        <div class="input-field">
                            <input title="Social ID" required type="text" name="socialid"  value="">
                            <label>Socialid of the Owner *</label>
                        </div>
                    <?php
                    }?>

                    <h5 style="">Name, Tagline, Stage, Sector</h5>
                
                    <div class="input-field">
                        <input id="name" type="text" title="Idea name" name="name"  value="" required>
                        <label class="color-red" data-error="Idea name doesn't quite look right">Name of the Idea <a href="#modal-add-idea" class="modal-trigger" tabindex="-1"><i class="material-icons" title="Name by which your idea would be known eg: Expertmile">info</i></a></label>
                    </div>


                    <div class="">
                        https://www.sayourideas.com/ideas/
                        <div class="input-field inline">
                            <input id="unique_url" required type="text" name="unique_url"  value=" " autocomplete="off">
                            <label style="color:#616161;font-weight: 500;" data-success="Looks great!" data-error="Sorry that url slug is not available">idea-slug</label>
                        </div>
                    </div>
                

                    <div class="input-field">
                        <input type="text" name="tagline"  value="">
                        <label>Tagline <a href="#modal-add-tag" tabindex="-2" class="modal-trigger"><i class="material-icons" title="A one-liner for your Idea. Eg: Unbox Zindagi.">info</i></a></label>
                    </div>

                    <div class="input-field">
                        <select  name="stage" required>

                            <?php
                            $array_options=array('Ideation','Prototype','Launching Soon','Operational','Expansion');
                            foreach($array_options as $value)
                            {
                                if(set_value('stage')==$value) {
                                    ?>
                                    <option value="<?php echo $value ?>" selected><?php echo $value ?></option>
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <option value="<?php echo $value?>" ><?php echo $value?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <label class="color-red">Select current stage <a href="#modal-add-stage" class="modal-trigger" tabindex="-2"><i class="material-icons" title="What stage is your idea: Eg - Ideation phase if no prototype is ready, or Expansion phase if you have a steady revenue and are in expansion plans.">info</i></a></label>
                    </div>

                    <div class="input-field">
                        <?php _f_dynamic_select_sector("sectorid"); ?>
                        <label>Sector of the Idea (Like Agriculture, Pharmaceuticals, Technology, Banking, etc)</label>
                    </div>

                    <div id="div_sector_other" class="input-field" style="display:none">
                        <input id="sector_other" type="text" name="sector_other">
                        <label for="">Please specify your sector</label>
                    </div>
                </div> 
            </div> <!--row ends-->
            
            <div class="row">
                <div class="col s12">
                    <h5>About your idea</h5>
                    <div class="input-field">
                        <textarea required title="Description" name="description" id="textarea1" class="materialize-textarea"></textarea>
                        <label class="color-red" for="textarea1">Explain your Idea in brief <a href="#modal-add-expidea" class="modal-trigger" tabindex="-2"><i class="material-icons" title="Where did you get this idea? What were the pain points you identified and what is a solution? Please be clear and precise as this would be the first thing about your idea to be seen by the investors and the corporate partners">info</i></a></label>
                    </div>
                </div>
            
                <div class="col s12" style="">
                    <div class="input-field">
                        <p for="start_date" class="color-red">When did you start working on this idea <a href="#modal-add-plan" class="modal-trigger"><i class="material-icons" title="It’s the approximate date when you had got the idea and started exploring it further, we recommend it to be realistic as this would be seen by the investors and corporate cos - eg: 12th December 2010">info</i></a></p>
                        <input id="start_date" required title="Start date" name="start_date" type="date" class="" value="" >

                    </div>
                </div>
                <div class="col s12">
                    <div class="input-field">
                        <p for="end_date" class="color-red">By when do you see your business idea would  be in expansion phase <a href="#modal-add-exp" class="modal-trigger"><i class="material-icons" title="It’s the date when you want your idea to be up and running successfully, we recommend you to be realistic in this considering current phase of your idea as it would be seen by investors and corporate companies.">info</i></a></p>
                        <input id="end_date" required title="End date" name="end_date" type="date" class="" value="">
                        
                    </div>
                </div>
            
                <div class="col s12">
                    <div class="input-field">
                        <textarea name="business_plan" id="textarea1" class="materialize-textarea" value="" required></textarea>
                        <label class="color-red" for="textarea1">Explain your Business Plan (Visible to investors, Please be very discrete) <a href="#modal-add-explan" class="modal-trigger"><i class="material-icons" title="Do not give any blueprints or confidential part of your idea. We would only want to know that how have you planned your idea to be a reality and successful going ahead and what are your near term and long term goals.">info</i></a></label>
                    </div>
                </div>
                 <div class="col s12">
                    <div class="input-field">
                        <textarea name="executive_summary" class="materialize-textarea" value=""></textarea>
                        <label for="textarea1">Executive Summary <a href="#modal-add-summary" class="modal-trigger"><i class="material-icons" title="Details of your founders, co-founders, mentors and other important people in the company.">info</i></a></label>
                    </div>
                </div>
            
                <div class="col s12">
                    <div class="input-field">
                        <textarea name="market_size_usp" id="textarea1" class="materialize-textarea" value="" required></textarea>
                        <label class="color-red" for="textarea1">What is the market size and your USP <a href="#modal-add-usp" class="modal-trigger"><i class="material-icons" title="How are you different from your competitors and what is the size of your market? eg: We provide AI based softwares in Healthcare sector (USD 2bln industry) and our technology, way of working, business model and expertise with the team differentiates us from existing players.">info</i></a></label>
                        
                    </div>
                </div>

                <div class="col s12">
                    <div class="input-field">
                        <textarea name="proof_of_concept" id="textarea1" class="materialize-textarea" value=""></textarea>
                        <label for="textarea1">Proof of concept <a href="#modal-add-prf" class="modal-trigger"><i class="material-icons" title="How do you plan to see the market acceptance of your product? Eg: Sold 100 products in Mumbai in 2 months and getting more orders or Got a seed funding from one of the investors for 10 lakhs to work further on the product.">info</i></a></label>
                    </div>
                </div>
                <div class="col s12" >
                    <div class="input-field">
                        <textarea name="revenue_model" id="textarea1" class="materialize-textarea" value=""></textarea>
                        <label for="textarea1">Explain your revenue model in brief <a href="#modal-add-rev" class="modal-trigger"><i class="material-icons" title="What are your revenue drivers and how do you plan to earn from your idea? Eg: subscription, product selling in large volumes, marketing revenue etc. ">info</i></a></label>
                        
                    </div>
                </div>
                <div class="col s12" >
                    <div class="input-field">
                        <textarea name="future_prospects" id="textarea1" class="materialize-textarea" value="" required></textarea>
                        <label class="color-red" for="textarea1">Future Objectives and Prospects <a href="#modal-add-obj" class="modal-trigger"><i class="material-icons" title="What are your future plans - near term and long term goals in detail; eg: Short term goal: expanding in 3 major cities with similar product range and..., Long term goal: Expanding the product range in the same field and further expanding to other countries like...">info</i></a></label>
                        
                    </div>
                </div>

                <div class="col s12">
                    <div class="input-field">
                        <textarea name="achievements" id="textarea1" class="materialize-textarea" value=""></textarea>
                        <label for="textarea1">Tell us about your achievements with this Idea <a href="#modal-add-abt" class="modal-trigger"><i class="material-icons" title="Eg: Won best startup idea in startup conclave or changed the way rural india used to work earlier. We recommend you to put any achievements with the proof such as link to the site or article if you have been mentioned anywhere.">info</i></a></label>
                    </div>
                </div>

                <div class="col s12">
                    <div class="input-field">
                        <textarea name="competitions" id="textarea1" class="materialize-textarea" value=""></textarea>
                        <label for="textarea1">Tell us about your competitions</label>
                    </div>
                </div>

                <div class="col s12 m6">
                    <div class="input-field">

                        <select name="patented" id="dt-select">
                            <?php
                            $arr_options = array('No','Yes');
                            foreach($arr_options as $option)
                            {
                                if($option == $patented)
                                {
                                    ?>
                                    <option id="sel" value="<?php echo $option?>"><?php echo $option?></option>
                                    <?php
                                }
                                else{
                                    ?>
                                    <option value="<?php echo $option?>"><?php echo $option;?></option>
                                    <?php
                                }
                            }

                            ?>

                        </select>


                        <label>Is your Idea patented <a class="modal-trigger"></a></label>
                    </div>
                </div>
                <div class="col s12 m6">
                    <div class="input-field" id="hidden_div">
                        <input name="patent_number" id="patent_date" type="text" class="tim-datepicker"  placeholder="If patented, then Patent number.">
                        <label>Patent Number</label>
                    </div>
                </div>
            
                <div class="col s12">
                    <div class="input-field">
                        <select id="visibility" name="visibility" title="Visibility">
                            
                            <?php
                            $array_options=array('Public'=>'Visible to public','Private'=>'Only visible to me and investors');

                            foreach($array_options as $value=>$option)
                            {
                                ?>
                                <option value="<?php echo $value ?>" selected><?php echo $option ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <label>Visibility</label>
                    </div>
                </div>

                <div class="col s12">
                    <div class="input-field">
                        <textarea id="services_required" class="materialize-textarea" name="services_required" type="text" value=""></textarea>
                        <label>Services Required at current stage of your idea <a href="#modal-add-serv" class="modal-trigger"><i class="material-icons" title="What are the services you required currently to move your idea ahead and make it a reality eg: Marketing, technical co-founder, market research, app developer etc.">info</i></a></label>
                    </div>
                </div>
        

                <div class="col s12">
                    <div class="input-field">
                        <p>Add Tags Eg: IOT, Elearning, Telecom, Big Data Analystics etc</p>
                        <div class="input-field">
                            <input id="idea_tags" name="tags" type="hidden">
                            <div class="chips chips-edit chips-idea-tags-edit" style="border:none !important; margin-bottom:0px !important; box-shadow: none; !important">
                            </div>
                        </div>

                    </div>
                </div>          
            </div>
            <div class="row">
                <div class="col s12">
                    <input class="btn" type="submit" value="Next" id="more_info_sub">
                </div>
            </div>
        </div>
        <!--    more detail form-->
        <div class="row"  id="more_detail_extra_form" style="display:none">
            <div class="col s12">
                <h3>More Details</h3>
            </div>
            <div class="col s12">
                <div class="input-field">
                    <input type="text" name="company"  value="">
                    <label for="company">Company Name (eg: A.B.C Ventures Pvt. Ltd.) <a href="#modal-add-comp" class="modal-trigger"><i class="material-icons" title="Your co. name - Apple, Microsoft, Google etc.">info</i></a></label>
                </div>
            </div>

            <div class="col s6">
                <div class="input-field">
                    <input type="text" name="address"  title="Address" value="" required>
                    <label class="color-red">Full Address of the Company <a href="#modal-add-add" class="modal-trigger"><i class="material-icons" title="Eg: Mumbai, India">info</i></a></label>
                </div>
            </div>
            <div class="col s6">
                <div class="input-field">
                    <input type="text" name="website"  value="">
                    <label>Website for more information about this idea <a href="#modal-add-web" class="modal-trigger"><i class="material-icons" title="Your website that has all the information about the Idea.">info</i></a></label> 
                </div>    
            </div> 

            <div class="col s12">
                <h5 style="">Your Team</h5>
                <div class="input-field">
                    <textarea name="team_overview" id="textarea1" class="materialize-textarea" value=""></textarea>
                    <label for="textarea1">Team Overview (Who is who) <a href="#modal-add-sum" class="modal-trigger"><i class="material-icons" title=" Details of your founders, co-founders, mentors and other important people in the company. Team size, roles in brief: Eg: We are team of 4 - one CTO, one CEO and two sales people working since 5 months">info</i></a></label>
                    
                </div>
            </div>

            <div class="col s12">
                
                <div class="input-field">
                    <input type="hidden" type="text" name="no_of_people"  value="">
                    <p>You can add your team members here and their linkedin profiles (optional)</p>
                </div>

                <div class="team_member_adding_form">
                    <div class="team-member" style="">
                        <div class="input-field">
                            <input type="text" name="team_members[name][]">
                            <label for="team_members[name][]">Full Name </label>
                        </div>
                        <div class="input-field">
                            <input type="email" name="team_members[email][]">
                            <label for="team_members[email][]">Email</label>
                            
                        </div>
                        <div class="input-field">
                            <input type="text" name="team_members[linkedin][]">
                            <label for="team_members[linkedin][]">Linkedin profile</label>
                        </div>
                        <div class="input-field">
                            <input type="text" name="team_members[position][]">
                            <label for="team_members[position][]">Position (eg: CFO, CSO, Manager)</label>
                        </div>
                        <div class="input-field">
                            <input type="text" name="team_members[brf-bio][]">
                            <label for="team_members[brf-bio][]">Brief Bio of the member</label>
                        </div>    
                        <a id="" class="btn red right btn_remove_team_member" href="#">Remove Member</a>
                    </div>

                    
                </div>

                    <a id="btn_add_more_team_members" class="btn green" href="#">Add more Members</a>
            </div>

            <div class="col s12">
                <h5 style="">Social presence of your Business Idea</h5>
            </div>
            <div class="col s12 m6">
                
                <div class="input-field">
                    <input type="text" name="facebook_link" title="Facebook Page" value="" required>
                    <label class="color-red">Facebook Page for your idea</label>
                </div>
            </div>
            <div class="col s12 m6">

                <div class="input-field">
                    <input type="text" name="linkedin_link" title="Linkedin Page" value="" required>
                    <label class="color-red">Linkedin Page for your idea</label>
                </div>
            </div>
            <div class="col s12 m6">
                <div class="input-field">
                    <input type="text" name="youtube_link"  value="">
                    <label>Youtube or Vimeo Video Link for your idea <a href="#modal-add-video" class="modal-trigger"><i class="material-icons" title="If you have any YouTube or Vimeo link of your product or services.">info</i></a></label>
                        <div id="modal-add-video" class="modal modal-add-idea">
                            <div class="modal-content">
                                <h4>Youtube or Vimeo Video Link</h4>
                                <p>If you have any YouTube or Vimeo link of your product or services.</p>
                            </div>
                            <div class="modal-footer">
                                <a href="#!" class="modal-action waves-effect waves-green btn-flat modal-add-bullet-close">Close</a>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col s12 m6">
                <div class="input-field">
                    <input type="text" name="twitter_link"  value="">
                    <label>Twitter Page for your idea</label>
                </div>
            </div>
            <div class="col s12 m6">
                <div class="input-field">
                    <input type="text" name="pinterest_link"  value="">
                    <label>Pinterest Page for your idea</label>
                </div>
            </div>
            <div class="col s12 m6">
                <div class="input-field">
                    <input type="text" name="link_other"  value="">
                    <label>Other Page Link for your idea</label>
                </div>
            </div>

            <div class="col s12">
                <a id="form_post_idea_btn" class="btn" type="submit" value="Create!">Add Idea</a><br><br>
                <a href="" title="Back" id="back_detail">Or go back to change details</a>
            </div>
        </div><!--mode detail ends here-->
    </form>

    <div class="container" id="message_idea_submit_success" style="display: none;">
        <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
            <div class="col s12">
                <h3 class="center color-teal"><i class="material-icons large">check_circle</i></h3>
                <h3 class="center">Success!</h3>
                <h5 class="center">Your Idea has been created successfully, go to <a href="/dashboard">Dashboard.</a></h5>
                <br><br>
            </div>
        </div>
    </div> <!-- End of Container -->
</div> <!-- DNE OF APP-->


<script>
$(document).ready(function(){

    $('.modal-trigger').leanModal();

    $("#sectorid").change(function(){
        var data=$(this).val();

        if(data==30)
        {
            $("#div_sector_other").slideDown();
        }
        else
        {
            $("#div_sector_other").slideUp();
        }
    });

    $("#more_info_sub").click(function (e) {
        e.preventDefault();

        var valid = 1;
        $("#prev_detail :input[required]").each(function (index) {
            if ($(this).val() == "") {
                Materialize.toast('Please fill the ' + $(this).attr('title'), 4000);
                valid = 0;
                return false;
            }
        });
        if (valid) {
            $("#prev_detail").fadeOut(400, function () {
                $("html,body").animate({
                    scrollTop: 0
                }, 200);
                $("#more_detail_extra_form").fadeIn();
            });

        }
    });

    $("#back_detail").click(function (e) {
        e.preventDefault();
        setTimeout(function () {
            $("#more_detail_extra_form").fadeOut(400,function () {
                $("html,body").animate({
                    scrollTop:0
                },200);
                $("#prev_detail").fadeIn();
            });
        });
    });

    $("#form_post_idea").on('submit',function(e){
        e.preventDefault();
        Materialize.toast("<span class='upload-status'>Hang on...!</span>");
        
        $.ajax({
            url: '/api2/ideas', // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {

                var obj=JSON.parse(data);
                if(obj.status=="success")
                {
                    //Materialize.toast("Idea Created Successfully",4000);
                    //setTimeout(function(){location.replace('/dashboard');},1000);
                    $("#more_detail_extra_form").fadeOut(400,function () {
                        $("html,body").animate({
                            scrollTop:0
                        },200);
                        $("#message_idea_submit_success").fadeIn();
                        $('.upload-status').parent().fadeOut();
                    });
                    localStorage.setItem('new_idea_created','true');
                }
                else{
                    Materialize.toast("Failure"+ obj.description,4000);
                }
            },
            error: function(data) {
                $('.upload-status').parent().fadeOut();
                Materialize.toast("Failure: "+ data.responseJSON.description,4000);
            }
        });
    });

    $("#form_post_idea_btn").on('click',(function(e) {
        e.preventDefault();
        var valid = 1;
        $("#more_detail_extra_form :input[required]").each(function (index) {
            if ($(this).val() == "") {
                Materialize.toast('Please fill the ' + $(this).attr('title'), 4000);
                valid = 0;
                return false;
            }
        });
        var idea_form=$("#form_post_idea");
        if (valid) {
            var url_pur="/api2/idea/urlavailability";
            // Check for url availability once again
            $.ajax({
                url:url_pur+"?url="+$("#unique_url").val(),
                type:"GET",
                success:function(data){
                    var obj=JSON.parse(data);
                    if(obj.status=="success"){
                        if(obj.available=="false"){
                            Materialize.toast("Sorry " + $("#unique_url").val() + " is taken!", 4000,'toastred');
                            //$("#unique_url").addClass('invalid');
                        }
                        else if(obj.available=="true")
                        {
                            $("#form_post_idea").submit();
                        }
                    }
                }
            });

        }
    }));

    var tabindex = 1;
    $('input,select,textarea').each(function() {
        if (this.type != "hidden") {
            var $input = $(this);
            $input.attr("tabindex", tabindex);
            tabindex++;
        }
    });

    $("#btn_add_more_team_members").click(function(e){
        e.preventDefault();
        $(".team-member").first().clone(true).appendTo(".team_member_adding_form").find('input').val('');
    });

    $(".btn_remove_team_member").click(function(e){
        e.preventDefault();
        if($(".team-member").length>1)
            $(this).parent().fadeOut(200,function(){$(this).remove()});
    });

    $("#logofile").change(function() {

        var file = this.files[0];
        var coverfile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((coverfile==match[0]) || (coverfile==match[1]) || (coverfile==match[2])))
        {

            Materialize.toast("Invalid image",4000)
            return false;
        }
        else
        {

            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);

        }
    });

    function imageIsLoaded(e) {
        $("#logofile").css("color","green");
        $('#logo-previewing').attr('src', e.target.result);
        $('#logo-previewing').attr('width', 'fixed');
        $('#logo-previewing').attr('height', 'auto');
    };

    $("#coverfile").change(function() {

        var file = this.files[0];
        var coverfile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((coverfile==match[0]) || (coverfile==match[1]) || (coverfile==match[2])))
        {

            Materialize.toast("Invalid image",4000)
            return false;
        }
        else
        {

            var reader = new FileReader();
            reader.onload = imageIsLoadedc;
            reader.readAsDataURL(this.files[0]);

        }
    });

    function imageIsLoadedc(e) {
        $("#coverfile").css("color","green");
        $('#cover-previewing').attr('src', e.target.result);
        $('#cover-previewing').attr('width', 'fixed');
        $('#cover-previewing').attr('height', 'auto');
    };

    $('#start_date').on('change',function(){
        start = $('#start_date').val();
        var b = start.split(/\D/);
        dt = new Date(b[0], --b[1], b[2]);
        dt.setMonth(dt.getMonth()+6);
        end = dt.toISOString().slice(0,10);
        $('.end-date').attr('min',end);
    });
    $('#end_date').on('change',function(){
        end = $('#end_date').val();
        var b = end.split(/\D/);
        edt = new Date(b[0], --b[1], b[2]);
        edt.setMonth(edt.getMonth()-6);
        end = edt.toISOString().slice(0,10);
        $('.start-date').attr('max',end);
    });
    $('#start_date,#end_date').on( "focusout", function(){
        start = $('#start_date').val();
        end = $('#end_date').val();
        console.log(start +' '+end)
        
        if(end.trim() && start.trim()){
            var one = Date.parse(start);
            var two = Date.parse(end);
            
            console.log(two-one);
            if((two-one) < 7776000000) {
                Materialize.toast('End date should be 6 months after Start date',6000);
            }
        }
    });
    $(".modal-add-bullet-close").click(function () {
        setTimeout(function () {
            $(".modal-add-idea").closeModal();
            $(".lean-overlay").remove();
        });
    });

    $("#tags").selectize({
        delimiter:',',
        persist:false,
        create: function(input) {
            return {
                value: input,
                text: input,
                label: input
            }
        },
        options: [
            <?php
            if(isset($suggestions)) {

                foreach($suggestions as $suggestion)
                {
                    $id=$suggestion['id'];
                    $value=$suggestion['value'];
                    echo "{id: $id ,value: '$value',label:'$value'},";
                }

            }
            if(isset($active) && $active !== '')
            {
                echo "{id: 1 ,value: '$active',label:'$active'},";
            }

            ?>

        ],
        valueField:'value',
        labelField:'value',
        searchField:'value',
        preload:true,
        openOnFocus: false,
        closeAfterSelect: true,
        addPrecedence: true,
    });

    $("#name").on('input',function(){
        var idea_name = $(this).val();
        idea_name = idea_name.replace(/\s+/g, '');
        idea_name = idea_name.replace(/\'+/g, '');
        idea_name = idea_name.replace(/-+/g, '');
        idea_name = idea_name.toLowerCase();
        $("#unique_url").val(idea_name);
        $("#unique_url").change();
    });
    
    var typingTimer;                //timer identified
    var doneTypingInterval = 400;  //time in ms
    var $input = $('#unique_url');

    $input.change(function () {
        console.log('inside');
        clearTimeout(typingTimer);
        if($("#unique_url").val()!=""){
            typingTimer = setTimeout(doneTyping, doneTypingInterval);

        }
    });
        
    $input.on('keyup',function () {
        console.log('inside');
        clearTimeout(typingTimer);
        if($("#unique_url").val()!=""){
            typingTimer = setTimeout(doneTyping, doneTypingInterval);

        }
    });

    $input.on('keydown', function(){
        clearTimeout(typingTimer);
    });

    function doneTyping () {
        //do something
        var url_pur = "/api2/idea/urlavailability";
        // Check for url availability once again
        if ($("#unique_url").val() != "") {
            $("#preloader_unique_url").slideDown(300);
            $.ajax({
                url: url_pur + "?url=" + $("#unique_url").val(),
                type: "GET",
                success: function (data) {
                    var obj = JSON.parse(data);
                    $("#preloader_unique_url").slideUp(300);
                    if (obj.status == "success") {
                        if (obj.available == "false") {
                            //Materialize.toast("Sorry " + $("#unique_url").val() + " is taken!", 4000,'toastred');
                            $("#unique_url").removeClass('valid');
                            $("#unique_url").addClass('invalid');




                        }
                        else if (obj.available == "true") {
                            //Materialize.toast($("#unique_url").val() + " is AVAILABLE!",4000, 'toastgreen');
                            $("#unique_url").removeClass('invalid');
                            $("#unique_url").addClass('valid');
                        }
                    }
                }
            });
        }
        else
            $("#preloader_unique_url").slideUp(300);
    }

    var chipdata=[];
    var obj={};
    $.each(obj,function(i,val){
        var temp={};
        temp.tag=val.tag;
        chipdata.push(temp);
    });
    console.log(chipdata);
    $('.chips-idea-tags-edit').material_chip({
        data:chipdata,
        placeholder: 'Enter a tag',
        secondaryPlaceholder: 'Type tag and press enter to address'
    });
    $("#idea_tags").val(JSON.stringify($('.chips-idea-tags-edit').material_chip('data')));
});
</script>