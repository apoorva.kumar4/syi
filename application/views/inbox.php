<div class="row margin50">
	<div class="col s12 marginauto floatnone">
		<h3 class="">Message Inbox 
			<a href="/messages/new" class="waves-effect waves-light btn btn1 right">New Message</a>
			<a href="/sentbox" class="linkfix right" style="padding:0 15px">Sentbox</a>
		</h3>
	</div>

	<div class="col s12 floatnone marginauto" style="">

		<ul class="collapsible" data-collapsible="expandable">
			<?php
			foreach($messages as $message)
			{
				$username=_get_username_from_socialid($message['sender']);
				if(date("F j, Y",strtotime("today")) == date("F j, Y",strtotime($message['timestamp'])))
				{
					$human_date="Today at ". date("h:ia",strtotime($message['timestamp']));
				}
				else if(date("F j, Y",strtotime("yesterday")) == date("F j, Y",strtotime($message['timestamp'])))
				{
					$human_date="Yesterday at ". date("h:ia",strtotime($message['timestamp']));
				}
				else
				{
					$human_date=date("F jS, Y",strtotime($message['timestamp']));
					
				}
				

				?>
				<li>
					
		      		<div class="collapsible-header"><i class="material-icons">chat</i>From: <a href="/users/<?php echo $username?>" class="username tooltipped" data-position="right" data-delay="50" data-tooltip="<?php echo $username?>"><?php echo _f_get_firstname_from_socialid($message['sender'])?></a><span class="hide-on-small-only	"style="float:right"><?php echo $human_date?></span></div>
		      		<div class="collapsible-body"><p><?php echo $message['message']?></p>
		      			<div class="btn-inbox-message-action" style="margin:10px 30px">
		      			
		      			<a class="btn btn-x waves gren" href="/messages/new/<?php echo $message['sender']?>">Reply</a>
		      			<a class="btn btn-x waves red messagedelete" data-hrefx="/api/v1/users/<?php echo $userid?>/messages/<?php echo $message['id']?>/delete">Delete</a>
		      			</div>

		      			
		      		</div>

		    	</li>
				<?php

			}
			?>
		</ul>
	</div>
</div>
