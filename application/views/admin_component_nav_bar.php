<nav-bar inline-template  :uploads="uploads" :users="users" :socialid="my_socialid" :unreadmessages="count_unread_messages">
    <!-- <ul id="slide-out" class="side-nav fixed fixedc"> -->
    <ul id="slide-out" class="sidenav sidenav-fixed">
        <li>
            <div class="user-view" style="background-color:#007ee6;">
                <div class="background">
                </div>
                <a href="#!user">
                    <img class="circle" v-bind:src="get_profile_picture_url(socialid)">
                </a>

                <a href="#!name">
                    <span class="white-text name" v-if="typeof users[socialid] !== 'undefined'" v-text="users[socialid].first_name"></span>
                </a>
                <a class="" href="/admin/admin_profile"><span class="white-text email">Go to Profile</span></a>
                <a class="" href="/logout"><span class="white-text email">Logout</span></a>
            </div>
        </li>
        <li>
            <a class="waves" href="/admin/dashboard"><i class="material-icons">dashboard</i>Dashboard</a>
        </li>
        <li v-if="socialid==='1'">
            <a class="waves" href="/admin/manage_admins"><i class="material-icons">assignment_ind</i>Manage Admins</a>
        </li>
        <li>
            <a class="waves" href="/admin/registrations"><i class="material-icons">people</i>All Users</a>
        </li>
        <li>
            <a class="waves" href="/admin/inactive_users"><i class="material-icons">perm_identity</i>Inactive Users</a>
        </li>
        <li>
            <a class="waves" href="/admin/recent_activity"><i class="material-icons">history</i>Recent Activity</a></li>
        <li>
            <a class="waves" href="/admin/analytics"><i class="material-icons">trending_up</i>Analytics</a>
        </li>
        <li>
            <a class="waves" href="/admin/about_us"><i class="material-icons">info_outline</i>About Us</a>
        </li>
        <li>
            <a class="waves" href="/admin/ideas"><i class="material-icons">lightbulb_outline</i>Ideas</a>
        </li>
        <li>
            <a class="waves" href="/admin/followed_ideas"><i class="material-icons">stars</i>Followed Ideas</a>
        </li>
        <li>
            <a class="waves" href="/admin/top_ideas"><i class="material-icons">star</i>Top Ideas</a>
        </li>
        <li>
            <a class="waves" href="/admin/messages" style="position:relative"><i class="material-icons">chat</i>Messages <span v-if="unreadmessages" style="float: right;background-color: rgb(0, 126, 230);height: 23px;position: absolute;right: 20px;top: 10px;line-height: 23px;border-radius: 22px;width: 23px;text-align: center;color: white;font-size: 0.8rem;">{{unreadmessages}}</span></a>
        </li>
        <li>
            <a class="waves" href="/admin/email"><i class="material-icons">email</i>Email</a>
        </li>
        <li>
            <a class="waves" href="/admin/bulk_email"><i class="material-icons">rss_feed</i>Bulk Email</a>
        </li>
        <li>
            <a class="waves" href="/admin/testimonials"><i class="material-icons">format_quote</i>Testimonials</a>
        </li>
        <li>
            <a class="waves" href="/admin/registrations/ideators/"><i class="material-icons">supervisor_account</i>Ideators</a>
        </li>
        <li>
            <a class="waves" href="/admin/registrations/service_providers"><i class="material-icons">supervisor_account</i>Service Providers</a>
        </li>
        <li>
            <a class="waves" href="/admin/registrations/investors"><i class="material-icons">supervisor_account</i>Investors</a>
        </li>
        <li>
            <a class="waves" href="/admin/events"><i class="material-icons">event</i>Events</a>
        </li>
        <li>
            <a class="waves" href="/admin/faq"><i class="material-icons">record_voice_over</i>FAQ's</a>
        </li>
        <li>
            <a class="waves" href="/admin/sectors"><i class="material-icons">public</i>Sectors</a>
        </li>
        <li>
            <a class="waves" href="/admin/partners"><i class="material-icons">business_center</i>Partners</a>
        </li>
    </ul>
</nav-bar>