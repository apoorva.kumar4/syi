<script>
    jQuery(document).ready(function($) {
        $("#uploadImage").on('submit', (function(e) {
            e.preventDefault();
            Materialize.toast("<span class='upload-status'>Uploading...</span>", 4000);
            var url_pur = "/api2/user/profile-picture/";

            $.ajax({
                url: url_pur, // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false
                success: function(data) // A function to be called if request succeeds
                {

                    if (data.status == "success") {
                        console.log(data);
                        Materialize.toast("File Uploaded Successfully", 4000);
                        $(".dynamic-pp").attr('src',data.url);
                        $("#change-profile-modal").closeModal();
                    } else {
                        Materialize.toast("Upload Failed: " + data.description, 4000);
                    }
                }
            });
        }));

        $("#remove-pp").click(function(e) {
            e.preventDefault();
            Materialize.toast("<span class='upload-status'>Removing Your Display Picture...</span>", 4000);
            var url_pur = "/api2/user/profile_picture";
            console.log(url_pur);

            $.ajax({
                url: url_pur, // Url to which the request is send
                type: "DELETE", // Type of request to be send, called as method
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false
                success: function(data) // A function to be called if request succeeds
                    {
                        
                        if (data.status == "success") {
                            Materialize.toast("Display Picture Removed Successfully", 4000);
                            $(".dynamic-pp").attr('src',data.gravatar_url);
                        } else {
                            Materialize.toast("Display Picture Removal Failed: " + data.description, 4000);
                        }
                    }
            });

        });

        $("#year_of_graduation").keypress(function (e) {
            if(e.which == 45 || e.which == 43)
            {

            }
            else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //if the letter is not digit then display error and don't type anything
                    return false;
                };
            }

        });
        $("#mobile").keypress(function (e) {
            if(e.which == 45 || e.which == 43)
            {

            }
            else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //if the letter is not digit then display error and don't type anything
                    return false;
                };
            }

        });

        $('#form-profile-edit').on('submit', function (e) {

            e.preventDefault();
            Materialize.toast("<p class='sp_pw'>Please Wait...</p>");
            var url_pur="/api2/user4/";
            $.ajax({
                type: 'PUT',
                url: url_pur,
                data: $('form').serialize(),
                success: function (data) {
                    console.log(data);

                    if(data.status=="success")
                    {
                        $(".sp_pw").text("Successfully Saved");
                        setTimeout(function(){
                            $(".sp_pw").parent().fadeOut();
                        },6000);
                        if(data.profile_completeness==100) {
                            $(".sp_pw").html('Your profile is 100% Complete! <br>This increases your chances of being contacted by Service Providers and Investors');
                        }
                    }
                    else
                    {
                        if(data.status=="failure")
                        {
                            // Materialize.toast("Failure: "+obj.description,4000);
                            $(".sp_pw").text("Failure: "+obj.description);
                            setTimeout(function(){
                                $(".sp_pw").parent().fadeOut();
                            },3000);

                        }
                        else
                        {
                            Materialize.toast("Error: Please check your changes.",4000);
                        }

                    }
                },
                error: function(xhr)
                {
                    obj=JSON.parse(xhr.responseText);
                    $(".sp_pw").text(obj.description.capitalizeFirstLetter());
                    setTimeout(function(){
                        $(".sp_pw").parent().fadeOut();
                    },3000);
                }

            });

        });
    });
</script>
<div class="container" style="margin-bottom:70px">
    <div class="row" style="margin:0 auto !important;">
            <div class="col s12 m4" style="text-align: center">

                <div id="change-profile-modal" class="modal bottom-sheet modal95">
                    <div class="modal-content">

                        <div class="row">
                            <div class="col s12 m12 l12 ">
                                <h4>Set a new Profile Picture</h4>
                                <p>Max File upload size is 2 Mb. Recommended Resolution 250x250px.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <img id="previewing" width="250px" height="250px" src="<?php echo $user['profile_picture_url'] ?>">

                            </div>
                            <div class="col s12">

                                <form id="uploadImage" action="/tempimage">
                                    <div class="file-field input-field">


                                        <input id="file" type="file" name="file">

                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Select File">
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <a href="#!" id="update-image-submit" class="modal-action waves-effect waves btn green">Save</a>
                        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>


                    </div>
                </div>
                <div class="" style="margin-bottom:20px;">
                    <img class="dynamic-pp" data-userid="<?php echo $user['id']?>" style="" src="<?php echo $me[0]['profile_picture_url'] ?>" width="200px" alt="">
                    <p><a class="waves-effect white modal-trigger" style="" href="#change-profile-modal">Change Profile Picture</a></p>
                <?php _f_dynamic_pp_remove($user['socialid']);?>
                <a class="" style="" href="/users/<?php echo $user['username']?>">View My Public Profile</a></p>

                </div>
            </div>
            <div class="col s12 m8">
                <!-- <h3 id="" class="activator grey-text text-darken-4" style="padding:0 0.75rem"><?php echo $user['first_name']." ".$user['last_name']?></h3> -->

                <form id="form-profile-edit" method="POST">
                    <div class="input-field col s12">
                        <input id="name" name="first_name" type="text" required value="<?php echo $user['first_name']?>">
                        <label for="name">First Name</label>
                    </div>

                    <div class="input-field col s12">

                        <input id="lastname" name="last_name" type="text" required value="<?php echo $user['last_name']?>">
                        <label>Last Name</label>
                    </div>
                
                    <div class="input-field col s12">

                        <input name="email" type="email" value="<?php echo $user['email']?>"><br><br>
                        <label for="disabled">Email</label>
                    </div>

                    <div class="input-field col s12">

                        <input name="username" type="text" value="<?php echo $user['username']?>"><br><br>
                        <label for="disabled">Username</label>
                    </div>

                    <div class="input-field col s12">

                        <input name="mobile" disabled type="tel" required value="<?php echo $user['mobile']?>" maxlength="16" pattern="^{\+0-9]*$" title="Please enter valid number" id="mobile"><br><br>
                        <label>Mobile</label>
                    </div>
                
                    <div class="input-field col s12">

                        <input name="institute" type="text" value="<?php if(array_key_exists('institute',$user))echo $user['institute']?>"><br><br>
                        <label>Institute</label>

                    </div>

                    <div class="input-field col s12">

                        <input name="year_of_graduation" type="text" maxlength="4" pattern="^[0-9]*$" id="year_of_graduation" value="<?php 
                        if(array_key_exists('year_of_graduation', $user)) echo $user['year_of_graduation']?>"><br><br>
                        <label>Year of Graduation</label>
                    </div>
               
                    <div class="input-field col s12">

                        <input name="role" type="text" value="<?php echo $user['role']?>"><br><br>
                        <label>Role</label>
                    </div>

                    <div class="input-field col s12">

                        <input name="location" type="text" value="<?php echo $user['location']?>"><br><br>
                        <label>Location</label>
                    </div>
                
                    <div class="input-field col s12">


                        <?php _f_dynamic_select_sector('sector_expertise',$user['sector_expertise']) ?>
                        <label>Sector Expertise</label>
                    </div>

                    <div class="input-field col s12">
                        <select name="student_alumni" id="student_alumni">
                        <?php
                            $array_opt=array('Student','Alumni');
                            foreach ($array_opt as $option)
                            {
                                if(strcasecmp($option,$user['student_alumni'])==0)
                                {
                                    echo "<option value='$option' selected>$option</option>";
                                }
                                else{
                                    echo "<option value='$option'>$option</option>";
                                }
                            }
                        ?>
                        </select>
                        <label for="student_alumni">Student or Alumni</label>
                    </div>
                

                    <div class="input-field col s12">
                        <input type="text" name="linkedin_username"  value="<?php echo $user['linkedin_username']?>"><br><br>
                        <label>LinkedIn Link</label>
                    </div>

                    <div class="input-field col s12">
                        <input type="text" name="facebook_username"  value="<?php echo $user['facebook_username']?>"><br><br>
                        <label>Facebook Username</label>
                    </div>
                    <div class="input-field col s12">
                        <input type="text" name="twitter_username" value="<?php echo $user['twitter_username']?>"><br><br>
                        <label>Twitter Username</label>
                    </div>

                    <div class="input-field col s12">
                        <input type="text" name="google_username" value="<?php echo $user['google_username']?>"><br><br>
                        <label>Google+ Username</label>
                    </div>

                    <div class="input-field col s12">
                        <input class="btn" type="Submit" value="Save Changes"><br><br>
                        
                    </div>

                    <div class="input-field col s12">
                        <h4>Delete Account?</h4>
                        <p>To proceed with account deletion, please click <a href="/delete" class="red-text">here.</a></p>
                    </div>
                
                </form>
            </div>
    </div>
