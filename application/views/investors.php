<div class="container">
	<div class="row">
		<div class="col s12 m12 l12">
			<h4 class="text-teal mtb30">Investors</h4>
		</div>
	</div>
	<div class="row">

		<?php 
        $this->load->helper('currency');
        foreach($investors as $iv)
		{
            $iv_extra=_f_get_investor_extra_details($iv['socialid']);

		?>
       		<div class="col s12 m6 l4">
                <div class="card">
                    <div class="card-content card-content-investors min350">
                        <div>
                        <ul class="collection">
                            <li class="collection-item avatar">
                                <img src="<?php echo $iv['profile_picture_url']?>" alt="" class="circle">
                                <span class="title"><a href="/users/<?php echo $iv['socialid']?>"><?php echo $iv['first_name']?> <?php echo $iv['last_name']?></a></span>

                                <?php
                                if($iv['verified_profile']==1)
                                {
                                    echo "<i class='material-icons right' title='Verified by SaYourIdeas' style='color: #009be5 !important;'>verified_user</i>";
                                }
                                else{

                                }
                                ?>

                                <p><?php echo $iv['location']?><br><?php echo _get_sector_name_from_id($iv['sector_expertise'])?></p>

                            </li>
                            </ul>
                        </div>
                       

                        <table>
                            <?php

                                if((isset($iv_extra[0]['investment_range_start']) && isset($iv_extra[0]['investment_range_end'])) && ($iv_extra[0]['investment_range_end']!=0) &&($iv_extra[0]['investment_range_start']!=0))
                                {
                                    ?>

                                    <tr>
                                        <td>Investment Range (<?php echo currency_formal_to_sign($iv_extra[0]['currency'])?>)</td>
                                        <td><p><?php
                                                echo _f_number_to_words($iv_extra[0]['investment_range_start'])." to "._f_number_to_words($iv_extra[0]['investment_range_end']);
                                                ?></p></td>


                                    </tr>

                                    <?php
                                }
                            ?>



                           
                            <tr>
                                <td>Sectors</td>
                                <td><p><?php 
                                

                                    
                                    $sectors=_f_get_list_of_investor_sectors($iv['socialid']);
                                    $counter=0;
                                    foreach($sectors as $sector)
                                    {
                                        if($counter<5)
                                        {
                                            $name=$sector['name'];
                                             echo "<span>".$sector['name']."</span><br>";
                                        }
                                        $counter++;
                                    }
                                    if($counter==0)
                                    {
                                        echo "N/A";
                                    }


                                ?></p></td>
                            </tr>
                        </table>
                    </div>
                </div>    
			</div>
		<?php
		}
		?>
	
	</div>
	
							        
