<div id="app_user_info_ideator" class="">
    <div id="modal_delete_user" class="modal">
        <div class="modal-content">
            <h4>Delete {{user.first_name}} {{user.last_name}}?</h4>
            <p>This action is <span class="red-text">IRREVERSIBLE. </span> Once the user is deleted, all data associated with the user will be permanently lost.</p>
        </div>
        <div class="modal-footer">
            <a v-on:click="user_delete" href="#!" class="modal-action waves-effect waves-red btn red">Yes Delete</a>
            <a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">No</a>
        </div>
    </div>
<div class="col s12 m12 l10">
    <div class="row center">
        <div class="col s12">
            <h3 class="admin-title">User Info</h3>
        </div>
    </div>
    <div class="">
        <div class="row view-user-cover_pic">
    
            <div class="col s12 m3" style="margin-bottom:20px;">
        
                <img class="dynamic-pp card" style="display:inline-block;margin:25px" v-bind:src="user.profile_picture_url" width="200px" alt="">
            </div>
            <div class="col s12 m6">
                <h3 class="view-user-username">{{user.first_name}} {{user.last_name}}
                
                <i v-if="user.verified_profile=='1'" class='material-icons' title='Verified by SaYourIdeas' style='color: #009be5 !important;font-size: 3rem !important;'>verified_user</i>
                <p class="user_profile-user_type">Investor</p></h3>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <button v-on:click="user_unblock" v-if="user.blocked=='1'" id="reg-user-block" class="btn red" data-action="unblock"><i class='material-icons left'>lock_open</i>Unblock User</button>
                <button v-on:click="user_block" v-else id="reg-user-block" class="btn red" data-action="block"><i class='material-icons left'>lock</i>Block User</button>
                <a class="btn red modal-trigger" href="#modal_delete_user">Delete User</a>
                <button v-on:click="user_verify" class="btn btn-blue" v-if="user.verified_profile!='1'">Mark as Verified</button>
                <button id="reg-user-approve" v-if="user.admin_approved=='0'" v-on:click="user_approve" class="btn btn-blue">Approve Investor</button>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <table class="highlight ideator_table">
                    <tr>
                        <td>Mobile</td>
                        <td><span class="notranslate">{{user.mobile}}</span></td>
                    </tr>
                    <tr>
                        <td>Username</td>
                        <td><span class="notranslate">{{user.username}}</span></td>
                    </tr>
                    <tr>
                        <td>Member Since</td>
                        <td>{{user.timestamp}}</td>
                    </tr>
                    <tr>
                        <td>Role</td>
                        <td>{{user.role}}</td>
                    </tr>
                    <tr>
                        <td>Sector Expertise</td>
                        <td>{{sectors[user.sector_expertise]}}</td>
                    </tr>
                    <tr>
                        <td>Industry</td>
                        <td>{{user.industry}}</span></td>
                    </tr>
                    <tr>
                        <td>Company</td>
                        <td>{{user.company}}</td>
                    </tr>
                    <tr>
                        <td>Working Since</td>
                        <td>{{user.working_since}}</td>
                    </tr>
                    <tr>
                        <td>Investment Start</td>
                        <td>{{user.investment_range_start}}</td>
                    </tr>
                    <tr>
                        <td>Investment End</td>
                        <td>{{user.investment_range_end}}</td>
                    </tr>
                    
                    <tr>
                        <td>Professional Background</td>
                        <td>{{user.professional_background}}</td>
                    </tr>
                    <tr>
                        <td>Alumni Network</td>
                        <td>{{user.alumni_network}}</td>
                    </tr>
                    <tr>
                        <td>Investment Network</td>
                        <td>{{user.investment_network}}</td>
                    </tr>
                    <tr>
                        <td>Members of Organisation</td>
                        <td>{{user.members_of_organisation}}</td>
                    </tr>
                    <tr>
                        <td>Sample companies invested</td>
                        <td>{{user.sample_companies_invested}}</td>
                    </tr>
                    <tr>
                        <td>Would Like to mentor startups</td>
                        <td>{{user.would_like_to_mentor_startups}}</td>
                    </tr>
                    <tr>
                        <td>Currently associated with as a mentor</td>
                        <td>{{user.companies_associated_with_as_a_mentor}}</td>
                    </tr>
                    <tr>
                        <td>Areas of Expertise/ How do I add value</td>
                        <td>{{user.areas_of_expertise_how_do_i_add_value}}</td>
                    </tr>

                    
                </table>
            </div>
            <div class="col s12">
                <h4>Social Links</h4>
                <table class="highlight ideator_table">
                    <tr>
                        <td>Facebook</td>
                        <td><span class="notranslate"><a v-bind:href="user.facebook_username">{{user.facebook_username}}</a></span></td>
                    </tr>
                    <tr>
                        <td>Twitter</td>
                        <td><span class="notranslate"><a v-bind:href="user.twitter_username">{{user.twitter_username}}</a></span></td>
                    </tr>
                    <tr>
                        <td>Linkedin</td>
                        <td><span class="notranslate"><a v-bind:href="user.linkedin_username">{{user.linkedin_username}}</a></span></td>
                    </tr>
                    <tr>
                        <td>Google+</td>
                        <td><span class="notranslate"><a v-bind:href="user.google_username">{{user.google_username}}</a></span></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</div>