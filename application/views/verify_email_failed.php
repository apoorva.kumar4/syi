<div class="container">
	<div class="row card text-center login-card" style="max-width:400px;margin:0 auto !important;">
		<form method="POST" class="col s12" action="/verify-email">
			<div class="row text-center">
				<p style="margin:10px">You entered a wrong code, please enter the correct one below:</p>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input id="code" name="code" type="text" required>
					<label for="name">Code here</label>
				</div>
			</div>
			<input class="btn" type="Submit" value="Verify">
			<a href="/dashboard" style="margin:10px">I'll do this later</a>
		</form>
	</div>
</div>
</section>