<style>
	@media screen and (min-width: 768px) {
		#col_invesor_form {
		padding:20px 250px;
		}
	}
</style>
<script>
jQuery(document).ready(function($){
	$('#form-profile-edit').on('submit', function (e) {

        e.preventDefault();
        Materialize.toast("<span class='sp_pw'>Please Wait...</span>");
        var url_pur="/api2/user";
        $.ajax({
            type: 'PUT',
            url: url_pur,
            data: $('form').serialize(),
            success: function (data) {
                console.log(data);

                if(data.status=="success")
                {
                   	$(".sp_pw").text("Success!");
                   	document.location.reload();
                    


                }
                else
                {
                    if(data.status=="failure")
                    {
                        //Materialize.toast("Failure: "+obj.description,4000);
                        $(".sp_pw").text("Failure: "+obj.description);
                        setTimeout(function(){
                        	$(".sp_pw").parent().fadeOut();
                    	},3000);

                    }
                    else
                    {
                        
                        $(".sp_pw").text("Unknown error occurred, please try again.");
                        setTimeout(function(){
                        	$(".sp_pw").parent().fadeOut();
                    	},3000);
                    }

                }
            },
            error: function(xhr)
            {
                obj=JSON.parse(xhr.responseText);
                $(".sp_pw").text(obj.description.capitalizeFirstLetter());
                setTimeout(function(){
                    $(".sp_pw").parent().fadeOut();
                },3000);
            }
        });

    });
});
</script>
<div class="row">
	<div id="col_invesor_form" class="col s12">
		<div class="form">
			<div class="head-wrapper">
				<h5>Hi, <?php echo $usss['first_name']?>.</h5>
				<p class="red-text">Investor Accounts go through an approval. We request you to fill out the below form to ease the process.</p>
				<form id="form-profile-edit" class="" method="POST" action="">
					<div class="input-field col s12">
						<input type="email" name="email" required />
						<label class="text-red" for="email">Email*</label>
					</div>
					<div class="input-field col s12">
                        <input name="location" type="text"  value="" required><br><br>
                        <label>Location*</label>
                    </div>
                    <div class="input-field col s12">
                        <input name="industry" type="text"  value=""><br><br>
                        <label>Industry</label>
                    </div>
                    <div class="input-field col s12">
                        <input class="wrk_year" name="working_since" type="text" id="wrk_year" maxlength="4" pattern="^[0-9]*$"  value=""><br><br>
                        <label>Working Since year: eg 2010</label>
                    </div>
                    <div class="input-field col s12">
                        <input name="company" type="text"  value="" required><br><br>
                        <label>Company*</label>
                    </div>
                    <div class="input-field col s12">
                        <input name="role" type="text"  value=""><br><br>
                        <label>Designation</label>
                    </div>
                    <div class="input-field col s12">
                        <input name="investment_range_start" type="text"  value=""><br><br>
                        <label>Investment Start</label>
                    </div>

                    <div class="input-field col s12">
                        <input name="investment_range_end" type="text"  value=""><br><br>
                        <label>Investment Upto</label>
                    </div>
                    <div class="input-field col s12">
                        <?php _f_dynamic_select_sector('sector_expertise',0) ?>
                        <label>Sector Expertise</label>
                    </div>
                    <div class="input-field col s12">
                        <input name="sector_other" type="text" value=""/> 
                        <label>If Others, please mention:</label>
                    </div>
                    <div class="input-field col s12">
                        <input name="professional_background" type="text"  value="" required><br><br>
                        <label>Professional Background*</label>
                    </div>
                    <div class="input-field col s12">
                        <input name="alumni_network" type="text"  value=""><br><br>
                        <label>Alumni Network</label>
                    </div>

                    <div class="input-field col s12">
                        <input name="investment_network" type="text"  value=""><br><br>
                        <label>Part of Investment Network</label>
                    </div>
                    <div class="input-field col s12">
                        <input name="members_of_organisation" type="text"  value=""><br><br>
                        <label>Members of Organization</label>
                    </div>

                    <div class="input-field col s12">
                        <input name="sample_companies_invested" type="text"  value=""><br><br>
                        <label>Sample Companies Invested in</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <input type="text" name="linkedin_username"  value="" required><br><br>
                        <label>LinkedIn Profile Link*</label>
                    </div>

                    <div class="input-field col s12 m6">
                        <input type="text" name="facebook_username"  required value=""><br><br>
                        <label>Facebook Username*</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <input type="text" name="twitter_username" value=""><br><br>
                        <label>Twitter Username</label>
                    </div>

                    <div class="input-field col s12 m6">
                        <input type="text" name="google_username" value=""><br><br>
                        <label>Google+ Username</label>
                    </div>
                    <div class="input-field col s12">
                            <input name="companies_associated_with_as_a_mentor" type="text" value=""><br><br>
                            <label>Companies Associated with as a Mentor</label>
                    </div>

                    <div class="input-field col s12">
                        <input name="areas_of_expertise_how_do_i_add_value" type="text" value=""><br><br>
                        <label>Areas of Expertise/How I add value</label>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input name="allow_direct_contact" type="checkbox" id="test5" value="Yes"/>

                            <label for="test5">Allow Startups to contact me directly</label>
                        </div>

                        <div class="input-field col s12">
                            <input name="would_like_to_mentor_startups" onchange="valueChanged()" type="checkbox" id="coupon_question" value="Yes"/>
                            <label for="coupon_question">I would like to mentor the start-ups</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 bottom-fix-12">
                            <p>Select your target sectors (Maximum: 10)</p>

                            <?php _f_dynamic_checkbox_sectors('service-target-sectors',[],'');?>
                        </div>
                    </div>
                    <div class="input-field col s12">
                    	<input class="btn" type="Submit" value="Submit">
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>