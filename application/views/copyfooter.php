</main>
            <footer class="page-footer">
          <div class="container">
            <div class="row" style="padding-bottom:40px; border-bottom:1px solid #e3e3e3;">
              
              <div class="col s12 m4">
                <h5 class="">SaYourIdeas</h5>
                <p class=" text-lighten-4">Emerging platform for the Ideators, Investors and Service Providers.<br></p>
              </div>
              <div class="col s12 m3 l2 offset-l2">
                <h5 class="">Company</h5>
                <ul>
                <li><a class=" text-lighten-3 hover-links" target="_blank" href="/about" onmouseover='this.style.textDecoration="underline"'
                       onmouseout='this.style.textDecoration="none"'>About</a></li>
                </ul>
              </div>
              <div class="col s12 m3 l2">
                <h5 class="">Connect</h5>
                <ul>
                  <li><a class=" text-lighten-3 hover-links" target="_blank" href="//facebook.com/sayourideas" onmouseover='this.style.textDecoration="underline"'
                         onmouseout='this.style.textDecoration="none"'>Facebook</a></li>
                  <li><a class=" text-lighten-3 hover-links" target="_blank" href="//twitter.com/sayourideas" onmouseover='this.style.textDecoration="underline"'
                         onmouseout='this.style.textDecoration="none"'>Twitter</a></li>
                  <li><a class=" text-lighten-3 hover-links" target="_blank" href="https://www.linkedin.com/company/sayourideas" onmouseover='this.style.textDecoration="underline"'
                         onmouseout='this.style.textDecoration="none"'>LinkedIn</a></li>
                  <li><a class=" text-lighten-3 hover-links" target="_blank" href="mailto:info@sayourideas.com" onmouseover='this.style.textDecoration="underline"'
                         onmouseout='this.style.textDecoration="none"'>Email</a></li>
                </ul>
              </div>

              <div class="col s12 m3 l2">
                <h5 class="">Support</h5>
                <ul>
                  <li><a class=" text-lighten-3 hover-links" href="/contact-us" onmouseover='this.style.textDecoration="underline"' onmouseout='this.style.textDecoration="none"'>Contact Us</a></li>
                  <li><a class=" text-lighten-3 hover-links" href="/faq" onmouseover='this.style.textDecoration="underline"'
                           onmouseout='this.style.textDecoration="none"'>FAQ's</a></li>
                  <li><a class=" text-lighten-3 hover-links" href="/terms" onmouseover='this.style.textDecoration="underline"'
                         onmouseout='this.style.textDecoration="none"'>Terms</a></li>
                  <li><a class=" text-lighten-3 hover-links" href="/privacy_policy" onmouseover='this.style.textDecoration="underline"'
                         onmouseout='this.style.textDecoration="none"'>Privacy Policy</a></li>
                  
                  
                    
                </ul>
              </div>
              
              
            </div>
            <div class="row">
              <div class="col s12">
                <div class="" style="margin-top:0px;font-weight:400;">© 2016 SaYourIdeas.com</div>
              </div>
            </div>
          </div>
          
        </footer>
  <script src="/js/masonry.pkgd.min.js"></script>
	<script src="/js/jquery.flexslider-min.js"></script>
	<script src="/js/testimonial.js"></script> 
	<script src="/js/clipboard.min.js"></script>
	<script src="/js/clipboard-custom.js"></script>
	<script src="/js/calendar.js"></script>

</body>
</html>
