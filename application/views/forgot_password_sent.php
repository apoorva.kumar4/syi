<style>
    main{
        background-color:#F6F9FC;
    }
</style>
<div class="container">
    <div class="row card div_sign_up_form" style="float:none;margin:auto;max-width: 400px;padding:20px">
        <form method="POST" action="/forgot/chk">
        
               	<div class="col s12">
                	<h4 class="center">Reset Password</h4>
                	<p class="center">Please input the OTP we sent on your mobile.</p>
            	</div>
                <div class="input-field col s12">
                    <input name="otp" type="number" required>
                    <label>OTP</label>
                </div>
                <div class="input-field col s12">
                	<input id="new_password" name="new_password" type="password" required />
                	<label for="new_password">New Password</label>
                </div>
                <input type="hidden" name="mobile" value="<?php echo $mobile?>" />
                <div class="input-field col s12">
                	<input class="btn blue fwidth" type="Submit" value="Submit"><br>
                </div>         
                
        </form>
    </div>
</div>
</section>