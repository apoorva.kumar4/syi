<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/fontawesome-stars-o.css">
<script src="/js/jquery.barrating.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
      $('.rating-holder').each(function(index,value){
      	var rating = $(this).attr('data-rating');
      	$(this).barrating({
        theme: 'fontawesome-stars-o',
        initialRating:rating,
        showValues: false,
        readonly: true,
      	});
      });
   });
</script>
<div class="row" style="margin-top:100px">
	<div class="col s12 m8 marginauto floatnone">
		<h3>Reviews</h3>
	</div>
</div>
<div class="row">
	<div class="col s12 m8 floatnone marginauto" style="">

		<style>
		.btn-inbox-message-action {
    		margin: 10px 25px;
		}
		</style>


	<?php
	if(!$reviews) {
		?>
		<p>Hey, get more and more reviews for your services by asking to give good reviews from your past clients.<br> More the reviews, better the opportunities for getting new clients.</p>
		<?php
	} else {
		?>
		<ul class="collapsible" data-collapsible="expandable">
		<?php
		
		foreach($reviews as $review)
		{
			$username=_get_username_from_socialid($review['reviewer']);
			if(date("F j, Y",strtotime("today")) == date("F j, Y",strtotime($review['date'])))
			{
				$human_date="Today at ". date("h:ia",strtotime($review['date']));
			}
			else if(date("F j, Y",strtotime("yesterday")) == date("F j, Y",strtotime($review['date'])))
			{
				$human_date="Yesterday at ". date("h:ia",strtotime($review['date']));
			}
			else
			{
				$human_date=date("F jS, Y",strtotime($review['date']));
				
			}
			

			?>
			<li>
				
	      		<div class="collapsible-header"><i class="material-icons">chat</i>From: <a href="/users/<?php echo $username?>" class="username tooltipped" data-position="right" data-delay="50" data-tooltip="<?php echo $username?>" style="padding: 0px 10px 0px 0px;"><?php echo _f_get_firstname_from_socialid($review['reviewer'])?></a>

	      			 <select class="rating-holder browser-default" name="rating"  data-rating="<?php echo $review['rating']?>">
			              <option value="1">1</option>
			              <option value="2">2</option>
			              <option value="3">3</option>
			              <option value="4">4</option>
			              <option value="5">5</option>
	              		</select>

	      		<span class="hide-on-small-only	"style="float:right"><?php echo $human_date?></span></div>
	      		<div class="collapsible-body"><p><?php echo $review['description']?></p>
	      			<div class="btn-inbox-message-action">
	      			<a class="btn btn-x waves gren" href="/messages/new/<?php echo $review['reviewer']?>">Reply</a>
	      			</div>

	      			
	      		</div>

	    	</li>
			<?php

		}
	}
	?>
</ul>
</div>
</div>