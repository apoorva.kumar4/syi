<?php $this->load->view('templates/admin_head');?>
<div id="admin_app">
    <div class="app-wrapper">
        <div id="admin-nav" class="nav-wrapper">
            <a id="admin-menu-icon" href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
        <div class="row fwidth">
            <div class="col s12 m4 l2">
                <?php require('admin_component_nav_bar.php');?>
            </div>
            <div class="col s12 m12 l10 right">
                <div class="container">
                    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
                        <div class="col s12">
                            <h3 class="center color-yellow"><i class="material-icons large">error</i></h3>
                            <h3 class="center">An error occurred!</h3>
                            <p class="center"><?php echo $error?></p>
                            <p class="center">An Error Occurred please try again!</p>
                            <br><br>
                        </div>
                    </div>
                </div> <!-- End of Container -->
            </div>

        </div>
    </div>
</div>

<?php $this->load->view('admin_footer');?>