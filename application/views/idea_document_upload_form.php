<div class="container" id="document_upload_parent_of_all">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <h3 class="center">Upload a new Document for <?php echo $idea_name?></h3>
            <div class="center red-text">
                <?php echo validation_errors();?>
            </div>

        </div>


        <form id="upload_idea_document" method="POST" enctype="multipart/form-data">
        <div class="row custom">
            <input type="hidden" name="ideaid" value="<?php echo $idea_id?>">
            <div class="input-field col s12">
                <input type="text" name="document-name"  value="<?php echo set_value('document-name')?>" required>
                <label>Name of the Document</label>
            </div>
            <div class="file-field input-field col s12">
                <input id="document" type="file" name="document" required>

                <div class="file-path-wrapper">
                    <p>Presentation files, Spreadsheets, PDFs and Image files can be uplaoded.</p>
                    <input class="file-path validate" type="text" placeholder="Click to select a file">
                </div>
            </div>
        </div>
        <input class="btn" type="submit" value="Upload">
        </form>
    </div>

</div>
<script>
    jQuery(document).ready(function($){
        $("#upload_idea_document").on('submit',function(e){
            e.preventDefault();
            Materialize.toast("<span class='upload-status'>Uploading Document...</span>",2000);

            $.ajax({
                url: '/api2/idea/documents', // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {

                    var obj=JSON.parse(data);
                    if(obj.status=="success")
                    {
                        //Materialize.toast("Idea Created Successfully",4000);
                        //setTimeout(function(){location.replace('/dashboard');},1000);
                        $("#document_upload_parent_of_all").fadeOut(400,function () {
                            $("html,body").animate({
                                scrollTop:0
                            },200);
                            $("#message_document_upload_success").fadeIn();
                        });
                    }
                    else{
                        Materialize.toast("Failure"+ obj.description,4000);
                    }
                }
            });
        });
    });
</script>
<div class="container" id="message_document_upload_success" style="display: none;">
    <div class="row card text-center login-card" style="max-width:550px;margin:50px auto !important;">
        <div class="col s12">
            <h3 class="center color-teal"><i class="material-icons large">check_circle</i></h3>
            <h3 class="center">Success!</h3>
            <h5 class="center">Your Document has been uploaded successfully, go to <a href="/ideas/documents/<?php echo $idea_id?>">Dashboard.</a></h5>
            <br><br>
        </div>
    </div>
</div>