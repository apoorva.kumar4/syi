
<div class="row margin50">
	<div class="col s12">
		<h3>Ideas<a href="<?php echo base_url()?>ideas/add" class="waves-effect waves-light btn btn1 right">New Idea</a></h3>
	</div>
</div>
<div class="row margin50">
	<div class="col s12">
		<!-- Modal Structure -->
		  <div id="modal1" class="modal bottom-sheet">
		    <div class="modal-content">
		      <h4 id="delete_message_header">Delete</h4>
		      <p>This will delete all the data, bullets, associated connections and this is irreversible, yes you get it!</p>
		    </div>
		    <div class="modal-footer">
		      <a id="modal_delete" href="#!" class=" modal-action modal-close waves-effect waves-green btn red">Yes, Delete!</a>
			  <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Back</a>
		    </div>
		  </div>
		  
		  <div id="modal-edit-bullet" class="modal">
			  <div class="modal-content">
				  <h4 class="modal-edit-bullet-title"></h4>
				  <form id="form-edit-bullet-modal" action="#" method="post">
					  <div class="row">
					 	 <div class="input-field col s12">
						 	 <input id="input-bullet-name-modal" type="text" value="" name="bullet-name">
						  	<label>Bullet Title</label>
					  	</div>
				  	  </div>
					  <div class="row">
					  	<div class="input-field col s12">
						  <input id="input-bullet-description-modal" type="text" name="bullet-description">
						  <label>Description</label>
					    </div>
				  	  </div>
					  <div class="row">
					    <div class="col s12">
					    	<input id="input-bullet-start-date-modal" type="date" class="start-datepicker" placeholder="" name="bullet-start-date">
					    	<label>Date to reach milestone</label>
					    </div>
					  </div>
				  </form>
						  
						  
			  </div>
			  <div class="modal-footer">
				  <a id="action-save-edit-bullet-modal" href="#" class="modal-action waves-effect waves btn teal">Save</a>
				  <a id="action-delete-edit-bullet-modal" href="#" class="modal-action waves-effect waves btn red">Delete</a>
				  <a href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
				  
			  </div>
		  </div>
				<?php
				if(!$ideas)
				{
						?> <h5>You have not added any ideas, click <a href="<?php echo base_url()?>ideas/add">Add</a></h5> <?php
				}
				else
				{?>
					<ul class="collapsible"  data-collapsible="expandable">
				<?php
				$counter=0;
				foreach($ideas as $idea)
				{
					
					?>
					
					<li>


					<div class="collapsible-header <?php if(!$counter) echo "active"; $counter++ ?>" data-ideaid="<?php echo $idea['id']?>">
						<span class="collapsible-idea-name" ><?php echo $idea['name']?></span>
				    <a class='dropdown-button' data-beloworigin="true" stoppropagation="true" style="padding:0;float:right;margin:0px 0px;margin-left:20px" href='#' data-activates='dropdown<?php echo $idea['id']?>'><i style="color:#424242;float:right;font-size:1rem;line-height:inherit;margin-right:0rem;" id="edit-<?php echo $idea['id']?>" class="material-icons">mode_edit</i></a>

				    <!-- Dropdown Structure -->
				    <ul id='dropdown<?php echo $idea['id']?>' class='dropdown-content'>
				      <li><a href="/ideas/<?php echo $idea['unique_url']?>" target="_blank">View</a></li>
				      <li><a href="/ideas/edit/<?php echo $idea['id']?>">Edit</a></li>
				      <li class="divider"></li>
					  <li>
						  <a class="click_modal1" href="" data-ideaid="<?php echo $idea['id']?>">Delete</a>
					  </li>
				      
				    </ul>
					
					
					<div class="switch cc_visibility" style="display: inline; float: right; margin-left: 20px;" data-ideaid="<?php echo $idea['id']?>">
						    <label>
								Private
						      <input <?php if(strnatcasecmp($idea['visibility'],'Visible')==0 || strnatcasecmp($idea['visibility'],'public')==0) echo "checked"?> type="checkbox">
						      <span class="lever"></span>
							  Public
						    </label>
	  					</div>
						
						<div class="chip chipwhite" style="float:right;margin-top:5px"><?php echo $idea['stage']?></div>
						
						<div class="chip chipwhite" style="float:right;margin-top:5px"><?php echo $idea['followers']." followers"?></div>
						
						
					</div>
					<div class="collapsible-body">
							<section class="cd-horizontal-timeline">
								<div class="timeline">
									<div class="events-wrapper">
										<div class="events">
											<ol>
													<?php

													$active=1;
													if($active)
													{


														$CI =& get_instance();
														$CI->load->model('bullet');
														$bullets=explode(', ',$idea['bullets']);

														foreach($bullets as $key=>$bullet)
														{
															
															
															$CI->bullet->id=$bullet;

															$CI->bullet->read();
															
															//var_dump($CI->bullet);
															if($key%2==0)
															{
																if($CI->bullet->template=="1")
																{
																	//var_dump($CI->bullet->template);
																	$da= new Datetime($CI->bullet->start_date);
																	$data_date=date_format($da,"d/m/y");
																	$data_human=date_format($da,"M'y");
																	$bullet_name=$CI->bullet->name;
																	$bullet_id=$CI->bullet->id;
																	if(strnatcasecmp($bullet_name,$idea['stage'])==0)
																	{
																		

																	echo "<li><a href='#$key' data-date='$data_date'  class='selected' data-bulletid='$bullet_id'>$bullet_name <br>$data_human</a></li>";
																	}
																	else
																	{


																	echo "<li><a href='#$key' data-date='$data_date' data-bulletid='$bullet_id'>$bullet_name <br>$data_human</a></li>";
																	
																	}											

																}
																else
																{
																	$da= new Datetime($CI->bullet->start_date);
																	$data_date=date_format($da,"d/m/y");
																	$data_human=date_format($da,"d-M'y");
																	$bullet_name=$CI->bullet->name;
																	$bullet_id=$CI->bullet->id;
																	if(1)
																	{
																	echo "<li><a class='semimilestone-even' href='#$key' data-date='$data_date' data-bulletid='$bullet_id'>$bullet_name <br>$data_human</a></li>";		
																	}
																	else
																	{
																	echo "<li><a class='semimilestone-even' href='#$key' data-date='$data_date' data-bulletid='$bullet_id'>$bullet_name <br>$data_human</a></li>";	
																	}
																	
																	
																}
															
															}
															else
															{
																if($CI->bullet->template=="1")
																{
																	$da= new Datetime($CI->bullet->start_date);
																	$data_date=date_format($da,"d/m/y");
																	$bullet_name=$CI->bullet->name;
																	$data_human=date_format($da,"M'y");
																	$bullet_id=$CI->bullet->id;
																	if(strnatcasecmp($bullet_name,$idea['stage'])==0)
																	echo "<li><a href='#$key' class='selected' data-date='$data_date' data-bulletid='$bullet_id'>$bullet_name <br>$data_human</a></li>";
																	else
																	echo "<li><a href='#$key' data-date='$data_date' data-bulletid='$bullet_id'>$bullet_name <br>$data_human</a></li>";
																}
																else
																{
																	$da= new Datetime($CI->bullet->start_date);
																	$data_date=date_format($da,"d/m/y");
																	$bullet_name=$CI->bullet->name;
																	$data_human=date_format($da,"d-M'y");
																	$bullet_id=$CI->bullet->id;
																	echo "<li><a class='semimilestone' href='#$key' data-date='$data_date' data-bulletid='$bullet_id'>$bullet_name <br>$data_human</a></li>";
																}
															
															}
														}
													}
														
													

													?>

											</ol>

											<span class="filling-line" aria-hidden="true"></span>
										</div> <!-- .events -->
									</div> <!-- .events-wrapper -->
										
									<ul class="cd-timeline-navigation">
										<li><a href="#0" class="prev inactive">Prev</a></li>
										<li><a href="#0" class="next">Next</a></li>
									</ul> <!-- .cd-timeline-navigation -->
								</div> <!-- .timeline -->
								<div class="col s12 text-center action-bar">
									<h4 class="text-center"><i class="material-icons zoom_out">zoom_out</i><i id="zoom_in_<?php echo $idea['id'];?>" class="material-icons zoom_in">zoom_in</i><a href="/ideas/bullets/new" style="font-size:1rem;margin-left:10px;vertical-align:bottom" data-ideaid="<?php echo $idea['id']?>">New Bullet</a><a href="#modal-edit-bullet" style="font-size:1rem;margin-left:10px;vertical-align:bottom" class="a-edit-bullet modal-trigger" data-bulletid=""></a></h4>
								</div>
							</section>	
					</div>
					</li>
					 

					<?php
					
				}
			}
				?>
      	</div>
	</div>
</div>
