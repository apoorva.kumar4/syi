<div class="row margin50">
	<div class="col s12 marginauto floatnone">
		<h3>Message SentBox <a href="/messages/new" class="waves-effect waves-light btn btn1 right">New Message</a><a href="/inbox" class="linkfix right " style="padding:0 15px">Inbox</a></h3>
	</div>


	<div class="col s12 floatnone marginauto" style="">

	<ul class="collapsible" data-collapsible="expandable">
	<?php
	foreach($messages as $message)
	{
		$username=_get_username_from_socialid($message['receiver']);
		if(date("F j, Y",strtotime("today")) == date("F j, Y",strtotime($message['timestamp'])))
		{
			$human_date="Today at ". date("h:ia",strtotime($message['timestamp']));
		}
		else if(date("F j, Y",strtotime("yesterday")) == date("F j, Y",strtotime($message['timestamp'])))
		{
			$human_date="Yesterday at ". date("h:ia",strtotime($message['timestamp']));
		}
		else
		{
			$human_date=date("F jS, Y",strtotime($message['timestamp']));
			
		}
		

		?>
		<li>
			
      		<div class="collapsible-header" id="<?php echo $message['id']?>"><i class="material-icons">chat</i>To: <a href="/users/<?php echo $username?>" class="username tooltipped" data-position="right" data-delay="50" data-tooltip="<?php echo $username?>"><?php echo _f_get_firstname_from_socialid($message['receiver'])?></a><span class="approval_status hide-on-small-only	" style="float:right;color: #c5c5c5;font-size: 0.8rem;padding: 0 15px;"><?php if($message['admin_approved']==1) echo "Approved"; else echo "Approval Pending"?></span><span class="hide-on-small-only	"style="float:right"><?php echo $human_date?></span></div>
      		<div class="collapsible-body"><p><?php echo $message['message']?></p>
      			<div class="btn-inbox-message-action">
      			
      			<a class="btn btn-x waves red messagedelete" data-hrefx="/api/v1/users/<?php echo $userid?>/messages/<?php echo $message['id']?>/delete" style="margin:0.5rem 2rem">Delete</a>
      			</div>
      			
      		</div>

    	</li>
		<?php

	}
	?>
</ul>
</div>
</div>
