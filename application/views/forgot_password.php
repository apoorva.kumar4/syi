<style>
    main{
        background-color:#F6F9FC;
    }
</style>
<div class="container">
    <div class="row card div_sign_up_form" style="float:none;margin:auto;max-width: 400px;padding:20px">
        <form method="POST" action="/forgot/submit">
        
                <div class="col s12">
                    <h4 class="center">Reset Password</h4>
                    <p class="center">You will receive an OTP on your Mobile.</p>
                </div>
                <div class="input-field col s12">
                    <input name="mobile" type="tel" required>
                    <label>Mobile</label>
                </div>
                <div class="input-field col s12">
                    <div class="g-recaptcha" data-sitekey="6LdIOjQUAAAAALRi6GT1xzV0nrg138PETl3RqLaz"></div>
                </div>
                <div class="input-field col s12">
                    <input class="btn blue fwidth" type="Submit" value="Send OTP"><br>
                </div>         
                
        </form>
    </div>
</div>
</section>