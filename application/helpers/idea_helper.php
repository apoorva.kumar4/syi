<?php
function _f_get_idea_phase($id)
{
	$CI=& get_instance();
	$query="select id,name,start_date from bullets where ideaid=$id and template=1 and start_date <= CURDATE() order by start_date desc limit 1;";
	$action=$CI->db->query($query);
	if($action){
		$row=$action->result_array();
		if(isset($row[0]))
		return $row[0];
	}
	else{
		return 0;
	}
}
function _f_get_idea_shareable_link($ideaid)
{
    return md5("random_salt_by_for_idea_shareable_links_4147744758".$ideaid);
}
function _f_get_idea_logo_url($ideaid)
{
	$CI =& get_instance();
	$query = "select url from uploads where id in (Select max(uploadid) from idea_logos where ideaid=$ideaid)";
            $action = $CI->db->query($query);
            if ($action) {
                $upload_path=$CI->config->item('upload_path'); 
		$row = $action->result_array();
                if ($row){
		  $remoteImage = $upload_path . $row[0]['url'];  
		  return $remoteImage;
                } 
                else {
                    return $upload_path . "bulb.png";
                }
            } else {
                return false;
            }
}
?>
