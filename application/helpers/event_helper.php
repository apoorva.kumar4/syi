<?php
function _f_get_event_logo_url($eventid)
{
	$CI =& get_instance();
	$query = "select url from uploads where id in (Select max(uploadid) from event_logos where eventid=$eventid)";
            $action = $CI->db->query($query);
            if ($action) {
                $upload_path=$CI->config->item('upload_path'); 
		$row = $action->result_array();
                if ($row){
		  $remoteImage = $upload_path . $row[0]['url'];  
		  return $remoteImage;
                } 
                else {
                    return $upload_path . "bulb.png";
                }
            } else {
                return false;
            }
}
function _f_get_event_cover_url($eventid)
{
	$CI =& get_instance();
	$query = "select url from uploads where id in (Select max(uploadid) from event_cover_pics where eventid=$eventid)";
            $action = $CI->db->query($query);
            if ($action) {
                $upload_path=$CI->config->item('upload_path'); 
		$row = $action->result_array();
                if ($row){
		  $remoteImage = $upload_path . $row[0]['url'];  
		  return $remoteImage;
                } 
                else {
                    return $upload_path . "bulb.png";
                }
            } else {
                return false;
            }
}
?>
