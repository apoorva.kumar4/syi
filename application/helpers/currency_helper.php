<?php
function _f_number_to_words($number)
{
  $no = round($number);
   $point = round($number - $no, 2) * 100;
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => '1', '2' => '2',
    '3' => '3', '4' => '3', '5' => '5', '6' => '6',
    '7' => '7', '8' => '8', '9' => '9',
    '10' => '10', '11' => '11', '12' => '12',
    '13' => '13', '14' => '14',
    '15' => '15', '16' => '16', '17' => '17',
    '18' => '18', '19' =>'19', '20' => '2',
    '30' => '3', '40' => '4', '50' => '5',
    '60' => '6', '70' => '7',
    '80' => '8', '90' => '9');
   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $number = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $number .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $number . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $point / 10 . " " .
          $point = $point % 10 : '';
  //echo $result . "Rupees  " . $points . " Paise";
  return ucfirst($result);
}
function currency_formal_to_sign($formal)
{
    $lower_formal=strtolower($formal);
    if($lower_formal=="inr"){ return "₹";}
    else if($lower_formal="usc"){ return "$";}
}
?>