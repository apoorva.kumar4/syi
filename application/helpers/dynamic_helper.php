<?php
function _f_dynamic_sector_select()
{
	$CI = & get_instance();
	$numargs= func_num_args();
	$arg_list= get_sectors();
	
	
	echo "<select multiple name='sector[]' id='sector'>";
	echo "<option name='' value='none'>All</option>";
	for($i=1;$i< sizeof($arg_list);$i++)
	{
			$match=0;
			$arr=$arg_list[$i]['name'];
			$arr_value=$arg_list[$i]['id'];
			$sectors_requested=explode("-",segy("sector"));
			foreach($sectors_requested as $sector)
			{
				if(strtolower($sector)==strtolower($arr))
				$match=1;	
			}
			if($match)
			{
				echo "<option id='$arr' value='$arr_value' selected='true'>$arr</option>";
			}
			else
			{
				echo "<option id='$arr' value='$arr_value'>$arr</option>";
			}
			
	}

	echo "</select>";
}

function _f_dynamic_follow_button($ideaid)
{
	if(_f_is_loggedin())
	{
		if(_is_user_following_idea($ideaid))
		{
		    $idea['id']=$ideaid;
			markup_unfollow_button($idea);
		}
		else
		{
            $idea['id']=$ideaid;
            markup_follow_button($idea);
		}
	}
	else
	{
        $idea['id']=$ideaid;
        markup_follow_button($idea);
	}
	
}
function markup_unfollow_button($idea)
{
    ?>
    <a href="#" data-hrefx="/api2/idea/<?php echo $idea['id']?>/unfollow" class="btn red followidea" data-ideaid="<?php echo $idea['id']?>">Unfollow Idea</a>
    <?php
}
function markup_follow_button($idea)
{
    ?>
    <a href="#" data-hrefx="/api2/idea/<?php echo $idea['id']?>/follow" class="btn green followidea" data-ideaid="<?php echo $idea['id']?>">Follow Idea</a>
    <?php
}
function _f_dynamic_select_sector()
{
    // Takes first argument as input name to be set
    // takes second argument as value to set
	
	$CI = & get_instance();
	$numargs= func_num_args();

	$arg_list= func_get_args();
	$value=set_value($arg_list[0]);
	$sectors=_f_get_sectors();

	echo "<select name='$arg_list[0]' id='$arg_list[0]'>";
	foreach($sectors as $sector)
	{
		$sectorid=$sector['id'];
		$sectorname=$sector['name'];
		if(isset($arg_list[1]))
        {
            if($arg_list[1]==$sectorid)
                echo "<option id='sector-$sectorid' value='$sectorid' selected='true'>$sectorname</option>";
            else
                echo "<option id='sector-$sectorid' value='$sectorid'>$sectorname</option>";
        }
		else
            echo "<option id='sector-$sectorid' value='$sectorid'>$sectorname</option>";

	}

	echo "</select>";
	
}
function _f_get_sectors()
{
	$CI = & get_instance();
	$query="Select * from sectors order by name asc";
	$action=$CI->db->query($query);
	$result=$action->result_array();
	return $result; 
}
function _f_dynamic_checkbox_default()
{
	
	$CI = & get_instance();
	$numargs= func_num_args();
	$arg_list= func_get_args();
	$value=set_value($arg_list[0]);
	for($i=1;$i<=$numargs-1;$i++)
	{
			$idx=$arg_list[0].$i.generateRandomString();
			$vid=$i;
			$name=$arg_list[0]."[]";
			echo "<p><input id='$idx' type='checkbox' name='$name' class='$arg_list[0]' value='$arg_list[$i]'><label for='$idx'>$arg_list[$vid]</label></p>";
	}
	echo "</select>";
}
function _f_dynamic_checkbox_phase_from_database()
{
	
	$CI = & get_instance();
	$numargs= func_num_args();
	$phases=array('Ideation','Prototype','Launching Soon','Operational','Expansion');
	$serviceid=segx(3);
	$action=$CI->db->query("select stage from service_target_phase where serviceid=$serviceid");
	$target_phases=$action->result_array();
	$i=0;
	
	foreach($phases as $phase)
	{
			$idx="service-target-phase".$i.generateRandomString();
			$vid=$i;
			if(array_search($phase,array_column($target_phases,'stage')) !== FALSE) {
				echo "<p><input id='$idx' type='checkbox' checked name='service-target-phases[]' class='service-target-phases' value='$phase'><label for='$idx'>$phase</label></p>";
				
			}
			else
			{
				echo "<p><input id='$idx' type='checkbox' name='service-target-phases[]' class='service-target-phases' value='$phase'><label for='$idx'>$phase</label></p>";
			}
			$i++;
	}
	echo "</select>";
}
function _f_dynamic_checkbox_sectors()
{
	
	$CI = & get_instance();
	$CI->load->model('Sectors_model');
	$numargs= func_num_args();
	$arg_list= func_get_args();
	$sectors_to_check=array();
	$target_sector_other="";
	if(array_key_exists(1, $arg_list)) {
		if(is_array($arg_list[1])) {
			$sectors_to_check=$arg_list[1];
		}	
	}

	if(array_key_exists(2, $arg_list)) {
		$target_sector_other=$arg_list[2];
			
	}
	
	
	$sectors=$CI->Sectors_model->get_all();
	$i=0;
	$length=sizeof($sectors);
	echo "<div class='col s6'>";
	for($i;$i<$length/2;$i++) {
		$idx=$arg_list[0].$i.generateRandomString();
		$sector_id=$sectors[$i]['id'];
		$name=$arg_list[0]."[]";
		$sector_name=$sectors[$i]['name'];
		if(in_array($sectors[$i]['id'],$sectors_to_check))
			echo "<div class='input-field'><input id='$idx' type='checkbox' name='$name' class='$arg_list[0]' value='$sector_id' checked><label for='$idx'>$sector_name</label></span></div>";
		else
			echo "<div class='input-field'><input id='$idx' type='checkbox' name='$name' class='$arg_list[0]' value='$sector_id'><label for='$idx'>$sector_name</label></span></div>";
		if($sectors[$i]['name']=="Others") {
			echo "<div class='input-field'><input type='text' name='target_sector_other' value='$target_sector_other'><label for='$idx'>$sector_name</label></span></div>";
		}
	}
	echo "</div>";
	echo "<div class='col s6'>";
	for($i;$i<$length;$i++) {
		$idx=$arg_list[0].$i.generateRandomString();
		$sector_id=$sectors[$i]['id'];
		$name=$arg_list[0]."[]";
		$sector_name=$sectors[$i]['name'];
		if(in_array($sectors[$i]['id'],$sectors_to_check))
			echo "<div class='input-field'><input id='$idx' type='checkbox' name='$name' class='$arg_list[0]' value='$sector_id' checked><label for='$idx'>$sector_name</label></span></div>";
		else
			echo "<div class='input-field'><input id='$idx' type='checkbox' name='$name' class='$arg_list[0]' value='$sector_id'><label for='$idx'>$sector_name</label></span></div>";

		if($sectors[$i]['name']=="Others") {
			echo "<div class='input-field hide'><input type='text' name='target_sector_other' value='$target_sector_other' placeholder='Please specify'></span></div>";
		}

	}
	echo "</div>";

	
	//echo "</select>";
}
function _f_dynamic_checkbox_sectors_from_database()
{
	
	$CI = & get_instance();
	$serviceid=segx(3);
	$action_q=$CI->db->query("select * from service_target_sector where serviceid=$serviceid");
	$sectors_q=$action_q->result_array();
	$action=$CI->db->query("Select * from sectors");
	$sectors=$action->result_array();
	$i=0;
	
	foreach($sectors as $sector) {
			$idx="service-target-sector".$i.generateRandomString();
			
			$sector_id=$sector['id'];
			$name="service-target-sectors[]";
			$sector_name=$sector['name'];
			

			if(array_search($sector_id,array_column($sectors_q,'sectorid')) !== FALSE)
			{

				if($i%4==0 && $i !=0)
				{
					echo "</div><div class='col s4'>";	
					echo "<input id='$idx' type='checkbox' checked='true' name='$name' class='service-target-sectors' value='$sector_id'><label for='$idx'>$sector_name</label></span>";
				}
				else
				{
					if($i==0)
					{
						echo "<div class='col s4'>";
						echo "<input id='$idx' type='checkbox' checked='true' name='$name' class='service-taret-sectors' value='$sector_id'><label for='$idx'>$sector_name</label></span>";

					}
					else
					{
						echo "</div><div class='col s4'>";
						echo "<input id='$idx' type='checkbox' checked='true' name='$name' class='service-target-sectors' value='$sector_id'><label for='$idx'>$sector_name</label></span>";
					}
					
				}
			}
			else
			{
				if($i%4==0 && $i !=0)
				{
					echo "</div><div class='col s4'>";	
					echo "<input id='$idx' type='checkbox' name='$name' class='service-target-sectors' value='$sector_id'><label for='$idx'>$sector_name</label></span>";
				}
				else
				{
					if($i==0)
					{
						echo "<div class='col s4'>";
						echo "<input id='$idx' type='checkbox' name='$name' class='service-taret-sectors' value='$sector_id'><label for='$idx'>$sector_name</label></span>";

					}
					else
					{
						echo "</div><div class='col s4'>";
						echo "<input id='$idx' type='checkbox' name='$name' class='service-target-sectors' value='$sector_id'><label for='$idx'>$sector_name</label></span>";
					}
					
				}
			}
			
			
			

			$i++;
	}
	echo "</select>";
}
function generateRandomString($length = 10) {

return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

}
function _f_dynamic_checkbox_services()
{

    $CI = & get_instance();
    $numargs= func_num_args();
    $arg_list= func_get_args();
    $action=$CI->db->query("Select * from services");
    $services=$action->result_array();
    $i=0;
    foreach($services as $service) {
        $idx=$arg_list[0].$i.generateRandomString();
        $vid=$i;
        $service_id=$service['id'];
        $name=$arg_list[0]."[]";
        $service_name=$service['name'];
        if($i%4==0 && $i !=0)
        {
            echo "</div><div class='col s4'>";
            echo "<input id='$idx' type='checkbox' name='$name' class='$arg_list[0]' value='$service_id'><label for='$idx'>$service_name</label></span>";
        }
        else
        {
            if($i==0)
            {
                echo "<div class='col s4'>";
                echo "<input id='$idx' type='checkbox' name='$name' class='$arg_list[0]' value='$service_id'><label for='$idx'>$service_name</label></span>";

            }
            else
            {
                echo "</div><div class='col s4'>";
                echo "<input id='$idx' type='checkbox' name='$name' class='$arg_list[0]' value='$service_id'><label for='$idx'>$service_name</label></span>";
            }

        }



        $i++;
    }
    //echo "</select>";
}
function _f_dynamic_select_default_option()
{
	
	$CI = & get_instance();
	$numargs= func_num_args();
	$arg_list= func_get_args();
	$selected=$arg_list[1];
	echo "<select name='$arg_list[0]' id='$arg_list[0]>";
	for($i=1;$i< $numargs;$i++)
	{
		if($arg_list[$i]==$selected)
		{
			echo "<option id='$arg_list[$i]' value='$arg_list[$i]' selected>$arg_list[$i]</option>";
			

		}
		else
		{
			echo "<option id='$arg_list[$i]' value='$arg_list[$i]'>$arg_list[$i]</option>";			
			
		}
	}

	echo "</select>";
	
}
function _f_dynamic_select_default_array($arr)
{
	
	$CI = & get_instance();
	$numargs= count($arr);
	$arg_list= $arr;
	
	
	echo "<select  name='$arg_list[0]' id='$arg_list[0]'>";
	for($i=1;$i< $numargs;$i++)
	{
			echo "<option id='$arg_list[$i]' value='$arg_list[$i]'>$arg_list[$i]</option>";			
			
	}

	echo "</select>";
	
}
function _f_message($message,$message_description)
{
	$CI =& get_instance();

	$CI->session->set_userdata('message',$message);
	$CI->session->set_userdata('message_description',$message_description);
	
}
function _f_dynamic_pp_remove($socialid = null)
{
	$CI =& get_instance();
	if(_f_is_loggedin())
	{

	    $si=is_null($socialid)?$CI->session->socialid:$socialid;
		$query="select count(*) from profile_pictures where socialid=$si";
		$action=$CI->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			if($row[0]["count(*)"]=="0"){

			}
			else
			{
				echo "<p><a id='remove-pp' class='red-text' href='#'>Remove Profile Picture</a></p>";
			}
		}
		else
		{

		}

	}

}
function remove_http($url) {
   $disallowed = array('http://', 'https://');
   foreach($disallowed as $d) {
      if(strpos($url, $d) === 0) {
         return str_replace($d, '', $url);
      }
   }
   return $url;
}
?>
