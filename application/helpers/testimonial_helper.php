<?php
function create_testimonial($details)
{
	// Designed for Admin Panel Ideator Signup, this function should not be used anywhere else
	$CI =& get_instance();
	$validated_id=isset($details['id'])?$details['id']:NULL;
   	$validated_first_name=$CI->db->escape(_f_validate('name',$details['first_name']));
	$validated_last_name=$CI->db->escape(_f_validate('lastname',$details['last_name']));
	$validated_description=$CI->db->escape(_f_validate('description',$details['description']));
	$validated_company=$CI->db->escape(_f_validate('description',$details['company']));

	if($details['active']=="Active")
	{
		$details['active'] = 1;
	}
	else
	{
		$details['active'] = 0;
	}

	$validated_active=$details['active'];

	//var_dump($details);

	// Step 1 check if he is already in database, if he is then he is just completing his signup
	// Step 2 Depening upon if he is database or not, perform an insert or an update query
	if($validated_id){
		$pre_query=$CI->db->query("Select count(*) from users where id=$validated_id");

		$row=$pre_query->row_array();	

		if($row['count(*)']>0)
		{
			// Testimonial is present so perform an update
			if(isset($validated_first_name,$validated_last_name,$validated_description,$validated_company,$validated_active))
			{
			$query="Update testimonials set first_name=$validated_name,last_name=$validated_last_name,description=$validated_description, company=$validated_company, active=$validated_active where id=$validated_id";
				$CI->db->query($query);

				$action['status']="success";
				return $action;
			}
			else
			{
				$action['status']="failure";
				$action['description']="Validation of data failed on update";
				return $action;
			}
		}
	}
	else
	{

		if(isset($validated_first_name,$validated_last_name,$validated_description,$validated_company,$validated_active))
		{
				$query="Insert into testimonials (first_name,last_name,description,company,active) values($validated_first_name,$validated_last_name,$validated_description,$validated_company,$validated_active)";

	
				$CI->db->query($query);
				$action['status']="success";
				return $action;
		}
		else
		{
			$action['status']="failure";
			$action['description']="Validation of data failed on insert";
			return $action;
		}
	}
}
?>