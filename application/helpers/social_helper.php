<?php
function socialify_link($link,$social_platform)
{
    if($social_platform=="facebook")
    {
        if(strpos($link,"facebook.com")===false) {
            return "https://facebook.com/".$link;
        }
        else return add_protocol($link);
    }
    else if($social_platform=="twitter") {
        if(strpos($link,"twitter")===false) {
            return "https://twitter.com/".$link;
        }
        else return add_protocol($link);
    }
    else if($social_platform=="google") {
        if(strpos($link,"google.com")===false) {
            return "https://plus.google.com/+".$link;
        }
        else return add_protocol($link);
    }
    else if($social_platform=="linkedin") {
        if(strpos($link,"linkedin.com")===false) {
            return "https://linkedin.com/user/".$link;
        }
        else return add_protocol($link);
    }
}
function add_protocol($link) {
    if(strpos($link,"http")===false) return "https://".$link;
    else return $link;
}
?>