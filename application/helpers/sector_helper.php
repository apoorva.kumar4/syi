<?php
function create_sector($details)
{
	// Designed for Admin Panel Ideator Signup, this function should not be used anywhere else
	$CI =& get_instance();
	$validated_id=isset($details['id'])?$details['id']:NULL;
   	$validated_name=$CI->db->escape(_f_validate('name',$details['sector_name']));
	

	//var_dump($details);

	// Step 1 check if he is already in database, if he is then he is just completing his signup
	// Step 2 Depening upon if he is database or not, perform an insert or an update query
	if($validated_id){
		$pre_query=$CI->db->query("Select count(*) from sectors where id=$validated_id");

		$row=$pre_query->row_array();	

		if($row['count(*)']>0)
		{
			// Testimonial is present so perform an update
			if(isset($validated_name))
			{
			$query="Update sectors set name=$validated_name where id=$validated_id";
				$CI->db->query($query);

				$action['status']="success";
				return $action;
			}
			else
			{
				$action['status']="failure";
				$action['description']="Validation of data failed on update";
				return $action;
			}
		}
	}
	else
	{

		if(isset($validated_name))
		{
				$query="Insert into sectors (name) values($validated_name)";

	
				$CI->db->query($query);
				$action['status']="success";
				return $action;
		}
		else
		{
			$action['status']="failure";
			$action['description']="Validation of data failed on insert";
			return $action;
		}
	}
}
function get_sector($id)
{
	$CI =& get_instance();

	$query="Select * from sectors where id=$id";
	$action=$CI->db->query($query);
	$result=$action->result_array();
	return $result[0];
}
?>