<?php
function _f_get_active_testimonials()
{
	$CI = & get_instance();
	$CI->load->model('Testimonial');
	$testimonials=$CI->Testimonial->read();
	foreach($testimonials as &$t) {
		$t['profile_picture_url']=$CI->Supload->get_complete_url($t['profile_picture']);

	}
	return $testimonials;
}
function _f_get_services()
{
	$CI = & get_instance();
		$q_query="Select id,name from services";
		$action = $CI->db->query($q_query);
		$row=$action->result_array();
		return $row;
}
function _f_get_cities()
{
	$CI = & get_instance();
		$q_query="Select * from cities order by name asc";
		$action = $CI->db->query($q_query);
		$row=$action->result_array();
		return $row;

}
function get_sectors()
{
	$CI =& get_instance();
	$action=$CI->db->query("Select id,name from sectors");
    	return $action->result_array();
}	
function _f_get_event($id)
{
	$CI =& get_instance();

        $query="select * from events where id=$id";

        $action=$CI->db->query($query);

        if($action)
        {
        	
            $row=$action->result_array();
            return $row[0];


        }
        else
        {
        	echo "NNNN"; die(0);
        }
}
function _get_list_of_services($socialid)
    {
    	$CI =& get_instance();

        $query="select * from services where ownerid=$socialid order by id limit 3";

        $action=$CI->db->query($query);

        if($action)
        {
            return $action->result_array();
        }
    }
function _get_list_of_rate_cards($socialid)
    {
    	$CI =& get_instance();

        $query="select * from rate_cards where socialid=$socialid order by id";

        $action=$CI->db->query($query);

        if($action)
        {
            return $action->result_array();
        }
    }
function _get_rating($socialid)
    {
    	$CI =& get_instance();

        $query="select avg(rating),count(*) as count from reviews where socialid=$socialid";

        $action=$CI->db->query($query);

        if($action)
        {
            return $action->result_array();
        }
    }
function _get_review_count($socialid)
    {
        $CI =& get_instance();

        $query="select count(*) as count from reviews where socialid=$socialid";

        $action=$CI->db->query($query);

        if($action)
        {
            return $action->result_array();
        }
    }
function _get_sector_name_from_id($id)
    {
        $CI =& get_instance();
        $action=$CI->db->get_where('sectors',array('id'=>$id));
        $row=$action->result_array();
        if(isset($row[0]))
        return $row[0]['name'];
        else
            return null;
    }
function _get_visibility_from_id($id)
    {
        if($id==1)
        {
            return "Public";
        }
        else{
            return "Private";
        }
    }
function _f_get_all_service_providers()
{
	$CI = & get_instance();
	$CI->load->model('User3');
    // * Formula used Bayesian Estimate
    // * weighted rating (WR) = (v ÷ (v+m)) × R + (m ÷ (v+m)) × C
    // * R = average for the movie (mean) = (Rating)
    // * v = number of votes for the movie = (votes)
    // * m = minimum votes required to be listed in the Top 250 (currently 1300)
    // * C = the mean vote across the whole report (currently 6.8)
    // Yet to be implemented
    $q_query="select *,( select ifnull(avg(rating),0) from reviews where reviews.socialid=users.socialid ) as rating from users where users.user_type='service_provider' and users.blocked=0 order by rating desc";
    $action = $CI->db->query($q_query);

    $row=$action->result_array();

    $massive_data=array();
    foreach($row as $user) {
    	$all_data=$CI->User3->read($user['socialid']);
    	array_push($massive_data,$all_data);

    }
    return $massive_data;
}
function _f_get_all_investors()
{
	$CI = & get_instance();
		
			

			$q_query="Select * from users where user_type='investor' and admin_approved=1 and blocked=0 order by first_name";
			$action = $CI->db->query($q_query);
			$row=$action->result_array();
			$massive_data=array();
    foreach($row as $user) {
    	$all_data=$CI->User3->read($user['socialid']);
    	array_push($massive_data,$all_data);

    }
    return $massive_data;
}
function _f_get_investor_extra_details($socialid)
{
	$CI = & get_instance();
		
			

			$q_query="Select * from investors where socialid=$socialid";
			$action = $CI->db->query($q_query);
			$row=$action->result_array();
			return $row;

}
function _f_get_list_of_investor_sectors($socialid)
{
	$CI = & get_instance();
		
			

			$q_query="select * from sectors where sectors.id in (select sectorid from investor_sectors  where socialid=$socialid)";
			$action = $CI->db->query($q_query);
			$row=$action->result_array();
			return $row;
}
function _f_get_all_service_providers_search($name,$city)
{
	$CI = & get_instance();
	$CI->load->model('User3');
		$namec=$name;
		$name="%".$name."%";

		$esc_name=$CI->db->escape($name);
		$esc_city=$CI->db->escape($city);

			$q_query="select users.socialid from users where users.socialid in (select distinct s.ownerid from services s where s.name like $esc_name or s.id in(SELECT t.serviceid from services_tags t where t.name like $esc_name))";

			$q_query_advanced="select users.* from users where socialid in (select distinct owner from ( SELECT s.id as ID, s.ownerid as Owner, s.name as servicename, std.name as Name  FROM services_tags std inner join services s on s.id = std.serviceid WHERE match(s.name) against ($esc_name) or match(std.name) against($esc_name) order by ( case when s.name like $esc_name then 0 when std.name like $esc_name then 1 when concat(s.name, ' ', std.name) like '%$namec%' then 2 when concat(std.name, ' ', s.name) like '%$namec%' then 3 when match(s.name) against ($esc_name) then 4 when match(std.name) against($esc_name) then 5 else 6 end ) asc ) as b group by owner )";

			//echo $q_query_advanced; die(0);

			$action = $CI->db->query($q_query);
			$row=$action->result_array();
			$ret_result=array();
			foreach($row as $user) {
				$full_user=$CI->User3->read($user['socialid']);
				array_push($ret_result, $full_user);

			}
			
			//var_dump($row);die(0);
			return $ret_result;

}
function _is_event_owned($eventid,$socialid)
{
	if(isset($eventid,$socialid))
	{
		$CI =& get_instance();
		$query="Select count(*) from events where id=$eventid and ownerid=$socialid";
		$action=$CI->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			if($row[0]['count(*)']==1)
			{
			return 1;
			}
			else
			{
			return 0;
			}
		}
		else
		{
			return 0;
		}
	
	}
	else
	{
		return 0;	
	}
	
}
function _f_is_message_owned($messageid,$socialid)
{
	if(isset($messageid,$socialid))
	{
		$CI =& get_instance();
		$query="Select count(*) from messages where id=$messageid and sender=$socialid";
		$action=$CI->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			if($row[0]['count(*)']==1)
			{
			return 1;
			}
			else
			{
			return 0;
			}
		}
		else
		{
			return 0;
		}
	
	}
	else
	{
		return 0;	
	}
	
}
function _f_is_message_receiver($messageid,$socialid)
{
	if(isset($messageid,$socialid))
	{
		$CI =& get_instance();
		$query="Select count(*) from messages where id=$messageid and receiver=$socialid";
		$action=$CI->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			if($row[0]['count(*)']==1)
			{
			return 1;
			}
			else
			{
			return 0;
			}
		}
		else
		{
			return 0;
		}
	
	}
	else
	{
		return 0;	
	}
	
}
function _f_get_all_approved_upcoming_events()
{
	$CI =& get_instance();
	$date=date("Y-m-d",strtotime("now"));
	$esc_date=$CI->db->escape($date);
	$query="select * from events where admin_approved=1 and id in (select id from events where date_format(date(date_start),'%Y-%m-%d') >= $esc_date) order by date_start desc";

	$action=$CI->db->query($query);
	if($action)
	{
		$row=$action->result_array();
		return $row;

	}
}
function _f_get_all_approved_past_events()
{
	$CI =& get_instance();
	$date=date("Y-m-d",strtotime("now"));
	$esc_date=$CI->db->escape($date);
	$query="select * from events where admin_approved=1 and id in (select id from events where date_format(date(date_start),'%Y-%m-%d') < $esc_date) order by date_start desc";
	
	$action=$CI->db->query($query);
	if($action)
	{
		$row=$action->result_array();
		return $row;

	}
}
function _f_is_event_owned($id)
{	
	$CI = & get_instance();
	$socialid=$CI->session->userdata('socialid');
		$esc_pid=$id;
		$query="Select count(*) from events where ownerid=$socialid and id=$esc_pid";
		$action = $CI->db->query($query);
		
		if($action)
			{
				$row=$action->result_array();
				if($row[0]['count(*)']==1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}	
}
function _f_is_user_present($email)
{
	$CI = & get_instance();
		
    $q_query="Select count(*) from users where email='$email'";
    $action = $CI->db->query($q_query);
    $row=$action->result_array();
    if($row[0]['count(*)']==1)
    {
        // Verified that the user is registered so return 1
        return 1;
    }
    else
    {
        return 0;
    }

} // IMPORTANT FUNCTION
function _f_is_user_authorized($email,$pass)
{
$CI = & get_instance();
		
			$q_query="Select password_hash from users where email='$email' and blocked=0";
			$action = $CI->db->query($q_query);
			$row=$action->row();
			if($action->num_rows()==1)
			{
				// Verified that the user is registered so return 1
				if(password_verify($pass,$row->password_hash))
				{
					return 1;
				}	
			}
			else
			{

				return 0;

			}	

 
}
function _f_is_user_approved()
{
    if(_f_is_loggedin()) {
        $CI = &get_instance();
        $socialid = isset($CI->session->socialid) ? $CI->session->socialid : 0;
        if ($socialid == 0) return 0;
        $q_query = "Select admin_approved from users where socialid=$socialid";
        $action = $CI->db->query($q_query);
        $row = $action->result_array();
        if ($row[0]['admin_approved']==1) {
            return true;
        } else {
            return false;
        }
    }
    else{
        return true;
    }
}
function _f_is_approved_investor()
{
    if(_f_is_loggedin()) {
        $CI = &get_instance();
        $socialid = isset($CI->session->socialid) ? $CI->session->socialid : 0;
        if ($socialid == 0) return 0;
        $q_query = "Select admin_approved from users where socialid=$socialid and user_type='investor'";
        $action = $CI->db->query($q_query);
        $row = $action->result_array();
        if($row) {
        	if ($row[0]['admin_approved']==1) {
            return 1;
        	} else {
            	return $row[0]['admin_approved'];
        	}	
        } else {return 0;}
        
    }
    else{
        return 1;
    }
}
function _f_get_userid_from_socialid($socialid)
{
	$CI = & get_instance();
		
			

			$q_query="Select * from users where socialid=$socialid";
			$action = $CI->db->query($q_query);
			$row=$action->row();
			return $row->id;
}
function _f_get_name_from_socialid($socialid)
{
	$CI = & get_instance();
		
	$q_query="Select first_name, last_name from users where socialid=$socialid";
	$action = $CI->db->query($q_query);
	$row=$action->result_array();
	
	return $row[0]["first_name"].' '.$row[0]['last_name'];

}
function _f_get_firstname_from_socialid($socialid)
{
	$CI = & get_instance();
		
			

			$q_query="Select first_name from users where socialid=$socialid";
			$action = $CI->db->query($q_query);
			$row=$action->row();
			return $row->first_name;

}
function _f_get_firstname_from_username($username)
{
	$CI = & get_instance();
		
			

			$q_query="Select first_name from users where username='$username'";
			$action = $CI->db->query($q_query);
			$row=$action->row();
			return $row->first_name;

}
function _f_get_socialid_from_username($username)
{
	$CI = & get_instance();
			$esc_username=$CI->db->escape($username);
			$q_query="Select socialid from users where username=$esc_username";
			//echo $q_query;
			$action = $CI->db->query($q_query);
			$row=$action->result_array();
			if($row) if($row[0]['socialid']) return $row[0]['socialid']; else return 0;
			else return 0;
}
function _f_does_user_exist($socialid)
{
	$CI = & get_instance();
	$esc_socialid=$CI->db->escape($socialid);
    $q_query="Select socialid from users where socialid=$esc_socialid or username=$esc_socialid";
    $action = $CI->db->query($q_query);
    $row=$action->row();
    if($action->num_rows()==1)
    {
        // Verified that the user is admin so return 1
        return $row->socialid;
    }
    else
    {
        return 0;
    }

}
function _get_username_from_socialid($socialid)
{
	$CI = & get_instance();
	if(is_numeric($socialid))
    {
        $q_query="Select username from users where socialid=$socialid";
        $action = $CI->db->query($q_query);
        $row=$action->result_array();
        if($row) if($row[0]['username']) return $row[0]['username']; else return "none";
        else return 0;
    }
    else
    {
        return 0;
    }

}
function _f_is_loggedin()
{
	
	if(isset($_SESSION['socialid'])) {
		log_message('debug','yes logged in');
		return 1;

	} else {
		log_message('debug','not logged in');	
		return 0;
	}
	
}
function getAuthorizationHeader(){
    $headers = null;
    if (isset($_SERVER['Authorization'])) {
        $headers = trim($_SERVER["Authorization"]);
    }
    else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
        $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
    } elseif (function_exists('apache_request_headers')) {
        $requestHeaders = apache_request_headers();
        // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
        $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
        //print_r($requestHeaders);
        if (isset($requestHeaders['Authorization'])) {
            $headers = trim($requestHeaders['Authorization']);
        }
    }
    return $headers;
}
function getBearerToken() {
    $headers = getAuthorizationHeader();
    // HEADER: Get the access token from the header
    if (!empty($headers)) {
        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
            return $matches[1];
        }
    }
    return null;
}
function _f_is_admin()
{
	$CI = & get_instance();
	if(!isset($CI->session->socialid)) {
		return 0;
	}

    $CI->load->model('Admin_model');

    $get_admin=$CI->Admin_model->get(array('socialid'=>$CI->session->socialid));

    //var_dump($get_admin);die(0);
    

    if($get_admin['status']!="success") return 0;
    
    if($get_admin['data'][0]['socialid']==$CI->session->socialid) {
    	return $get_admin['data'][0];
    }
    else {
    	return 0;
    }

}
function _f_is_admin_all_privileges() {
	$authority=0;
	$user_admin=_f_is_admin();
	if($user_admin) {
		foreach($user_admin['permissions'] as $permission) {
			if($permission=="*") {
				$authority=1;
			}
		}
	}

	return $authority;
}
function _f_get_user_details($fetch)
{
	$CI = & get_instance();	

	$socialid=isset($CI->session->socialid)?$CI->session->socialid:0;
	if($socialid) {

		try {

			$q_query = "Select $fetch from users where socialid=$socialid";

			$action = $CI->db->query($q_query);
			$row = $action->result_array();

			return $row[0]["\"$fetch\""];

		} catch (Exception $e) {
			return 0;
		}
	}
	else{
		return 0;
	}

}
function _f_username_availability($username)
{
    if(is_numeric($username)){
        return false;
    }
    $CI= & get_instance();
    $CI->db->select('count(*)');
    $CI->db->where('username',$username);
    $action=$CI->db->get('users');
    $row=$action->result_array();
    if($row[0]['count(*)']>0)
    {
        return false;
    }
    else
    {
        return true;
    }
}
function _f_get_user_id()
{
	$CI = & get_instance();

	$socialid=isset($CI->session->socialid)?$CI->session->socialid:0;
	if($socialid) {

		try {

			$q_query = "Select id from users where socialid=$socialid";
			$action = $CI->db->query($q_query);
			$row = $action->result_array();

			return $row[0]['id'];

		} catch (Exception $e) {
			return 0;
		}
	}
	else{
		return 0;
	}
}
function _f_get_socialid()
{
	$CI = & get_instance();

	$socialid=isset($CI->session->socialid)?$CI->session->socialid:0;
	if($socialid) {

		try {

			$q_query = "Select socialid from users where socialid=$socialid";
			$action = $CI->db->query($q_query);
			$row = $action->result_array();

			return $row[0]['socialid'];

		} catch (Exception $e) {
			return 0;
		}
	}
	else{
		return 0;
	}
}
function _f_get_user_type()
{
    $CI = & get_instance();

    $socialid=isset($CI->session->socialid)?$CI->session->socialid:0;
    if($socialid) {

        try {
            $esc_socialid = $CI->db->escape($socialid);
            $q_query = "Select user_type from users where socialid=$esc_socialid";

            $action = $CI->db->query($q_query);
            $row = $action->result_array();

            return $row[0]['user_type'];

        } catch (Exception $e) {
            return 0;
        }
    }
    else{
        return 0;
    }
}
function _f_get_user_type_from_socialid($socialid)
{
	$CI = & get_instance();

    
    if($socialid) {

        try {
            $esc_socialid = $CI->db->escape($socialid);
            $q_query = "Select user_type from users where socialid=$esc_socialid";

            $action = $CI->db->query($q_query);
            $row = $action->result_array();

            if($row[0]['user_type']=="service_provider")
            {
            	return "Service Provider";
            }
            else if($row[0]['user_type']=="investor")
            {
            	return "Investor";
            }
            else if($row[0]['user_type']=="ideator")
            {
            	return "Ideator";
            }

        } catch (Exception $e) {
            return 0;
        }
    }
    else{
        return 0;
    }
}
function _f_check_logged_user($id)
{
  $CI = & get_instance();
  $access_token=$CI->session->userdata('access_token');
  $query="Select count(*) from users where access_token=".$CI->db->escape($access_token)." and id=$id";
  $action=$CI->db->query($query);
  $row=$action->result_array();
  if($row[0]['count(*)']==1)
  {
    return 1;

  }
  else{
    return 0;

  }
}
function _f_generate_socialid($id)
{
    return hexdec(substr(hash("sha256","randomsaltaashay0101".$id),0,6));
} //IMPORTANT FUNCTION
function _f_get_all_public_ideas()
{
	$CI = & get_instance();
		
			

			//$q_query="Select i.*,(select u.blocked from users u where u.socialid=i.ownerid) as blocked from ideas i where i.visibility= 1 having blocked=0";
			$q_query="select * from view_ideas where id in (Select id from ideas where visibility= 1 and ownerid not in (select socialid from users where blocked=1)) order by followers desc";
			$action = $CI->db->query($q_query);
			$row=$action->result_array();
			return $row;
}
function _f_get_top_ideas($limit=3)
{
	$CI = & get_instance();
    $q_query="select ti.ideaid,ti.rank,i.* from top_ideas ti left join view_ideas i on ti.ideaid=i.id order by ti.rank asc limit $limit";
    $action = $CI->db->query($q_query);
    $row=$action->result_array();
    return $row;
}
function _f_get_all_public_ideas_phase($tag)
{
	$CI = & get_instance();
		
			
			$esc_tag=$CI->db->escape($tag);
			$q_query="Select * from ideas where (visibility='public' or visibility='visible' or visibility='Public' or visibility='Visible') and stage like $esc_tag";
			$action = $CI->db->query($q_query);
			$row=$action->result_array();
			return $row;
}
function _f_get_all_public_ideas_by_phases($tags,$sectors=null)
{

	$CI = & get_instance();

	if($tags==null && $sectors==null) {
		$q_query="Select * from view_ideas where visibility=1";
		$action = $CI->db->query($q_query);
		
		$row=$action->result_array();
		return $row;
	}

	if($tags!=null && $sectors==null) {
		if(!in_array("All",$tags)) {

			$q_query="Select * from view_ideas where (visibility=1) and (0 or ";

			foreach($tags as $tag)
			{

				$esc_tag=$CI->db->escape($tag);
				$q_query .= "stage like " . $esc_tag . " or ";	
			}
			$q_query .= "0 )";
			
			$action = $CI->db->query($q_query);
			
			$row=$action->result_array();
			return $row;
		}
		else
		{
			$q_query="Select * from view_ideas where visibility=1";
			$action = $CI->db->query($q_query);
			
			$row=$action->result_array();
			return $row;
		}
	}

	if($tags==null && $sectors!=null) {
		if(!in_array("All",$sectors)) {

			$q_query="Select * from view_ideas where (visibility=1) and (0 or ";

			foreach($sectors as $sector)
			{

				$esc_tag=$CI->db->escape($sector);
				$q_query .= "sectorid like " . $esc_tag . " or ";	
			}
			$q_query .= "0 )";
			
			$action = $CI->db->query($q_query);
			
			$row=$action->result_array();
			//var_dump($row);die(0);
			return $row;
		}
		else
		{
			$q_query="Select * from view_ideas where visibility=1";
			$action = $CI->db->query($q_query);
			
			$row=$action->result_array();
			return $row;
		}
	}

	if($tags!=null && $sectors!=null) {
		if(!in_array("All",$sectors) && (!in_array("All",$tags))) {
			$q_query="Select * from view_ideas where (visibility=1) and (0 or ";

			foreach($tags as $tag)
			{

				$esc_tag=$CI->db->escape($tag);
				$q_query .= "stage like " . $esc_tag . " or ";	
			}
			$q_query .= "0 ) and (0 or ";

			foreach($sectors as $sector)
			{

				$esc_tag=$CI->db->escape($sector);
				$q_query .= "sectorid like " . $esc_tag . " or ";	
			}
			$q_query .= "0 )";

			//var_dump($q_query);die(0);
			$action = $CI->db->query($q_query);
			
			$row=$action->result_array();
			return $row;

		} else {
			$q_query="Select * from view_ideas where visibility=1";
			$action = $CI->db->query($q_query);
			
			$row=$action->result_array();
			return $row;
		}
	}
			
}
function _f_get_idea_followers($id)
{
	$CI = & get_instance();
		
			

			$q_query="Select count(*) from ideas_followers where ideaid=$id";
			$action = $CI->db->query($q_query);
			$row=$action->result_array();
			return $row[0]['count(*)'];
}
function _f_get_user_from_socialid($socialid=null)
{
	$CI = & get_instance();
	if($socialid==null) {
		$socialid=$this->session->socialid;
	}
	$q_query="Select * from users where socialid=$socialid";
	$action = $CI->db->query($q_query);
	$row=$action->result_array();
	return $row[0];
	

}
function _is_user_following_idea($ideaid)
{
	$CI = & get_instance();

	$socialid=$CI->session->socialid;
    $q_query="Select count(*) from ideas_followers where socialid=$socialid and ideaid=$ideaid";
    $action = $CI->db->query($q_query);
    $row=$action->result_array();
    if($row[0]['count(*)']>=1)
        return 1;
    else
        return 0;

}
function _f_get_all_followed_ideas($socialid)
{
    $CI=& get_instance();
    $CI->db->select('ideaid');
    $CI->db->where('socialid',$socialid);
    $action=$CI->db->get('ideas_followers');
    if($action)
    {
        $result=$action->result_array();
        return $result;
        //var_dump($result);die(0);
    }
    else
    {
        return false;
    }
}
function _f_get_unread_notification_count()
{
	$CI =& get_instance();
	$socialid=$CI->session->socialid;
	$CI->load->model('user');
	$unread_notifications=$CI->user->get_unread_notifications();
	return sizeof($unread_notifications);
	
}
function getenv_from_db($item)
{

	$CI =& get_instance();
	$CI->db->select('item,value');
	$CI->db->where('item',$item);
	$action= $CI->db->get('config');
	if($action) {
		$result=$action->result_array();
		return $result[0]['value'];
		
	} else {
		return false;
	}
}
function _f_get_partners() {
	$CI =& get_instance();
	$action=$CI->db->get('partners');
	$result=$action->result_array();
	foreach($result as &$partner) {
		$url=$CI->Supload->get_complete_url($partner['upload_id']);
		$partner['logo_url']=$url;

	}

	return $result;
}
?>
