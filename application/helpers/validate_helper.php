<?php

function _f_validate($type,$parameter)
{
	if($type=='name')
	{
		
		
		if (!preg_match("/^[a-zA-Z ]*$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}
	}
	else if($type=='lastname')
	{
		if (!preg_match("/^[a-zA-Z ]*$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}

	}
	else if($type=='username')
	{
		if (!preg_match("/^[A-Za-z][A-Za-z0-9]{1,31}$/",$parameter)) 
		{
  		
  			//echo "Only letters and numbers allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}

	}
	else if($type=='santext')
	{
		if (!preg_match("/^[a-zA-Z ]*$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}

	}
	else if($type=='email')
	{
		if (!filter_var($parameter, FILTER_VALIDATE_EMAIL)) 
		{
  			return NULL;
		}
		else
		{
			return $parameter;
		}
	}
	else if($type=='mobile')
	{
		if (!preg_match("/^[0-9]{10}$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}

	}
	else if($type=='institute')
	{
		if (!preg_match("/^[a-zA-Z .]*$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}
	}
	else if($type=='company')
	{
		if (!preg_match("/^[a-zA-Z .]*$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}
	}
	else if($type=='industry')
	{
		if (!preg_match("/^[a-zA-Z .]*$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}
	}
	else if($type=='year_of_graduation')
	{
		if (!preg_match("/^(19|20)\d{2}$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}

	}
	else if($type=='datetime')
	{
		if (!preg_match("/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}

	}
	else if($type=='date')
	{
		if (!preg_match("/^(\d{4})-(\d{2})-(\d{2})$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return '2016-01-01';

		}
		else
		{
			return $parameter;
		}

	}
	else if($type=='working_since')
	{
		if (!preg_match("/^(19|20)\d{2}$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}

	}
	else if($type=='role')
	{
		if (!preg_match("/^[a-zA-Z .]*$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}
	}
	else if($type=='location')
	{
		if (!preg_match("/^[a-zA-Z ]*$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}
		
	}
	else if($type=='sector_expertise')
	{
		if (!preg_match("/^[a-zA-Z .]*$/",$parameter)) 
		{
  		
  			//echo "Only letters and white space allowed";
  			return NULL;

		}
		else
		{
			return $parameter;
		}
		
	}
	else if($type=='student_alumni')
	{
		if($parameter == "Student")
		{
			return $parameter;
		}
		else if($parameter == "Alumni")
		{
			return $parameter;
		}
		else
		{
			return NULL;
		}
	}
	else if($type=='visibility')
	{
		if($parameter == "Private")
		{
			return $parameter;
		}
		else if($parameter == "Public")
		{
			return $parameter;
		}
		else
		{
			return NULL;
		}
	}
	else if($type=='url')
	{
		if(!preg_match("https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)",$parameter))
		{
			return null;
		} 
		else
		{
			return $parameter;
		}
	}
	else if($type=="description")
	{
		if(!preg_match("/^[a-zA-Z0-9 .,']*$/",$parameter))
		{
			return NULL;
		}
		else
		{
			
			return $parameter;
		}

	}
	else if($type=="intnumber")
	{
		if(!preg_match("/^\d+$/",$parameter))
		{
			return NULL;
		}
		else
		{
			
			return $parameter;
		}

	}
	
}

?>