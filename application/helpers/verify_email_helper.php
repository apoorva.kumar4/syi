<?php
function _f_send_email_code($email)
{
	//Collect parameters
    $CI = & get_instance();
	$subject = "Verification Code from SaYourIdeas.com";
	$verification_code= _f_generate_code();
	//var_dump($verification_code);
	if($verification_code)
	{
		
	
		$message = "Your verification code for SaYourIdeas is $verification_code , please enter it before it expires in 180 seconds";
		$CI->load->library('email');
		$frrr="sayourideas.com";
		$CI->email->from("admin@$frrr", 'Sayouideas.com');
        $CI->email->to($email);
		$CI->email->subject($subject);
		$CI->email->message($message);

		if($CI->email->send())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}

function _f_generate_code()
{
	// Generate a random code for an email and insert it's password hash into database table email_verification_codes
	$CI = & get_instance();
	$email=$CI->db->escape($CI->session->email);
	$digits = 6;
	$code=rand(pow(10, $digits-1), pow(10, $digits)-1);
	//$code_hash=password_hash($code,PASSWORD_DEFAULT);
	$code_hash=$code;
    $CI->db->replace('email_verification_codes',array('emailid'=>$email,'code'=>$code_hash));
    return $code;
	

}
function _f_verify_code($code)
{
    // Checks the database for code and matches it with the entered code;
    // if matched then marks the email as verified in the database

	$CI = & get_instance();
	$email=$CI->session->email;
    $where_arr=array(
        'emailid' => $email,
        'code' => $code
    );
	$CI->db->where($where_arr);
	$CI->db->select('count(*)',FALSE);
	$check_code=$CI->db->get('email_verification_codes');
	$result=$check_code->row_array();

    if($result['count(*)']==1)
    {
        $query2=$CI->db->query("Update users set emailverified=1 where email='$email'");

        return 1;
    }
    else
    {
        return 0;
    }

}
function _f_is_email_verified()
{
	$CI= & get_instance();
	$email=$CI->session->email;

	$is_verified=$CI->db->query("Select count(*) from users where email='$email' and emailverified=1");
	$result=$is_verified->row_array();

	if($result['count(*)']==1)
	{
		return true;
	}
	else
	{
		return false;
	}

}
?>