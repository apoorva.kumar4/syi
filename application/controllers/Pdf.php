<?php
class Pdf extends CI_Controller
{
  public function index()
  {
    $this->load->library('fpdf');
    $this->fpdf->AddFont('Roboto-light','');
    $this->fpdf->AddFont('Roboto-regular','');
    $this->fpdf->AddPage();
    $img_url=base_url().'assets/logo.png';
    $this->fpdf->Image("$img_url",76,10,-300,0,'PNG',"https://sayourideas.com");
    $this->fpdf->SetFont('roboto-light','',10);

    $this->fpdf->Cell(0,35,'Emerging Platform for Ideators, Service Providers and Investors.',0,2,'C');
    $this->fpdf->SetFont('roboto-light','',14);
    $this->fpdf->MultiCell(40,0,"User Details",0,'L');


    $this->fpdf->Output();
  }
}
?>

