<?php
class Services extends CI_Controller
{
	public function index()
	{

	}	
	public function id()
	{
		$this->load->model('User3');

		$data = array();
		if(isset($this->session->socialid))
		{
			
			$user=$this->User3->read($this->session->socialid);
			$data['me'][0]=$user;

		}

		$service_id= segx(3);
		if(is_numeric($service_id)){
			$this->load->model('service');	
			$ret_service = $this->service->read($service_id);
			if(!$ret_service){
				exit();
			}
			$data['title']="SaYourIdeas.com";
			$data['cities']=_f_get_cities();
			$data['services']=_f_get_services();

			
			$ret_user=$this->User3->read($ret_service[0]['ownerid']); 
			$data['service_providers'][]=$ret_user;

			$this->parser->parse('templates/header',$data);
			$this->load->view('service_providers',$data);
			$this->load->view('copyfooter');	

		}
		else{

		}
	}
	public function add()
	{
		$data = array();
		if(isset($this->session->socialid))
		{
			$this->load->model('User3');
			$user=$this->User3->read($this->session->socialid);
			$data['me'][0]=$user;

		}
		$data['title']="Add new Service";



		$config = array (
			array(
				'field'=>'service-name',
				'label'=>'Service Name',
				'rules'=>'required|callback_santext[Idea Name]'
			),
			array(
				'field'=>'service-description',
				'label'=>'Description',
				'rules'=>'required'
			));

		$this->form_validation->set_rules($config);



		if ($this->form_validation->run() == FALSE)
		{
			$this->parser->parse('templates/header',$data);
			$this->load->view('new_service',$data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->load->model('service','new');
			$this->new->name=$_POST['service-name'];
			$this->new->description=$_POST['service-description'];
			$this->new->servicetags=json_decode($_POST['service-tags'],true);
			$this->new->target_phases=$_POST['service-target-phases'];
			$this->new->target_sectors=$_POST['service-target-sectors'];
			$action=$this->new->create();

			if($action==1)
			{
				$uuu=base_url().'dashboard';
				header("Location: $uuu");
				//echo "added";

			}
			else
			{
				_f_message("Error Occured","Something went wrong when adding Service, please try again.");
				$uuu=base_url().'message';
				header("Location: $uuu");
			}

		}

	}
	public function edit()
	{
		$this->load->model('User3');
		$user=$this->User3->read($this->session->userdata('socialid'));

		$data['me'][0]=$user;

		$this->load->model('service');
		//if($this->uri->segment(3,0))
		//	{
		$checkid=$this->uri->segment(3,0);
		//var_dump($checkid);die(0);
		if($this->service->_is_service_owned($checkid))
		{
			$data['title']="Edit Service";

			$this->parser->parse('templates/header',$data);

			$config = array (
				array(
					'field'=>'service-name',
					'label'=>'Service Name',
					'rules'=>'required|callback_santext[Service Name]'
				),
				array(
					'field'=>'service-description',
					'label'=>'Description',
					'rules'=>'required'
				));

			$this->form_validation->set_rules($config);



			if ($this->form_validation->run() == FALSE)
			{
				$this->load->model('service','new');
				if($this->uri->segment(3,0))
				{	
					$serviceid=$this->new->id=$this->uri->segment(3,0);
					$service_data=$this->new->read($serviceid);

					$data['service_id']=$serviceid;
					$data['service']=$service_data[0];
					$data['tags']=$this->new->get_tag_names();

					$data['service_description']=$this->new->description;
					$this->load->view('edit_service',$data);
					$this->load->view('templates/footer');
				}
				else
				{
					echo "No resource id set";
				}

			}
			else
			{
				$this->load->model('service','new');
				$serviceid=$this->new->id=$this->uri->segment(3,0);
				$this->new->id=$serviceid;
				$this->new->name=$_POST['service-name'];
				$this->new->description=$_POST['service-description'];
				$this->new->servicetags=json_decode($_POST['service-tags'],true);
				$this->new->target_phases=$_POST['service-target-phases'];
				$this->new->target_sectors=$_POST['service-target-sectors'];

				$action=$this->new->patch();
				//$action=$this->new->update_tags();
				if($action==1)
				{
					$uuu=base_url().'dashboard';
					header("Location: $uuu");

				}	
				elseif(isset($action['status'])?$action['status']=='error':1)
				{
					_f_message("Error Occured",$action['description']);

					$uuu=base_url().'message';
					header("Location: $uuu");


				}
				else
				{
					_f_message("Error Occured","Something went wrong when adding Idea, please try again.".$action['description']);
					$uuu=base_url().'message';
					header("Location: $uuu");
				}

			}

		}
		else
		{
			echo "Unauthorized";
		}

		//}
		//	else
		//	{
		//		echo "No resource id set";
		//	}
	}


	public function is_service_of_owner($ideaname)
	{
		$ideaname=$this->db->escape($ideaname);
		$ownerid=$this->session->socialid;
		$query="select count(*) from ideas where name=$ideaname  and ownerid=$ownerid";

		$action=$this->db->query($query);


		if($action)
		{
			$row=$action->result_array();
			//var_dump($row);
			if(isset($row))
			{

				if($row[0]['count(*)']==1)
				{
					return true;
				}
				else
				{
					return false;

				}

			}


		}

	}	
	public function santext($parameter,$field)
	{         
		$this->form_validation->set_message('santext', "No special characters allowed in $field");
		$result=_f_validate('santext',$parameter);


		if($result==NULL)
		{

			return FALSE;


		}
		else if($result==$parameter)
		{
			return TRUE;
		}

	}
	public function form_validate($parameter,$type)
	{
		$result=_f_validate($type,$parameter);

		if($result==NULL)
		{

			return FALSE;


		}
		else if($result==$parameter)
		{
			return TRUE;
		}

	}

}
?>
