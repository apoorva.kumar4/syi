<?php
class Social extends CI_Controller{
    public function index()
    {

    }
    protected function facebook()
    {
        $this->load->model('User3');
        echo "Contacting facebook<br>";
        /*------------------- STEP 1: We check if we are already logged in via facebook ------------------------------------------------------ */
        $facebook_appid="1207206172646530"; /* Sayourideas facebook app id */


        /* We do not have access token present so we check if we have 'code' present to exchange it from facebook */
        //$loc_socialid=$this->session->userdata('socialid');
        $facebook_code=isset($_GET["code"])?$_GET["code"]:NULL;
        if(isset($facebook_code) && !empty($facebook_code))
        {
            //We have a 'code' so perform code check and get the access token
            //Perform 'code' check, exchange it with facebook access token
            $curl_url="https://graph.facebook.com/v2.3/oauth/access_token?client_id=".$facebook_appid."&redirect_uri=".base_url()."social/facebook&client_secret=1fb2e30e117f2fd80e97630478de2a1b&code=".$facebook_code;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $curl_url
            ));

            $resp= curl_exec($curl);
            $result=json_decode($resp);

            $access_token=$result->{'access_token'};

            //var_dump($access_token);die(0);

            //echo "Receiving data<br>";

            // We have set the session access_token variable now get the socialid from facebook graph and validate it with our database,
            // if the database does not have the entry then perform an insertion in database with name, email, and socialid.
            $curl_g= curl_init();
            curl_setopt_array( $curl_g, array(CURLOPT_RETURNTRANSFER => 1,CURLOPT_URL => 'https://graph.facebook.com/v2.8/me?fields=id,first_name,last_name,email&access_token='.$access_token));
            $resp2 = curl_exec($curl_g);
            $result = json_decode($resp2);
            //var_dump($result);die(0);
            if(!$result->{'email'})
            {
                $result->{'email'}=$result->{'id'}."@facebook.com";
            }


            $soc_data=array();
            $soc_data['first_name']=$result->{'first_name'};
            $soc_data['last_name']=$result->{'last_name'};
            $soc_data['email']=$result->{'email'};
            $soc_data['id']=$result->{'id'};
            $soc_data['access_type']="facebook";

            if($user=$this->User3->is_user_present($soc_data['email'])) {
                $soc_data['registered']=true;  
                $this->session->set_userdata('socialid',$user['socialid']);
                $soc_data['socialid']=$user['socialid'];
            } else $soc_data['registered']=false;
            
            $this->return_data_window($soc_data);
        }
        else
        {
            //Redirect him to facebook login
            //echo "Redirecting to facebook";
            $base=base_url();
            $url="https://www.facebook.com/dialog/oauth?client_id=$facebook_appid&redirect_uri=$base"."social/facebook&scope=public_profile,email";
            echo "<script>window.location.replace('$url')</script>";
        }
    }
    protected function google()
    {
        echo "Contacting Google<br>";
        $client = new Google_Client();
        $redirect_uri =  'https://www.sayourideas.com/social/google';
        //$client->setAuthConfig('client_secret_1051696419271-meinnilhvembfujffl4sp08aam91i73c.apps.googleusercontent.com.json');
        $client->setAuthConfig('client_secret_1051696419271-c25kfe06bk2hlv55or5afe6ica5hejvm.apps.googleusercontent.com.json');
        $client->setScopes(array('https://www.googleapis.com/auth/userinfo.email','https://www.googleapis.com/auth/userinfo.profile'));

        if(isset($_GET['code']))
        {
            echo "got code";die(0);
            $client->authenticate($_GET['code']);
            $assert=$client->getAccessToken();
            $client->setAccessToken($assert);
            $headers = array();
            $access_token_array=$assert;
            $token=$access_token_array['access_token'];
            $headers[]="Authorization: Bearer ".$token;
            $curl_url="https://www.googleapis.com/oauth2/v2/userinfo";
            $curl_g = curl_init();
            curl_setopt_array($curl_g, array(
                CURLOPT_URL=>$curl_url,
                CURLOPT_RETURNTRANSFER=>1,
                CURLOPT_HTTPHEADER=>$headers,
            ));

            $resp=curl_exec($curl_g);
            $result=json_decode($resp);
            $trim_name= explode(' ',trim($result->{'name'}));
            $soc_data=array();
            var_dump($result);die(0);
            $soc_data['first_name']=$trim_name[0];
            $soc_data['last_name']=$result->{'family_name'};
            $soc_data['email']=$result->{'email'};
            $soc_data['id']=$result->{'id'};
            $soc_data['access_type']="google";
            if($user=$this->User3->is_user_present($soc_data['email'])) {
                $soc_data['registered']=true;  
                $this->session->set_userdata('socialid',$user['socialid']);
                $soc_data['socialid']=$user['socialid'];
            } else $soc_data['registered']=false;
            
            $this->return_data_window($soc_data);
        }
        else
        {
            $ccc=$client->createAuthUrl();
            header("Location: $ccc");
        }
    }
    protected function linkedin()
    {


        //LinkedIn Application Specific constants
        echo "Contacting LinkedIn<br>";
        $private_state='ECDiuq34y56w74e57g45ljsdv6'; // A ramdom state parameter to prevent CSRF
        $base=base_url();
        $redirect_uri="$base"."social/linkedin";
        $client_id='771ttq6vetzfq3';
        $client_secret='fTPwkNoKSpStl1Mb';

            /* We do not have access token present so we check if we have 'code' present to exchange it from LinkedIn */

            $linkedin_code=isset($_GET["code"])?$_GET["code"]:NULL;
            if(isset($linkedin_code) && !empty($linkedin_code))
            {
                //We have a 'code' so perform code check and get the access token
                //Before we exchange it for an access token we verify that the code belongs to our app by checking the state parameter returned in the the GET

                $returned_state=isset($_GET["state"])?$_GET["state"]:NULL;
                if($returned_state==$private_state)
                {
                    //The code matches
                    ////Perform 'code' check, exchange it for a LinkedIn access token

                    $curl_url="https://www.linkedin.com/oauth/v2/accessToken";
                    $curl_post_query="grant_type=authorization_code&code=".$linkedin_code."&redirect_uri=".$redirect_uri."&client_id=".$client_id."&client_secret=".$client_secret;
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => $curl_url,
                        CURLOPT_POST =>1,
                        CURLOPT_POSTFIELDS => $curl_post_query
                    ));

                    $resp= curl_exec($curl);
                    $result=json_decode($resp,true);

                    $access_token=isset($result['access_token'])?$result['access_token']:NULL;

                    $headers=array();

                    $headers[]="Authorization: Bearer ".$access_token;
                    $curl_g= curl_init();
                    curl_setopt_array( $curl_g, array(CURLOPT_RETURNTRANSFER => 1,CURLOPT_URL => 'https://api.linkedin.com/v1/people/~:(id,firstName,lastName,email-address)?format=json&access_token='.$access_token,CURLOPT_HTTPHEADER=>$headers));
                    $resp2 = curl_exec($curl_g);
                    $result = json_decode($resp2,true);

                    $soc_data=array();
                    $soc_data['first_name']=$result['firstName'];
                    $soc_data['last_name']=$result['lastName'];
                    $soc_data['email']=$result['emailAddress'];
                    $soc_data['access_type']="linkedin";
                    $soc_data['registered']=_f_is_user_present($soc_data['email'])==1?true:false;
                    $this->return_data_window($soc_data);
                }
                else
                {
                    //Auth failed
                    echo "Authorization  Failed, please try again";

                }
            }
            else
            {
                $linkedin_url=	"https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=".$client_id."&redirect_uri=".$redirect_uri."&scope=r_basicprofile r_emailaddress&state=".$private_state;
                echo"<script>window.location.replace('$linkedin_url')</script>";
            }
    }
    public function sayourideas()
    {
        //Retrieve email and password from POST
        $this->load->model('User3');

        $post_email=isset($_POST['email'])?$_POST['email']:NULL;
        $post_password=isset($_POST['password'])?$_POST['password']:NULL;
        
        // verify credentials and fetch user
        $user=$this->User3->is_user_present($post_email);
        if($user) {
            if(password_verify($post_password,$user['password_hash'])){
                // user validated
                //$this->session->set_userdata('socialid',$user['socialid']);
                if($user['blocked']==1) {
                     $data['status']="failure";
                     $data['description']="Your Account is blocked. Contact us for help.";
                    echo json_encode($data);
                    die(0);
                }
                $_SESSION['socialid']=$user['socialid'];
                log_message('debug',print_r($_SESSION,true));
                $soc_data['email']=$user['email'];
                $analytics['socialid']=$user['socialid'];
                $analytics['event_category']="user";
                $analytics['event_action']="login";
                $analytics['event_value']="sayourideas";
                $analytics['event_label']="login";
                $analytics['event_description']="User logged in";
                a_send($analytics);
                
                $data['status']="success";
                $data['description']="login success";
                echo json_encode($data);

            }
            else {
                $data['status']="failure";
                $data['description']="Your email/password combination is not correct!";
                echo json_encode($data);
            }
        } else
        { 
            $data['status']="failure";
            $data['description']="Your email/password combination is not correct!";
            echo json_encode($data);
        }

    }
    protected function return_data_window($data){
            $soc_json=json_encode($data);

            if($data['registered']==true) {
                $analytics['socialid']=$data['socialid'];
                $analytics['event_category']="user";
                $analytics['event_action']="login";
                $analytics['event_value']=$data['access_type'];
                $analytics['event_description']="User logged in";
                $analytics['event_label']="login";
                a_send($analytics);    
            }
                        
            ?>
            <script>
                window.opener.perform_check();
                window.close();
            </script>
            <?php
    }
}
?>