<?php
class Delete_me extends CI_Controller {
    public function index() {
        if(!_f_is_loggedin()) {
            header('Location: /login');
            die(0);
        }

        if($_SERVER['REQUEST_METHOD']=="POST") {
            $this->load->model('User3');
            $this->User3->delete($this->session->socialid);
            
            $array_items = array('access_token','access_type','f_userid','l_userid','socialid','name','lastname','email','g_userid','company');
            $this->session->unset_userdata($array_items);
            
            $this->load->view('account_deleted');

        } else {
            header('Location: /dashboard');
        }
    }
}
?>