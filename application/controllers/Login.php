<?php
class Login extends CI_Controller{
    public function index()
	{
        log_message('debug','at login page');
        if(_f_is_loggedin())
        {
            log_message('debug','redirecting to dashboard...');
            $uuu='/dashboard';
            header("Location: $uuu");
            exit(0);

        }
        else
        {
            $data['title']="Welcome to SaYourIdeas.com";
            $this->parser->parse('templates/header',$data);
            $this->load->view('login',$data);
            $this->load->view('templates/footer',$data);
            
        }
	}		
}
?>
