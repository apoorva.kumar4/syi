<?php
class Reviews extends CI_Controller
{
	public function index()
	{
		if(_f_is_loggedin())
		{
			$this->load->model('User3');
        	$user=$this->User3->read($this->session->userdata('socialid'));

        	$data['me'][0]=$user;
			$this->load->model('user');
			$data['reviews']=$this->user->get_reviews();
			$data['title']="Reviews";
			$this->parser->parse('templates/header',$data);
			$this->load->view('reviews');
			$this->load->view('templates/footer');

		}
	}
	public function id()
	{

	}
	public function add()
	{
		$data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
		$data['title']="Review a Service Provider";
		$config = array (
			array(
				'field'=>'rate_card-name',
				'label'=>'Rate Card Name',
				'rules'=>'required|callback_santext[Idea Name]`'
			),
			array(
				'field'=>'service-description',
				'label'=>'Description',
				'rules'=>'required'
			));

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE)
		{
			$this->parser->parse('templates/header',$data);
			$this->load->view('new_review',$data);
			$this->load->view('templates/footer');
		}
		else
		{

		}
	}
	public function santext($parameter,$field)
	{
		$this->form_validation->set_message('santext', "No special characters allowed in $field");
		$result=_f_validate('santext',$parameter);


		if($result==NULL)
		{

			return FALSE;


		}
		else if($result==$parameter)
		{
			return TRUE;
		}

	}
	public function form_validate($parameter,$type)
	{
		$result=_f_validate($type,$parameter);

		if($result==NULL)
		{

			return FALSE;


		}
		else if($result==$parameter)
		{
			return TRUE;
		}

	}

}
?>