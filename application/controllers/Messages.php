<?php
class Messages extends CI_Controller
{
	public function index()
	{

	}
	public function new()
	{
        $data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
		if(_f_is_loggedin())
        {

            $status=_f_is_user_approved();
            if($status==1)
            {
                $data['title']="New Message";
                $this->load->model('user');
                $suggestions=$this->user->get_message_sender_suggestions($this->session->socialid);
                $data['suggestions']=$suggestions;
                if(segx(3)!=0) $data['active']=_get_username_from_socialid(segx(3));
                else $data['active']='';
                $this->parser->parse('templates/header',$data);

                $this->load->view('new_message');
                $this->load->view('templates/footer');
            }else if($status==2)
            {
                $data['title'] = "Dashboard";
                $this->parser->parse('templates/header', $data);
                $this->load->view('account_rejected', $data);
                $this->load->view('templates/footer', $data);
            }
            else{
                $data['title'] = "Dashboard";
                $this->parser->parse('templates/header', $data);
                $this->load->view('account_pending_approval', $data);
                $this->load->view('templates/footer', $data);
            }

        }
        else{
		    header('Location: /login');
        }
	}

}
?>