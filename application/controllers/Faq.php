<?php
class Faq extends CI_Controller{
    public function index()
    {
        $data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
        
        if(_f_is_loggedin()){
            $analytics['socialid']=$this->session->socialid;
            $analytics['event_category']="page_visit";
            $analytics['event_action']="visit";
            $analytics['event_label']="faq_page";
            $analytics['event_value']='faq';
            $analytics['event_description']="Visited Faq page";
            a_send($analytics);
        }

        $data['title']="SaYourIdeas.com - FAQ's";
        $data['ideator_faqs'] = $this->get_faqs('ideator');	
        $data['sp_faqs'] = $this->get_faqs('service_provider');
	$data['vc_faqs'] = $this->get_faqs('investor');
	$this->parser->parse('templates/header',$data);
        $this->load->view('faq',$data);
	
        $this->load->view('copyfooter');
    }
	private function get_faqs($user_type) {
		$action=$this->db->get_where('tbl_posts',array('user_type'=>$user_type));
		$results=$action->result_array();
		return $results;
		
    	}
}
?>
