<?php

class Stay_tuned extends CI_Controller
{
  public function index()
  {

    $data['title']="Stay Tuned - SaYourIdeas.com";
    $this->parser->parse('templates/header',$data);

    if (empty($_POST))
    {
      $this->load->view('stay_tuned',$data);
    }
    else
    {
      // looks like someone has submitted a form, let's validate it

      if (isset($_POST['email'],$_POST['g-recaptcha-response']))
      {
        //Check captcha with google using curl
        $secret="6LfJEwcUAAAAAAa_oAlSvTfi_z8wbEcYiXQjm2ek"; //Secret for aashay0101@gmail.com's no-captcha account
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $decoded_response=json_decode($verifyResponse,true);

        if($decoded_response['success']==true)
        {
          // success! your form was validated
          // do what you like next ...
          $this->prelaunch_notify->email=$_POST['email'];
          if($this->prelaunch_notify->create())
          {
            $this->load->view('stay_tuned_success');
          }
          else
          {
            $this->load->view('stay_tuned_failure');
          }
        }
        else
        {

          $this->load->view('stay_tuned_invalid_captcha',$data);
        }


      }
      else
      {
        // alas! the validation has failed, the user might be a spam bot or just got the result wrong
        // handle this as you like
        $this->load->view('stay_tuned_invalid_captcha',$data);
      }

    }
    $this->load->view('templates/footer',$data);
  }
}
?>