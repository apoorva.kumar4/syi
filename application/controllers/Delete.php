<?php
class Delete extends CI_Controller {
    public function index() {
        if(!_f_is_loggedin()) {
            header('Location: /login');
            die(0);
        }

        //fetch all information about the logged in user
        $this->load->model('User3');
        $user=$this->User3->read($this->session->userdata('socialid'));

        $data['me'][0]=$user;

        $data['title']="Delete your Account";
        $this->load->view('delete_account',$data);
    }
}
?>