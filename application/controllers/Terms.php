<?php
class Terms extends CI_Controller{
    public function index()
    {
    	$data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
        $this->load->model('about_us');
        $data['terms']=$this->about_us->read('terms');

        $data['title']="Terms and Conditions - SaYourIdeas.com";
        $this->parser->parse('templates/header',$data);
        $this->load->view('terms',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('copyfooter');
    }
}
?>