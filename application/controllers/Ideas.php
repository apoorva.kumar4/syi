<?php
class Ideas extends CI_Controller
{
	public function index()
	{
        header('Location: /startups');
	}
    public function _remap($method)
    {
        if($method === 'bullets')
        {
            $this->bullets();
        }
        else if($method==='add')
        {
            $this->add();
        }
        else if($method==='edit')
        {
            $this->edit();
        }
        else if($method==='index')
        {
            $this->index();
        }
        else if($method==='documents')
        {
            $this->documents();
        }
        else if($method==='notes'){
            $this->notes();
        }
        else
        {
            $this->default_method();

        }
    }
    public function documents()
    {
        $data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
        if(segx(3)==="upload")
        {

            $this->load->model('ideator');
            $this->load->model('idea');

            $checkid=$this->uri->segment(4,0);
            if($this->ideator->_is_idea_owned($checkid))
            {

                    $this->load->model('idea');

                    $fetched_idea=$this->idea->read($checkid);
                    $data['idea_id']=$fetched_idea['id'];
                    $data['idea_name']=$fetched_idea['name'];
                    $data['title']="Upload document for your idea";
                    $this->parser->parse('templates/header.php',$data);
                    $this->load->view('idea_document_upload_form',$data);

            }
            else {
                echo "You are not authorized to access the page : 841";
            }
        }
        else
        {
            $this->load->model('ideator');
            $this->load->model('idea');

            $checkid=$this->uri->segment(3,0);
            if($this->ideator->_is_idea_owned($checkid))
            {
                //echo "here are your documents";
                $data['title']="Documents";
                $this->parser->parse('templates/header',$data);
                $fetched_idea=$this->idea->read($checkid);
                $data['idea_name']=$fetched_idea['name'];
                $data['idea_id']=$fetched_idea['id'];
                $data['documents']=$this->idea->get_documents($checkid);
                $this->load->view('idea_documents_dashboard',$data);
            }
            else
            {
                echo "You are not authorized to access the page : 841";
            }

        }

    }
    public function notes()
    {
	   $data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
        if(segx(3)==="add")
        {

            $this->load->model('ideator');
            $this->load->model('idea');

            $checkid=$this->uri->segment(4,0);
            if($this->ideator->_is_idea_owned($checkid))
            {
                if(segx(5)==="do")
                {
                    $config = array (
                        array(
                            'field'=>'note_content',
                            'label'=>'Note content',
                            'rules'=>'required'
                        )
                    );

                    $this->form_validation->set_rules($config);

                    if($this->form_validation->run() == FALSE) {
                        $this->load->model('idea');

                        $fetched_idea=$this->idea->read($checkid);
                        $data['idea_id']=$fetched_idea['id'];
                        $data['idea_name']=$fetched_idea['name'];
                        $data['title']="Notes";
                        $this->load->view('templates/header.php',$data);
                        $this->load->view('idea_notes_form',$data);

                    } else{
                        $db_data= array('ideaid'=>$checkid,'name'=>isset($_POST['note_name'])?$_POST['note_name']:null,'text'=>$_POST['note_content']);
                        $action=$this->db->insert('idea_notes',$db_data);
                        if($action)
                        {
                            $data['title']="Success!";
                            $data['idea_id']=$checkid;
                            $this->load->view('templates/header.php',$data);
                            $this->load->view('idea_note_success', $data);
                        }
                        else
                        {
                            $data['title']="Failure!";
                            $data['idea_id']=$checkid;
                            $this->load->view('templates/header.php',$data);
                            $this->load->view('idea_note_failure', $data);
                        }

                    }
                }
                else
                {
                    $this->load->model('idea');

                    $fetched_idea=$this->idea->read($checkid);
                    $data['idea_id']=$fetched_idea['id'];
                    $data['idea_name']=$fetched_idea['name'];
                    $data['title']="Notes";
                    $this->load->view('templates/header.php',$data);
                    $this->load->view('idea_notes_form',$data);
                }




            }
            else {
                echo "You are not authorized to access the page : 841";
            }
        }
        else if(segx(3)==="edit") {
            $this->load->model('ideator');
            $this->load->model('idea');
            $checkid=segx(4);
            $action=$this->db->get_where('idea_notes',array('id'=>$checkid));
            $row=$action->result_array();

            if($this->ideator->_is_idea_owned($row[0]["ideaid"]))
            {
                if(segx(5)==="do")
                {
                    if(segx(6))
                    {

                    }
                    else
                    {

                    }
                    $config = array (
                        array(
                            'field'=>'note_content',
                            'label'=>'Note content',
                            'rules'=>'required'
                        )
                    );

                    $this->form_validation->set_rules($config);

                    if($this->form_validation->run() == FALSE) {
                        $this->load->model('idea');

                        $fetched_idea=$this->idea->read($checkid);
                        $data['idea_id']=$fetched_idea['id'];
                        $data['idea_name']=$fetched_idea['name'];
                        $data['title']="Notes";
                        $this->parser->parse('templates/header.php',$data);
                        $this->load->view('idea_notes_form',$data);

                    } else{
                        $_POST['note_content']=htmlspecialchars($_POST['note_content']);
                        $db_data= array('name'=>isset($_POST['note_name'])?$POST['note_name']:null,'text'=>$_POST['note_content']);
                        $this->db->where('id',$checkid);
                        $action=$this->db->update('idea_notes',$db_data);
                        if($action)
                        {
                            $data['title']="Success!";
                            $data['idea_id']=$row[0]["ideaid"];
                            $this->load->view('templates/header.php',$data);
                            $this->load->view('idea_note_success', $data);
                        }
                        else
                        {
                            $data['title']="Failure!";
                            $data['idea_id']=$checkid;
                            $this->load->view('templates/header.php',$data);
                            $this->load->view('idea_note_failure', $data);
                        }

                    }
                }
                else
                {
                    $this->load->model('idea');
                    $data['note']=$row[0];
                    $data['idea_id']=$row[0]["ideaid"];
                    $data['title']="Edit Note";
                    $this->load->view('templates/header.php',$data);
                    $this->load->view('idea_notes_form',$data);
                }




            }
            else {
                echo "You are not authorized to access the page : 841";
            }

        }
        else
        {
            $this->load->model('ideator');
            $this->load->model('idea');

            $checkid=$this->uri->segment(3,0);
            if($this->ideator->_is_idea_owned($checkid))
            {
                $fetched_idea=$this->idea->read($checkid);
                $data['idea_name']=$fetched_idea['name'];
                $data['idea_id']=$fetched_idea['id'];
                $data['title']="Notes for ".$data['idea_name'];
                $data['note']=$this->idea->get_note_for_idea($checkid);
                if(sizeof($data['note'])==0)
                {
                    $this->load->model('idea_note');
                    $par = array (
                        'name' => '',
                        'text' => '',
                        'ideaid' => $fetched_idea['id']
                    );
                    $this->idea_note->create($par);
                }
                $data['note']=$this->idea->get_note_for_idea($checkid);


                $this->parser->parse('templates/header',$data);
                $this->load->view('idea_notes_dashboard',$data);
            }
            else
            {
                echo "You are not authorized to access the page : 841";
            }

        }

    }
    public function add() {
			if(_f_is_loggedin()) {
			$this->load->model('User3');
      $user=$this->User3->read($this->session->userdata('socialid'));
      $data['me'][0]=$user;

			}
			$data['title']="Add a new Idea";
        $this->parser->parse('templates/header',$data);
        $this->load->view('new_idea2');
        $this->load->view('templates/footer');
	}
	public function edit()
	{
        $data = array();
        if(_f_is_loggedin()) {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->userdata('socialid'));
            $data['me'][0]=$user;

        }
        
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
            $this->load->model('idea');
            $fetched_idea=$this->idea->read(segx(3));
            if($fetched_idea['ownerid']==$this->session->socialid || _f_is_admin())
            {
                $data['title']="Edit Idea";
                $data['idea']=$fetched_idea;
                $this->parser->parse('templates/header',$data);
                $this->load->view('edit_idea_extra_v3',$data);
                $this->load->view('templates/footer');
            }
            else
            {
                echo "Unauthorized";
            }
	}
    public function is_idea_of_owner($ideaname)
	 {
		$ideaname=$this->db->escape($ideaname);
		$ownerid=$this->session->socialid;
		$query="select count(*) from ideas where name=$ideaname  and ownerid='$ownerid'";

		$action=$this->db->query($query);


			if($action)
			{
				$row=$action->result_array();
				//var_dump($row);
				if(isset($row))
				{

					if($row[0]['count(*)']==1)
					{
							return true;
					}
					else
					{
						return false;

					}

				}


			}

	 }
	public function santext($parameter,$field)
	 {
		$this->form_validation->set_message('santext', "No special characters allowed in $field");
		$result=_f_validate('santext',$parameter);


	 if($result==NULL)
	 {

	 	return FALSE;


	 }
	 else if($result==$parameter)
	 {
	 	return TRUE;
	 }

	 }
	public function url_availablility($param)
	 {
	 	$this->load->model('idea');
		if(isset($this->session->socialid))
		{
			$this->idea->ownerid=$this->session->socialid;
		}

	 	$this->form_validation->set_message('url_availablility', "URL $param is already taken, please choose another one");
	 	$ret=$this->idea->check_url_availability($param);

	 	return $ret;

	 }
    public function visibility_validate($param)
    {
        $this->form_validation->set_message('visibility_validate', "Please select the correct option for Idea Visibility");
        if($param == 1 || $param == 0)
            return true;
        else
            return false;
        $this->load->model('idea');

    }
    public function default_method()
    {

        $this->load->model('User3');
        $socialid=$this->session->socialid;    
        $user=$this->User3->read($socialid);
        $data['user']=$user;
        $data['me'][0]=$user;

        // The default method will check if the idea with the name exists and if it is public it will be shown
        $slug=segx(2);
        $esc_slug=$this->db->escape($slug);
        $query="Select * from ideas where unique_url=$esc_slug";
        $action=$this->db->query($query);
        $row=$action->result_array();
        $ret_id=$row[0]['id'];
    	if(_f_is_loggedin() || _f_is_admin() || _f_get_idea_shareable_link($ret_id)==$_GET['token']) {

            $action2=$this->db->get_where('idea_team_members',array('ideaid'=>$ret_id));
            if ($action2)
            $row[0]['team_members']=$action2->result_array();
            if($row[0]['ownerid']==$this->session->socialid)
            {
                if($ret_id!==NULL)
                {

                    $this->load->model('idea');
                    $data['title']="SaYourIdeas - ".$row[0]['name'];
                    $data['idea']=$row[0];
                    $this->parser->parse('templates/header',$data);
                    $this->load->view('idea_view');
                    $this->load->view('templates/footer');
                }
                else
                {

                }
            }
            else
            {
                if($row[0]['visibility']==1 || _f_is_admin() || _f_is_approved_investor())
                {
                    $this->load->model('idea');
                    $data['title']="SaYourIdeas - ".$row[0]['name'];
                    $data['idea']=$row[0];
                    $this->parser->parse('templates/header',$data);
                    $this->load->view('idea_view');
                    $this->load->view('templates/footer');
                }
            }
    	}
    	else {
    		header('Location: /login');
    	}
        
    }
   
}
?>
