<?php
class Forgot extends CI_Controller
{
    public function index()
    {
        if(_f_is_loggedin())
        {
            header('Location: /dashboard');
        }
        else{
            $data = array();
            if(isset($this->session->socialid))
            {
                $this->load->model('User3');
                $user=$this->User3->read($this->session->socialid);
                $data['me'][0]=$user;
                
            }
        
            $data['title']="Reset your password";
            $this->parser->parse('templates/header',$data);
            $this->load->view('forgot_password');

        }
    }
    public function submit()
    {
        $required = array('mobile','g-recaptcha-response');
        $this->load->library('sanitize');
        $sanitized=$this->sanitize->filter_and_sanitize($required,$_POST);
        


        // if required properties not met, die
        if(!array_key_exists('mobile',$sanitized)) {
            header('Location: /forgot');
            die(0);
        }

        if(!array_key_exists('g-recaptcha-response',$sanitized)) {
            header('Location: /forgot');
            die(0);
        }         

        // if user is not present, die.
        $this->load->model('User3');

        if(!$this->User3->is_user_present($sanitized['mobile']))
        {
            header('Location: /forgot');
            die(0);
        }

        $this->load->library('Recaptcha'); 
        $captcha_api_secret=$this->Syiconfig->get('captcha_api_secret');
        $capthca_api_url=$this->Syiconfig->get('captcha_api_url');
        $g_captcha_response=$sanitized['g-recaptcha-response'];
        $is_valid=$this->recaptcha->verify_captcha($g_captcha_response,$captcha_api_secret,$capthca_api_url);
        
        
        // die if captcha is not valid

        if(!$is_valid) die(0);


        $this->load->model('Otp');
        $otp= $this->Otp->generate_otp();
        $insert_otp= $this->Otp->insert_otp($sanitized['mobile'],$otp);


        if(!$insert_otp) {
            // failed to insert otp
            //http_response_code(500);
            //echo "Error in sending otp,please try again";
            die(0);
        }

        $send_sms = $this->Otp->send_sms($sanitized['mobile'],"Your verification code for SaYourIdeas is $otp");

        if(!$send_sms) {
            http_response_code(500);
            //echo "Error in sending otp,please try again later.";
            die(0);
        }
            
        $data['title']="Reset your password";
        $data['mobile']=$sanitized['mobile'];
        $this->parser->parse('templates/header',$data);
        $this->load->view('forgot_password_sent');     
    }
    public function fd()
    {
        $data['title']="Reset your password";
        $this->parser->parse('templates/header',$data);
        $this->load->view('forgot_password_new_form');
    }
    public function chk()
    {
        $required=array('otp','mobile','new_password');
        $this->load->library('Sanitize');
        $sanitized=$this->sanitize->filter_and_sanitize($required,$_POST);    

        if(!array_key_exists('otp', $sanitized)){
            die(0);
        }    
        
        if(!array_key_exists('new_password',$sanitized)) {
            die(0);
        }

        if(!array_key_exists('mobile',$sanitized)) {
            die(0);
        }

        $this->load->model('Otp');

        $verify=$this->Otp->verify_otp($sanitized['mobile'],$sanitized['otp']);

        if(!$verify) {
            $data['title']="Reset your password";
            $data['mobile']=$sanitized['mobile'];
            $this->parser->parse('templates/header',$data);
            $this->load->view('forgot_password_sent');
            

        } else {

            $this->load->model('User3');
            $is_user_present=$this->User3->is_user_present($sanitized['mobile']);
            if(!$is_user_present) die(0);
            
            $update_data= array('socialid'=>$is_user_present['socialid'],'password_hash'=>password_hash($sanitized['new_password'],PASSWORD_DEFAULT));

            $action=$this->User3->update($update_data);

            //var_dump($update_data);
            //var_dump($action);
            //die(0);
            if($action) {

                $data['title']="Password was reset successfully.";
                $this->parser->parse('templates/header',$data);
                $this->load->view('forgot_password_success');

            } else {
                
            }

        }


    }
}
?>