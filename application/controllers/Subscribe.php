<?php
class Subscribe extends CI_Controller {
	public function index() {
		if(_f_is_loggedin()) {
			$this->load->model('User3');
			$user=$this->User3->read($this->session->userdata('socialid'));
			$data['me'][0]=$user;

		}	
		if(!array_key_exists('email', $_POST)) {
			$data['title']="Subscribe to our newsletter";
			$this->parser->parse('templates/header',$data);
			$this->load->view('subscribe',$data);
			$this->parser->parse('templates/footer',$data);
		} else {
			if(!array_key_exists('g-recaptcha-response', $_POST)) {
				die(0);
			}

			$is_valid=$this->verify_captcha($_POST['g-recaptcha-response']);

			if(!$is_valid) die(0);

			if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)) die(0);

			$action=$this->opt($_POST['email']);

			if(!$action) die(0);

			$data['title']="Thank you for registering for newsletter";
			$this->parser->parse('templates/header',$data);
			$this->load->view('subscribe_opt_success',$data);
			$this->parser->parse('templates/footer',$data);
		}
	}

	public function opt_out() {
		if(!array_key_exists('email', $_POST)) {
			$data['title']="Opt out from newsletter";
			$this->parser->parse('templates/header',$data);
			$this->load->view('subscribe_opt_out',$data);
			$this->parser->parse('templates/footer',$data);
		} else {
			if(!array_key_exists('g-recaptcha-response', $_POST)) {
				die(0);
			}

			$is_valid=$this->verify_captcha($_POST['g-recaptcha-response']);

			if(!$is_valid) die(0);

			if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)) die(0);

			$action=$this->do_opt_out($_POST['email']);

			$data['title']="Opt out from newsletter";
			$this->parser->parse('templates/header',$data);
			$this->load->view('subscribe_opt_out_success',$data);
			$this->parser->parse('templates/footer',$data);
		}
	}

	protected function opt($email) {
		$this->load->model('Subscribe_newsletter');
		$this->load->model('Semail');
		//check if email is already subscribed to newsletters
		
		$pre_check=$this->Subscribe_newsletter->is_email_subscribed($email);

		if($pre_check['status']!="success") {
			return false;
		}

		if($pre_check['is_subscribed']==true) {
			return true;
		}

		$action=$this->Subscribe_newsletter->add_email($email);
		if(!$action) return false;

		//send confirmation email
		
		$message="Thank you for Subscribing to SaYourIdeas Newsletter. We are excited to share the latest annoucements and updates with you. \r\n \r\nIf you wish to Unsubscribe at any point please click 'https://www.sayourideas.com/subscribe/opt_out'";

		$this->Semail->send('admin@sayourideas.com',$email,null,null,'[SYI] Thank you for subscribing!',$message);

		return true;

	}	

	protected function do_opt_out($email) {
		$this->load->model('Subscribe_newsletter');
		$action=$this->Subscribe_newsletter->delete_email($email);
		if($action) return true;
		else return false;
	}

	protected function verify_captcha($response) {
		$this->load->library('Recaptcha');
		$g_captcha_response=$response;
		$captcha_api_secret=$this->config->item('captcha_api_secret');
		$capthca_api_url=$this->config->item('captcha_api_url');
		$is_valid=$this->recaptcha->verify_captcha($g_captcha_response,$captcha_api_secret,$capthca_api_url);
		return $is_valid;
	}

	public function test_opt() {
		$data['title']="Thank you for subscribing!";
		$this->parser->parse('templates/header',$data);
		$this->parser->parse('subscribe_opt_out_success',$data);
		$this->parser->parse('templates/footer',$data);
	}

}
?>
