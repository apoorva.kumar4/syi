<?php
class Home extends CI_Controller{
	public function index()
	{
		$data = array();
		if(isset($this->session->socialid))
		{
						header('Location: /dashboard');
		}
		$data['title']="Welcome to SaYourIdeas.com";
		$data['meta_description']="Turn your unique ideas into a successful business, meet the investors, mentors, service providers, successful entrepreneurs and much more. SaYourIdeas connects ideators/start-ups with corporate organisations, investors, service providers, market experts, mentors and other successful entrepreneurs and founders. Our unique structure and technology helps up-and-coming entrepreneurs meet the right partners at different stages of their start-ups’ cycle.";
		$data['top_ideas']=_f_get_top_ideas();
		$data['testimonials']=_f_get_active_testimonials();
		$data['cities']=_f_get_cities();
		$data['services']=_f_get_services();
		$data['partners'] = _f_get_partners();
		$this->parser->parse('templates/header',$data);
		$this->load->view('home',$data);
		$this->load->view('copyfooter');
		
	}
}
?>
