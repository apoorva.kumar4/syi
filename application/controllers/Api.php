<?php

class Api extends CI_Controller
{
    public function index()
    {
    }

    public function v1()
    {
        $data = array();
        $seg3 = $this->uri->segment(3);

        // We do a priliminary check if the user is logged in , api is accessible to logged in users only

        if ((_f_is_loggedin())) {
            if ($seg3 === "ideas") {
                $this->v1_ideas();
            }
            else if ($seg3 === "users") {
                    $this->v1_users();
                }
            else if ($seg3 === "services") {
                        $this->v1_services();
                    }
            else {
                            $data['status'] = "failure";
                            $data['description'] = "Unknown class";
                            echo json_encode($data);
                        }
        }
        else {
            $data['status'] = "failure";
            $data['description'] = "You are not loggedin";
            echo json_encode($data);
        }
    }

    public function v3()
    {
        if (segx(3) === "ideas") {
            $this->v3_ideas();
        }

        if (segx(3) === "users") {
            $this->v3_users();
        }

        if (segx(3) === "events") {
            $this->v3_events();
        }

        if (segx(3) === "services") {
            $this->v3_services();

        }
    }
    public function v3_services()
    {
        if (segx(4) === "search") {
            if (segx(5)) {
                $validated_name = $this->db->escape("%" . segx(5) . "%");

                $action = $this->db->query("Select id,name as value from services where name like $validated_name");

                $result = $action->result_array();

                echo json_encode($result);

            } else {


                $action = $this->db->query("Select id,name as value from services");

                $result = $action->result_array();

                echo json_encode($result);

            }
        }
    }

    public function v1_ideas()
    {
        $this->load->model('idea');
        // The next parameter in the segment is going to be a unique id of an idea so perform an authorization check
        if (segx(4)) {
            $seg4=segx(4);
            $fetched_idea=$this->idea->read(segx(4));
            if ($fetched_idea['ownerid']==$this->session->socialid || _f_is_admin()) {
                $seg5 = $this->uri->segment(5, 0);
                if ($seg5 === "visibility") {
                    $seg6 = $this->uri->segment(6, 0);
                    if ($seg6 === "public" || $seg6 === "private" || $seg6 === "toggle") {
                        if ($seg6 === "toggle") {
                            if ($fetched_idea['visibility']== 1 ) {
                                $up_array = array(
                                    'id' =>segx(4),
                                    'visibility'=>"0"
                                );
                                if ($this->idea->patch($up_array)) {
                                    $data['status'] = "success";
                                    $data['ideaid'] = segx(4);
                                    $data['visibility'] = 'private';
                                    echo json_encode($data);
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "An unknown error occured, please try again.";
                                    echo json_encode($data);
                                }
                            } else
                                if ($fetched_idea['visibility'] == 0) {
                                    $up_array = array(
                                        'id' =>$fetched_idea['id'],
                                        'visibility'=>1
                                    );
                                    if ($this->idea->patch($up_array)){
                                        $data['status'] = "success";
                                        $data['ideaid'] = segx(4);
                                        $data['visibility'] = "public";
                                        echo json_encode($data);
                                    } else {
                                        $data['status'] = "failure";
                                        $data['description'] = "An unknown error occured, please try again.";
                                        echo json_encode($data);
                                    }
                                }
                        } else {
                            $up_array = array(
                                'id' =>$fetched_idea['id'],
                            );
                            if(segx(6)=="private")
                            {
                                $up_array['visibility']=0;
                            }
                            else if(segx(6)=="public")
                            {
                                $up_array['visibility']=1;
                            }
                            else
                            {
                                $up_array['visibility']=0;
                            }
                            if ($this->idea->patch($up_array)) {
                                $data['status'] = "success";
                                $data['ideaid'] = $seg4;
                                $data['visibility'] = $seg6;
                                echo json_encode($data);
                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "An unknown error occured, please try again.";
                                echo json_encode($data);
                            }
                        }
                    } else {
                        $data['status'] = "failure";
                        $data['description'] = "Unknown value for property $seg6";
                        echo json_encode($data);
                    }
                } else if ($seg5 === "logo")
                {
                        $seg6 = $this->uri->segment(6, 0);
                        if ($seg6) {
                            if ($seg6 === "upload") {
                                $file = $_FILES['logofile'];
                                $upload_params = array(
                                    'socialid' => $this->session->socialid,
                                    'file' => $file,
                                    'permissions' => 3,
                                    'accepted_types' => "image"
                                );
                                $this->load->model('supload');
                                $this->load->model('idea');
                                $uploadid = $this->supload->create($upload_params);
                                $tem_arr=array('ideaid'=>$seg4, 'uploadid'=>$uploadid['id']);
                                $action=$this->idea->add_logo($tem_arr);
                                echo json_encode($action);
                            } else
                                if ($seg6 === "remove") {
                                    $query = "Delete from idea_logos where ideaid=$seg4";
                                    $action = $this->db->query($query);
                                    if ($action) {
                                        $data['status'] = "success";
                                        $data['description'] = "Idea Logo Removed Successfully";
                                        echo json_encode($data);
                                    } else {
                                        $data['status'] = "failure";
                                        $data['description'] = "Temporary error try again";
                                        echo json_encode($data);
                                    }
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Invalid action for Idea Logo";
                                    echo json_encode($data);
                                }
                        } else {
                            /*Nothing set after /api/v1/idea/<id>/logo, so just return the latest logo for idea */
                            $seg4=segx(4);
                            $query = "select * from uploads where id in (select max(uploadid) from idea_logos where ideaid=$seg4)";
                            $action = $this->db->query($query);
                            if ($action) {
                                $row = $action->result_array();

                                if ($row) $remoteImage = $this->config->item('upload_path') . $row[0]['url'];
                                else {

                                    // $hash=hash("md5",$dataof_user[0]['email']);
                                    // $remoteImage="http://www.gravatar.com/avatar/".$hash."?s=300&d=identicon";

                                    $remoteImage = "Not set";
                                }

                                if ($imginfo = getimagesize($remoteImage)) {
                                    header("Content-type: " . $imginfo['mime']);
                                    readfile($remoteImage);
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Invalid File Type";
                                    echo json_encode($data);
                                }
                            } else {
                            }
                        }
                    } else if ($seg5 === "cover_pic") {
                        $seg6 = $this->uri->segment(6, 0);
                        if ($seg6) {
                            if ($seg6 === "upload") {
                                $file = $_FILES['coverfile'];
                                $upload_params = array(
                                    'socialid' => $this->session->socialid,
                                    'file' => $file,
                                    'permissions' => 3,
                                    'accepted_types' => "image"
                                );
                                $this->load->model('supload');
                                $this->load->model('idea');
                                $uploadid = $this->supload->create($upload_params);
                                $tem_arr=array('ideaid'=>$seg4, 'uploadid'=>$uploadid['id']);
                                $action=$this->idea->add_cover($tem_arr);
                                echo json_encode($action);
                            } else if ($seg6 === "remove") {
                                $query = "Delete from idea_cover_pics where ideaid=$seg4";
                                $action = $this->db->query($query);
                                if ($action) {
                                    $data['status'] = "success";
                                    $data['description'] = "Idea Cover Pic Removed Successfully";
                                    echo json_encode($data);
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Temporary error try again";
                                    echo json_encode($data);
                                }
                            } else {

                            }
                        } else {
                            $query = "select * from uploads where id in (select max(uploadid) from idea_cover_pics where ideaid=$seg4)";
                            $action = $this->db->query($query);
                            if ($action) {
                                $row = $action->result_array();
                                if ($row) $remoteImage = $this->config->item('upload_path') . $row[0]['url'];
                                else {

                                    // $hash=hash("md5",$dataof_user[0]['email']);
                                    // $remoteImage="http://www.gravatar.com/avatar/".$hash."?s=300&d=retro";

                                    $remoteImage = "Not set";
                                }

                                if ($imginfo = getimagesize($remoteImage)) {
                                    header("Content-type: " . $imginfo['mime']);
                                    readfile($remoteImage);
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Invalid File Type";
                                    echo json_encode($data);
                                }
                            }
                        }
                    } else if ($seg5 === "documents") {
                        $documentid = segx(6);
                        $this->load->model('upload');
                        $fetched_upload = $this->upload->get_upload($documentid);

                        if ($fetched_upload['socialid'] == $this->session->socialid) {
                            if (segx(7) === "delete") {
                                $this->upload->delete($documentid);
                                $this->db->delete('idea_uploads', array('uploadid' => $documentid));
                                $data['status'] = "success";
                                $data['description'] = "Delete Successful";
                                echo json_encode($data);
                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "Invalid Action on Document";
                                echo json_encode($data);
                            }
                        } else {
                            $data['status'] = "failure";
                            $data['description'] = "Access Denied for Deletion";
                            echo json_encode($data);
                        }

                    } else if ($seg5 === "notes") {
                        $noteid = segx(6);
                        $action=$this->db->get_where('idea_notes',array('id'=>$noteid));
                        $row=$action->result_array();
                        $fetched_note=$row[0];

                        if (segx(7) === "delete") {
                            $action2=$this->db->delete('idea_notes', array('id' => $noteid));
                            $data['status'] = "success";
                            $data['description'] = "Delete Successful";
                            echo json_encode($data);
                        } else if(segx(7)==="do"){
                            $_POST['note_content']=htmlspecialchars($_POST['note_content']);
                            $this->db->set('text',$_POST['note_content']);
                            $this->db->where('id',segx(6),false);
                            $this->db->where('ideaid',segx(4),false);
                            $action=$this->db->update('idea_notes');
                            if($action)
                            {
                                //success
                                $data['status'] = "success";
                                $data['description'] = "Note saved successfully man!";
                                echo json_encode($data);

                            }
                            else
                            {
                                $data['status'] = "failure";
                                $data['description'] = "Saving failed";
                                echo json_encode($data);
                            }
                        }
                        else {
                            $data['status'] = "failure";
                            $data['description'] = "Invalid Action on Document";
                            echo json_encode($data);
                        }
                    }
                    else
                        if ($seg5 === 0) {
                            $this->load->model('idea');
                            echo json_encode($this->idea->read(segx(4)));
                        } else
                            if ($seg5 === "delete") {
                                $this->load->model('idea');
                                if ($this->idea->delete(segx(4))) {
                                    $data['status'] = "success";
                                    $data['description'] = "Delete Successful";
                                    echo json_encode($data);
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Unknown property";
                                    echo json_encode($data);
                                }
                            } else
                                if ($seg5 === "bullets") {
                                    $seg6 = $this->uri->segment(6, 0);
                                    if (segx(6) === "timeline") {
                                        //headers("Content-type: application/json");
                                        $bullets_data = $this->idea->get_bullets_for_timeline(segx(4));

                                        foreach ($bullets_data as &$bullet) {

                                            if ($bullet['template'] == "1") {
                                                $bullet['className'] = "bluex card hoverable";
                                                $bullet['content'] = "<span class='largetemplate'>" . $bullet['content'] . "</span>";
                                            }

                                        }

                                        echo json_encode($bullets_data);
                                    } else if (segx(6) === "update") {

                                        if (isset($_POST['data'])) {
                                            $data = json_decode($_POST['data'], true);

                                            foreach ($data['_data'] as $act_bullet) {
                                                var_dump($act_bullet);
                                                echo "<br>";
                                            }
                                            /*
                                            $bullet_name = $data[0]['value'];
                                            $bullet_description = $data[1]['value'];
                                            $bullet_start_date = $data[2]['value'];
                                            $this->load->model('bullet');
                                            $this->bullet->id = $seg6;
                                            $act1 = $this->bullet->update('name', $bullet_name);
                                            $act2 = $this->bullet->update('description', $bullet_description);
                                            $act3 = $this->bullet->update('start_date', $bullet_start_date);
                                            if ($act1 && $act2 && $act3) {
                                                $data1['status'] = "success";
                                                $data1['description'] = "All parameters updated successfuly";
                                                echo json_encode($data1);
                                            }
                                            else {
                                                $data1['status'] = "failure";
                                                $data1['description'] = "Some parameters could not be updated";
                                                echo json_encode($data1);
                                            }*/
                                        } else {
                                            $data['status'] = "failure";
                                            $data['description'] = "No Post Data Found";
                                            echo json_encode($data);
                                        }


                                    } else if ($seg6 === "add") {
                                        $this->load->model('bullet');
                                        if (isset($_POST['data'])) {
                                            $data = json_decode($_POST['data'], true);
                                            $bullet_name = $data['content'];
                                            $bullet_description = $data['description'];
                                            $bullet_start_date = date("Y-m-d", strtotime($data['start']));
                                            $this->bullet->ideaid = segx(4);
                                            $this->bullet->description = $bullet_description;
                                            $this->bullet->name = $bullet_name;
                                            $this->bullet->start_date = $bullet_start_date;
                                            $act1 = $this->bullet->create();
                                            if ($act1) {
                                                if(_f_is_loggedin()){
                                                    $analytics['socialid']=$this->session->socialid;
                                                    $analytics['event_category']="idea";
                                                    $analytics['event_action']="bullet";
                                                    $analytics['event_label']="added";
                                                    $analytics['event_value']=$fetched_idea['name'];
                                                    $analytics['event_description']="Created new bullet named ".$bullet_name;
                                                    a_send($analytics);
                                                }
                                                $data1['status'] = "success";
                                                $data1['description'] = "Added Bullet Successfuly";
                                                $data1['item'] = $this->bullet->get_bullet_from_id($act1);
                                                $this->load->model('idea');
                                                $bullets_data = $this->idea->get_bullets_for_timeline(segx(4));

                                                foreach ($bullets_data as &$bullet) {

                                                    if ($bullet['template'] == "1") {
                                                        $bullet['className'] = "bluex card hoverable";
                                                        $bullet['content'] = "<span class='largetemplate'>" . $bullet['content'] . "</span>";
                                                    }

                                                }

                                                //
                                                $data1['bullets'] = $bullets_data;
                                                echo json_encode($data1);
                                            } else {
                                                $data1['status'] = "failure";
                                                $data1['description'] = "Bullet creation failed";
                                                echo json_encode($data1);
                                            }
                                        } else {
                                            $data1['status'] = "failure";
                                            $data1['description'] = "No POST data found";
                                            echo json_encode($data1);
                                        }
                                    } else if ($seg6) {
                                        $seg7 = $this->uri->segment(7, 0);
                                        if ($seg7) {
                                            if ($seg7 === "update") {
                                                if (isset($_POST['data'])) {
                                                    $data = json_decode($_POST['data'], true);
                                                    $bullet_name = $data['content'];
                                                    $bullet_description = $data['description'];
                                                    $bullet_start_date = $data['start'];
                                                    $this->load->model('bullet');
                                                    $this->bullet->id=$seg6;
                                                    $this->bullet->name=$bullet_name;
                                                    $this->bullet->description=$bullet_description;
                                                    $this->bullet->start_date=$bullet_start_date;
                                                    $act1 = $this->bullet->update();
                                                    if ($act1) {
                                                        $data1['status'] = "success";
                                                        $data1['description'] = "All parameters updated successfuly";
                                                        if(_f_is_loggedin()){
                                                            $analytics['socialid']=$this->session->socialid;
                                                            $analytics['event_category']="idea";
                                                            $analytics['event_action']="bullet";
                                                            $analytics['event_label']="updated";
                                                            $analytics['event_value']="updated";
                                                            $analytics['event_description']="Updated bullet name ".$bullet_name;
                                                            a_send($analytics);
                                                        }
                                                        echo json_encode($data1);
                                                    } else {
                                                        $data1['status'] = "failure";
                                                        $data1['description'] = "Some parameters could not be updated";
                                                        echo json_encode($data1);
                                                    }
                                                } else {
                                                    $data['status'] = "failure";
                                                    $data['description'] = "No Post Data Found";
                                                    echo json_encode($data);
                                                }
                                            } else if ($seg7 === "updatex") {
                                                $this->load->model('bullet');
                                                if (isset($_POST['data'])) {
                                                    $data = json_decode($_POST['data'], true);
                                                    $bullet_name = strip_tags($data['content']);
                                                    $bullet_description = strip_tags($data['description']);
                                                    $bullet_start_date = date("Y-m-d", strtotime($data['start']));
                                                    $this->bullet->id = segx(6);
                                                    $this->bullet->description = $bullet_description;
                                                    $this->bullet->name = $bullet_name;
                                                    $this->bullet->start_date = $bullet_start_date;
                                                    $act1 = $this->bullet->update();
                                                    if ($act1) {
                                                        $data1['status'] = "success";

                                                        $data1['description'] = "Bullet Edited Successfuly";
                                                        if(_f_is_loggedin()){
                                                            $analytics['socialid']=$this->session->socialid;
                                                            $analytics['event_category']="idea";
                                                            $analytics['event_action']="bullet";
                                                            $analytics['event_label']="updated";
                                                            $analytics['event_value']="updated";
                                                            $analytics['event_description']="Updated bullet name ".$bullet_name;
                                                            a_send($analytics);
                                                        }
                                                        echo json_encode($data1);
                                                    } else {
                                                        $data1['status'] = "failure";
                                                        $data1['description'] = "Bullet Updation failed";
                                                        echo json_encode($data1);
                                                    }
                                                } else {
                                                    $data1['status'] = "failure";
                                                    $data1['description'] = "No POST data found";
                                                    echo json_encode($data1);
                                                }
                                            } else
                                                if ($seg7 === "delete") {
                                                    $this->load->model('bullet');
                                                    $this->bullet->id = $seg6;
                                                    $this->bullet->read();
                                                    $temp_name=$this->bullet->name;
                                                    $act1 = $this->bullet->delete();
                                                    if ($act1) {
                                                        $data1['status'] = "success";
                                                        $data1['description'] = "Bullet: $seg6 deleted successfully";
                                                        if(_f_is_loggedin()){
                                                            $analytics['socialid']=$this->session->socialid;
                                                            $analytics['event_category']="idea";
                                                            $analytics['event_action']="bullet";
                                                            $analytics['event_label']="deleted";
                                                            $analytics['event_value']="deleted";
                                                            $analytics['event_description']="Deleted bullet ".$temp_name;
                                                            a_send($analytics);
                                                        }
                                                        echo json_encode($data1);
                                                    } else {
                                                        $data1['status'] = "failure";
                                                        $data1['description'] = "Cannot Delete inbuilt bullet";
                                                        echo json_encode($data1);
                                                    }
                                                }
                                        } else {
                                            $this->load->model('bullet');
                                            echo json_encode($this->bullet->get_bullet_from_id($seg6));
                                        }
                                    } else {
                                        $this->load->model('idea');
                                        $this->idea->id = $seg4;
                                        echo json_encode($this->idea->get_bullets_for_timeline());
                                    }
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Unknown property";
                                    echo json_encode($data);
                                }
            } else {
                $data['status'] = "failure";
                $data['description'] = "You are not authorized to access this resource";
                echo json_encode($data);
            }
        } else {
            $data['status'] = "failure";
            $data['description'] = "No resource id set";
            echo json_encode($data);
        }
    }

    public function v1_services()
    {

        // The next parameter in the segment is going to be a unique id of an service so perform an authorization check

        $seg4 = $this->uri->segment(4, 0);
        if ($seg4) {
            $this->load->model('service_provider');
            if ($this->service_provider->is_service_owned($seg4)) {
                $seg5 = $this->uri->segment(5, 0);
                if ($seg5 === "tags") {
                    $this->load->model('service');
                    $this->service->id = $seg4;
                    $service_tags = $this->service->get_tag_names();
                    $data['status'] = "success";
                    $data['service_tags'] = $service_tags;
                    echo json_encode($data);
                } else {
                    $data['status'] = "failure";
                    $data['description'] = "Unknown property";
                    echo json_encode($data);
                }
            } else {
                $data['status'] = "failure";
                $data['description'] = "You are not authorized to access this resource";
                echo json_encode($data);
            }
        } else {
            $data['status'] = "failure";
            $data['description'] = "No resource id set";
            echo json_encode($data);
        }
    }

    public function admin()
    {
        $data = array();
        if (_f_is_admin() == 1) {

            // Admin is logged in now, so proceed further to process the segment

            $seg3 = $this->uri->segment(3, 0);
            if ($seg3) {
                if ($seg3 == "users") {
                    $seg4 = $this->uri->segment(4, 0);
                    if ($seg4) {
                        $seg5 = $this->uri->segment(5, 0);
                        if ($seg5) {
                            $this->load->model('user');
                            $this->user->id = $seg4;
                            $user_info = $this->user->_read();
                            if ($user_info) {
                                if ($seg5 == "json") {
                                    $data['status'] = "success";
                                    $data['data'] = $user_info[0];
                                    echo json_encode($data);
                                } else
                                    if ($seg5 === "pdf") {

                                        $data['title'] = "UID-" . $user_info[0]['id'] . "-" . $user_info[0]['email'];
                                        $data['userinfo'] = $user_info[0];
                                        $html_for_pdf_header = $this->load->view('pdf_header', $data, true);
                                        $html_for_pdf = $this->load->view('pdf_user_info', $data, true);
                                        // echo $html_for_pdf_header;
                                        // echo $html_for_pdf;
                                        $this->load->library('pdf');

                                        $html_data = $html_for_pdf_header . $html_for_pdf;
                                        $this->pdf->loadHtml($html_data);
                                        $this->pdf->setPaper('A4', 'potrait');

                                        $this->pdf->render();

                                        echo $html_data;

                                        $this->pdf->stream($data['title'], array(
                                            "Attachment" => 0
                                        ));
                                    } else {
                                                $data['status'] = "failure";
                                                $data['description'] = "Action $seg5 not found";
                                                echo json_encode($data);
                                            }
                            }
                        } else {
                            $this->load->model('user');
                            $this->user->id = $seg4;
                            $user_info = $this->user->_read();
                            if ($user_info) {
                                $data['status'] = "success";
                                $data['data'] = $user_info[0];
                                echo json_encode($data);
                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "user not found";
                                echo json_encode($data);
                            }
                        }
                    } else {
                        $data['status'] = "failure";
                        $data['description'] = "unknown resource id";
                        echo json_encode($data);
                    }
                }
                else if ($seg3 === "ideas") {
                        $seg4 = $this->uri->segment(4, 0);
                        if ($seg4) {
                            $this->load->model('idea');
                            $this->idea->id = $seg4;
                            $seg5 = $this->uri->segment(5, 0);
                            if ($seg5 === "cover_pic") {
                                $ideaid = segx(4);
                                $query = "select * from uploads where id in (select max(uploadid) from idea_logos where ideaid=$seg4)";
                                $action = $this->db->query($query);
                                if ($action) {
                                    $row = $action->result_array();
                                    if ($row) $remoteImage = $this->config->item('upload_path') . $row[0]['url'];
                                    else {
                                        $remoteImage = "/assets/img/bulb.jpg";
                                    }

                                    if ($imginfo = getimagesize($remoteImage)) {
                                        header("Content-type: " . $imginfo['mime']);
                                        readfile($remoteImage);
                                    } else {
                                        $data['status'] = "failure";
                                        $data['description'] = "Invalid File Type";
                                        echo json_encode($data);
                                    }
                                } else {
                                }
                            } else
                                if ($seg5 === 0) {
                                    $this->load->model('idea');
                                    echo json_encode($this->idea->read(segx(4)));
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Unknown property";
                                    echo json_encode($data);
                                }
                        }
                    } // Add your new api resources here
            } else {
                $data['status'] = "failure";
                $data['description'] = "no resource set";
                echo json_encode($data);
            }
        } else {
            echo "Not admin";
        }
    }

    public function approve_event($id)
    {
        $query = "Update events set admin_approved=1 where id=$id";
        $action = $this->db->query($query);
        if ($action) {
            return 1;
        } else {
            return 0;
        }
    }

    public function reject_event($id)
    {
        $query = "Update events set admin_approved=2 where id=$id";
        $action = $this->db->query($query);
        if ($action) {
            return 1;
        } else {
            return 0;
        }
    }

    public function v1_users()
    {
        
        
            $this->load->model('User3');
            $dataof_user[0] = $this->User3->read($this->session->socialid);
            $seg5 = $this->uri->segment(5, 0);
            if ($seg5 === "profile-picture") {
                $seg6 = $this->uri->segment(6, 0);
                if ($seg6) {
                    if ($seg6 === "upload") {
                        if (isset($_FILES["file"]["type"])) {
                            $validextensions = array(
                                "jpeg",
                                "jpg",
                                "png"
                            );
                            $temporary = explode(".", $_FILES["file"]["name"]);
                            $file_extension = end($temporary);
                            if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < 5242880) //Approx. 5MB files can be uploaded.
                                && in_array($file_extension, $validextensions)
                            ) {
                                if ($_FILES["file"]["error"] > 0) {
                                    $data['status'] = "failure";
                                    $data['description'] = "Invalid File Type";
                                    echo json_encode($data);
                                } else {
                                    $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
                                    $targetfilename = hash("sha256", "ransalt_aashay0101_edd" . $dataof_user[0]['socialid'] . $_FILES['file']['name']) . "." . $file_extension;
                                    $targetPath = $this->config->item('upload_path') . $targetfilename; // Target path where file is to be stored
                                    move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file
                                    $data['status'] = $dataof_user[0]['socialid'];
                                    $esc_socialid = $this->db->escape($dataof_user[0]['socialid']); // Escape socialid
                                    $esc_url = $this->db->escape($targetfilename); // Escape filename;
                                    $query = "Insert into profile_pictures(socialid,url) values($esc_socialid,$esc_url)";
                                    $action = $this->db->query($query);
                                    if ($action) {
                                        $data['status'] = "success";
                                        $data['description'] = "File Uploaded Successfully";
                                        echo json_encode($data);
                                    } else {
                                        $data['status'] .= "failure";
                                        $data['description'] = "Temporary error".$dataof_user[0];
                                        echo json_encode($data);
                                    }
                                }
                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "Invalid File Type";
                                echo json_encode($data);
                            }
                        }
                    } else
                        if ($seg6 === "remove") {
                            $id = $dataof_user[0]['socialid'];
                            $query = "Delete from profile_pictures where socialid=$id";
                            $action = $this->db->query($query);
                            if ($action) {
                                $data['status'] = "success";
                                $data['description'] = "Profile Picture Removed Successfully";
                                echo json_encode($data);
                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "Temporary error try again";
                                echo json_encode($data);
                            }
                        } else {
                            $data['status'] = "failure";
                            $data['description'] = "Invalid action for profile-picture";
                            echo json_encode($data);
                        }
                } else {
                    /*Nothing set after /api/v1/users/<id>/profile-picture, so just return the latest profile picture */
                    $esc_socialid = $this->db->escape($dataof_user[0]['socialid']);
                    $query = "Select url from profile_pictures where socialid=$esc_socialid order by timestamp desc";
                    $action = $this->db->query($query);
                    if ($action) {
                        $row = $action->result_array();
                        if ($row) $remoteImage = $this->config->item('upload_path') . $row[0]['url'];
                        else {
                            $hash = hash("md5", $dataof_user[0]['email']);
                            $remoteImage = "https://www.gravatar.com/avatar/" . $hash . "?s=300&d=identicon";
                        }

                        if ($imginfo = getimagesize($remoteImage)) {
                            header("Content-type: " . $imginfo['mime']);
                            readfile($remoteImage);
                        } else {
                            $data['status'] = "failure";
                            $data['description'] = "Invalid File Type";
                            echo json_encode($data);
                        }
                    } else {
                    }
                }
            } else if ($seg5 === "rate-cards") {
                    $seg6 = $this->uri->segment(6, 0);
                    if ($seg6) {
                        if ($seg6 === "upload") {
                            if (isset($_FILES["rate-card"]["type"])) {
                                $validextensions = array(
                                    "jpeg",
                                    "jpg",
                                    "png",
                                    "pdf"
                                );
                                $temporary = explode(".", $_FILES["rate-card"]["name"]);
                                $file_extension = end($temporary);
                                if ((($_FILES["rate-card"]["type"] == "image/png") || ($_FILES["rate-card"]["type"] == "image/jpg") || ($_FILES["rate-card"]["type"] == "image/jpeg") || ($_FILES["rate-card"]["type"] == "application/pdf")) && ($_FILES["rate-card"]["size"] < 5242880) //Approx. 5MB files can be uploaded.
                                    && in_array($file_extension, $validextensions)
                                ) {
                                    if ($_FILES["rate-card"]["error"] > 0) {
                                        $data['status'] = "failure";
                                        $data['description'] = "Invalid File Type";
                                        echo json_encode($data);
                                    } else {
                                        $sourcePath = $_FILES['rate-card']['tmp_name']; // Storing source path of the file in a variable
                                        $targetfilename = hash("sha256", "ransalt_aashay0101_edd" . $dataof_user[0]['socialid'] . $_FILES['rate-card']['name']) . "." . $file_extension;
                                        $targetPath = $this->config->item('upload_path') . $targetfilename; // Target path where file is to be stored
                                        move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file
                                        $esc_socialid = $this->db->escape($dataof_user[0]['socialid']); // Escape socialid
                                        $esc_url = $this->db->escape($targetfilename); // Escape filename;
                                        $esc_name = $this->db->escape($_POST['ratecard-name']);
                                        $query = "Insert into rate_cards(name,socialid,url) values($esc_name,$esc_socialid,$esc_url)";
                                        $action = $this->db->query($query);
                                        if ($action) {
                                            $data['status'] = "success";
                                            $data['description'] = "File Uploaded Successfully";
                                            echo json_encode($data);
                                        } else {
                                            $data['status'] = "failure";
                                            $data['description'] = "Temporary error";
                                            echo json_encode($data);
                                        }
                                    }
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Invalid File Type";
                                    echo json_encode($data);
                                }
                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "No file";
                                echo json_encode($data);
                            }
                        } else
                            if ($seg6 === "delete") {
                                $id = $dataof_user[0]['socialid'];
                                $query = "Delete from rate_cards where id=$id";
                                $action = $this->db->query($query);
                                if ($action) {
                                    $data['status'] = "success";
                                    $data['description'] = "Rate Card Removed Successfully";
                                    echo json_encode($data);
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Temporary error try again";
                                    echo json_encode($data);
                                }
                            } else {
                                $seg7 = segx(7);
                                if ($seg7 === "delete") {
                                    $socialid = $dataof_user[0]['socialid'];
                                    $ratecardid = segx(6);
                                    $query = "Delete from rate_cards where id=$ratecardid and socialid=$socialid";
                                    $action = $this->db->query($query);
                                    if ($action) {
                                        $data['status'] = "success";
                                        $data['description'] = "Deleted id:$ratecardid from rate cards";
                                        echo json_encode($data);
                                    } else {
                                    }
                                } else {
                                    $esc_socialid = $this->db->escape($dataof_user[0]['socialid']);
                                    $ratecardid = $this->db->escape(segx(6));
                                    $query = "Select id,url from rate_cards where id=$ratecardid and socialid=$esc_socialid";
                                    $action = $this->db->query($query);
                                    if ($action) {
                                        $row = $action->result_array();
                                        echo json_encode($row);
                                    } else {
                                        $data['status'] = "failure";
                                        $data['description'] = "Temporary error try again: 1093";
                                        echo json_encode($data);
                                    }
                                }
                            }
                    } else {
                        /*Nothing set after /api/v1/users/<id>/rate-cards, so just return the latest profile picture */
                        $esc_socialid = $this->db->escape($dataof_user[0]['socialid']);
                        $query = "Select id,url from rate_cards where socialid=$esc_socialid order by timestamp desc";
                        $action = $this->db->query($query);
                        if ($action) {
                            $row = $action->result_array();
                            echo json_encode($row);
                        } else {
                        }
                    }
                } else
                    if ($seg5 === "edit") {
                        $this->user->socialid=$dataof_user[0]['socialid'];
                        $action = $this->user->_update();
                        if ($action == -5) {
                            $data['status'] = "failure";
                            $data['description'] = "Username already taken";
                            echo json_encode($data);
                        } else
                            if ($action) {
                                $data['status'] = "success";
                                $data['description'] = "Profile Updated Successfully";
                                $data['extras'] = $action[0];
                                //_f_set_session_user_profile($this->session->userdata('socialid'));
                                echo json_encode($data);
                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "Temporary error: 10793";
                                echo json_encode($data);
                            }
                    } else
                        if ($seg5 === "messages") {
                            if (segx(6) === "send") {
                                $receivers = explode(',', $_POST['receiversx']);
                                $message = $this->db->escape($_POST['message']);
                                $esc_sender = $dataof_user[0]['socialid'];

                                // var_dump($receivers);

                                //spam just die
                                if(count($receivers)>5) die(0);

                                foreach ($receivers as $receiver) {
                                    $res = $receiver;
                                    $receiver_id = _f_get_socialid_from_username($res);

                                    // var_dump($receiver_id);
                                    $query="";
                                    if(_f_is_admin() || $receiver_id==1)
                                    {
                                        $query = "Insert into messages(sender,receiver,message,admin_approved) values($esc_sender,$receiver_id,$message,1)";
                                    }
                                    else{
                                        $query = "Insert into messages(sender,receiver,message) values($esc_sender,$receiver_id,$message)";
                                    }
                                    if ($receiver_id != 0) $action = $this->db->query($query);
                                    else $action = 0;
                                    if ($action) {
                                        $data['sent'][] = $res;
                                    } else {
                                        $data['notsent'][] = $res;
                                    }
                                }

                                $data['status'] = "success";
                                echo json_encode($data);
                            } else if (segx(6) === "suggestions") {
                                $socialid = $dataof_user[0]['socialid'];
                                $query = "select socialid as id,username as value from users where socialid in (Select distinct receiver from messages where sender=$socialid union select distinct sender from messages where receiver=$socialid)";

                                $action = $this->db->query($query);
                                $result = $action->result_array();
                                echo json_encode($result);

                            } else if (segx(6)) {
                                if (_f_is_message_owned(segx(6), $this->session->socialid)) {
                                    if (segx(7) === "delete") {
                                        $id = segx(6);
                                        $query = "Update messages set sender_trashed=1 where id=$id";
                                        $action = $this->db->query($query);
                                        if ($action) {
                                            $data['status'] = "success";
                                            $data['description'] = "Message successfully deleted";
                                            echo json_encode($data);
                                        } else {
                                            $data['status'] = "failure";
                                            $data['description'] = "Temporary Error: 14110359";
                                            echo json_encode($data);
                                        }


                                    }
                                } else if (_f_is_message_receiver(segx(6), $this->session->socialid)) {
                                    if (segx(7) === "delete") {
                                        $id = segx(6);
                                        $query = "Update messages set trashed=1 where id=$id";
                                        $action = $this->db->query($query);
                                        if ($action) {
                                            $data['status'] = "success";
                                            $data['description'] = "Message successfully deleted";
                                            echo json_encode($data);
                                        } else {
                                            $data['status'] = "failure";
                                            $data['description'] = "Temporary Error: 14110425";
                                            echo json_encode($data);
                                        }
                                    }
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Unauthorized message access";
                                    echo json_encode($data);
                                    echo $this->session->socialid;
                                }
                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "Unknown method $seg6";
                                echo json_encode($data);
                            }
                        } else if ($seg5 === "reviews") {
                            if (segx(6) === "add") {
                                $reviewer = $dataof_user[0]['socialid'];
                                $service_provider_c = is_numeric(segx(7)) ? segx(7) : 0;
                                if ($service_provider_c) {
                                    $pre_query = "Select count(*) from reviews where reviewer=$reviewer and socialid=$service_provider_c";
                                    $predefined_ratings = array(1, 2, 3, 4, 5);

                                    $rating = in_array((int)($_POST['rating']), $predefined_ratings) ? $_POST['rating'] : 0;


                                    $validated_description = isset($_POST['review']) ? $this->db->escape(_f_validate('description', $_POST['review'])) : NULL;

                                    if ($rating != 0) {
                                        $pre_action = $this->db->query($pre_query);

                                        $count = $pre_action->result_array();

                                        if ($count[0]['count(*)'] == 0) {

                                            if (!($reviewer == $service_provider_c)) {
                                                $query = "Insert into reviews(socialid,reviewer,rating,description) values($service_provider_c,$reviewer,$rating,$validated_description)";

                                                $action = $this->db->query($query);

                                                if ($action) {
                                                    $data['status'] = "success";

                                                    echo json_encode($data);
                                                } else {
                                                    $data['status'] = "failure";
                                                    $data['description'] = "Something went wrong: Error 02121657";
                                                    echo json_encode($data);
                                                }
                                                // if ($receiver_id != 0) $action = $this->db->query($query);
                                                // else $action = 0;
                                                // if ($action) {
                                                //  $data['sent'][] = $res;
                                                // }
                                                // else {
                                                //  $data['notsent'][] = $res;
                                                // }

                                            } else {
                                                $data['status'] = "failure";
                                                $data['description'] = "You cannot review yourself";
                                                echo json_encode($data);
                                            }


                                        } else {
                                            $data['status'] = "failure";
                                            $data['description'] = "Already Reviewed";
                                            echo json_encode($data);
                                        }
                                    } else {
                                        $data['status'] = "failure";
                                        $data['description'] = "Invalid Rating";
                                        echo json_encode($data);
                                    }


                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Unknown service provider";
                                    echo json_encode($data);
                                }

                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "Unknown action for reviews";
                                echo json_encode($data);
                            }

                        } else {
                            $data['status'] = "failure";
                            $data['description'] = "Unknown method $seg5";
                            echo json_encode($data);
                        }
    }
    public function v3_ideas()
    {

        // The next parameter in the segment is going to be a unique id of an idea so perform an check to see if the idea is public visible

        $seg4 = $this->uri->segment(4, 0);
        if ($seg4) {
            $this->load->model('idea');
            $this->idea->id = $seg4;
            $fetched_idea=$this->idea->read($seg4);
            if($fetched_idea)
            {
                $visible=$fetched_idea['visibility'];
                if ($visible || _f_is_admin() || $fetched_idea['ownerid']==$this->session->socialid) {
                    $seg5 = $this->uri->segment(5, 0);
                    if ($seg5 === "cover_pic") {
                        $ideaid = segx(4);
                        $query = "select * from uploads where id in (select max(uploadid) from idea_cover_pics where ideaid=$ideaid)";
                        $action = $this->db->query($query);
                        if ($action) {
                            $row = $action->result_array();
                            if ($row) $remoteImage = $this->config->item('upload_path') . $row[0]['url'];
                            else {
                                $remoteImage = base_url() . "/assets/img/bulb.jpg";
                            }

                            if ($imginfo = getimagesize($remoteImage)) {
                                header("Content-type: " . $imginfo['mime']);
                                readfile($remoteImage);
                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "Invalid File Type";
                                echo json_encode($data);
                            }
                        } else {
                        }
                    }
                    elseif ($seg5 === "logo") {
                        $ideaid = segx(4);
                        $query = "select * from uploads where id in (select max(uploadid) from idea_logos where ideaid=$seg4)";
                        $action = $this->db->query($query);
                        if ($action) {
                            $row = $action->result_array();
                            if ($row) $remoteImage = $this->config->item('upload_path') . $row[0]['url'];
                            else {
                                $remoteImage = base_url() . "assets/light-idea.png";
                            }

                            if ($imginfo = getimagesize($remoteImage)) {
                                header("Content-type: " . $imginfo['mime']);
                                readfile($remoteImage);
                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "Invalid File Type";
                                echo json_encode($data);
                            }
                        } else {
                        }
                    }

                    if ($seg5 === "follow") {
                        if (_f_is_loggedin()) {
                            //$action = $this->follow_idea($seg4);
                            if ($action) {
                                if ($action['status'] == "success") {
                                    $data['status'] = "success";
                                    $data['description'] = "Successfully followed idea";
                                    $data['followers'] = $action['followers'];
                                    echo json_encode($data);
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = $action['description'];
                                    echo json_encode($data);
                                }
                            } else {
                                $data['status'] = "failure";
                                $data['description'] = "Temporary error, try again";
                                echo json_encode($data);
                            }
                        } else {
                            $data['status'] = "failure";
                            $data['description'] = "You must me logged in to follow idea!";
                            echo json_encode($data);
                        }
                    } else
                        if ($seg5 === "unfollow") {
                            if (_f_is_loggedin()) {
                                //$action = $this->unfollow_idea($seg4);
                                if ($action) {
                                    if ($action['status'] == "success") {
                                        $data['status'] = "success";
                                        $data['description'] = "Successfully unfollowed idea";
                                        $data['followers'] = $action['followers'];
                                        echo json_encode($data);
                                    } else {
                                        $data['status'] = "failure";
                                        $data['description'] = $action['description'];
                                        echo json_encode($data);
                                    }
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "Temporary error, try again";
                                    echo json_encode($data);
                                }
                            } else {
                                $data['status'] == "failure";
                                $data['description'] = "You must me logged in to unfollow idea!";
                                echo json_encode($data);
                            }
                        } else
                            if ($seg5 === "shortlist") {
                                if (_f_is_loggedin()) {
                                    $this->load->model('user');
                                    $action = $this->user->shortlist_idea($seg4, $this->session->socialid);
                                    if ($action) {
                                        if ($action['status'] == "success") {
                                            $data['status'] = "success";
                                            $data['description'] = "Successfully Shortlisted idea";

                                            echo json_encode($data);
                                        } else {
                                            $data['status'] = "failure";
                                            $data['description'] = $action['description'];
                                            echo json_encode($data);
                                        }
                                    } else {
                                        $data['status'] = "failure";
                                        $data['description'] = "Temporary error, try again";
                                        echo json_encode($data);
                                    }
                                } else {
                                    $data['status'] = "failure";
                                    $data['description'] = "You must me logged in to Shortlist idea!";
                                    echo json_encode($data);
                                }
                            } else
                                if ($seg5 === "delist") {
                                    if (_f_is_loggedin()) {
                                        $this->load->model('user');
                                        $action = $this->user->delist_idea($seg4, $this->session->socialid);

                                        if ($action) {
                                            if ($action['status'] == "success") {
                                                $data['status'] = "success";
                                                $data['description'] = "Successfully Delisted idea";

                                                echo json_encode($data);
                                            } else {
                                                $data['status'] = "failure";
                                                $data['description'] = $action['description'];
                                                echo json_encode($data);
                                            }
                                        } else {
                                            $data['status'] = "failure";
                                            $data['description'] = "Temporary error, try again";
                                            echo json_encode($data);
                                        }
                                    } else {
                                        $data['status'] == "failure";
                                        $data['description'] = "You must me logged in to Shortlist idea!";
                                        echo json_encode($data);
                                    }
                                } else
                                    if ($seg5 === 0) {
                                    } else {
                                        $data['status'] = "failure";
                                        $data['description'] = "Unknown property";
                                        echo json_encode($data);
                                    }
                } else {
                    $data['status'] = "failure";
                    $data['description'] = "You are not authorized to access this resource";
                    echo json_encode($data);
                }
            }
            else
            {
                $data['status'] = "failure";
                $data['description'] = "You are not authorized to access this resource";
                echo json_encode($data);
            }

        } else {
            $data['status'] = "failure";
            $data['description'] = "No resource id set";
            echo json_encode($data);
        }
    }

    public function get_sectors_json()
    {
        echo json_encode(get_sectors());
    }
}

?>
