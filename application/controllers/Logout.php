<?php
class Logout extends CI_Controller
{
	public function index()
	{
			// Perform session destroy
        if(isset($this->session->socialid))
        {
            $analytics['socialid']=$this->session->socialid;
            $analytics['event_category']="user";
            $analytics['event_action']="logout";
            $analytics['event_value']="logout";
            $analytics['event_label']="logout";
            $analytics['event_description']="User logged out";
            a_send($analytics);
        }

			$data['title']="Logout";
			$array_items = array('access_token','access_type','f_userid','l_userid','socialid','name','lastname','email','g_userid','company');
			$this->session->unset_userdata($array_items);
			session_destroy();

			redirect('/login');

	}
}
?>