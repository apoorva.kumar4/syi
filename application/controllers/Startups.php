<?php
class Startups extends CI_Controller{
	public function index()
	{
        $this->load->model('Sector');
        
		$data = array();
        $sectors=$this->Sector->get();
        $data['sectors']=$sectors;
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
        if(_f_is_loggedin()){
            $analytics['socialid']=$this->session->socialid;
            $analytics['event_category']="page_visit";
            $analytics['event_action']="visit";
            $analytics['event_label']="startups";
            $analytics['event_value']='startups';
            $analytics['event_description']="Visited Startups page";
            a_send($analytics);
        }
		$data['title']="SaYourIdeas.com - Startups";
		
		$data['ideas']=_f_get_all_public_ideas();
		$data['followed_ideas']=array();
		if(_f_is_loggedin())
        {
            $f_i=_f_get_all_followed_ideas($this->session->socialid);
            foreach($f_i as $key=>$item)
            {
                $data['followed_ideas'][$key]=$f_i[$key]['ideaid'];
            }
        }
        else{
		    $data['followed_ideas']=array();
        }

        $this->parser->parse('templates/header',$data);
        $this->parser->parse('startups',$data);
		$this->load->view('copyfooter');
		
	}
	public function filter()
	{
        $this->load->model('Sector');
        
        $data = array();
        $sectors=$this->Sector->get();
        $data['sectors']=$sectors;
		
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
        
		$data['title']="SaYourIdeas.com - Startups";
		$this->parser->parse('templates/header',$data);
        if(array_key_exists('idea-stage', $_GET)) {
            $requested_phases=$_GET['idea-stage'];
            $data['requested_phases']=$requested_phases;
                
        } else $requested_phases= null;
		
        if(array_key_exists('sector', $_GET)) {
            $requested_sectors=$_GET['sector'];
            $data['requested_sectors']=array();
            
            foreach($_GET['sector'] as $key=>$item) {
                //$data['requested_sectors'][]+="$item";
                array_push($data['requested_sectors'], $item);
            }

            //var_dump($data['requested_sectors']);die(0);
                
        } else $requested_sectors= null;

        //var_dump($requested_phases);die(0);
        $data['ideas']=_f_get_all_public_ideas_by_phases($requested_phases,$requested_sectors);

        //var_dump($data['ideas']);die(0);
        $data['followed_ideas']=array();
        if(_f_is_loggedin())
        {
            $f_i=_f_get_all_followed_ideas($this->session->socialid);
            foreach($f_i as $key=>$item)
            {
                $data['followed_ideas'][$key]=$f_i[$key]['ideaid'];
            }
        }
        else{
            $data['followed_ideas']=array();
        }
        $this->parser->parse('startups',$data);
		$this->load->view('copyfooter');
	}
}
?>