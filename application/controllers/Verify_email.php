<?php
class Verify_email extends CI_Controller
{
	public function index()
	{
		$this->load->helper('verify_email');

		if(isset($this->session->socialid))
		{
			// fetch user
			$this->load->model('User3');
			
			$fetched_user=$this->User3->read($this->session->socialid);
			
			if(!$fetched_user) {
				echo "Fatal error 9845";
				exit(0);
			} 

			if(_f_is_email_verified())
			{
                $uuu=base_url().'dashboard';
				header("Location: $uuu");
			}
			else
			{
				if(isset($_POST['code']))
				{
					$where_arr=array(
        				'emailid' => $fetched_user['email'],
        				'code' => $code
    				);
					$this>db->where($where_arr);
					$this->db->select('count(*)',FALSE);
					$check_code=$CI->db->get('email_verification_codes');
					$result=$check_code->row_array();

				    if($result['count(*)']==1)
				    {
				    	$properties = array (
				    		'socialid'=>$fetched_user['socialid'],
				    		'emailverified'=>1 
				    	);
				    	$action=$this->User3->update($properties);
				        $uuu=base_url().'dashboard';
						header("Location: $uuu");
				    }
					else
					{
                        $data['title'] = "Verify Email";
                        $this->parser->parse('templates/header', $data);
                        $this->load->view('verify_email_failed');
                        $this->load->view('templates/footer');
					}

					
				}
				else
				{
                    if(isset($this->session->email_code) && $this->session->email_code!="")
                    {
                        $data['title'] = "Verify Email";
                        $this->parser->parse('templates/header', $data);
                        $this->load->view('verify_email');
                        $this->load->view('templates/footer');
                    }
                    else {
                        $digits = 6;
                        $code_hash = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
                        $verification_code = $code_hash;
                        $this->db->replace('email_verification_codes', array('emailid' => $fetched_user['email'], 'code' => $code_hash));
                        $this->session->set_userdata('email_code',$verification_code);
                        $message = "Your verification code for SaYourIdeas is $verification_code.";
                        $this->load->library('email');
                        $frrr = "sayourideas.com";
                        $subject = "Verification Code from SaYourIdeas.com";
                        $this->email->from("admin@$frrr", 'Sayouideas.com');
                        $this->email->to($fetched_user['email']);
                        $this->email->subject($subject);
                        $this->email->message($message);

                        if ($this->email->send()) {
                            $data['title'] = "Verify Email";
                            $this->parser->parse('templates/header', $data);
                            $this->load->view('verify_email');
                            $this->load->view('templates/footer');
                        } else {
                            $data['title'] = "Verify Email";
                            $data['description'] = "Email could not be sent, please try again.";
                            $data['extras'] = "";
                            $this->parser->parse('templates/header', $data);
                            $this->load->view('error_request', $data);
                            $this->load->view('templates/footer');
                        }
                    }
				}
			}
		}
		else
		{
			echo "Session parameters socialid and email not set";
			//echo $this->session->socialid;
			//echo $this->session->email;

		}
	}
	public function failed()
	{
		$data['title']="Verify Email";	
				$this->parser->parse('templates/header',$data);
				$this->load->view('verify_email_failed');
				$this->load->view('templates/footer');

	}


}
?>