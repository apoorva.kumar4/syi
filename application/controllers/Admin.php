<?php
class Admin extends CI_Controller{

	protected function get_new_message_count(){
		$query="select count(*) from messages where admin_viewed=0";
		$action=$this->db->query($query);
		if($action){
			$row=$action=$action->result_array();
			return $row[0]['count(*)'];
		}
		else
		{
			return false;
		}
	}
	
	protected function get_recent_unapproved_events(){
		$this->load->model('Event');
		$params = array(
			'admin_approved'=>0
		);
		$get_events=$this->Event->scan($params);

		if($get_events['status']!="success") {
			return false;
		}

		return $get_events['data'];
	}

	protected function get_registrations($tag){
		$this->load->model('User3');
		$action=$this->User3->scan(array('user_type'=>$tag,'order_by_timestamp'=>'desc'));
		return $action;
	}

	protected function get_registrations_between($tag,$date_start,$date_end) {
		$query="SELECT id,socialid,first_name,last_name,email,user_type,timestamp,location from users where registration_complete='Y' and user_type='$tag' and timestamp between '$date_start' and '$date_end'order by id desc";
		$action=$this->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			return $row;
		}
		else
		{
			return false;
		}
	}
	
	protected function get_messages(){

		$query="SELECT * from messages order by id desc";
		$action=$this->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			return $row;
		}
		else
		{
			return false;
		}
	}

	protected function get_unapproved_messages() {
		$query="SELECT * from messages where admin_approved=0 order by id desc";
		$action=$this->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			return $row;
		}
		else
		{
			return false;
		}
	}
	
	protected function get_recent_ideas() {

		$query="SELECT ideas.id,ideas.name,ideas.tagline,ideas.description,ideas.stage,ideas.created_on, (select followers from view_ideas c where c.id=ideas.id) as followers, unique_url from ideas order by ideas.id desc limit 5";
		$action=$this->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			return $row;
		}
		else
		{
			return false;
		}
	}
	
	protected function get_ideas() {

		$query="SELECT ideas.id,ideas.name,ideas.tagline,ideas.description,ideas.stage,ideas.created_on, (select followers from view_ideas c where c.id=ideas.id) as followers, unique_url from ideas order by ideas.created_on desc";
		$action=$this->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			return $row;
		}
		else
		{
			return false;
		}
	}
	
	protected function get_events() {

		$query="SELECT * from events order by id desc";
		$action=$this->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			return $row;
		}
		else
		{
			return false;
		}
	}
	
	protected function get_unapproved_events() {

		$query="SELECT * from events where admin_approved=0 order by id desc";
		$action=$this->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			return $row;
		}
		else
		{
			return false;
		}
	}
	
	protected function get_count_ideators() {
		$query="SELECT count(*) from users where user_type='ideator'";
		$action=$this->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			return $row[0]['count(*)'];
		}
		else
		{
			return false;
		}
	}
	
	protected function mark_messages_as_viewed() {
		$query="update messages set admin_viewed = 1";
		$action=$this->db->query($query);
	}
	
	protected function santext($parameter,$field) {
		$this->form_validation->set_message('santext', "No special characters allowed in $field");
		$result=_f_validate('santext',$parameter);


		if($result==NULL)
		{

			return FALSE;


		}
		else if($result==$parameter)
		{
			return TRUE;
		}
	}

	protected function get_email_suggestions() {
		$this->db->select('email');
		$action=$this->db->get('users');
		$result=$action->result_array();
		$array = array();
		foreach($result as $term) {
			$array[]=$term['email'];
		}
		return $array;
	}
	
	protected function get_activity_categories(){
		$query="select distinct event_category from activity order by event_category asc";
		$action=$this->db->query($query);
		if($action)
		{
			$row=$action->result_array();

			foreach($row as $index=>$item)
			{
				$row[$index]['event_category']=str_replace("_"," ",ucfirst($row[$index]['event_category']));
			}
			return $row;
		}
		else
		{
			return false;
		}
	}

	protected function get_count_ideas() {
		$query="SELECT count(*) from ideas";
		$action=$this->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			return $row[0]['count(*)'];
		}
		else
		{
			return false;
		}
	}

	protected function get_count($var) {
		$query="SELECT count(*) from users where user_type='$var'";
		$action=$this->db->query($query);
		if($action)
		{
			$row=$action->result_array();
			return $row[0]['count(*)'];
		}
		else
		{
			return false;
		}
	}

	public function registrations() {
		if(!_f_is_admin()) {
			echo "Not admin";
			die(0);
		}
		
		if(segx(3)==="ideators")
		{
			if(segx(4)==="add"){

				$data['title']="Add New Ideator";
				$this->load->view('registration_ideator_admin');
			}
			else {
				
				$data['recent_registrations']=$this->get_registrations('ideator');
				$this->load->view('admin_all_registrations',$data);		
			}
		}
		else if(segx(3)==="service_providers")
		{

			if(segx(4)==="add"){
				
				$this->load->view('registration_service_provider_admin');

			}
			else {
			
			$data['recent_registrations']=$this->get_registrations('ideator');
			$this->load->view('admin_all_registrations',$data);		
			}
		}
		else if(segx(3)==="investors")
		{
			if(segx(4)==="add"){

				
				$this->load->view('registration_investor_admin');
	
			}
			else {
			
			$data['recent_registrations']=$this->get_registrations('ideator');
			$this->load->view('admin_all_registrations',$data);		
			}
		}
		else {
			
			$data['recent_registrations']=$this->get_registrations('ideator');
			$this->load->view('admin_all_registrations',$data);		
		}
	}

	public function messages() {
		$this->mark_messages_as_viewed();
		if(segx(3)==="old")
		{
			if(_f_is_admin()) {
				$data['title']="All Messages";
				$data['all_messages']=$this->get_messages();
				$this->load->view('admin_all_messages_old',$data);
			}	
		}

		else if(segx(3)==="download") {
			$this->load->dbutil();
			$this->load->helper('file');
			$this->load->helper('download');
			$delimiter = ",";
			$newline = "\r\n";
			$filename = "messages.csv";
			$query = "SELECT * FROM messages WHERE id=0";
			$message_ids = $_POST['download-message'];
			foreach ($message_ids as $messageid) {
				$query .= " or id=$messageid";
			}
			//echo $query;
			$result = $this->db->query($query);
			$data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
			force_download($filename, $data);
		}
		else if(segx(3)==="add")
		{
			if(isset($_POST['message']))
			{
				//var_dump($_POST);die(0);
				if(!_f_is_admin()) die(0);
				
				$this->load->model('message');

				if(isset($_POST['message']))
				{
					$action=$this->message->admin_message($_POST);
					if($action['status']=="success"){
						$this->load->view('admin_message_success');
					}
					else{
						$this->load->view('admin_message_failed');
					}
				}
				else
				{
					echo "Message not set";
				}
				
			}
			else{
				$this->load->view('admin_new_msg');
			}

		}
		else
		{
			if(_f_is_admin()) {
				$data['title']="All Messages";
				$this->mark_messages_as_viewed();
				$data['all_messages']=$this->get_unapproved_messages();
				$this->load->view('admin_all_messages',$data);
			}		
		}
	}
	
	public function ideas() {
		if(!_f_is_admin()) die(0);

		if(segx(3)==="add"){
			redirect('/ideas/add');
		}
		else {	
			$this->load->view('admin_all_ideas');		
		}
	}

	public function events() {
		if (!_f_is_admin()) die(0);

		if (segx(3) === "add") {
			$data['title'] = "Create Event";
			$data['page_title'] = "Create Event";
			$data['new_message_count']=$this->get_new_message_count();
			$this->load->view('admin_header',$data);
			$this->load->view('event_create_admin', $data);
			$this->load->view('admin_footer');
		}
		else if(segx(3)==="download")
		{
			$this->load->dbutil();
			$this->load->helper('file');
			$this->load->helper('download');
			$delimiter = ",";
			$newline = "\r\n";
			$filename = "event.csv";
			$query = "SELECT * FROM events WHERE id=0";
			$event_ids=$_POST['download-event'];
			foreach($event_ids as $eventid)
			{
				$query .= " or id=$eventid";
			}
			//echo $query;
			$result = $this->db->query($query);
			$data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
			force_download($filename, $data);

		}
		else {

			$all_users_action=$this->User3->get2();
			$all_events_action=$this->Event->get2(array('order_by_id'=>'desc'));
			$all_uploads_action=$this->Supload->get2();

			$data['asset_path']=$this->config->item('upload_path');
			$data['users']= $all_users_action['data'];
			$data['uploads']= $all_uploads_action['data'];
			$data['recent_events'] = $all_events_action['data'];

			$data['title'] = "All Events";
			$data['page_title'] = "All Events";
			$this->load->view('admin_all_events', $data);
		}
	}

	public function inactive_users() {
		if(!_f_is_admin()) die(0);
		
		$this->load->model('activity');
		$data['title']="Inactive Users";
		$action=$this->activity->get_inactive_users();
		$data['inactive_users']=$action['inactive_users'];
		//var_dump($data['inactive_users']);die(0);
		$this->load->view('admin_inactive_users',$data);
	}

	public function recent_activity() {
		if(!_f_is_admin()) die(0);
		$this->load->view('admin_recent_activity');
		
	}

	public function email() {
		if(!_f_is_admin()) die(0);
		
		$data['title']="Admin Email";
		
		$this->load->view('admin_email');
		
	}

	public function bulk_email() {
		if(!_f_is_admin()) die(0);
		
		$data['title']="Bulk Email";
		
		$this->load->view('admin_bulk_email');
	}

	public function admin_profile() {
		if(!_f_is_admin()) die(0);
		$this->load->view('admin_profile');
	}

	public function about_us() {
		if(!_f_is_admin()) {
			die(0);
		}
		
		
		$this->load->model('about_us');
		$data['how_we_do_it']=$this->about_us->read('how_we_do_it');
		$data['what_do_we_do']=$this->about_us->read('what_do_we_do');
		$data['youtube_link']=$this->about_us->read('youtube_link');
		
		$this->load->view('admin_about_us',$data);
	}
	public function top_ideas() {
		$this->load->model('Idea4');
		if(!_f_is_admin()) die(0);


		$data['top_ideas']=$this->Idea4->get_top_ideas(100);
		//var_dump($data['top_ideas']);die(0)
		
		$this->load->view('admin_top_ideas',$data);
		
	}

	public function sectors() {
		if(!_f_is_admin()) die(0);

		if(segx(3)==="add") {
			$this->load->model('sector');
			$ideas_others_sectors=$this->sector->get_ideas_other_sectors();
			$user_other_sectors=$this->sector->get_users_other_sectors();
			$data['title']="Add New Sector";
			$data['ideas_other_sectors']=$ideas_others_sectors['sectors'];
			$data['user_other_sectors']=$user_other_sectors['sectors'];
			$config = array (
				array(
					'field'=>'sector-name',
					'label'=>'Sector Name',
					'rules'=>'required'
				)
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('new_sector_admin',$data);	
			}
			else
			{

				$details['sector_name']=$_POST['sector-name'];
				$this->load->helper('sector');

				$action=create_sector($details);

				if(isset($action['status'])?$action['status']=='success':0)
				{
					$uuu='/admin/sectors';
					header("Location: $uuu");

				}
				elseif(isset($action['status'])?$action['status']=='failure':1)
				{
					//_f_message("Error Occured",$action['description']);

					//$uuu=base_url().'message';
					//header("Location: $uuu");
					echo "Error occured: 17111605";
					echo $action['description'];
				}
				else
				{
					_f_message("Error Occured","Something went wrong when adding Sector, please try again.");
					$uuu=base_url().'message';
					header("Location: $uuu");
				}

			}

		}
		else if(segx(3)==="edit") {
			$data['title']="Add New Sector";

			$config = array (
				array(
					'field'=>'sector-name',
					'label'=>'Sector Name',
					'rules'=>'required'
				)
			);

			$this->form_validation->set_rules($config);



			if ($this->form_validation->run() == FALSE)
			{
				$data['sector-id']=segx(4);
				$this->load->helper('sector');
				$ret_sector=get_sector($data['sector-id']);
				$data['sector']=$ret_sector;
				
				$this->load->view('edit_sector_admin',$data);
				

			}
			else
			{

				$details['sector_name']=$_POST['sector-name'];
				$details['id']=segx(4);
				$this->load->helper('sector');

				$action=create_sector($details);

				if(isset($action['status'])?$action['status']=='success':0)
				{
					$uuu='/admin/sectors';
					header("Location: $uuu");

				}
				elseif(isset($action['status'])?$action['status']=='failure':1)
				{
					//_f_message("Error Occured",$action['description']);

					//$uuu=base_url().'message';
					//header("Location: $uuu");
					echo "Error occured: 17111605";
					echo $action['description'];
				}
				else
				{
					_f_message("Error Occured","Something went wrong when adding Idea, please try again.");
					$uuu=base_url().'message';
					header("Location: $uuu");
				}

			}
		}
		else {
			$data['title']="Add Sectors";
			
			$query="Select * from sectors";
			$action=$this->db->query($query);
			$row=$action->result_array();

			$data['sectors']=$row;
			$this->load->view('admin_all_sectors',$data);
			
		}
	}

	public function faq() {
		if(!_f_is_admin()) die(0);
		
		if(segx(3)==="add") {

			$data['title']="Add New Faq";


			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('admin_add_faq');
			}
		}
		else if(segx(3)==="edit") {
			$data['title']="Edit Faq";

			$data['faq-id']=segx(4);
			$this->load->helper('faq');
			$ret_faq=get_faq($data['faq-id']);			 
			$data['faq']=$ret_faq[0];
			
			
			$this->load->view('edit_faq_admin',$data);
			



		}
		else {
			$data['title']="Add Testimonials";
			
			
			$this->load->helper('faq');
			$data['ideator_faqs']=get_ideator_faqs();
			$data['service_provider_faqs']=get_service_provider_faqs();
			$data['investor_faqs']=get_investor_faqs();
			$this->load->view('admin_faq',$data);
			
		}
	}
	
	public function index() {


		if(!_f_is_admin())
		{
			redirect('/login');
			die(0);
		}


		$this->load->view('admin_dashboard');
	}
	public function manage_admins() {
		$this->load->model('User3');

		if(!_f_is_admin_all_privileges()) {
			die(0);
		}
		if(segx(3)==="add") {
			$this->load->view('add_admin');	
		}
		else {
			$this->load->view('manage_admins');	
			
		}
	}
	
	public function testimonials() {
		if(!_f_is_admin()) die(0);
		
		if(segx(3)==="add") {

			$data['title']="Add New Testimonial";
			$this->load->view('new_testimonial_admin');
		}
		else {
			$data['title']="Add Testimonials";
			$query="Select * from testimonials";
			$action=$this->db->query($query);
			$row=$action->result_array();
			$data['testimonials']=$row;
			
			$this->load->view('admin_all_testimonials',$data);
			
		}
	}

	public function dashboard() {
		redirect('/dashboard');
	}
	
	public function analytics() {
		if(!_f_is_admin()) die(0);
		$this->load->view('admin_analytics4');
	}
	public function followed_ideas() {
		if(!_f_is_admin()) die(0);
		$this->load->view('admin_followed_ideas');

	}

	public function partners() {
		if(!_f_is_admin()) {
			die(0);
		}
		if(segx(3)==="add") {
			$this->load->view('admin_partners_add');	
		} else {
			$this->load->view('admin_partners');
		}
	}
	
}
?>
