<?php
class Api3 extends CI_Controller {

	// Common utilities---------------------------------------------------------

	protected function send_response_json($http_code,$data) {
		http_response_code($http_code);
		header('Content-Type: application/json');
		echo json_encode($data);
		die(0);
	}
	protected function not_authorized($output_data=null) {
		if($output_data==null) {
			$output_data = array(
				'status'=>'failure',
				'description'=>'Not Authorized'
			);
		}
		$this->send_response_json(401,$output_data);
	}
	protected function bad_request($output_data=null) {
		if($output_data==null) {
			$output_data = array(
				'status'=>'failure',
				'description'=>'bad request'
			);
		}
		
		$this->send_response_json(400,$output_data);
	}
	protected function success($output_data=null) {
		
		if($output_data==null) {
			$output_data = array(
				'status'=>'success',
				'description'=>'request completed successfully'
			);
		}
		$this->send_response_json(200,$output_data);
	}
	protected function internal_server_error($output_data=null) {
		if($output_data==null) {
			$output_data = array(
				'status'=>'failure',
				'description'=>'an unknown error occrred'
			);
		}
		$this->send_response_json(501,$output_data);
	}

	protected function filter_and_sanitize_data($reference,$array){
		$filter_array = array();

		foreach($reference as $key) {

			if(array_key_exists($key, $array)){
				$filter_array[$key]=is_string($array[$key])?strip_tags($array[$key]):$array[$key];
			}
		}

		return $filter_array;
	}

	// User functions----------------------------------------------------------
	protected function user_block($params) {
		$socialid=$params['socialid'];
		$this->load->model('user');
		$action=$this->user->block($socialid);
		if($action) {
			return array('status'=>'success');
		}
		else return array('status'=>'failure','description'=>'internel error');
	}

	protected function user_unblock($params) {
		$socialid=$params['socialid'];
		$this->load->model('user');
		$action=$this->user->unblock($socialid);
		if($action) {
			return array('status'=>'success');
		}
		else return array('status'=>'failure','description'=>'internel error');
	}

	protected function user_verify_badge($params) {
		$socialid=$params['socialid'];
		$this->load->model('user');
		$action=$this->user->verify($socialid);
		if($action) {
			return array('status'=>'success');
		}
		else return array('status'=>'failure','description'=>'internel error');
	}

	protected function user_approve($params) {
		$socialid=$params['socialid'];
		$this->load->model('user');
		$action=$this->user->approve($socialid);
		if($action) {
			return array('status'=>'success');
		}
		else return array('status'=>'failure','description'=>'internel error');
	}

	protected function user_delete($params) {
		$socialid=$params['socialid'];
		$this->load->model('user');
		$action=$this->user->delete($socialid);
		if($action) {
			return array('status'=>'success');
		}
		else return array('status'=>'failure','description'=>'internel error');
	}

	protected function user_change_password($data) {
		$this->load->model('User3');
		$user=$this->User3->read($this->session->socialid);
		if(!$user) {
			$this->bad_request();
		}	
		
		//verify current password
		$password_valid=password_verify($data['current_password'],$user["password_hash"]);
		if(!$password_valid) {
			$this->not_authorized();
		}

		$new_password_hash=password_hash($data['new_password'],PASSWORD_DEFAULT);
		$action=$this->User3->update(array('password_hash'=>$new_password_hash,'socialid'=>$this->session->socialid));
		if($action){
			$this->success();
		} else{
			$this->bad_request();
		}


	}

	protected function user_update($data) {
		$this->load->model('User3');
		$user=$this->User3->read($this->session->socialid);
		if(!$user) {
			$this->bad_request();
		}

		if(array_key_exists('username', $data)) {
			if($user['username']==$data['username']) {
				unset($data['username']);
			}
		}

		$data['socialid']=$this->session->socialid;
		$action=$this->User3->update($data);
		if($action) {
			$this->success();
		} else {
			$this->internal_server_error();
		}
	}

	protected function user_username_availability($data) {
		$this->load->model('User3');
		$user=$this->User3->read($this->session->socialid);
		if($user['username']==$data['username']) {
			$this->success();
		}
		$available=$this->User3->is_username_available($data['username']);
		if(!$available) {
			$this->bad_request();
		}

		$this->success();

	}
	
	public function add_rate_card() {
		if(!isset($this->session->socialid)) die(0);
		$socialid=$this->session->socialid;

		$this->load->model('User_wrapper');
		$user=$this->User_wrapper->read($socialid);
		if(!$user) die(0);

		if($user['user_type']!="service_provider") die(0);

		$this->load->model('Supload');

		$upload_params = array(
			'socialid'=>$socialid,
			'file'=>$_FILES['rate-card'],
			'permissions'=>3,
			'accepted_types'=>'image_document'
		);

		$action=$this->Supload->create($upload_params);

		//add to user rate cards

		$this->load->model('User_rate_card');

		if(!$action) {
			$data['status'] = "failure";
			$data['description'] = "There was an unknown error, please try again!";
			http_response_code(501);
			header('Content-Type: application/json');
			echo json_encode($data);
			die(0);

		}

		$new_rate_card = array(
			'uploadid'=>$action['id'],
			'socialid'=>$socialid,
			'name'=>$_POST['ratecard-name']
		);	
		$action=$this->User_rate_card->create($new_rate_card);

		if($action['status']=='success') {
			$data['status'] = "success";
			$data['description'] = "File Uploaded Successfully";
			http_response_code(200);
			header('Content-Type: application/json');
			echo json_encode($data);
		} else {
			$data['status'] = "failure";
			$data['description'] = "There was an error, please try again.";
			http_response_code(401);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	public function delete_rate_card(){

		if(!isset($this->session->socialid)) die(0);
		$socialid=$this->session->socialid;

		$this->load->model('Supload');
		$this->load->model('User_rate_card');

		$fetch_rate_card = $this->User_rate_card->get_id($_POST['id']);
		if(!$fetch_rate_card) die(0); //why to give any hacker any hints? So just die.

		if($fetch_rate_card[0]['socialid']!=$this->session->socialid) die(0); //unauthorised

		$uploadid=$fetch_rate_card[0]['uploadid'];

		$action=$this->User_rate_card->delete($_POST['id']);

		//delete the upload

		$this->Supload->delete($uploadid);

		if(!$action) {
			$data['status'] = "failure";
			$data['description'] = "There was an unknown error, please try again!";
			http_response_code(501);
			header('Content-Type: application/json');
			echo json_encode($data);
			die(0);

		}
		else {
			$data['status'] = "success";
			$data['description'] = "Rate card deleted successfully";
			http_response_code(200);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	// User handler-------------------------------------------------------------
	public function user() {
		
		$request_body= stripslashes(file_get_contents("php://input"));
		$request= json_decode($request_body,true);

		if($request==NULL) {
			$this->bad_request();
		}
		else if($request['action']=="change_password") {
			$this->user_change_password($request['data']);
		}
		else if($request['action']=="update") {
			$this->user_update($request['data']);
		}
		else if($request['action']=="check_username_availability") {
			$this->user_username_availability($request['data']);
		}
		else if($request['action']=="block") {
			if(!_f_is_admin()) {
				$this->not_authorized();
			}
			$action=$this->user_block($request);
			if($action) $this->success();
			else $this->internal_server_error();
		}
		else if($request['action']=="delete") {
			if(!_f_is_admin()) {
				$this->not_authorized();
			}
			$action=$this->user_delete($request);
			if($action) $this->success();
			else $this->internal_server_error();
		}
		else if($request['action']=="unblock") {
			if(!_f_is_admin()) {
				$this->not_authorized();
			}
			$action=$this->user_unblock($request);
			if($action) $this->success();
			else $this->internal_server_error();
		}
		else if($request['action']=="approve") {
			if(!_f_is_admin()) {
				$this->not_authorized();
			}
			$action=$this->user_approve($request);
			if($action) $this->success();
			else $this->internal_server_error();
		}
		else if($request['action']=="verify_badge") {
			if(!_f_is_admin()) {
				$this->not_authorized();
			}
			$action=$this->user_verify_badge($request);
			if($action) $this->success();
			else $this->internal_server_error();
		}
		else if($request['action']=="get") {
			
			if(array_key_exists('socialid', $request))
			$data=$this->User3->scan(array('socialid'=>$request['socialid']));
			else 
				$data=$this->User3->scan(array());

			if(_f_is_admin()) {
				$output_data=array(
					'status'=>'success',
					'data'=>$data
				);

				$this->success($output_data);
			}
			else {
				$filter_data_properties=array(
					'socialid',
					'first_name',
					'last_name',
					'user_type',
					'timestamp',
					'username',
					'role',
					'location',
					'sector_expertise',
					'sector_other',
					'verified_profile',
					'facebook_username',
					'twitter_username',
					'linkedin_username',
					'google_username',
					'profile_picture_url',
					'institute',
					'year_of_graduation',
					'student_alumni'
				);

				$filtered_data=$this->filter_and_sanitize_data($filter_data_properties,$data[0]);
				$output_data=array(
					'status'=>'success',
					'data'=>array($filtered_data)
				);
				$this->success($output_data);	


			}
		} else if($request['action']=="get2") {
			
			if(!_f_is_admin()) {
				$this->not_authorized();
			}

			$all_users=$this->User3->get2();
			$output_data= array(
				'status'=>'success',
				'data'=>$all_users['data']
			);

			$this->success($output_data);

		} else {
			$this->bad_request();
		}
	}
	
	// Sectors handler
	public function sectors() {

		$request_body= stripslashes(file_get_contents("php://input"));
		$request= json_decode($request_body,true);


		if($request['action']=="get") {
			$this->load->model('Sector');
			$sectors=$this->Sector->get_names();
			$output_data=array(
				'status'=>'success',
				'data'=>$sectors
			);

			$this->success($output_data);
		} else {
			$this->bad_request();
		}
	}


	// Idea handler
	public function idea() {
		$request_body= file_get_contents("php://input");
		$request= json_decode($request_body,true);
	
		if($request['action']=="get") {
			$this->idea_get($request);
		} else if($request['action']==="update") {
			
			$this->idea_update($request['data']);
		} else if($request['action']=="add") {
			
			$this->idea_add($request['data']);
		} else {
			$this->bad_request();
		}
	}

	protected function idea_get($params) {
		$collection=$this->Idea4->get2($params);

		if(array_key_exists('error', $collection)) {
			$this->internal_server_error();
		}

		//check if the user is admin
		if(_f_is_admin()) {
			$output_data= array(
				'status'=>'success',
				'data'=>$collection['data']
			);
			$this->success($output_data);

		} else {
			// since the user is not admin we check for idea ownership
			if(array_key_exists('id', $params)) {
				if($collection['data'][$params['id']]['ownerid']==$this->session->socialid) {
					$output_data= array(
						'status'=>'success',
						'data'=>$collection['data']
					);
					$this->success($output_data);
					die(0);
				} else {
					$this->not_authorized();
					die(0);
				}
			} else {
				//the user is not allowed to fetch all ideas at once
				$this->bad_request();
				die(0);
			}
		}
		
	}

	protected function idea_update($request) {
		$this->load->model('Idea4');

		$idea_data=$this->Idea4->get($request);


		//check ownership of idea
		if($idea_data[0]['ownerid']!=$this->session->socialid) {
			if(!_f_is_admin()) {
				$this->not_authorized();
			}
		}

		$update_action=$this->Idea4->update($request);
		if($update_action['status']=="success") {
			$output_data=array('status'=>'success');
			$this->success($output_data);
		} else {
			$output_data=array('status'=>'failure','description'=>'DB query error');
			$this->internal_server_error($output_data);
		}
	}
	protected function idea_add($request) {
		$this->load->model('Idea4');
		// add ownership to idea
		$request['ownerid']=$this->session->socialid;

		$update_action=$this->Idea4->add($request);
		if($update_action['status']=="success") {
			$output_data=array('status'=>'success');
			$this->success($output_data);
		} else {
			$output_data=array('status'=>'failure','description'=>'DB query error');
			$this->internal_server_error($output_data);
		}
	}

	// admin info handler
	public function admin() {
		$request_body= stripslashes(file_get_contents("php://input"));
		$request= json_decode($request_body,true);

		if($request['action']=="get") {
			$this->admin_get($request['data']);
		} else if($request['action']=="add") {
			$this->admin_add($request['data']);

		} else if($request['action']=="delete") {
			$this->admin_delete($request['data']);

		} else {
			$this->bad_request();
		}

	}

	protected function admin_get($data) {
		$this->load->model('User3');
		$this->load->model('Admin_model');

		$admin=_f_is_admin();

		if(!$admin) {
			$this->not_authorized();
		}
		

		$user=$this->Admin_model->get2($data);
		

		$output_data=array(
			'status'=>'success',
			'data'=>$user['data']
		);

		$this->success($output_data);
	}

	protected function admin_add($data) {
		$this->load->model('User4');
		$user=$this->User4->get("mobile",$data['mobile']);
		if(!$user) $this->bad_request();
		$data['socialid']=$user[0]['socialid'];


		// only admins with all privileges can add other admins
		if(!_f_is_admin_all_privileges()) {
			$this->not_authorized();
		}

		$this->load->model('Admin_model');
		$add_action=$this->Admin_model->add($data);
		if($add_action['status']!="success") {
			$this->bad_request($add_action);
		}
		$this->load->model('User');
		$this->User->approve($data['socialid']);

		$this->success();
	}

	protected function admin_delete($data) {
		
		// only admins with all privileges can add other admins
		if(!_f_is_admin_all_privileges()) {
			$this->not_authorized();
		}

		if($data['socialid']==$this->session->socialid) {
			$output_data = array('status'=>'failure','description'=>'Cannot delete this admin!');
			$this->bad_request($output_data);
		}

		$this->load->model('Admin_model');
		$delete_action=$this->Admin_model->delete($data);
		if($delete_action['status']!="success") {
			$this->bad_request($add_action);
		}

		$this->success();
	}


	public function get_stats() {
		if(!_f_is_admin()) $this->not_authorized();

		$ideators = $this->User3->count(array('user_type'=>'ideator'));
		$sps = $this->User3->count(array('user_type'=>'service_provider'));
		$vcs = $this->User3->count(array('user_type'=>'investor'));
		$ideas = $this->Idea4->count();

		$output_data = array(
			'status'=>'success',
			'data'=>array(
				'ideas'=>$ideas,
				'ideators'=>$ideators,
				'service_providers'=>$sps,
				'investors'=>$vcs,
			)
		);

		$this->success($output_data);
	}


	//events handler
	public function events() {

	}

	public function admin_add_investors() {
		// special endpoint created for Admin to add Investor
		if(!_f_is_admin()) die(0);
		$this->load->model('Admin_add_investor');

		$act=$this->Admin_add_investor->new_investor($_POST);
		if($act['status']!="success") {
			http_response_code(500);
			echo json_encode($act);
		}
		else {
			http_response_code(200);
			echo json_encode($act);
		}
	}
}
?>
