<?php
class Message extends CI_Controller
{
	public function index()
	{
		if(isset($this->session->socialid))
		{
            if(_f_is_user_approved())
			{
				$data = array();
		        if(isset($this->session->socialid))
		        {
		            $this->load->model('User3');
		            $user=$this->User3->read($this->session->socialid);
		            $data['me'][0]=$user;
		            
		        }
                $data['title']="SaYourIdeas.com";
                $this->parser->parse('templates/header',$data);
                $this->load->view('message');
                $this->load->view('templates/footer',$data);
			}
		}				
	}
}
?>
