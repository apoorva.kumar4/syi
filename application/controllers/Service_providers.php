<?php
class Service_providers extends CI_Controller{
	public function index()
	{
		
		$data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
		$data['title']="SaYourIdeas.com - Startups";
		$data['service_providers']=_f_get_all_service_providers();
		//var_dump($data['service_providers']);die(0);
		$this->parser->parse('templates/header',$data);
		$data['cities']=_f_get_cities();
		$data['services']=_f_get_services();
		$this->load->view('service_providers',$data);
		$this->load->view('copyfooter');
		
	}
	public function search()
	{
		$data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
		if(isset($_POST['service-name']))
		{
			$data['title']="SaYourIdeas.com - Startups";
			$data['service_providers']=_f_get_all_service_providers_search($_POST['service-name'],$_POST['city']);
			$data['cities']=_f_get_cities();
			$data['services']=_f_get_services();
			$this->parser->parse('templates/header',$data);
			$query=strtolower($_POST['service-name']);
			$query_capital=ucwords($query);

			$data['search_message']="Showing ". sizeof($data['service_providers'])." results for ". $query_capital;
			$this->load->view('service_providers',$data);
			$this->load->view('copyfooter');
		}
		else if(segx(3))
		{
			$data['title']="SaYourIdeas.com - Startups";
			$data['cities']=_f_get_cities();
			$data['services']=_f_get_services();
			$data['service_providers']=_f_get_all_service_providers_search(segx(3),segx(4));
			$this->parser->parse('templates/header',$data);
			$query=strtolower(segx(3));
			$query_capital=ucwords(urldecode($query));

			$data['search_message']="Showing ". sizeof($data['service_providers'])." results for ". $query_capital;
			$this->load->view('service_providers',$data);
			$this->load->view('copyfooter');	
		}

	}
}
/*Materialize.fadeInImage('#image-test')*/
?>