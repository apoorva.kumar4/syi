<?php
class Events extends CI_Controller
{
	public function index()
	{
		$data = array();
		if(isset($this->session->socialid))
		{
			$this->load->model('User3');
			$user=$this->User3->read($this->session->socialid);
			$data['me'][0]=$user;

		}
		if(_f_is_loggedin()){
			$analytics['socialid']=$this->session->socialid;
			$analytics['event_category']="page_visit";
			$analytics['event_action']="visit";
			$analytics['event_label']="event_page";
			$analytics['event_value']='event';
			$analytics['event_description']="Visited Events page";
			a_send($analytics);
		}

		$data['title'] = "Events";
		$this->parser->parse('templates/header', $data);
		$data['events']=_f_get_all_approved_upcoming_events();
		$data['pastevents']=_f_get_all_approved_past_events();
		$this->load->view('events', $data);
		$this->load->view('copyfooter');

	}
	public function calendar()
	{
		$this->load->view('calendar');

	}
	public function _remap($method)
	{
		if($method ==="index")
		{
			$this->index();

		}
		else if($method==="create")
		{
			$this->create();
		}
		else if($method==="edit")
		{
			$this->edit();
		}
		else
		{
			$this->default_method();
		}

	}
	public function edit()
	{
		$data = array();
		if(isset($this->session->socialid))
		{
		    $this->load->model('User3');
		    $user=$this->User3->read($this->session->socialid);
		    $data['me'][0]=$user;
		    
		}
		$this->load->model('event');

		$checkid=$this->uri->segment(3,0);
		$socialid=$this->session->userdata('socialid');
		if(_is_event_owned($checkid,$socialid))
		{   
			$this->session->set_userdata('current_eventid',$checkid);
			$data['title']="Edit Event";

			$this->parser->parse('templates/header',$data);

			$config = array (
				array(
					'field'=>'event-name',
					'label'=>'Event Name',
					'rules'=>'required'
				),
				array(
					'field'=>'event-description_short',
					'label'=>'Event Description Short',
					'rules'=>'required'
				),
				array(
					'field'=>'event-description_long',
					'label'=>'Event Description Long',
					'rules'=>'required'
				),
				array(
					'field'=>'event-slug',
					'label'=>'URL',
					'rules'=>'required|callback_slug_update_availablility'
				)
			);

			$this->form_validation->set_rules($config);



			if ($this->form_validation->run() == FALSE) {
				$this->load->model('event','new');
				if($this->uri->segment(3,0))
				{
					$data['event']=_f_get_event(segx(3));
					$this->load->view('edit_event',$data);
					$this->load->view('templates/footer');
				}
				else
				{
					echo "No resource id set";
				}

			} else {
				$this->load->model('event','new_event');
				$this->new_event->id=segx(3);
				$this->new_event->name=$_POST['event-name'];
				$this->new_event->description_short=$_POST['event-description_short'];
				$this->new_event->description_long=$_POST['event-description_long'];
				$this->new_event->date_start=$_POST['event-date_start'];
				$this->new_event->date_end=$_POST['event-date_end'];
				$this->new_event->slug=$_POST['event-slug'];


				$action=$this->new_event->patch();

				if($action==1)
				{
					//$uuu=base_url().'dashboard/events';
					//header("Location: $uuu");

					$data['title']="Suceess!";

					$this->load->view('event_edit_success');

				}
				else
				{
					_f_message("Error Occured","Something went wrong when adding Idea, please try again.");
					$uuu=base_url().'message';
					header("Location: $uuu");
				}
			}

		}
		else
		{
			echo "Unauthorized";
		}
	}
	public function slug_update_availablility($param)
	{
		$this->load->model('event');
		if(isset($this->session->socialid))
		{
			$this->event->ownerid=$this->session->socialid;
		}

		$this->form_validation->set_message('slug_update_availablility', "URL $param is already taken, please choose another one");
		$current_eventid=$this->session->current_eventid;
		$ret=$this->event->check_slug_update_availability($current_eventid,$param);

		return $ret;

	}
	public function default_method()
	{

		$this->load->model('User3');
		$socialid=$this->session->socialid;

		$user=$this->User3->read($socialid);

		$data['me'][0]=$user;

		$this->load->model('event');
		$this->event->slug=segx(2);
		$ret_event=$this->event->get_event_from_slug();
		$this->event->id=$ret_event['id'];

		if($ret_event)
		{
			if($ret_event['admin_approved']==1)
			{
				$eventid=$ret_event['id'];
				$socialid=$this->session->socialid;

				if(_f_is_admin() || _is_event_owned($eventid,$socialid)) {
				}
				else{

					if(_f_is_loggedin() && !(_f_is_admin()))
						$this->event->increment_views($eventid);
				}

				$data['event']=$ret_event;
				$data['title']=$ret_event['name'];
				$this->parser->parse('templates/header',$data);
				$this->load->view('event_view',$data);
				$this->load->view('templates/footer');
			}
			else
			{
				$eventid=$ret_event['id'];
				$socialid=$this->session->socialid;

				if(_f_is_admin() || _is_event_owned($eventid,$socialid))
				{   
					$data['event']=$ret_event;
					$data['title']=$ret_event['name'];
					$this->parser->parse('templates/header',$data);
					$this->load->view('event_view',$data);
					$this->load->view('templates/footer');
				}
				else
				{


				}
			}

		}
		else
		{
			//show_404(segx(2));
		}
	}
	public function create()
	{

		$data['title'] = "Events";
		if (!_f_is_loggedin())
		{
			header("Location: /login");
		}

		$this->load->model('User3');
		$socialid=$this->session->socialid;

		$user=$this->User3->read($socialid);

		$data['me'][0]=$user;
		$this->parser->parse('templates/header', $data);
		$this->load->view('events_create', $data);
		$this->load->view('templates/footer');

	}
	public function santext($parameter,$field)
	{
		$this->form_validation->set_message('santext', "No special characters allowed in $field");
		$result=_f_validate('santext',$parameter);


		if($result==NULL)
		{

			return FALSE;


		}
		else if($result==$parameter)
		{
			return TRUE;
		}

	}
	public function slug_availablility($param)
	{
		$this->load->model('event');
		if(isset($this->session->socialid))
		{
			$this->event->ownerid=$this->session->socialid;
		}

		$this->form_validation->set_message('slug_availablility', "URL $param is already taken, please choose another one");
		$ret=$this->event->check_slug_availability($param);

		return $ret;

	}
}
?>
