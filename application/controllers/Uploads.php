<?php
class Uploads extends CI_Controller
{
  public function u()
  {

      $this->load->model('supload');
      $upload=$this->supload->get_upload_from_slug(segx(3));

      if($upload['permissions']!=3){
        if($upload['socialid']==$this->session->socialid || _f_is_admin())
        {
          $this->load->library('aws_upload');
          $par['targetfilename']=$upload['url'];
          $par['content-disposition']="attachment";
          $result=$this->aws_upload->get_upload($par);
          
          header("Content-Type: {$result['ContentType']}");
          echo $result['Body'];
        }

      } else {
        $remoteImage = $this->config->item('upload_path').segx(3);
        if($imginfo = getimagesize($remoteImage)) {
            header("Content-type: ".$imginfo['mime']);
            readfile($remoteImage); 
        } else {
            //$data['status']="failure";
            //$data['description']="Invalid File Type";
            //echo json_encode($data);
            $this->load->helper('download');
            $this->load->helper('file');
            $contents=read_file($remoteImage);

            $action = $this->db->get_where('uploads',array('url' => segx(3)),1);

            $row=$action->result_array();

            $temporary = explode(".",$row[0]['url']);
            $file_extension = end($temporary);

            $filename=$row[0]['name'].".".$file_extension;

            force_download($filename,$contents,null);
        }
      }
  }
}
?>