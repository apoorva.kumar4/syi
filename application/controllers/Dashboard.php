<?php
class Dashboard extends CI_Controller
{
    

    public function index()
    {
        
        if(!_f_is_loggedin()) {
            header('Location: /login');
            exit(0);
        }

        if(_f_is_admin()) {
            header('Location: /admin');
            exit(0);
        }

        $this->load->model('User_wrapper');
        $usss=$this->User_wrapper->read($this->session->socialid);

        //var_dump($usss);die(0);

        $this->load->model('User3');
        $user=$this->User3->read($this->session->userdata('socialid'));

        $data['me'][0]=$user;
        $data['title'] = "Dashboard";
        $this->parser->parse('templates/header', $data);
        //var_dump($usss);die(0);
        if($usss['user_type']=="investor" && $usss['admin_approved']==0) {
                if($usss['email']!=null){
                    $this->load->view('account_pending_approval',$data);
                    $this->load->view('templates/footer', $data);
                } else {
                    $data['usss']=$usss;
                    $this->load->view('investor_form',$data);
                    $this->load->view('templates/footer', $data);
                }
                
        }
        else {
            if ($user['user_type'] == "ideator") {     
            $this->load->model('idea');            
            $data['ideas'] = $this->idea->get_ideas_for_user($this->session->socialid);
            $data['profile_completeness']=$this->User3->get_completeness_score($user['socialid']); 
            $this->load->view('dashboard_timeline', $data);
            $this->load->view('templates/footer', $data);
            
            }
            else if ($user['user_type'] == "service_provider") {
                $this->load->model('service_provider', 'current_sp', true);
		          $services = $this->current_sp->get_list_of_services($this->session->socialid);
                $data['services'] = $services;
		
        		$this->load->model('User_rate_card');
        		$data['rate_cards']=array();
        		$rate_cards=$this->User_rate_card->get($this->session->socialid);
        		foreach($rate_cards as $rate_card) {
        			// fetch upload using Supload
        			$this->load->model('Supload');
        			$url=$this->Supload->get_complete_url($rate_card['uploadid']);
        			$rate_card_data = array(
        				'url'=>$url,
        				'name'=>$rate_card['name'],
        				'id'=>$rate_card['id']
        			);
        			array_push($data['rate_cards'],$rate_card_data);
        		}

                $data['ideas'] = $this->current_sp->get_suggested_ideas($this->session->socialid);
                $this->load->view('dashboard_sp', $data);
                $this->load->view('templates/footer', $data);
                
            }
            else if ($user['user_type'] == "investor") {
                
                
                $this->load->model('user');
                $ideas=$this->user->get_shortlisted_ideas($this->session->socialid);
                $this->load->model('investor');
                $sug_ideas=$this->investor->get_suggested_ideas($this->session->socialid);
                $data['ideas']=$ideas;
                $data['sug_ideas']=$sug_ideas;
                $this->ideas=$ideas;
                $this->load->view('dashboard_vc', $data);
                $this->load->view('templates/footer', $data);
            }
            
            else if ($user['user_type']==null)
            {

                if(array_key_exists("select_user_type", $_POST)) {

                    if($_POST["select_user_type"]=="ideator" || $_POST["select_user_type"]=="service_provider") {
                        //var_dump($_POST);
                        //die(0);
                        $update_data = array('user_type'=>$_POST['select_user_type']);
                        $this->db->where('socialid',$this->session->socialid);
                        $action=$this->db->update('users',$update_data);
                        if($_POST['select_user_type']=="ideator") {
                            $this->load->model('Ideator4');
                            $this->Ideator4->insert($this->session->socialid,array());
                        }
                        else if($_POST['select_user_type']=="service_provider") {
                            $this->load->model('Service_provider4');
                            $this->Service_provider4->insert($this->session->socialid,array());
                        }

                        if($action) {
                            
                            header('Location: /dashboard');

                        } else {
                            echo "fatal error: 1508156591";
                        }
                    }
                    else if($_POST["select_user_type"]=="investor") {
                        
                        $this->db->where('socialid',$this->session->socialid);
                        $this->db->set(array('admin_approved'=>0,'user_type'=>'investor'));

                        $action=$this->db->update('users');
                        if($action) {
                            header('Location: /dashboard');

                        } else {
                            echo "fatal error: 1508835959";
                        }                     
                    }
                } else {
                    $data['me'][0]=$user;
                    $data['title']="Select your Account type";
                    $this->parser->parse('select_user_type',$data);
                    $this->parser->parse('templates/footer',$data);
                }
                    
            }
        }        

    }
    public function profile()
    {
        
        if(!_f_is_loggedin()){
            header('Location: /login');
            exit(0);
        }

        if(_f_is_admin()) {
            header('Location: /admin/admin_profile');
            exit(0);
        }
        
            $this->load->model('User3');
            $socialid=$this->session->socialid;
            
            $user=$this->User3->read($socialid);
            if(!$user) {
                log_message('debug','error fetching logged in user on dashboard/profile');
                exit(0);
            }
            if($user['user_type']=="ideator")
            {
                
                $data['title']="Profile - SaYourIdeas.com";
                $data['user']=$user;
                $data['me'][0]=$user;
                //var_dump($data);die(0);
                $this->parser->parse('templates/header',$data);
                $this->load->view('dashboard_profile2',$data);
                $this->load->view('templates/footer',$data);
            }
            else if($user['user_type']=="service_provider")
            {
                $data['title']="Profile - SaYourIdeas.com";
                $data['user']=$user;
                $data['me'][0]=$user;
                $this->parser->parse('templates/header',$data);
                $this->load->view('dashboard_profile_sp2',$data);
                $this->load->view('templates/footer',$data);

            }
            else if($user['user_type']=="investor")
            {
                $data['user']=$user;
                //var_dump($user);die(0);
                $data['me'][0]=$user;
                $data['title']="Profile - SaYourIdeas.com";
                $this->parser->parse('templates/header',$data);
                $this->load->view('dashboard_profile_vc2',$data);
                $this->load->view('templates/footer',$data);
            }
            else if($user['user_type']=="admin"){
                header('Location: /admin/admin_profile');
            }        
    }
    public function events()
    {
        if(!_f_is_loggedin())
        {
            header('Location: /login');
            exit(0);
        } 

        if(_f_is_admin()) {
            header('Location: /admin');
            exit(0);
        }
        
        $this->load->model('User3');
        $socialid=$this->session->socialid;
            
        $user=$this->User3->read($socialid);
        
        $data['me'][0]=$user;

        if($user['admin_approved']==1)
        {
            $data['title']="Your Events - SaYourIdeas.com";
            $this->load->model('ideator','current_ideator',true);
            $events=$this->current_ideator->_get_list_of_events($this->session->socialid);
            $data['events']=$events;

            $this->parser->parse('templates/header',$data);
            $this->load->view('dashboard_events');
            $this->load->view('templates/footer');
        }else if($user['admin_approved']==2)
        {
            $data['title'] = "Dashboard";
            $this->parser->parse('templates/header', $data);
            $this->load->view('account_rejected', $data);
            $this->load->view('templates/footer', $data);
        }
        else{
            $data['title'] = "Dashboard";
            $this->parser->parse('templates/header', $data);
            $this->load->view('account_pending_approval', $data);
            $this->load->view('templates/footer', $data);
        }
        
    }
}
?>
