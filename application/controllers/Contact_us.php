<?php
class Contact_us extends CI_Controller{
	public function index()
	{
        $data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
        
        if(_f_is_loggedin()){
            $analytics['socialid']=$this->session->socialid;
            $analytics['event_category']="page_visit";
            $analytics['event_action']="visit";
            $analytics['event_label']="contact_page";
            $analytics['event_value']='contact';
            $analytics['event_description']="Visited Contact us page";
            a_send($analytics);
        }
		$data['title']="SaYourIdeas.com - Contact Us";
		$this->parser->parse('templates/header',$data);
		$this->load->view('contact_us',$data);
		$this->load->view('copyfooter');
		
	}
	public function submit_message()
    {
        $data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;

        }
        
        $secret="6LfJEwcUAAAAAAa_oAlSvTfi_z8wbEcYiXQjm2ek"; //Secret for aashay0101@gmail.com's no-captcha account
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $decoded_response=json_decode($verifyResponse,true);

        if($decoded_response['success']==true)
        {
            // success! your form was validated
            // do what you like next ...
            $this->load->helper('form');
            $this->load->library('form_validation');

            $config = array (
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'subject',
                    'label' => 'Subject',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'message',
                    'label' => 'Message',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'g-recaptcha-response',
                    'label' => 'Captcha',
                    'rules' => 'required'
                )
            );

            $this->form_validation->set_rules($config);

            if($this->form_validation->run() == FALSE)
            {
                $data['title']="Contact us";
                $this->parser->parse('templates/header',$data);
                $this->load->view('contact_us');
                $this->load->view('copyfooter');
            }
            else
            {
                $prep_data= array(
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
                    'mobile' => $_POST['mobile'],
                    'subject' => $_POST['subject'],
                    'message' => $_POST['message'],

                );
                $this->db->insert('contact_us_form',$prep_data);

                $this->load->library('email');

                $this->email->from('no-reply@sayourideas.com', 'Contact Us Form');
                $this->email->to('contact@sayourideas.com');
                $name=$_POST['name'];
                $subject="Message from $name";
                $message=$_POST['message'];
                $this->email->subject($subject);
                $this->email->message($message);
                $this->email->send();

                $data['title']="Contact us";
                $this->parser->parse('templates/header',$data);
                $this->load->view('contact_us_form_success');
            }
        }
        else
        {

            $data['title']="Contact us";
            $this->parser->parse('templates/header',$data);
            $this->load->view('contact_us');
            $this->load->view('copyfooter');
        }
    }
}
?>