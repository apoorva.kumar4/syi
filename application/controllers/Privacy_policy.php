<?php
class Privacy_policy extends CI_Controller{
    public function index()
    {
    	$data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
        $data['title']="SaYourIdeas.com - Privacy Policy";
        $this->parser->parse('templates/header',$data);
        $this->load->view('privacy_policy',$data);
        $this->load->view('copyfooter');
    }
}
?>