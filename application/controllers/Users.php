<?php
class Users extends CI_Controller
{
	public function index()
	{
		header('Location: /home');

	}
	public function _remap($method)
	{
		if($method ==="index")
		{
			$this->index();

		}
		else
		{
			$this->default_method();
		}

	}
	public function default_method()
	{
		$this->load->model('User4');
		$this->load->model('User_wrapper');
		$this->load->model('Supload');
		$this->load->model('Syiconfig');
		$this->load->model('Idea4');
		$this->load->model('Service4');
		$this->load->model('Service_provider4');
		$this->load->model('Investor4');
		$this->load->model('User_rate_card');

		//if the user is not logged in take him to login page
		if(!isset($this->session->socialid))
		{
			header('Location: /login');
		}

		//preliminary check if the requested user exists
		$username=segx(2);
		$ret_user= $this->User_wrapper->read($username);
		//var_dump($ret_user);die(0);
		if(!$ret_user) die(0);


		//data is passed to the views
		$data = array();

		//fetch data of the logged in user
		$user=$this->User_wrapper->read($this->session->socialid);
		$data['me'][0]=$user;

		$data['user'][0]=$ret_user;
		$data['title']=$ret_user['first_name']; 
		$this->parser->parse('templates/header',$data);


		if($ret_user['user_type']=="ideator") {
			$data['ideas']=$this->Idea4->get_public_ideas_for_user($ret_user['socialid']);
			$data['followed_ideas']=$this->Idea4->get_ideas_followed_by_user($ret_user['socialid']);
			$ret_ideator=$this->Ideator4->get('socialid',$ret_user['socialid']);
			if($ret_ideator) $data['user'][0]+=$ret_ideator[0];
			$this->parser->parse('user_view_ideator',$data);
		}

		else if($ret_user['user_type']=="service_provider") {
			$data['services']=$this->Service4->get_services_for_user($ret_user['socialid']);
			$data['rating']=$this->Service_provider4->get_rating($ret_user['socialid']);
			$data['rate_cards']=array();
			$rate_cards=$this->User_rate_card->get($ret_user['socialid']);
			foreach($rate_cards as $rate_card) {
				// fetch upload using Supload
				$this->load->model('Supload');
				$url=$this->Supload->get_complete_url($rate_card['uploadid']);
				$rate_card_data = array(
					'url'=>$url,
					'name'=>$rate_card['name'],
					'id'=>$rate_card['id']
				);
				array_push($data['rate_cards'],$rate_card_data);
			}
			$ret_sp=$this->Service_provider4->get('socialid',$ret_user['socialid']);
			
			$data['user'][0]+=$ret_sp[0];
			$this->parser->parse('user_view_sp',$data);
		}

		else if($ret_user['user_type']=="investor") {
			$data['ideas']=array();
			$followed_ideas=$this->Idea4->get_ideas_followed_by_user($ret_user['socialid']);
			foreach($followed_ideas as $idea) {
				$fetched_idea=$this->Idea4->get(array('id'=>$idea['ideaid']));
				array_push($data['ideas'],$fetched_idea[0]);

			}
			//var_dump($data['ideas']);die(0);
			$ret_investor=$this->Investor4->get('socialid',$ret_user['socialid']);
			if($ret_investor) $data['user'][0]+=$ret_investor[0];

			$this->parser->parse('user_view_vc',$data);
		}

		else if($ret_user['user_type']=="admin") {
			$this->load->view('user_view_admin',$data);
		}

		$this->load->view('templates/footer');

	}
	public function create()
	{
		$data['title'] = "Events";
		if (_f_is_loggedin())
		{
			$this->parser->parse('templates/header', $data);



			$config = array(
				array(
					'field' => 'event-name',
					'label' => 'Event Name',
					'rules' => 'required|callback_santext[Event Name]'
				),
				array(
					'field' => 'event-description_short',
					'label' => 'Event Description Short',
					'rules' => 'required'
				),
				array(
					'field' => 'event-description_long',
					'label' => 'Event Description Long',
					'rules' => 'required'
				),
				array(
					'field' => 'event-date_start',
					'label' => 'Event Start Date',
					'rules' => 'required'
				),
				array(
					'field' => 'event-date_end',
					'label' => 'Event Date End',
					'rules' => 'required'
				),
				array(
					'field' => 'event-slug',
					'label' => 'URL',
					'rules' => 'required|callback_slug_availablility'
				)
			);

			$this->form_validation->set_rules($config);

			if ($this->form_validation->run() == FALSE) {
				$this->load->view('events_create', $data);
				$this->load->view('templates/footer');
			}
			else
			{
				$this->load->model('event','new_event');
				$this->new_event->name=$_POST['event-name'];
				$this->new_event->description_short=$_POST['event-description_short'];
				$this->new_event->description_long=$_POST['event-description_long'];
				$this->new_event->date_start=$_POST['event-date_start'];
				$this->new_event->date_end=$_POST['event-date_end'];
				$this->new_event->slug=$_POST['event-slug'];


				$action=$this->new_event->create();

				if($action==1)
				{
					$uuu=base_url().'dashboard/events';
					header("Location: $uuu");

				}
				else
				{
					_f_message("Error Occured","Something went wrong when adding Idea, please try again.");
					$uuu=base_url().'message';
					header("Location: $uuu");
				}


			}
		}
		else
		{
			header("Location: /login");
		}
	}
	public function santext($parameter,$field)
	{
		$this->form_validation->set_message('santext', "No special characters allowed in $field");
		$result=_f_validate('santext',$parameter);


		if($result==NULL)
		{

			return FALSE;


		}
		else if($result==$parameter)
		{
			return TRUE;
		}

	}
	public function slug_availablility($param)
	{
		$this->load->model('event');
		if(isset($this->session->socialid))
		{
			$this->event->ownerid=$this->session->socialid;
		}

		$this->form_validation->set_message('url_availablility', "URL $param is already taken, please choose another one");
		$ret=$this->event->check_slug_availability($param);

		return $ret;

	}


}
?>
