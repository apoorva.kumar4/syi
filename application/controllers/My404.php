<?php
/**
 * Created by PhpStorm.
 * User: aashayshah
 * Date: 07/10/16
 * Time: 5:45 PM
 */
class My404 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
        $this->output->set_status_header('404');
        $data['content'] = 'error_404'; // View name
        $this->parser->parse('templates/header',$data);
        $this->load->view('my404');//loading in my template
        $this->load->view('templates/footer');
    }
}
?>