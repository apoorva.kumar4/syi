<?php
class Api2 extends CI_Controller
{

		public function _remap($resource)
		{
				$sub_resource=segx(3,0);
				$sub_resource=str_replace("-", "_",$sub_resource);
				$method= strtolower($_SERVER['REQUEST_METHOD']);

				if(!is_numeric($sub_resource))
				{
						$function_ef=$resource."_".$sub_resource."_".$method;
				}
				else{
						$resource_id=segx(3);
						$sub_resource=segx(4);
						if($sub_resource)
						{
								$function_ef=$resource."_".$sub_resource."_".$method;
						}
						else{
								$function_ef=$resource."_".$method;
						}
				}
				//var_dump($function_ef);die(0);
				if(method_exists($this,$function_ef)){
						$this->$function_ef();
				}
				else
				{

						http_response_code(400);
						$data['status']="failure";
						$data['description']="No such resource or method exists";
						echo json_encode($data);

				}
		}
		protected function output_data($http_status_code,$data="",$override_format=""){
				http_response_code($http_status_code);

				$dformat=isset($_GET['format'])?$_GET['format']:"json";
				if($override_format!="")
				{
						$format=$override_format;
				}
				else
				{
						$format=$dformat;
				}

				if($format=="json")
				{
						header('Content-Type: application/json');
						if($data!="")
								echo json_encode($data,JSON_UNESCAPED_SLASHES);
								
				}
				else if($format=="xml")
				{
						header('Content-Type: application/xml');
						$xml_data = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
						$this->array_to_xml($data,$xml_data);
						print $xml_data->asXML();
				}
				else if($format=="pdf")
				{
						$this->load->library('pdf');
						$this->pdf->setPaper('A4', 'potrait');

						if(gettype($data)=="string"){
								$this->pdf->loadHtml($data);
								$this->pdf->render();

						}else if(gettype($data=="array")){
								die(0);
						}

						$this->pdf->stream($data['title'], array(
								"Attachment" => 0
						));
				}
		}
		protected function array_to_xml( $data, &$xml_data ) {
				foreach( $data as $key => $value ) {
						if( is_numeric($key) ){
								$key = 'item'.$key; //dealing with <0/>..<n/> issues
						}
						if( is_array($value) ) {
								$subnode = $xml_data->addChild($key);
								$this->array_to_xml($value, $subnode);
						} else {
								$xml_data->addChild("$key",htmlspecialchars("$value"));
						}
				}
		}
		protected function ideas_post()
		{
				$par = array();
				$par += $_POST;
				//Check for socialid first
				if(isset($this->session->socialid)) {

						$par['ownerid'] = $this->session->socialid;
				}
				else if(_f_is_admin())
				{
						$par['ownerid']=$_POST['socialid'];
				}
				else{
						http_response_code(403);
						$data['status']="failure";
						$data['description']="You are not logged in";
						echo json_encode($data);
						die(0);
				}

				$this->load->model('idea');

				$par['cover_uploadid']="";
				$par['logo_uploadid']="";

				// Get the logo file from the $_FILES array received
				$logo_file=$_FILES['logofile']['error'] > 0?null:$_FILES['logofile'];


				// Get the cover file from the $_FILES array received
				$cover_file=$_FILES['coverfile']['error'] > 0?null:$_FILES['coverfile'];


				// Proceed to upload logo with the Supload model and retrieve upload id ,
				// use upload id and associate it with appropriate entity
				if (isset($logo_file["type"]) && $logo_file!=0) {

						$this->load->model('supload');
						$upload_params = array(
								'socialid' => $this->session->socialid,
								'file' => $logo_file,
								'permissions' => 3,
								'accepted_types' => "image"
						);
						$logo_uploadid = $this->supload->create($upload_params);
						if($logo_uploadid['status']!="error")
						{
								$par['logo_uploadid']=$logo_uploadid['id'];
						}
				}

				// Proceed to upload cover file with the Supload model and retrieve upload id ,
				// use upload id and associate it with appropriate entity
				if (isset($cover_file["type"]) && $cover_file!=0) {
						$upload_params = array(
								'socialid' => $this->session->socialid,
								'file' => $cover_file,
								'permissions' => 3,
								'accepted_types' => "image"
						);
						$this->load->model('supload');
						$cover_uploadid = $this->supload->create($upload_params);
						if($cover_uploadid['status']!="error") $par['cover_uploadid']=$cover_uploadid['id'];
						else
						{
								echo "Invalid image;";
								echo $cover_uploadid['description'];
								echo $logo_uploadid['status'];
								die(0);
						}
				}

				// We have all the details and logo and cover file as well,
				// now create the idea

				$action=$this->idea->create($par);

				if(isset($action['status'])?$action['status']=="success":0)
				{
						$analytics['socialid']=$this->session->socialid;
						$analytics['event_category']="idea";
						$analytics['event_action']="created";
						$analytics['event_label']=$par['name'];
						$analytics['event_value']=$action['ideaid'];
						$analytics['event_description']="Created new Idea";
						a_send($analytics);

						http_response_code(201);
						$data['status']="success";
						$data['description']="Idea Created";
						$data['ideaid']=$action['ideaid'];
						echo json_encode($data);

				}
				elseif(isset($action['status'])?$action['status']=="error":1)
				{

						http_response_code(400);
						header('Content-Type: application/json');
						$data['status']="error";
						$data['description']=$action['description'];
						echo json_encode($data);
				}
				else
				{
						http_response_code(400);
						$data['status']="error";
						$data['description']=$action['description'];
						echo json_encode($data);
				}
		}
		protected function idea_get()
		{
				$this->load->model('idea');
				if(segx(3) && is_numeric(segx(3)))
				{
						$data=$this->idea->public_details(segx(3));
						if($data)
						{
								$this->output_data(200,$data);
						}
						else{
								$this->output_data(404);
						}
				}
				else{

						$data['status']="failure";
						$data['description']="No resource id set";
						$this->output_data(400,$data);
				}
		}
		protected function idea_pdf_get()
		{
				if(_f_is_admin())
				{
						$this->load->model('idea');
						if(segx(3) && is_numeric(segx(3)))
						{
								$idea=$this->idea->public_details(segx(3));
								if($idea)
								{
										$ideaid=$idea[0]['id'];
										$query = "select url from uploads where id in (Select max(uploadid) from idea_logos where ideaid=$ideaid)";
										$remoteImage="";
										$url_action = $this->db->query($query);
										if ($url_action) {
												$row = $url_action->result_array();
												if ($row) $remoteImage = $this->config->item('upload_path') . $row[0]['url'];
										}

										$data=array();
										$data['title'] = $idea[0]['name'];
										$data['idea']=$idea[0];
										$data['idea']['visibility']=_get_visibility_from_id($data['idea']['visibility']);
										$data['idea_logo']=$remoteImage;

										$html_for_pdf_header = $this->load->view('pdf_header', $data, true);
										$html_for_pdf = $this->load->view('pdf_idea_info', $data, true);
										$html_data = $html_for_pdf_header . $html_for_pdf;

										$this->output_data(200,$html_data,"pdf");
								}
								else{
										$this->output_data(404);
								}
						}
						else{
								$data['status']="failure";
								$data['description']="No resource id set";
								$this->output_data(400,$data);
						}
				}
				else{
						$data=array('forbidden'=>"Admins only area");
						$this->output_data(403,$data);
				}
		}
		protected function uploads_get() {
			if(!_f_is_admin()) {
				$this->output_data(403,array());
			}

			$params = array('fields'=>'id,socialid,name,url');

			$uploads = $this->Supload->get2($params);

			$this->output_data(200,$uploads);

		}

		protected function partners_get() {
			http_response_code(200);
			header('content-type: application/json');
			$partners=_f_get_partners();
			echo json_encode($partners,JSON_UNESCAPED_SLASHES);	
		}
		protected function partners_post() {
			//this endpoint is for admin only
			if(!_f_is_admin()) { 
				die(0);
			}

			$this->load->model('supload');
			$socialid=$this->session->socialid;

			$file=$_FILES['file']['error'] > 0?null:$_FILES['file'];
			$uploadid="";
			//var_dump($file);die(0);

			if (!(isset($file["type"]) && $file!=0)) { 
					http_response_code(400);
					$ret_data['status']="error";
					$ret_data['description']="Invalid file";
					echo json_encode($ret_data);
					die(0);
			}

			$upload_params = array(
				'socialid'=>$socialid,
				'file'=>$file,
				'permissions'=>3,
				'accepted_types'=>"document_image",
				'input'=>'document'
			);

			$uploadac=$this->supload->create($upload_params);

			if($uploadac['status']!="error"){
				$uploaid=$uploadac['id'];
				$ins_array= array(
					'name'=>$_POST['name'],
					'website'=>$_POST['website'],
					'upload_id'=>$uploaid
				);

				$this->db->set($ins_array);
				$action=$this->db->insert('partners');

				if($action)
				{
					http_response_code(200);
					header('Content-Type: application/json');
					$data['status']="success";
					$data['description']="Partner Added successfully";
					$data['url']=$this->config->item('upload_path').$uploadac['url'];
					$data['uploadid']=$uploaid;
					
					echo json_encode($data);
				} else {
					http_response_code(500);
					header('Content-Type: application/json');
					$data['status']="failure";
					$data['description']="DB error occurred";	
					echo json_encode($data);
				}
			}
			else{
				http_response_code(400);
				$data['status']="error";
				$data['description']=$uploadac['description'];
				echo json_encode($data);
				die(0);
			}
		}
		protected function partners_delete() {
			//this endpoint is for admin only
			if(!_f_is_admin()) { 
				die(0);
			}
			$partner_id=segx(3);
			if(!$partner_id) {
				http_response_code(400);
				$data['status']="error";
				$data['description']="Partner id invalid";
				echo json_encode($data);
				die(0);
			}

			$this->db->where('id',$partner_id);
			$action=$this->db->get('partners');
			if(!$action) {
				http_response_code(500);
				$data['status']="error";
				$data['description']="Unknwon error occurred";
				echo json_encode($data);
				die(0);
			}
			$result=$action->result_array();

			$this->db->where('id',$partner_id);
			$this->db->delete('partners');
			
			$upload_id = $result[0]['upload_id'];
			$delete_upload_action=$this->Supload->delete($upload_id);

			
			http_response_code(200);
			$data['status']="success";
			$data['description']="Partner deleted successfully";
			echo json_encode($data);
			die(0);
		}
		protected function idea_timeline_get(){
				$this->load->model('ideator');
				if(isset($_GET['token'])){}else{$_GET['token']="";}
				if(_f_is_admin() || (_f_is_user_approved() && $this->session->user_type=="investor") || _f_get_idea_shareable_link(segx(4))==$_GET['token'] || $this->ideator->_is_idea_owned(segx(4))){
						if(is_numeric(segx(4)))
						{
								$this->load->model('idea');
								$bullets_data = $this->idea->get_bullets_for_timeline(segx(4));

								foreach ($bullets_data as &$bullet) {

										if ($bullet['template'] == "1") {
												$bullet['className'] = "bluex card hoverable";
												$bullet['content'] = "<span class='largetemplate'>" . $bullet['content'] . "</span>";
										}

								}
								echo json_encode($bullets_data);
						}
						else{
								http_response_code(400);
						}
				}
				else{
						http_response_code(401);
				}
		}
		protected function me_get() {
			if(!_f_is_loggedin()) $this->output_data(401);

			$data = array(
				'socialid'=>$this->session->socialid
			);

			$this->output_data(200,$data);

		}
		protected function idea_delete(){
				$seg3=segx(3);
				$this->load->model('idea');
				$fetched_idea=$this->idea->read($seg3);
				if ($fetched_idea['ownerid']==$this->session->socialid || _f_is_admin()) {

						$action=$this->idea->delete($seg3);
						if($action)
						{
								if(_f_is_loggedin()){
										$analytics['socialid']=$this->session->socialid;
										$analytics['event_category']="idea";
										$analytics['event_action']="delete";
										$analytics['event_label']="Deleted";
										$analytics['event_value']=$fetched_idea['name'];
										$analytics['event_description']="Deleted Idea ".$fetched_idea['name'];
										a_send($analytics);
								}
								http_response_code(200);
								$data['status']="success";
								$data['description']="Idea deleted successfully";
								echo json_encode($data);
						}

				}
				else if(_f_is_admin())
				{
						$action=$this->idea->delete($seg3);
						if($action)
						{
								http_response_code(200);
								$data['status']="success";
								$data['description']="Idea deleted successfully";

								echo json_encode($data);
						}
				}
				else{
						http_response_code(401);
						$data['status']="failure";
						$data['description']="Unauthorized";
						echo json_encode($data);
				}
		}
		protected function idea_put(){
				$seg3=segx(3);
				$this->load->model('idea');
				$fetched_idea=$this->idea->read($seg3);
				//var_dump($fetched_idea);die(0);
				if (($fetched_idea['ownerid']==$this->session->socialid) || _f_is_admin()) {
						$this->load->model('idea');
						$par = array();
						$par['ownerid']=$fetched_idea['ownerid'];

						$putData="";
						parse_str(file_get_contents("php://input"),$putData);

						var_dump($putData);die(0);

						$par['id']=$fetched_idea['id'];
						$par += $putData;
						$action=$this->idea->update($par);
						if($action['status']==="success")
						{
								if(_f_is_loggedin()){
										$analytics['socialid']=$this->session->socialid;
										$analytics['event_category']="idea";
										$analytics['event_action']="update";
										$analytics['event_label']="updated";
										$analytics['event_value']=$fetched_idea['name'];
										$analytics['event_description']="Updated Idea ".$fetched_idea['name'];
										a_send($analytics);
								}
								http_response_code(200);
								$data['status']="success";
								$data['description']="Idea edited successfully";
								echo json_encode($data);
						}
						else{
								http_response_code(500);
								$data['status']="failure";
								$data['description']=$action['description'];
								echo json_encode($data);
						}
				}
		}
		protected function idea_top_patch()
		{
				if(_f_is_admin()){
						if(is_numeric(segx(3)))
						{
								$this->load->model('idea');
								$action=$this->idea->top(segx(3));
								if($action['status']=="success")
								{
										http_response_code(200);
										echo json_encode($action);
								}
								else{
										http_response_code(400);
										echo json_encode($action);
								}
						}
						else{
								http_response_code(500);
						}
				}
				else
				{
						http_response_code(401);
				}
		}
		protected function idea_untop_patch()
		{
				if(_f_is_admin()){
						if(is_numeric(segx(3)))
						{
								$this->load->model('idea');
								$action=$this->idea->untop(segx(3));
								if($action['status']=="success")
								{
										http_response_code(200);
										echo json_encode($action);
								}
								else{
										http_response_code(400);
										echo json_encode($action);
								}
						}
						else{
								http_response_code(500);
						}
				}
				else
				{
						http_response_code(401);
				}
		}
		protected function ideas_patch(){
				$seg3=segx(3);
				$this->load->model('idea');
				$fetched_idea=$this->idea->read($seg3);
				if ($fetched_idea['ownerid']==$this->session->socialid) {
						$this->load->model('idea');
						$par = array();
						$par['ownerid']=$this->session->socialid;

						$putData="";
						parse_str(file_get_contents("php://input"),$putData);

						$par['id']=$fetched_idea['id'];
						$par += $putData;
						$action=$this->idea->patch($par);
						if($action['status']==="success")
						{
								http_response_code(200);
								$data['status']="success";
								$data['description']="Idea edited successfully";
								echo json_encode($data);
						}
						else{
								http_response_code(500);
								$data['status']="failure";
								$data['description']=$action['description'];
								echo json_encode($data);
						}
				}
		}
		protected function idea_urlavailability_get()
		{
				if(isset($_GET['url'])){
						$this->load->model('idea');
						$ava=$this->idea->check_url_availability($_GET['url']);
						if($ava)
						{
								http_response_code(200);
								$data['status']="success";
								$data['available']="true";
								echo json_encode($data);
						}
						else{
								http_response_code(200);
								$data['status']="success";
								$data['available']="false";
								echo json_encode($data);
						}
				}
				else{
						http_response_code(400);
						$data['status']="failure";
						$data['description']="No URL set for check";
						echo json_encode($data);
				}
		}
		protected function user_profile_picture_post(){
				$this->load->model('User3');
				$this->load->model('supload');

				$socialid=$this->session->socialid;

				$file=$_FILES['file']['error'] > 0?null:$_FILES['file'];
				$uploadid="";
				//var_dump($file);die(0);

				if (!(isset($file["type"]) && $file!=0)) { 
						http_response_code(400);
						$ret_data['status']="error";
						$ret_data['description']="Invalid file";
						echo json_encode($ret_data);
						die(0);
				}

				$upload_params = array(
						'socialid'=>$socialid,
						'file'=>$file,
						'permissions'=>3,
						'accepted_types'=>"document_image",
						'input'=>'document'
				);

				$uploadac=$this->supload->create($upload_params);

				if($uploadac['status']!="error"){
						$uploaid=$uploadac['id'];
						$ins_array= array(
								'socialid'=>$socialid,
								'uploadid'=>$uploaid
						);
						$action=$this->User3->add_profile_picture($ins_array);
						if($action)
						{
								http_response_code(200);
								header('Content-Type: application/json');
								$data['status']="success";
								$data['description']="Profile Picture uploaded successfully";
								$data['url']=$this->config->item('upload_path').$uploadac['url'];
								$data['uploadid']=$uploaid;
								
								echo json_encode($data);
						}
				}
				else{
						http_response_code(400);
						$data['status']="error";
						$data['description']=$uploadac['description'];
						echo json_encode($data);
						die(0);
				}
		}
		protected function user_profile_picture_delete() {

				if(!_f_is_loggedin()) {
						die(0);
				}
				$this->load->model('User3');
				$this->load->model('Supload');
				$socialid=$this->session->socialid;
				$uploadid=$this->User3->get_profile_picture($socialid);
				$action=$this->User3->remove_profile_picture($socialid);
				if(!$action) {
						$data = array (
								'status'=>'failure',
								'description'=>'An error has occurred, please try again!'
						);
						http_response_code(500);
						header('Content-Type: application/json');
						echo json_encode($data);
						die(0);
				}


				$delete_upload_action=$this->Supload->delete($uploadid);


				$email=$this->User3->get_email($socialid);
				$gravatar_url=$this->User3->get_gravatar_url($email);


				http_response_code(200);
				$data = array (
						'status'=>'success',
						'gravatar_url'=>$gravatar_url
				);

				header('Content-Type: application/json');

				echo json_encode($data);
		}
		protected function idea_documents_post(){
				//Step 1: Verify the idea credentials with the current logged in user
				//Step 2: Get the upload
				//Step 3: Insert the document with the reference id of idea use the IDEA MODEL
				$socialid=$this->session->socialid;

				$ideaid=is_numeric($_POST['ideaid'])?$_POST['ideaid']:null;
				if(!is_null($ideaid))
				{
						$this->load->model('idea');
						$fetched_idea=$this->idea->read($ideaid);
						if ($fetched_idea['ownerid']==$this->session->socialid) {

								$file=$_FILES['document']['error'] > 0?null:$_FILES['document'];
								$uploadid="";

								if (isset($file["type"]) && $file!=0) {
										$this->load->model('supload');
										$upload_params = array(
												'socialid'=>$this->session->socialid,
												'file'=>$file,
												'permissions'=>1,
												'accepted_types'=>"document_image",
												'input'=>'document'
										);
										$uploadac=$this->supload->create($upload_params);
										if($uploadac['status']!="error"){
												$uploaid=$uploadac['id'];
												$ins_array= array(
														'ideaid'=>$fetched_idea['id'],
														'uploadid'=>$uploaid,
														'name'=>$_POST['document-name']
												);
												$action=$this->idea->insert_document($ins_array);
												if($action)
												{
														if(_f_is_loggedin()){
																$analytics['socialid']=$this->session->socialid;
																$analytics['event_category']="idea";
																$analytics['event_action']="document_upload";
																$analytics['event_label']="Uploaded";
																$analytics['event_value']=$fetched_idea['name'];
																$analytics['event_description']="Uploaded new document for ".$fetched_idea['name'];
																a_send($analytics);
														}
														http_response_code(200);
														$data['status']="success";
														$data['description']="Document uploaded successfully";
														echo json_encode($data);
												}
										}
										else{
												http_response_code(400);
												$data['status']="error";
												$data['description']=$uploadac['description'];
												echo json_encode($data);
												die(0);
										}


								}
						}
				}
		}
		protected function idea_document_delete(){
				$uploadid=segx(4);
				$action=$this->db->get_where('idea_uploads',array('uploadid'=>$uploadid));
				$row=$action->result_array();
				$ideaid=$row[0]['ideaid'];
				$id=$row[0]['id'];
				$this->load->model('idea');
				$fetched_idea=$this->idea->read($ideaid);
				if ($fetched_idea['ownerid']==$this->session->socialid) {
						$action=$this->idea->delete_document($id);
						if($action)
						{
								if(_f_is_loggedin()){
										$analytics['socialid']=$this->session->socialid;
										$analytics['event_category']="idea";
										$analytics['event_action']="document_delete";
										$analytics['event_label']="Deleted";
										$analytics['event_value']=$fetched_idea['name'];
										$analytics['event_description']="Deleted a document for ".$fetched_idea['name'];
										a_send($analytics);
								}
								http_response_code(200);
								$data['status']="success";
								$data['description']="Document deleted successfully";
								echo json_encode($data);
						}

				}
		}
		protected function idea_logo_get(){
				$ideaid=segx(3);
				if(is_numeric($ideaid))
				{
						$this->load->model('idea');
						$fetched_idea=$this->idea->read($ideaid);
						if(!($fetched_idea['visibility']==1) && !(_f_is_admin())  && !(_f_is_approved_investor()) && !($fetched_idea['ownerid']==$this->session->socialid))
						{
								http_response_code(401);
								echo "Unauthorized";
								die(0);
						}
						$query = "select url from uploads where id in (Select max(uploadid) from idea_logos where ideaid=$ideaid)";
						$action = $this->db->query($query);
						if ($action) {
								$row = $action->result_array();
								if ($row){
										$remoteImage = $this->config->item('upload_path') . $row[0]['url'];  
										header('Location: '.$remoteImage);
								} 
								else {

										header("Location: ".$this->config->item('upload_path') . "bulb.png");
								}


						} else {
								echo "fatal error: 887";
						}
				}
		}
		protected function idea_cover_get(){
				$ideaid=segx(3);
				if(is_numeric($ideaid))
				{
						$this->load->model('idea');
						$fetched_idea=$this->idea->read($ideaid);
						if(!($fetched_idea['visibility']==1) && !(_f_is_admin()) && !(_f_is_approved_investor()) && !($fetched_idea['ownerid']==$this->session->socialid))
						{
								http_response_code(400);
								echo "Unauthorized";
								die(0);
						}
						$query = "select url from uploads where id in (Select max(uploadid) from idea_cover_pics where ideaid=$ideaid)";
						$action = $this->db->query($query);
						if ($action) {
								$row = $action->result_array();
								if ($row) {
										$remoteImage = $this->config->item('upload_path') . $row[0]['url'];   
										header("Location: ".$remoteImage);
								}
								else {
										header("Location: ".$this->config->item('upload_path') . "image-2.jpg");
								}
						} else {
								echo "fatal error: 887";
						}
				}
		}
		protected function idea_follow_put(){
				if(_f_is_loggedin() && is_numeric(segx(3))){
						$this->load->model('idea');
						$ideaid=segx(3);
						$fetched_idea=$this->idea->read($ideaid);

						$action=$this->idea->follow($ideaid,$this->session->socialid);
						if($action['status']=="success")
						{
								if(_f_is_loggedin()){
										$analytics['socialid']=$this->session->socialid;
										$analytics['event_category']="idea";
										$analytics['event_action']="follow";
										$analytics['event_label']="followed";
										$analytics['event_value']=$fetched_idea['name'];
										$analytics['event_description']="Followed Idea ".$fetched_idea['name'];
										a_send($analytics);
								}
								$data['status']="success";
								$data['description']="Idea followed successfully";
								$followers_query = "Select name,followers from view_ideas where id=$ideaid";
								$follower_action = $this->db->query($followers_query);
								if ($follower_action) {
										$row = $follower_action->result_array();
										$data['followers'] = $row[0]['followers'];

								}
								$this->output_data(200,$data);
						}
						else{

								$data['status']="failure";
								$data['description']=$action['description'];
								$this->output_data(400,$data);

						}
				}
				else{
						$data['status']="failure";
						$data['description']="You must login to follow idea";
						$this->output_data(401,$data);

				}
		}
		protected function idea_unfollow_put(){
				if(_f_is_loggedin() && is_numeric(segx(3))){
						$this->load->model('idea');
						$ideaid=segx(3);
						$fetched_idea=$this->idea->read($ideaid);
						$action=$this->idea->unfollow($ideaid,$this->session->socialid);
						if($action)
						{
								http_response_code(200);

								if(_f_is_loggedin()){
										$analytics['socialid']=$this->session->socialid;
										$analytics['event_category']="idea";
										$analytics['event_action']="unfollow";
										$analytics['event_label']="unfollowed";
										$analytics['event_value']=$fetched_idea['name'];
										$analytics['event_description']="Unfollowed Idea ".$fetched_idea['name'];
										a_send($analytics);
								}
								$data['status']="success";
								$data['description']="Idea unfollowed successfully";
								$followers_query = "Select name,followers from view_ideas where id=$ideaid";
								$follower_action = $this->db->query($followers_query);
								if ($follower_action) {
										$row = $follower_action->result_array();
										$data['followers'] = $row[0]['followers'];

								}
								header('Content-Type: application/json');
								echo json_encode($data);
						}
						else{
								http_response_code(500);
						}
				}
				else{
						http_response_code(401);
				}
		}
		protected function top_idea_put(){
				if(_f_is_admin())
				{
						// Use of this function is to edit the top ideas that are visible on the homepage
						// Extract the put data
						$putData="";
						parse_str(file_get_contents("php://input"),$putData);

						$this->db->where('rank',$putData['rank']);
						$this->db->set('ideaid',$putData['ideaid']);
						$this->db->set('rank',$putData['rank']);
						$action=$this->db->replace('top_ideas');
						if($action)
						{
								$data['status']="success";
								$data['description']="Rank of Idea ".$putData['ideaid']." changed to ".$putData['rank'];
								$this->output_data(200,$data);
						}
						else
						{
								$data['status']="failure";
								$data['description']="Query failed";
								$this->output_data(500,$data);
						}

				}
				else
				{
						$data['status']="failure";
						$data['description']="unauthorized";
						$this->output_data(400,$data);
				}
		}
		protected function top_idea_drag_put(){
				if(_f_is_admin())
				{
						// Use of this function is to edit the top ideas that are visible on the homepage
						// Extract the put data
						$putData="";
						parse_str(file_get_contents("php://input"),$putData);
						$this->load->model('idea');
						$action=$this->idea->drag($putData['ideaid'],$putData['rank']);

						if($action)
						{
								$data['status']="success";
								$data['description']="Rank of Idea ".$putData['ideaid']." changed to ".$putData['rank'];
								$this->output_data(200,$data);
						}
						else
						{
								$data['status']="failure";
								$data['description']="Query failed";
								$this->output_data(500,$data);
						}

				}
				else
				{
						$data['status']="failure";
						$data['description']="unauthorized";
						$this->output_data(400,$data);
				}
		}
		protected function events_get() {
			$params = array();

			if(array_key_exists('limit',$_GET)) {
				$params['limit']=$_GET['limit'];
			}

			if(array_key_exists('order_by_id',$_GET)) {
				$params['order_by_id']=$_GET['order_by_id'];
			}

			if(array_key_exists('order_by_date_start',$_GET)) {
				$params['order_by_date_start']=$_GET['order_by_date_start'];
			}

			$all_events=$this->Event->get2($params);
			$this->output_data(200,$all_events);
		}
		protected function events_post(){
				$par = array();
				if(_f_is_admin()){
						$par['ownerid']=1;
				}
				else{
						$par['ownerid']=$this->session->socialid;
				}
				$this->load->model('event');
				$par += $_POST;

				// Get the logo file from the $_FILES array received
				$logo_file=$_FILES['logofile']['error']>0?0:$_FILES['logofile'];

				// Get the cover file from the $_FILES array received
				$cover_file=$_FILES['coverfile']['error']>0?0:$_FILES['coverfile'];

				// Proceed to upload logo with the Supload model and retrieve upload id ,
				// use upload id and associate it with appropriate entity
				if (isset($logo_file["type"]) && $logo_file!=0) {

						$this->load->model('supload');
						$upload_params = array(
								'socialid' => $par['ownerid'],
								'file' => $logo_file,
								'permissions' => 3,
								'accepted_types' => "image"
						);
						$logo_uploadid = $this->supload->create($upload_params);
						if($logo_uploadid['status']!="error")
						{
								$par['logo_uploadid']=$logo_uploadid['id'];
						}
				}

				// Proceed to upload cover file with the Supload model and retrieve upload id ,
				// use upload id and associate it with appropriate entity
				if (isset($cover_file["type"]) && $cover_file!=0) {
						$upload_params = array(
								'socialid' => $par['ownerid'],
								'file' => $cover_file,
								'permissions' => 3,
								'accepted_types' => "image"
						);
						$this->load->model('supload');
						$cover_uploadid = $this->supload->create($upload_params);
						if($cover_uploadid['status']!="error") $par['cover_uploadid']=$cover_uploadid['id'];
						else
						{
								echo "Invalid image;";
								echo $cover_uploadid['description'];
								echo $logo_uploadid['status'];
								die(0);
						}
				}

				// We have all the details and logo and cover file as well,
				// now create the event
				$action=$this->event->create($par);

				if(isset($action['status'])?$action['status']=="success":0)
				{
						http_response_code(201);
						$data['status']="success";
						$data['description']="Event Created";
						$data['ideaid']=$action['eventid'];
						echo json_encode($data);
				}
				elseif(isset($action['status'])?$action['status']=="error":1)
				{


						$data['status']="error";

						if($action['description']=="conflict")
						{
								http_response_code(409);
								$slug=$_POST['slug'];
								$data['description']="Event slug $slug already taken";
						}
						else
						{
								http_response_code(400);
								$data['description']=$action['description'];
						}
						echo json_encode($data);
				}
				else
				{
						http_response_code(400);
						$data['status']="error";
						$data['description']=$action['description'];
						echo json_encode($data);
				}
		}
		protected function event_put(){
				$par = array();
				$this->load->model('event');
				$putData="";
				parse_str(file_get_contents("php://input"),$putData);
				$par += $putData;

				$action=$this->event->update($par);

				if(isset($action['status'])?$action['status']=="success":0)
				{

						$data['status']="success";
						$data['description']="Event Updated";
						$this->output_data(200,$data);
				}
				elseif(isset($action['status'])?$action['status']=="error":1)
				{
						$data['status']="error";

						if($action['description']=="conflict")
						{

								$slug=$_POST['slug'];
								$data['description']="Event slug $slug already taken";
								$this->output_data(409,$data);
						}
						else
						{


								$data['description']=$action['description'];
								$this->output_data(400,$data);
						}

				}
				else
				{
						$data['status']="error";
						$data['description']=$action['description'];
						$this->output_data(400,$data);
				}
		}
		protected function event_get(){
				$eventid=is_numeric(segx(3))?segx(3):0;
				if($eventid){
						$this->load->model('event');
						$fetched_event=$this->event->read($eventid);
						if(($fetched_event['admin_approved']==1 || _f_is_admin() || $fetched_event['ownerid']==$this->sesion->socialid) && $fetched_event!=0)
						{
								$data['id']=$fetched_event['id'];
								$data['ownerid']=$fetched_event['ownerid'];
								$data['name']=$fetched_event['name'];
								$data['slug']=$fetched_event['slug'];
								$data['description_short']=$fetched_event['description_short'];
								$data['description_long']=$fetched_event['description_long'];
								$data['date_start']=$fetched_event['date_start'];
								$data['date_end']=$fetched_event['date_end'];
								$data['views']=$fetched_event['views'];
								$data['admin_approved']=$fetched_event['admin_approved'];
								http_response_code(200);
								header('Content-Type: application/json');
								echo json_encode($data);
						}
						else{
								http_response_code(404);
								header('Content-Type: application/json');
								$data['status']="failure";
								$data['action']="not found";
								$data['description']="the event not found";
								echo json_encode($data);
						}
				}
				else{
						http_response_code(404);
						header('Content-Type: application/json');
						$data['status']="failure";
						$data['action']="not found";
						$data['description']="the event not found";
						echo json_encode($data);
				}
		}
		protected function event_logo_get(){
				$eventid=segx(3);
				if(is_numeric($eventid))
				{
						$this->load->model('event');
						$query = "select url from uploads where id in (Select max(uploadid) from event_logos where eventid=$eventid)";
						$action = $this->db->query($query);
						if ($action) {
								$row = $action->result_array();
								if ($row) {
										$remoteImage = $this->config->item('upload_path') . $row[0]['url'];

										header('Location: '. $remoteImage);
								}   
								else {
										header("Location: ".$this->config->item('upload_path') . "bulb.png");
								}


						} else {
								echo "fatal error: 887";
						}
				}
		}
		protected function event_cover_get(){
				$eventid=segx(3);
				if(is_numeric($eventid))
				{
						$this->load->model('event');
						$query = "select url from uploads where id in (Select max(uploadid) from event_cover_pics where eventid=$eventid)";
						$action = $this->db->query($query);
						if ($action) {
								$row = $action->result_array();
								if ($row) {
										$remoteImage = $this->config->item('upload_path') . $row[0]['url'];

										header('Location: '. $remoteImage);
								}   
								else {

								}
						} else {
								echo "fatal error: 887";
						}
				}
		}
		protected function event_logo_post(){
				if(is_numeric(segx(3)))
				{
						$eventid=segx(3);
						$this->load->model('event');
						$fetched_event=$this->event->read($eventid);
						$par = array();
						$par += $_POST;
						//Check for socialid first
						if(isset($this->session->socialid)) {

								$par['ownerid'] = $this->session->socialid;
								if($this->session->socialid==$fetched_event['ownerid'])
								{}
								else
								{
										http_response_code(401);
										$data['status'] = "error";
										$data['description'] = "unauthorized";
										echo json_encode($data);
										die(0);
								}
						}
						else if(_f_is_admin())
						{
								$par['ownerid']=$_POST['socialid'];
						}
						else{
								http_response_code(403);
								$data['status']="failure";
								$data['description']="You are not logged in";
								echo json_encode($data);
								die(0);
						}


						$par['logo_uploadid']="";

						// Get the logo file from the $_FILES array received
						$logo_file=$_FILES['logofile']['error'] > 0?null:$_FILES['logofile'];

						if (isset($logo_file["type"]) && $logo_file!=0) {

								$this->load->model('supload');
								$upload_params = array(
										'socialid' => $this->session->socialid,
										'file' => $logo_file,
										'permissions' => 3,
										'accepted_types' => "image"
								);
								$logo_uploadid = $this->supload->create($upload_params);
								if($logo_uploadid['status']!="error")
								{
										$par['logo_uploadid']=$logo_uploadid['id'];
								}
						}

						$tem_arr_cover=array('eventid'=>$eventid, 'uploadid'=>$par['logo_uploadid']);
						$action=$this->event->add_logo($tem_arr_cover);
						if($action['status']=="success")
						{
								http_response_code(201);
								$data['status']="success";
								$data['description']="Logo Uploaded";
								$data['eventid']=$eventid;
								echo json_encode($data);
						}
						else{
								http_response_code(400);
								$data['status']="error";
								$data['description']=$action['description'];
								echo json_encode($data);
						}
				}
				else{
						http_response_code(400);
						$data['status']="error";
						$data['description']=$action['description'];
						echo json_encode($data);
				}
		}
		protected function event_cover_post(){
				if(is_numeric(segx(3))) {
						$eventid=segx(3);
						$par = array();
						$par += $_POST;
						$this->load->model('event');
						$fetched_event=$this->event->read($eventid);
						//Check for socialid first
						if (isset($this->session->socialid)) {
								$par['ownerid'] = $this->session->socialid;
								if($this->session->socialid==$fetched_event['ownerid'])
								{}
								else{
										http_response_code(401);
										$data['status'] = "error";
										$data['description'] = "unauthorized";
										echo json_encode($data);
										die(0);
								}
						} else if (_f_is_admin()) {
								$par['ownerid'] = $_POST['socialid'];
						} else {
								http_response_code(403);
								$data['status'] = "failure";
								$data['description'] = "You are not logged in";
								echo json_encode($data);
								die(0);
						}


						$par['cover_uploadid'] = "";

						// Get the cover file from the $_FILES array received
						$cover_file = $_FILES['coverfile']['error'] > 0 ? null : $_FILES['coverfile'];

						if (isset($cover_file["type"]) && $cover_file != 0) {

								$this->load->model('supload');
								$upload_params = array(
										'socialid' => $this->session->socialid,
										'file' => $cover_file,
										'permissions' => 3,
										'accepted_types' => "image"
								);
								$cover_uploadid = $this->supload->create($upload_params);
								if ($cover_uploadid['status'] != "error") {
										$par['cover_uploadid'] = $cover_uploadid['id'];
								}
						}

						$tem_arr_cover = array('eventid' => $eventid, 'uploadid' => $par['cover_uploadid']);
						$action = $this->event->add_cover($tem_arr_cover);
						if ($action['status'] == "success") {
								http_response_code(201);
								$data['status'] = "success";
								$data['description'] = "Cover Uploaded";
								$data['eventid'] = $eventid;
								echo json_encode($data);
						} else {
								http_response_code(400);
								$data['status'] = "error";
								$data['description'] = $action['description'];
								echo json_encode($data);
						}
				}
				else{
						http_response_code(401);
						$data['status'] = "error";
						$data['description'] = "unauthorized";
						echo json_encode($data);
				}
		}
		protected function event_gallery_get(){
				$event_id=segx(3);
				$this->load->model('event');
				$list=$this->event->get_gallery($event_id);
				if($list)
				{
						http_response_code(200);
						header('Content-Type: application/json');
						echo json_encode($list,JSON_UNESCAPED_SLASHES);
				}
				else{
						http_response_code(500);
				}
		}
		protected function event_gallery_post(){
				$event_id=segx(3);
				$this->load->model('event');

				$fetched_event=$this->event->read($event_id);
				if(!$fetched_event) die(0);
				if(($fetched_event['ownerid']!=$this->session->socialid))
				{
						if(!_f_is_admin()) die(0);
				}	
				// Belongs to the owner or is he is admin then proceed

				// retrieve the array of files for the gallery

				$ultimate_status=array();
				//var_dump($_FILES);die(0);
				foreach($_FILES['images']['name'] as $key=>$_file)
				{
						$par['uploadid']="";

						$gallery_file['name']=$_FILES['images']['name'][$key];
						$gallery_file['size']=$_FILES['images']['size'][$key];
						$gallery_file['tmp_name']=$_FILES['images']['tmp_name'][$key];
						$gallery_file['type']=$_FILES['images']['type'][$key];


						if (isset($gallery_file["type"]) && $gallery_file!=0) {

								$this->load->model('supload');
								$upload_params = array(
										'socialid' => $fetched_event['ownerid'],
										'file' => $gallery_file,
										'permissions' => 3,
										'accepted_types' => "image"
								);
								$uploadid = $this->supload->create($upload_params);
								if($uploadid['status']!="error")
								{
										$par['uploadid']=$uploadid['id'];
								}
						}

						$tem_arr_cover=array('eventid'=>$event_id, 'uploadid'=>$par['uploadid']);
						$action=$this->event->add_gallery_pic($tem_arr_cover);
						if($action['status']=="success")
						{
								$file_name=$gallery_file['name'];
								$ultimate_status[$file_name]="success";
						}
						else{

								$gallery_file['name'];
								$ultimate_status[$file_name]="failed";

						}
				}

				http_response_code(200);
				echo json_encode($ultimate_status);
		}
		public function generateUpToDateMimeArray($url){
				$s=array();
				foreach(@explode("\n",@file_get_contents($url))as $x)
						if(isset($x[0])&&$x[0]!=='#'&&preg_match_all('#([^\s]+)#',$x,$out)&&isset($out[1])&&($c=count($out[1]))>1)
								for($i=1;$i<$c;$i++)
										$s[]='&nbsp;&nbsp;&nbsp;\''.$out[1][$i].'\' => \''.$out[1][0].'\'';
				return @sort($s)?'$mime_types = array(<br />'.implode($s,',<br />').'<br />);':false;
		}

		protected function user_profile_picture_get(){
				if(is_numeric(segx(3))){
						$socialid=segx(3);
						$this->load->model('User3');
						$this->load->model('Supload');

						$uploadid=$this->User3->get_profile_picture($socialid);
						if ($uploadid) {
								$upload=$this->Supload->get_upload($uploaid);
								if(!upload) {
										die(0);
								}
								$remoteImageUrl=$this->config->item('upload_path').$upload['url'];
								header("Location: $remoteImageUrl");
						} else {
								$user=$this->User3->read($socialid);
								if(!$user) {
										die(0);
								}
								$hashe=hash("md5",$user['email']);
								$remoteImage="https://www.gravatar.com/avatar/".$hashe."?s=$dimension&d=identicon";
								// $imginfo['mime'] = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
								// $header_content_type = $imginfo["mime"];
								header("Location: $remoteImage");
						}

				}
		}
		protected function event_approve_patch(){
				if(_f_is_admin())
				{
						$eventid=is_numeric(segx(3))?segx(3):0;
						if($eventid)
						{
								$this->load->model('event');
								$action=$this->event->approve($eventid);
								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="approved";
										$data['description']="Event Approved successfully";
										echo json_encode($data);
								}
						}
				}
				else{
						http_response_code(403);
						header('Content-Type: application/json');
						$data['status']="failure";
						$data['action']="forbidden";
						$data['description']="You are not authorized to access this resource";
						echo json_encode($data);
				}
		}
		protected function event_reject_patch(){
				if(_f_is_admin())
				{
						$eventid=is_numeric(segx(3))?segx(3):0;
						if($eventid)
						{
								$this->load->model('event');
								$action=$this->event->reject($eventid);
								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="approved";
										$data['description']="Event Approved successfully";
										echo json_encode($data);
								}
						}
				}
				else{
						http_response_code(403);
						header('Content-Type: application/json');
						$data['status']="failure";
						$data['action']="forbidden";
						$data['description']="You are not authorized to access this resource";
						echo json_encode($data);
				}
		}
		protected function event_delete(){
				if(_f_is_admin())
				{
						$eventid=is_numeric(segx(3))?segx(3):0;
						if($eventid)
						{
								$this->load->model('event');
								$action=$this->event->delete($eventid);
								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="deleted";
										$data['description']="Event deleted successfully";
										echo json_encode($data);
								}
						}
				}
				else{
						http_response_code(403);
						header('Content-Type: application/json');
						$data['status']="failure";
						$data['action']="forbidden";
						$data['description']="You are not authorized to access this resource";
						echo json_encode($data);
				}
		}
		protected function testimonials_post(){
			if(!_f_is_admin()){
				$this->output_data(401);
				die(0);
			}
			
			if(!array_key_exists('profile_file',$_FILES)){
				$ret = array('status'=>'failure','description'=>"Profile Picture is required");
				$this->output_data(400,$ret);
				die(0);
			}

			$required_properties = array(
				'first_name'=>'First Name',
				'last_name'=>'Last Name',
				'company'=>'Company',
				'position'=>'Position',
				'description'=>'Description',
				'website'=>'Website Link'
			);

			// check post data for required properties
			foreach($required_properties as $key=>$property) {
				if(!array_key_exists($key,$_POST)){
					
					
					$ret = array('status'=>'failure','description'=>"$property is required");
					$this->output_data(400,$ret);
					die(0);
				} else if($_POST[$key]=="") {
					$ret = array('status'=>'failure','description'=>"$property is required");
					$this->output_data(400,$ret);
					die(0);
					
				}
			}

			$upload_params = array(
				'socialid'=>$this->session->socialid,
				'permissions'=>3,
				'file'=>$_FILES['profile_file'],
			);

			$upload_status=$this->Supload->create($upload_params);

			if($upload_status['status']!="success") {
				$ret=array('status'=>'failure','description'=>$upload_status['description']);
				$this->output_data(500,$ret);
				die(0);
			}

			$upload_id = $upload_status['id'];
			$testimonial_data = array(
				'first_name'=>$_POST['first_name'],
				'last_name'=>$_POST['last_name'],
				'company'=>$_POST['company'],
				'position'=>$_POST['position'],
				'description'=>$_POST['description'],
				'profile_picture'=>$upload_id,
				'website'=>$_POST['website']
			);

			$this->load->model('Testimonial');
			$this->Testimonial->create($testimonial_data);

			$ret = array('status'=>'success');
			$this->output_data(201,$ret);
			
		}
		protected function testimonials_get() {
			$this->load->model('Testimonial');
			$testimonials=$this->Testimonial->read();
			foreach($testimonials as &$t) {
				$t['profile_picture_url']=$this->Supload->get_complete_url($t['profile_picture']);

			}
			$this->output_data(200,array('status'=>'success','data'=>$testimonials));
			
		}
		protected function testimonial_activate_patch(){
				if(_f_is_admin())
				{
						if(is_numeric(segx(3))){
								$testimonial_id=segx(3);
								$this->load->model('testimonial');
								$action=$this->testimonial->activate($testimonial_id);

								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="activated";
										$data['description']="Testominial activated successfully";
										echo json_encode($data);
								}

						}

				}
				else{

				}
		}
		protected function testimonial_deactivate_patch(){
				if(_f_is_admin())
				{
						if(is_numeric(segx(3))){
								$testimonial_id=segx(3);
								$this->load->model('testimonial');
								$action=$this->testimonial->deactivate($testimonial_id);

								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="deactivated";
										$data['description']="Testominial deactivated successfully";
										echo json_encode($data);
								}

						}

				}
				else{

				}
		} // Activated testimonial
		protected function testimonial_delete(){
				if(_f_is_admin())
				{
						if(is_numeric(segx(3)))
						{
								$testimonial_id=segx(3);
								$this->load->model('testimonial');
								$action=$this->testimonial->delete($testimonial_id);
								
								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="deleted";
										$data['description']="Testominial deleted successfully";
										echo json_encode($data);
								}

						}
				}
		} // Delete a testimonial from table
		protected function message_approve_patch(){
				if(_f_is_admin())
				{
						$message_id=segx(3);
						if(is_numeric($message_id))
						{
								$this->load->model('message');
								$this->load->model('user');
								$action=$this->message->approve($message_id);
								if($action)
								{
										// notify the user that message is approved
										// get the message senders socialid
										$message=$this->message->get($message_id);
										$this->notification->socialid=$message['sender'];

										$this->user->socialid=$message['receiver'];
										$receiver=$this->user->get_public_visible_data();

										$this->notification->name="Your message is delivered to ".$receiver[0]['first_name'];
										$this->notification->description="<p>Your <a href='/sentbox/#$message_id'>message</a> for <a href='/users/".$receiver[0]['socialid']."'>".$receiver[0]['first_name']."</a> has been approved by Admin.";
										if($this->notification->new()) {}

										// notify the receiver that he has got a new message


										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="approved";
										$data['description']="Message has been approved";
										echo json_encode($data);
								}
								else{
										http_response_code(500);
										header('Content-Type: application/json');
										$data['status']="failure";
										$data['action']="none";
										$data['description']="Server Error";
										echo json_encode($data);
								}

						}
						else{
								http_response_code(400);
								header('Content-Type: application/json');
								$data['status']="failure";
								$data['action']="none";
								$data['description']="Bad request";
								echo json_encode($data);
						}
				}
				else{
						http_response_code(400);
						header('Content-Type: application/json');
						$data['status']="failure";
						$data['action']="none";
						$data['description']="Unauthorized";
						echo json_encode($data);
				}
		} // Message once approved will be visible to the receiver
		protected function message_reject_patch(){
				if(_f_is_admin())
				{
						$message_id=segx(3);
						if(is_numeric($message_id))
						{
								$this->load->model('message');
								$action=$this->message->reject($message_id);
								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="rejected";
										$data['description']="Message has been rejected";
										echo json_encode($data);
								}
								else{
										http_response_code(500);
										header('Content-Type: application/json');
										$data['status']="failure";
										$data['action']="none";
										$data['description']="Server Error";
										echo json_encode($data);
								}

						}
						else{
								http_response_code(400);
								header('Content-Type: application/json');
								$data['status']="failure";
								$data['action']="none";
								$data['description']="Bad request";
								echo json_encode($data);
						}
				}
				else{
						http_response_code(400);
						header('Content-Type: application/json');
						$data['status']="failure";
						$data['action']="none";
						$data['description']="Unauthorized";
						echo json_encode($data);
				}
		} // Rejected messages will not be visible to the receiver but will be visible to the sender in sentbox
		protected function users_get(){
				if(!_f_is_admin()) die();
				$get_array=array();
				
				if(isset($_GET['filter_location'])) {
					$get_array['location']=$_GET['filter_location'];
				}
				
				if(isset($_GET['filter_sector'])) {
					$get_array['sector']=$_GET['filter_sector'];
				}

				if(isset($_GET['filter_date_start']) && isset($_GET['filter_date_end'])) {				
					$get_array['between_timestamp']=array($_GET['filter_date_start'],$_GET['filter_date_end']);
				}

				if(isset($_GET['filter_user_type'])) {
					$get_array['user_type']=$_GET['filter_user_type'];
				}
				
				$this->load->model('user3');
				$users=$this->user3->scan($get_array);
				http_response_code(200);
				header('Content-Type: application/json');
				echo json_encode($users);

		}

		protected function users_inactive_get() {
			if(!_f_is_admin()) {
				$this->output_data(401);
				die(0);
			}

			$this->load->model('Activity');
			$action=$this->Activity->get_inactive_users();
			$socialids=array();

			foreach($action['inactive_users'] as $inactive_user){
				array_push($socialids,$inactive_user['socialid']);	
			}
			$this->output_data(200,array('status'=>'success','data'=>$socialids));

		}
		
		protected function user_put(){

				$this->load->model('User3');
				$data = array();

				$user=$this->User3->read($this->session->socialid);
				$putData="";
				parse_str(file_get_contents("php://input"),$putData);
				$data += $putData;
				$data['socialid']=$this->session->socialid;

				if(array_key_exists('username', $data)) {
						if($data['username']==$this->session->socialid || $data['username']==$user['username'])
						{
								unset($data['username']);
						}
						else {
								if(!$this->User3->is_username_available($data['username'])) {
										http_response_code(400);
										$ret_data['status']="failure";
										$ret_data['description']='Sorry, Username already taken.';
										echo json_encode($ret_data);
										exit(0);
								}
						}
				}


				if(array_key_exists('email', $data) && $data['email']!="") {
						$user_present=$this->User3->is_user_present($data['email']);
						if($user_present) {
								if($user_present['socialid']==$this->session->socialid){
										unset($data['email']);
								} else {
										http_response_code(400);
										$ret_data['status']="failure";
										$ret_data['description']='Sorry, Email already taken.';
										echo json_encode($ret_data);
										exit(0);
								}

						}
				}

				if(array_key_exists('mobile', $data)) {
						unset($data['mobile']);
				}

				$action=$this->User3->update($data);
				if($action) {
						$ret_data['status']="success";
						$ret_data['description']="data updated successfully";


						if($user['user_type']=="ideator") {
								$score=$this->User3->get_completeness_score($user['socialid']);
								$ret_data['profile_completeness']=$score;

						}

						$this->output_data(200,$ret_data);    
				}
				else {
						http_response_code(500);
						$ret_data['status']="failure";
						$ret_data['description']='There was an unknown error in updating User, please try again.';
						echo json_encode($ret_data);    
				}
		}
		protected function user4_put() {
				$this->load->model('User4');
				$data = array();

				$user=$this->User4->get('socialid',$this->session->socialid);
				$user=$user[0];

				$putData="";
				parse_str(file_get_contents("php://input"),$putData);
				$data += $putData;

				$data['socialid']=$this->session->socialid;

				$is_valid_data=$this->User4->check_update_data($data);

				if(!$is_valid_data) {
						http_response_code(400);
						$ret_data['status']="failure";
						$ret_data['description']=$is_valid_data;
						echo json_encode($ret_data);
						die(0);    
				}

				if(array_key_exists('username', $data)) {
						if($data['username']==$this->session->socialid || $data['username']==$user['username'])
						{
								unset($data['username']);
						}
						else {
								if(!$this->User3->is_username_available($data['username'])) {
										http_response_code(400);
										$ret_data['status']="failure";
										$ret_data['description']='Sorry, Username already taken.';
										echo json_encode($ret_data);
										exit(0);
								}
						}
				}

				$action=$this->User4->update($this->session->socialid,$data);
				if(!$action) {
						http_response_code(500);
						$ret_data['status']="failure";
						$ret_data['description']='There was an unknown error in updating User, please try again.';
						echo json_encode($ret_data);   
						die(0);
				}

				$ret_data['status']="success";
				$ret_data['description']="data updated successfully";
				$score_user=$this->User4->get_profile_completeness($user['socialid']);
				$score2=0;


				if($user['user_type']=="ideator") {
						$this->load->model('Ideator4');
						$action2=$this->Ideator4->update($user['socialid'],$data);
						if(!$action2) {
								var_dump($action2);dir(0);
								http_response_code(500);
								$ret_data['status']="failure";
								$ret_data['description']='There was an unknown error in updating User, please try again.';
								echo json_encode($ret_data);   
								die(0);
						}

						$score2=$this->Ideator4->get_profile_completeness($user['socialid']);
				}
				else if($user['user_type']=="service_provider") {
						$this->load->model('Service_provider4');
						$action2=$this->Service_provider4->update($user['socialid'],$data);
						if(!$action2) {
								http_response_code(500);
								$ret_data['status']="failure";
								$ret_data['description']='There was an unknown error in updating User, please try again.';
								echo json_encode($ret_data);   
								die(0);
						}
						$score2=$this->Service_provider4->get_profile_completeness($user['socialid']);
				}
				else if($user['user_type']=="investor") {
						$this->load->model('Investor4');
						$action2=$this->Investor4->update($user['socialid'],$data);
						if(!$action2) {
								http_response_code(500);
								$ret_data['status']="failure";
								$ret_data['description']='There was an unknown error in updating User, please try again.';
								echo json_encode($ret_data);   
								die(0);
						}
						$score2=$this->Investor4->get_profile_completeness($user['socialid']);
				}

				$completeness=round(($score_user+$score2)/2);
				$ret_data['profile_completeness']=$completeness;

				$this->output_data(200,$ret_data);    

		}
		protected function user_followed_ideas_get() {
			$socialid=$this->session->socialid;
			$followed_ideas=$this->User3->get_followed_ideas($socialid);
			$followed_ideas_ids=array();
			foreach($followed_ideas as $idea) {
				array_push($followed_ideas_ids,$idea['ideaid']);
			}
			$this->output_data(200,array('status'=>'success','data'=>$followed_ideas_ids));
			

		}
		protected function user_block_patch(){
				if(_f_is_admin()){
						if(is_numeric(segx(3)))
						{
								$user_socialid=segx(3);
								$this->load->model('user');
								$action=$this->user->block($user_socialid);
								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="blocked";
										$data['description']="User has been blocked successfully";
										echo json_encode($data);
								}
								else{
										http_response_code(500);
										header('Content-Type: application/json');
										$data['status']="failure";
										$data['action']="error";
										$data['description']="Internal Server Error";
										echo json_encode($data);

								}

						}
						else{
								http_response_code(400);
								header('Content-Type: application/json');
								$data['status']="failure";
								$data['action']="error";
								$data['description']="Bad request";
								echo json_encode($data);
						}
				}
				else{
						http_response_code(401);
						header('Content-Type: application/json');
						$data['status']="failure";
						$data['action']="error";
						$data['description']="Unauthorized";
						echo json_encode($data);
				}
		} // Blocked users will not be able to login into website, thier ideas and their profile will be visible on website
		protected function user_verify_patch(){
				if(_f_is_admin()){
						if(is_numeric(segx(3)))
						{
								$user_socialid=segx(3);
								$this->load->model('user');
								$action=$this->user->verify($user_socialid);
								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="blocked";
										$data['description']="User has been blocked successfully";
										echo json_encode($data);
								}
								else{
										http_response_code(500);
										header('Content-Type: application/json');
										$data['status']="failure";
										$data['action']="error";
										$data['description']="Internal Server Error";
										echo json_encode($data);

								}

						}
						else{
								http_response_code(400);
								header('Content-Type: application/json');
								$data['status']="failure";
								$data['action']="error";
								$data['description']="Bad request";
								echo json_encode($data);
						}
				}
				else{
						http_response_code(401);
						header('Content-Type: application/json');
						$data['status']="failure";
						$data['action']="error";
						$data['description']="Unauthorized";
						echo json_encode($data);
				}
		} // Give a verified badge to the user
		protected function user_unblock_patch(){
				if(_f_is_admin()){
						if(is_numeric(segx(3)))
						{
								$user_socialid=segx(3);
								$this->load->model('user');
								$action=$this->user->unblock($user_socialid);
								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="unblocked";
										$data['description']="User has been unblocked successfully";
										echo json_encode($data);
								}
								else{
										http_response_code(500);
										header('Content-Type: application/json');
										$data['status']="failure";
										$data['action']="error";
										$data['description']="Internal Server Error";
										echo json_encode($data);

								}

						}
						else{
								http_response_code(400);
								header('Content-Type: application/json');
								$data['status']="failure";
								$data['action']="error";
								$data['description']="Bad request";
								echo json_encode($data);
						}
				}
				else{
						http_response_code(401);
						header('Content-Type: application/json');
						$data['status']="failure";
						$data['action']="error";
						$data['description']="Unauthorized";
						echo json_encode($data);
				}
		}
		protected function user_approve_patch(){
				if(_f_is_admin()){
						if(is_numeric(segx(3)))
						{
								$user_socialid=segx(3);
								$this->load->model('user');
								$action=$this->user->approve($user_socialid);
								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="approved";
										$data['description']="User has been approved successfully";
										echo json_encode($data);
								}
								else{
										http_response_code(500);
										header('Content-Type: application/json');
										$data['status']="failure";
										$data['action']="error";
										$data['description']="Internal Server Error";
										echo json_encode($data);

								}

						}
						else{
								http_response_code(400);
								header('Content-Type: application/json');
								$data['status']="failure";
								$data['action']="error";
								$data['description']="Bad request";
								echo json_encode($data);
						}
				}
				else{
						http_response_code(401);
						header('Content-Type: application/json');
						$data['status']="failure";
						$data['action']="error";
						$data['description']="Unauthorized";
						echo json_encode($data);
				}
		} // Approve and reject methods are for investors only, all others are approved by default
		protected function user_reject_patch(){
				if(_f_is_admin()){
						if(is_numeric(segx(3)))
						{
								$user_socialid=segx(3);
								$this->load->model('user');
								$action=$this->user->reject($user_socialid);
								if($action)
								{
										http_response_code(200);
										header('Content-Type: application/json');
										$data['status']="success";
										$data['action']="rejected";
										$data['description']="User has been rejected successfully";
										echo json_encode($data);
								}
								else{
										http_response_code(500);
										header('Content-Type: application/json');
										$data['status']="failure";
										$data['action']="error";
										$data['description']="Internal Server Error";
										echo json_encode($data);

								}

						}
						else{
								http_response_code(400);
								header('Content-Type: application/json');
								$data['status']="failure";
								$data['action']="error";
								$data['description']="Bad request";
								echo json_encode($data);
						}
				}
				else{
						http_response_code(401);
						header('Content-Type: application/json');
						$data['status']="failure";
						$data['action']="error";
						$data['description']="Unauthorized";
						echo json_encode($data);
				}
		} // Approve and reject methods are for investors only, all others are approved by default
		protected function user_delete(){
				$user_socialid=segx(3);
				$this->load->model('user');
				$action=$this->user->delete($user_socialid);
				if($action==200)
				{
						$data['status']="success";
						$data['action']="deleted";
						$data['description']="User has been deleted successfully";
						$this->output_data(200,$data);
				}
				else if($action==400) {
						$data['status']="failure";
						$data['action']="error";
						$data['description']="Bad request";
						$this->output_data(400,$data);
				}
				else if($action==401) {
						$data['status']="failure";
						$data['action']="error";
						$data['description']="Unauthorized";
						$this->output_data(401,$data);
				}
		} // Permanently delete the user and all its data, including ideas, services, etc.
		protected function about_us_put(){
				if(_f_is_admin())
				{
						$putData="";
						parse_str(file_get_contents("php://input"),$putData);
						$ins_data['content']=htmlspecialchars($putData['content']);
						$ins_data['section']=htmlspecialchars($putData['section']);

						$this->load->model('about_us');
						$action=$this->about_us->update($ins_data);
						if($action)
						{
								//success
								http_response_code(200);
								header('Content-Type: application/json');
								$data['status'] = "success";
								$data['description'] = "Changed successfully";
								echo json_encode($data);

						}
						else
						{
								http_response_code(200);
								header('Content-Type: application/json');
								$data['status'] = "failure";
								$data['description'] = "Server Error";
								echo json_encode($data);
						}
				}
				else
				{
						http_response_code(400);
						header('Content-Type: application/json');
						$data['status'] = "failure";
						$data['description'] = "Bad request";
						echo json_encode($data);
				}

		}
		protected function sector_delete(){
				$this->load->model('sector');
				$sector_id=segx(3);
				$action=$this->sector->delete($sector_id);
				if($action['status']=="success"){
						http_response_code(200);
						$data['status']="success";
						$data['description']=$action['description'];
						header('Content-Type: application/json');
						echo json_encode($data);
				}
				else{
						http_response_code(400);
						$data['status']="failure";
						$data['description']=$action['description'];
						header('Content-Type: application/json');
						echo json_encode($data);
				}
		}
		protected function admin_reset_password_patch(){
				$patchData="";
				parse_str(file_get_contents("php://input"),$patchData);
				if(_f_is_admin())
				{
						if(isset($patchData['current_password'],$patchData['new_password']) && $patchData['new_password']==$patchData['repeat_password'])
						{
								$esc_username='admin';
								$query="Select * from admins where username='$esc_username'";
								$action=$this->db->query($query);
								if($action)
								{
										$row=$action->result_array();
										if($row)
										{
												$check=password_verify($patchData['current_password'],$row[0]['password_hash']);
												if($check)
												{
														// return access token
														$new_ps=password_hash($patchData['new_password'],PASSWORD_DEFAULT);
														$query="update admins set password_hash='$new_ps'";
														$action=$this->db->query($query);
														if($action)
														{
																http_response_code(200);
																header('Content-Type: application/json');
																$data['status']="success";
																$data['description']="password changed successfully";
																echo json_encode($data);
														}
														else{

																http_response_code(500);
																header('Content-Type: application/json');
																$data['status']="internal server error";
																$data['description']="Database query failed";
																echo json_encode($data);
														}

												}
												else
												{
														http_response_code(400);
														header("Content-Type: application/json");
														$data['status']="failure";
														$data['description']="Current password is incorrect";
														echo json_encode($data);


												}
										}
										else
										{
												http_response_code(400);
												$data['status']="failure";
												$data['description']="fatal error";

										}

								}
								else
								{
										return 0;
								}
						}
						else{
								http_response_code(400);
								header('Content-Type: application/json');
								$data['status']="failure";
								$data['description']="passwords don't match";
								echo json_encode($data);
						}
				}
				else{
						http_response_code(401);

				}
		}
		protected function email_post(){
			if(!_f_is_admin())
			{
				$data['status']="failure";
				$data['description']="Unauthorized";
				$this->output_data(401,$data);
				die(0);
			}
			
			$from=$_POST['from'];
			if(!stripos($from,'sayourideas')) {
				$from.="@sayourideas.com";
			}
			
			$to=$_POST['to'];
			$cc=$_POST['cc'];
			$bcc=$_POST['bcc'];
			$subject=$_POST['subject'];
			$message=$_POST['message'];
			$attachments= array();

			foreach($_FILES['attachments']['name'] as $key=>$fe)
			{
					$path=$_FILES['attachments']['tmp_name'][$key];
					$name=$_FILES['attachments']['name'][$key];
					$attach[$key]['path']=$path;
					$attach[$key]['name']=$name;

					$attachments+=$attach;
			}

			$this->load->model('Bulk_email');
			$action=$this->Bulk_email->send($from,$to,$subject,$message,$attachments,$cc,$bcc);
			if($action==1)
			{
				$data['status']="success";
				$data['description']="Emails sent successfully";
				$this->output_data(201,$data);
			}
			else{
				$data['status']="failure";
				$data['description']='There was an error sending email';
				$this->output_data(500,$data);
			}
			
		}
		protected function bulk_email_post(){
			$this->load->model('bulk_email');

			if(!_f_is_admin())
			{
				$data['status']="failure";
				$data['description']="Unauthorized";
				$this->output_data(401,$data);
				die(0);
			}
			
			$from=$_POST['from'];
			if(!stripos($from,'sayourideas')) {
				$from.="@sayourideas.com";
			}
			
			$subject=$_POST['subject'];
			$message=$_POST['message'];

			$email_receivers = array();

			$get_users=$this->User3->get2();
			$users=$get_users['data'];

			foreach($users as $user) {
				if($user['email']=="" || $user['email']==null) {
					//do nothing
				}
				else if($user['user_type']=="ideator" && array_key_exists('list_ideators',$_POST)) {
					array_push($email_receivers,$user['email']);
				}
				else if($user['user_type']=="service_provider" && array_key_exists('list_service_providers',$_POST)) {
					array_push($email_receivers,$user['email']);
				}
				else if($user['user_type']=="investor" && array_key_exists('list_investors',$_POST)) {
					array_push($email_receivers,$user['email']);
				} else if($user['last_active'] < (time() - 7890000) && array_key_exists('list_inactive_users',$_POST)) {
					//inactive user
					array_push($email_receivers,$user['email']);
				}
			}
			

			//var_dump($email_receivers);die(0);
			
			
			$batch_id=$this->bulk_email->new_batch($from,$subject,$message);
			$this->bulk_email->add_batch_receivers($batch_id,$email_receivers);
			
			$data['status']="success";
			$data['description']="Emails Scheduled successfully!";
			$this->output_data(201,$data,"json");
			
			
		}
		protected function sectors_post(){
				if(_f_is_admin())
				{
						// Currently limiting adding sectors to sectors table to admin only
						$name=$_POST['name'];
						$this->load->model('sector');
						$action=$this->sector->create($name);
						if($action['status']=="success")
						{
								$this->output_data(201,$action);
						}
						else{
								$this->output_data(500,$action);
						}

				}
				else{

				}
		}
		protected function sectors_others_get()
		{
				if(_f_is_admin())
				{
						$this->load->model('sector');
						$action=$this->sector->get_ideas_other_sectors();
						if($action['status']=="success")
						{
								$this->output_data(200,$action);

						}
				}
				else{

				}
		}
		protected function activity_get(){
				if(!_f_is_admin()){
					$data['status']="failure";
					$data['description']="Unauthorized";
					$this->output_data(400,$data);
					die(0);
				}

				$this->load->model('activity');

				$filters=array();

				if(isset($_GET['filter_date_start'])){
						$filters['filter_date_start']=$_GET['filter_date_start'];
				}
				if(isset($_GET['filter_date_end'])){
						$filters['filter_date_end']=$_GET['filter_date_end'];
				}
				if(isset($_GET['filter_event_category'])){
						$filters['filter_event_category']=$_GET['filter_event_category'];
				}
				if(isset($_GET['filter_socialid'])){
						$filters['filter_socialid']=$_GET['filter_socialid'];
				}

				$activity_array=$this->activity->get($filters);

				
				$ret_data['status']="success";
				$ret_data['description']="fetched activity";
				$ret_data['activity']=$activity_array;
				$this->output_data(200,$ret_data);
				
						
				
		}
		protected function activityall_get(){
			if(!_f_is_admin()){
				$data['status']="failure";
				$data['description']="Unauthorized";
				$this->output_data(400,$data);
				die(0);
			}

			$this->load->model('activity');

			$filters= array();

			if(array_key_exists('date_start',$_GET) && $_GET['date_start']!="") {
				$filters['date_start']=$_GET['date_start'];
			}

			if(array_key_exists('date_end',$_GET) && $_GET['date_end']!="") {
				$filters['date_end']=$_GET['date_end'];
			}
			
			$activity_array=$this->activity->get2($filters);

			
			$ret_data['status']="success";
			$ret_data['description']="fetched activity";
			$ret_data['activity']=$activity_array;
			$this->output_data(200,$ret_data);
				
		}
		protected function inactive_users_get(){
				if(_f_is_admin()){
						$this->load->model('activity');
						$rows=$this->activity->get_inactive_users();
						if($rows['status']="success")
						{
								$data['status']="success";
								$data['description']="fetched inactive users successfully";
								$data['inactive_users']=$rows['inactive_users'];
								$this->output_data(200,$data);
						}
						else
						{
								$data['status'] = "";
								$data['description'] = "error";
								$this->output_data(500, $data);
						}
				}
				else{
						$data['status'] = "failure";
						$data['description'] = "unauthorized";
						$this->output_data(400, $data);
				}
		}

		protected function faqs_post(){
				if(_f_is_admin())
				{
						if(isset($_POST['post_title'],$_POST['post_desc'],$_POST['user_type']))
						{
								$ins_data['post_title']=$_POST['post_title'];
								$ins_data['post_desc']=$_POST['post_desc'];
								$ins_data['user_type']=$_POST['user_type'];    

								$action=$this->db->insert('tbl_posts',$ins_data);
								if($action)
								{
										$data['status']="success";
										$data['description']="faq added successfully";
										$this->output_data(201,$data);
								}
								else
								{
										$data['status']="failure";
										$data['description']="error in performing query";
										$this->output_data(500,$data);
								}

						}
						else{
								$data['status']="failure";
								$data['description']="error in form data";
								$this->output_data(400,$data);
						}

				}
				else
				{
						$data['status']="failure";
						$data['description']="unauthorized";
						$this->output_data(400,$data);
				}

		}
		protected function faq_put(){
				if(_f_is_admin())
				{
						$putData="";
						parse_str(file_get_contents("php://input"),$putData);
						$par = $putData;

						if(isset($par['post_id'],$par['post_title'],$par['post_desc']))
						{
								$ins_data['post_title']=$par['post_title'];
								$ins_data['post_desc']=$par['post_desc'];

								$this->db->set($ins_data);
								$this->db->where('post_id',$par['post_id']);
								$action=$this->db->update('tbl_posts');

								if($action)
								{
										$data['status']="success";
										$data['description']="faq updated successfully";
										$this->output_data(200,$data);
								}
								else
								{
										$data['status']="failure";
										$data['description']="error in performing query";
										$this->output_data(500,$data);
								}

						}
						else{
								$data['status']="failure";
								$data['description']="error in form data";
								$this->output_data(400,$data);
						}

				}
				else
				{
						$data['status']="failure";
						$data['description']="unauthorized";
						$this->output_data(400,$data);
				}

		} 
		protected function faq_delete(){
				if(_f_is_admin())
				{
						$putData="";
						parse_str(file_get_contents("php://input"),$putData);
						$par = $putData;

						if(isset($par['post_id']))
						{
								$this->db->where('post_id',$par['post_id']);
								$action=$this->db->delete('tbl_posts');

								if($action)
								{
										$data['status']="success";
										$data['description']="faq deleted successfully";
										$this->output_data(200,$data);
								}
								else
								{
										$data['status']="failure";
										$data['description']="error in performing query";
										$this->output_data(500,$data);
								}

						}
						else{
								$data['status']="failure";
								$data['description']="error in form data";
								$this->output_data(400,$data);
						}

				}
				else
				{
						$data['status']="failure";
						$data['description']="unauthorized";
						$this->output_data(400,$data);
				}

		}

		protected function ideas_get() {
			if(!_f_is_admin()) {
				$data=array('status'=>'failure','description'=>'unauthorized');
				$this->output_data(401,$data);
			}

			$params = array();

			if(array_key_exists('limit',$_GET)) {
				$params['limit']=$_GET['limit'];
			}

			if(array_key_exists('order_by_id',$_GET)) {
				$params['order_by_id']=$_GET['order_by_id'];
			}

			$ideas = $this->Idea4->get2($params);
			$this->output_data(200,$ideas);

		}

		protected function ideators_post() {
			if(!_f_is_admin()) die(0);
			$first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];
			$mobile = $_POST['mobile'];
			$password = $_POST['password'];
			$password_hash = password_hash($password,PASSWORD_DEFAULT);
			$user_type='ideator';

			$data=array(
				'first_name'=>$first_name,
				'last_name'=>$last_name,
				'mobile'=>$mobile,
				'user_type'=>$user_type,
				'password_hash'=>$password_hash
			);

			$action=$this->User3->create($data);

			if($action) {
				$this->output_data(200,array('status'=>'success','description'=>'User created successfully!'));
			}else {
				$this->output_data(400,array('status'=>'failure','description'=>'User could not be created!'));
			}




		}

		protected function service_providers_post() {
			if(!_f_is_admin()) die(0);
			$first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];
			$mobile = $_POST['mobile'];
			$password = $_POST['password'];
			$password_hash = password_hash($password,PASSWORD_DEFAULT);
			$user_type='service_provider';

			$data=array(
				'first_name'=>$first_name,
				'last_name'=>$last_name,
				'mobile'=>$mobile,
				'user_type'=>$user_type,
				'password_hash'=>$password_hash
			);

			$action=$this->User3->create($data);

			if($action) {
				$this->output_data(200,array('status'=>'success','description'=>'User created successfully!'));
			}else {
				$this->output_data(400,array('status'=>'failure','description'=>'User could not be created!'));
			}
		}

		protected function investors_post() {
			if(!_f_is_admin()) die(0);
			
			$password = $_POST['password'];
			$password_hash = password_hash($password,PASSWORD_DEFAULT);
			

			$_POST['password_hash']=$password_hash;

			$action=$this->User3->create($_POST);
			
			if($action) {
				$this->output_data(200,array('status'=>'success','description'=>'User created successfully!'));
			}else {
				$this->output_data(400,array('status'=>'failure','description'=>'User could not be created!'));
			}




		}

		protected function rate_cards_get() {
			$this->load->model('User_rate_card');
			$data['data']=array();
			$data['status']="success";

			$rate_cards=$this->User_rate_card->get($this->session->socialid);

			foreach($rate_cards as $rate_card) {
				// fetch upload using Supload
				
				$url=$this->Supload->get_complete_url($rate_card['uploadid']);
				$rate_card_data = array(
					'url'=>$url,
					'name'=>$rate_card['name'],
					'id'=>$rate_card['id']
				);
				array_push($data['data'],$rate_card_data);
			}

			$this->output_data(200,$data);
		}	
		protected function unapproved_messages_count_get() {
			if(!_f_is_admin()){
				$data['status']="failure";
				$data['description']="Unauthorized";
				$this->output_data(400,$data);
				die(0);
			}

			$query="SELECT count(*) from messages where admin_approved=0 and admin_viewed=0 order by id desc";
			$action=$this->db->query($query);
			if($action)
			{
				$row=$action->result_array();
				
				$count=$row[0]['count(*)'];
				$data=array(
					'status'=>'success',
					'count'=>$count
				);
				$this->output_data(200,$data);

			}
			else
			{
				$data=array(
					'status'=>'failure',
					'description'=>'DB Unavailable'
				);
				$this->output_data(500,$data);
			}
		}
}
?>
