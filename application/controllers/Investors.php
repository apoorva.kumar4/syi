<?php
class Investors extends CI_Controller{
	public function index()
	{
		$data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
		$data['title']="SaYourIdeas.com - Startups";
		$data['investors']=_f_get_all_investors();
		$this->parser->parse('templates/header',$data);
		$this->load->view('investors',$data);
		$this->load->view('copyfooter');
		
	}
}
/*Materialize.fadeInImage('#image-test')*/
?>