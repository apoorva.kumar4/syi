<?php
class Know_more extends CI_Controller {
	public function index() {
		$data=array();
		$data['title']="Organise your Startup!";
		if(isset($this->session->socialid)) {
			$user=$this->User_wrapper->read($this->session->socialid);
        	$data['me'][0]=$user;	
		}
		

		$this->parser->parse('templates/header',$data);
		$this->load->view('know_more',$data);
		$this->parser->parse('templates/footer',$data);


	}
}
?>