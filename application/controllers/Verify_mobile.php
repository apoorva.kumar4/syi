<?php
class Verify_mobile extends CI_Controller{

	protected $required_properties = array (
			'g-recaptcha-response',
			'mobile',
			'first_name',
			'last_name',
			'password'
		);

	public function index() {
		if(_f_is_loggedin()) {
			header('Location: /dashboard');
		}
		$sanitized_data = $this->filter_and_sanitize_data('required_properties',$_POST);
		if(!$sanitized_data) {
			//http_response_code(400);
			//echo "error:GFDM9";
			die(0);
		}

		// verify the captcha
		$this->load->library('Recaptcha');
		$captcha_api_secret=$this->Syiconfig->get('captcha_api_secret');
		$capthca_api_url=$this->Syiconfig->get('captcha_api_url');
		$g_captcha_response=$sanitized_data['g-recaptcha-response'];
		$is_valid=$this->recaptcha->verify_captcha($g_captcha_response,$captcha_api_secret,$capthca_api_url);
		
		// die if captcha is not valid
		if(!$is_valid) die(0);

		$this->load->model('User3');
		if($this->User3->is_user_present($sanitized_data['mobile'])) {
			//header('Location: /login');
			//die(0);
			if(getenv_from_db("login_signup_enabled")=="true")
            {

                $data['title']="Welcome to SaYourIdeas.com";
                $data['message']="You have already Signed Up earlier, please login to continue.";
                $this->parser->parse('templates/header',$data);
                $this->parser->parse('login',$data);
                $this->load->view('templates/footer',$data);
                
            }
            else
            {
                $uuu=base_url().'stay_tuned';
                header("Location: $uuu");
                
            }
		} else {

			$this->load->model('Otp');
			$otp= $this->Otp->generate_otp();
			$insert_otp= $this->Otp->insert_otp($sanitized_data['mobile'],$otp);

			if(!$insert_otp) {
				// failed to insert otp
				//http_response_code(500);
				//echo "Error in sending otp,please try again";
				die(0);
			}

			$send_sms = $this->Otp->send_sms($sanitized_data['mobile'],"Your verification code for SaYourIdeas is $otp");

			if(!$send_sms) {
				http_response_code(500);
				echo "Error in sending otp,please try again later.";
				die(0);
			}

			$data['title']="Please input OTP";
			$data['mobile']=$_POST['mobile'];
			$data['first_name']=$_POST['first_name'];
			$data['last_name']=$_POST['last_name'];
			$data['password']=$_POST['password'];

			$this->parser->parse('templates/header',$data);
			$this->parser->parse('otp_form',$data);
			$this->parser->parse('templates/footer',$data);
		}

	}
	public function verify() {
		$mobile=$_POST['mobile'];
		$otp=$_POST['otp'];
		$first_name=$_POST['first_name'];
		$last_name=$_POST['last_name'];
		$password=$_POST['password'];

		$this->load->model('Otp');

		$is_otp_valid=$this->Otp->verify_otp($mobile,$otp);

		if(!$is_otp_valid) {
			$data['title']="Please input OTP";
			$data['mobile']=$_POST['mobile'];
			$data['first_name']=$_POST['first_name'];
			$data['last_name']=$_POST['last_name'];
			$data['password']=$_POST['password'];
			$this->parser->parse('templates/header',$data);
			$this->parser->parse('verify_mobile_failed',$data);
			$this->parser->parse('templates/footer',$data);
			
		} else {
			$this->load->model('User3');

			$action=$this->User3->create(array(
				'mobile'=>$mobile,
				'first_name'=>$first_name,
				'last_name'=>$last_name,
				'password_hash'=>password_hash($password,PASSWORD_DEFAULT)
			));

			if(!$action) {
				header('Location: /');
			}

			$this->session->set_userdata('socialid',$action);

			header('Location: /dashboard');
		}

		
	}
	protected function filter_and_sanitize_data($reference,$array){
        $filter_array = array();

        foreach($this->$reference as $key) {
            
            if(array_key_exists($key, $array)){
                $filter_array[$key]=is_string($array[$key])?strip_tags($array[$key]):$array[$key];
            }
        }

        foreach ($filter_array as $key=>$value) {
           if($value===null || $value=="") {
                return false;
           }
        }

        return $filter_array;

    }
}