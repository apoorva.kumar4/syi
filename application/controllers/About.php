<?php
class About extends CI_Controller{
	public function index()
	{
        $data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
        
        $data['title']="Welcome to SaYourIdeas.com";
		$this->load->model('about_us');
		$data['how_we_do_it']=$this->about_us->read('how_we_do_it');
        $data['what_do_we_do']=$this->about_us->read('what_do_we_do');
        $data['youtube_link']=$this->about_us->read('youtube_link');

        if(_f_is_loggedin()){
            $analytics['socialid']=$this->session->socialid;
            $analytics['event_category']="page_visit";
            $analytics['event_action']="visit";
            $analytics['event_label']="about_page";
            $analytics['event_value']='about';
            $analytics['event_description']="Visited About us page";
            a_send($analytics);
        }

		$this->parser->parse('templates/header',$data);
		$this->load->view('about',$data);
		$this->load->view('copyfooter');
	}
}
?>