<?php
class Inbox extends CI_Controller
{
	public function index()
	{
		$data = array();
        if(isset($this->session->socialid))
        {
            $this->load->model('User3');
            $user=$this->User3->read($this->session->socialid);
            $data['me'][0]=$user;
            
        }
		if(_f_is_loggedin())
		{
			$this->load->model('user');
			$data['userid']=_f_get_userid_from_socialid($this->session->socialid);
			$data['messages']=$this->user->get_messages();
			$data['title']="Inbox";
			$this->parser->parse('templates/header',$data);
			$this->load->view('inbox');
			$this->load->view('templates/footer');

		}
	}
} 
?>