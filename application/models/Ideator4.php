<?php
class Ideator4 extends CI_Model {
	const TABLE = 'ideators';
	
	public function get($key,$value) {
		$table=self::TABLE;
		$this->db->from($table);
		$this->db->where($key,$value);
		$action=$this->db->get();
		if(!$action) {
			return false;
		} else {
			$result=$action->result_array();
			if(!$result) return false;
			else return $result;
		}
	}

	public function update($socialid, $data) {
		$table=self::TABLE;
		$updateable_properties = array(
	        'institute',
	        'year_of_graduation',
	        'student_alumni'
	    );

		$filtered_array=array();

		foreach($updateable_properties as $key) {
			if(array_key_exists($key, $data)) {
				$filtered_array[$key]=$data[$key];	
			}
			
		}
		if(array_key_exists('year_of_graduation',$data)) {
			if($data['year_of_graduation']=="")
			{
							unset($filtered_array['year_of_graduation']);
			}
		}
		$this->db->where('socialid',$socialid);
		$this->db->set($filtered_array);
		$action=$this->db->update(self::TABLE);
		if(!$action) return $this->db->error();
		else return true;
	}

	public function insert($socialid,$data) {
		if($this->get('socialid',$socialid)){
			return "user already exists";
		}

		$insert_properties = array(
			'institute',
			'year_of_graduation',
			'student_alumni'
		);

		$filtered_array=array();
		foreach($insert_properties as $key) {
			if(array_key_exists($key, $data)) {
				$filtered_array[$key]=$data[$key];	
			}
			
		}

		$this->db->set('socialid',$socialid);
		$this->db->set($filtered_array);
		$action=$this->db->insert(self::TABLE);
		if(!$action) return $this->db->error();
		else return true;

	}

	public function get_profile_completeness($socialid) {
		$score=0;
		$u= $this->get('socialid',$socialid);

		if(!$u) return false;
        
        $total_properties= array (
	        'institute',
	        'year_of_graduation',
	        'student_alumni'
    	);

    	
        $total=count($total_properties);

        foreach ($total_properties as $key) {
            if($u[0][$key]!="" && $u[0][$key]!=null) {
                $score++;
            }
        }

        return round(($score/$total)*100);
	}	

	public function delete($socialid){
		if(!$this->get('socialid',$socialid)) return false;

		$this->db->where('socialid',$socialid);
		$action=$this->db->delete(self::TABLE);
		if($action) return true;
		else return false;
	}

	public function get2($params=array()) {

        if(array_key_exists('socialid',$params)) {
                $this->db->where('socialid',$params['socialid']);
        }

        $action=$this->db->get(self::TABLE);
        if(!$action) {
            return array(
                'status'=>'failure',
                'description'=>'DB Error',
                'error'=>$this->db->error()
            );
        }

        $result=$action->result_array();

        $users = array();

        foreach($result as &$row) {
            $temp_socialid=$row['socialid'];
            $users[$temp_socialid]=$row;
        }

        return array(
            'status'=>'success',
            'data'=>$users
        );

    }

    public function count() {
    	$this->db->select('count(*)');
    	$action=$this->db->get(self::TABLE);
    	if(!$action) return 0;

    	$result=$action->result_array();
    	if(!$result) return 0;
    	return $result[0]['count(*)'];

    }
}
?>
