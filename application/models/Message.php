<?php
class Message extends CI_Model{
    public function approve($id)
    {
        if(is_numeric($id))
        {
            $this->db->where('id',$id);
            $action=$this->db->update('messages',array('admin_approved'=>1));
            if($action)
            {
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    public function reject($id)
    {
        if(is_numeric($id))
        {
            $this->db->where('id',$id);
            $action=$this->db->update('messages',array('admin_approved'=>2));
            if($action)
            {
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    public function admin_message($array)
    {
        if(_f_is_admin())
        {
            $receivers=array();
            if(isset($array['ideator'])){
                $receivers[]="ideator";
            }
            if(isset($array['service_provider'])){
                $receivers[]="service_provider";
            }
            if(isset($array['investor'])){
                $receivers[]="investor";
            }
            if(isset($array['message']))
            {
                //Send the message now;
                $re=implode("','",$receivers);
                $message=$array['message'];
                $query="insert into messages(sender,receiver,message,admin_approved) select 1,users.socialid,'$message',1 from users where users.socialid not like 1 and users.user_type in ('$re')";
                $action=$this->db->query($query);
                if($action)
                {
                    $data['status']="success";
                    return $data;

                }
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    public function get($id) {
		$query="select * from messages where id=$id";
		$action=$this->db->query($query);
		if($action) {
			$row=$action->result_array();
			return $row[0];
		} else {return false;}

	}	
}
?>
