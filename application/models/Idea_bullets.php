<?php
class Idea_bullets extends CI_Model {
	public function create_default_bullets($params) {
		$ideaid=$params['idea_id'];
		$phase=$params['idea_phase'];
		$start_date=$params['start_date'];
		$end_date=$params['ebd_date']
		$dt_start=DateTime::createFromFormat("Y-m-d", $start_date);
        $dt_end=DateTime::createFromFormat("Y-m-d", $end_date);
        $d1=$dt_start;
        $d2=$dt_end;
        $dt_pot=$dt_start; // $dt_pot acts as a variable because it is added intervals to add consecutive bullets


        $dt_now=DateTime::createFromFormat("Y-m-d",date('Y-m-d')); // Store the current date in $dt_now
        $index=0;
        $fetch_current_phase_bullet=$this->db->get('default_idea_phases'); // Get the default phases list
        if(!$fetch_current_phase_bullet) return 0;
        
        $row=$fetch_current_phase_bullet->result_array();
        foreach($row as $key=>$item)
        {
            if($item['name']==$phase) //Check if the selected phase matches the item
            {

                $index=$key; // it matches to set the index to the $key so that we can split the array using this $index

            }
        }

        // Split phases into 2 parts based on the current phase
        $rest_of_the_stuff=array_slice($row,$index+1);
        $before_stuff=array_slice($row,0,$index);

        // Insert the very first bullet

        $prep_first_bullet= array(
            'name'=>$row[0]['name'],
            'ideaid'=>$ideaid,
            'description'=>$row[0]['description'],
            'start_date'=>$dt_start->format('Y-m-d'),
            'template'=>1
        );

        $sql=$this->db->set($prep_first_bullet)->insert('bullets');
        //$sql=$this->db->set($prep_first_bullet)->get_compiled_insert('bullets');

        //echo $sql."<br>";

        if($index!=0)
        {
            unset($before_stuff[0]);
        }

        // Add the last phase bullet to the database

        $prep_data= array(
            'name'=>$row[sizeof($row)-1]['name'],
            'ideaid'=>$ideaid,
            'description'=>$row[sizeof($row)-1]['description'],
            'start_date'=>$dt_end->format('Y-m-d'),
            'template'=>1
        );
        $sql=$this->db->set($prep_data)->insert('bullets');
        //$sql=$this->db->set($prep_data)->get_compiled_insert('bullets');
        //echo $sql."<br>";
        // Remove the last bullet from the array
        unset($rest_of_the_stuff[sizeof($rest_of_the_stuff)-1]);

        // Add the current phase bullet to the database
        if(($index!=0 && $index!=sizeof($row)-1))
        {
            $prep_data= array(
                'name'=>$row[$index]['name'],
                'ideaid'=>$ideaid,
                'description'=>$row[$index]['description'],
                'start_date'=>$dt_now->format('Y-m-d'),
                'template'=>1
            );
            //$sql=$this->db->set($prep_data)->get_compiled_insert('bullets');
            $sql=$this->db->set($prep_data)->insert('bullets');
            //echo $sql."<br>";
        }


        // prepeare to create bullets for 1st half
        // Determine the interval between the bullets

        $count_bf=sizeof($before_stuff);

        //echo "count_bf=".$count_bf."<br>";

        if($count_bf)
        {
            $diff= $d1->diff($dt_now)->format('%a');

            //echo "diff1=".$diff."<br>";

            $interval=intval($diff/($count_bf+1));
            $date_interval= new DateInterval('P'.$interval.'D');
            $dt_pot=date_add($dt_start,$date_interval);
            foreach($before_stuff as $item)
            {
                $prep_data= array(
                    'name'=>$item['name'],
                    'ideaid'=>$ideaid,
                    'description'=>$item['description'],
                    'start_date'=>$dt_pot->format('Y-m-d'),
                    'template'=>1
                );

                //$sql=$this->db->set($prep_data)->get_compiled_insert('bullets');
                $sql=$this->db->set($prep_data)->insert('bullets');
                $dt_pot=date_add($dt_pot,$date_interval);
                //echo $sql."<br>";

            }
        }

        $count_af=sizeof($rest_of_the_stuff);

        //echo "count_af=".$count_af."<br>";

        if($count_af)
        {
            $diff2= $d2->diff($dt_now)->format("%a"); // $d2 is the difference between the end date and currrent date

            //echo "diff2=".$diff2."<br>";

            $interval=intval($diff2/($count_af+1));

            //echo "interval=".$interval."<br>";

            $date_interval= new DateInterval('P'.$interval.'D');
            $dt_pot=date_add($dt_now,$date_interval);
            foreach($rest_of_the_stuff as $item)
            {
                $prep_data= array(
                    'name'=>$item['name'],
                    'ideaid'=>$ideaid,
                    'description'=>$item['description'],
                    'start_date'=>$dt_pot->format('Y-m-d'),
                    'template'=>1
                );

                //$sql=$this->db->set($prep_data)->get_compiled_insert('bullets');
                $sql=$this->db->set($prep_data)->insert('bullets');
                //echo $sql."<br>";
                $dt_pot=date_add($dt_pot,$date_interval);

            }
        }
	}
}
?>