<?php
class Supload extends CI_Model
{
    const TABLE='uploads';

    public $valid_file_type=0;
    public function get_upload($uploadid)
    {
        $action = $this->db->get_where('uploads', array('id' => $uploadid), 1);
        $row = $action->result_array();
        if(isset($row[0]))
        return $row[0];
        else return false;
    }
    public function get_upload_from_slug($slug)
    {

        $action = $this->db->get_where('uploads', array('url' => $slug), 1);
        $row = $action->result_array();
        if(isset($row[0]))
        return $row[0];
        else return -1;
    }
    public function delete($uploadid)
    {
        log_message('debug','In Supload:delete($uploadid)');

        
        $upload=$this->get_upload($uploadid);
        if(!$upload) return false;

        $key=$upload['url'];
        $this->load->library('aws_upload');
        $delete_action=$this->aws_upload->delete($key);
        $action = $this->db->delete('uploads', array('id' => $uploadid));
        return true;

    }
    public function create($par)
    {
        // The function expects the following parameters in $par
        // 1: socialid: required
        // 2: file - the actual file received via $_FILES
        //      name - the name you want the file to be saved as
        //      tmp_name - the source file name when uploaded
        // 3: permissions (defaults to 1 below if not set)
        //      0: Nobody
        //      1: uploader's view only
        //      2: restricted view
        //      3: publicly accessible to all
        // 4: accepted types: this will be verified with the actual file type, can be image | document | image_document

        // Returns and array indicating
        // status : success | failure
        // description: description of success or reason for failure
        // id: if success then returns id of the upload

            
        $validextensions = array(
            "jpeg",
            "jpg",
            "png",
            "pdf",
            "xlsx",
            "xls",
            "doc",
            "docx",
            "ppt",
            "pptx"
        );  

        $mime_types = array(
            'image/png',
            'image/jpg',
            'image/jpeg',
            'application/pdf',
            'application/vnd.ms-excel',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/msword',
            'application/powerpoint',
            'application/mspowerpoint',
            'application/vnd.ms-powerpoint',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        );

        $temporary = explode(".", $par['file']["name"]);
        $file_extension = end($temporary);

        $valid=0;
        foreach($mime_types as $type) {
            if($par['file']['type']==$type) $valid=1;
        }

        if(!$valid) {
            $ret_par['status'] = "error";
            $ret_par['description'] = "Invalid File type";
            return $ret_par;
        }

        if($par['file']['size'] > 5242880) {
            $ret_par['status'] = "error";
            $ret_par['description'] = "Invalid File Size";
            return $ret_par;   
        }
        
        $sourcePath = $par['file']['tmp_name']; // Storing source path of the file in a variable
        $targetfilename = hash("sha256", "ransalt_aashay0101_edd" . $par['socialid'] . $par['file']['name']) . "." . $file_extension;
        $par['targetfilename']=$targetfilename;
        if($par['permissions']==3)
            $par['access']='public-read';
        else $par['access']='private';


        
        $temporary = explode(".", $par['file']["name"]);
        


        $this->load->library('aws_upload');
        $action=$this->aws_upload->do_upload($par);
        if($action==true)
        {
            $up_data = array(
                'socialid'=>$par['socialid'],
                'name' => $par['file']['name'],
                'url' => $targetfilename,
                'permissions' => $par['permissions']
            );

            $action = $this->db->insert('uploads',$up_data);
            if ($action) {
                $ret_par['status'] = "success";
                $ret_par['description'] = "File Uploaded Successfully";
                $ret_par['id']=$this->db->insert_id();
                $ret_par['url']=$targetfilename;
                return $ret_par;
            }
            else {
                $ret_par['status'] = "error";
                $ret_par['description'] = "Temporary error";
                return $ret_par;
            }

        } else {
            log_message('debug',$action);
        }
    }

    public function get_complete_url($uploadid) {
        $this->load->model('Syiconfig');
        $syi_config=$this->Syiconfig->get_all();
        $upload=$this->get_upload($uploadid);
        if($upload) {
            $return_url="https://s3.".$syi_config['aws_s3_region'].".amazonaws.com/".$syi_config['aws_s3_bucket']."/";    
            $return_url.=$upload['url'];
            return $return_url;
        } else {
            return '/assets/ffffff.png';
        }
        
    }

    public function concatenate_complete_url($url)
    {
        $aws_s3_region= $this->config->item('aws_s3_region');
        $aws_s3_bucket= $this->config->item('aws_s3_bucket');

        $return_url="https://s3.".$aws_s3_region.".amazonaws.com/".$aws_s3_bucket."/";
        $return_url .= $url;
        return $return_url;
    }

    public function get2($params=array()) {

        if(array_key_exists('id',$params)) {
            $this->db->where('id',$params['id']);
        }

        if(array_key_exists('fields',$params)) {
            $this->db->select($params['fields']);
        }

        $action=$this->db->get(self::TABLE);
        if(!$action) {
            return array(
                'status'=>'failure',
                'description'=>'DB Error',
                'error'=>$this->db->error()
            );
        }

        $result=$action->result_array();

        $return_data = array();

        foreach($result as &$row) {
            $temp_id=$row['id'];
            $row['complete_url']=$this->concatenate_complete_url($row['url']);
            $return_data[$temp_id]=$row;
        }

        return array(
            'status'=>'success',
            'data'=>$return_data
        );

    }
}
?>
