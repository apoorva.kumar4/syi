<?php
class Rate_card extends CI_Model {
	const TABLE='rate_cards';

	public function get_rate_cards_for_user($socialid) {
		$this->db->where('socialid',$socialid);
		$action=$this->db->get(self::TABLE);
		if(!$action) return false;
		$result=$action->result_array();
		if(!$result) return array();
		return $result;

	}
}
?>