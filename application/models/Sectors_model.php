<?php
class Sectors_model extends CI_Model {
	const TABLE= 'sectors';
	public function get_all() {
		$this->db->select('id,name');
		$this->db->order_by('name');
		$action=$this->db->get(self::TABLE);
		if($action) {
			return $action->result_array();
		}
		else return array();

	}
}
?>