<?php
class Semail extends CI_Model{

    protected $props = array(
        'protocol'=>'smtp',
        'charset'=>'iso-8859-1',
        'wordwrap'=>TRUE,
        'smtp_host'=>"ssl://smtp.zoho.com",
        'smtp_port'=>465,
                'newline'=>"\r\n",
                'crlf'=>"\r\n",
				'useragent'=>'SaYourIdeas Mailer'
    );

    protected $senders = array(
        'admin@sayourideas.com'=>"War@machine0101!!",
        'ideators@sayourideas.com'=>"V7Uqsj9mH5nv",
        'serviceproviders@sayourideas.com'=>"bvRNF8TrEQQK",
        'investors@sayourideas.com'=>"85nSWYBUMif4",
        'info@sayourideas.com'=>"95fHsTBx3Vet",
    );

    public function send($from,$to,$cc=null,$bcc=null,$subject,$message,$attachments="")
    {
        // $from is a string which can be one of the predefined $senders
        // $to is a comma delimited list of email addresses
        // $cc is a comma delimited list of email addresses
        // $bcc is a comma delimited list of email addresses
        // $subject is a string
        // $message is a string
        // $attachments is an array of : array of( 'path' which is path of the file, 'name' which will appear to receiver

        // STEP 1: Check for predefined senders and set the config file for codeigniters email library

        $this->load->library('email');

        foreach($this->senders as $sender=>$password)
        {
            if($sender==$from)
            {
                $this->props['smtp_user']=$sender;
                $this->props['smtp_pass']=$password;

            }
        }


        $this->email->initialize($this->props);

        // STEP 2: Add the $to, $cc, $bcc and $subject and $message parameters
				 $this->email->from($this->props['smtp_user'],"SYI");
				 $this->email->to($to);
         if($cc!=null) $this->email->cc($cc);
         if($bcc!=null) $this->email->bcc($bcc);
				 $this->email->subject($subject);
	       $this->email->message($message);
				 $this->email->set_header('X-Mailer','SaYourIdeas');
        // STEP 3: Attach the attachments to the email library

        if($attachments=="")
        {
            // No attachments; do nothing
        }
        else{
            // Attachments provided
            foreach($attachments as $attachment)
            {

                $path=$attachment['path'];
                $name=$attachment['name'];
                $this->email->attach($path,'attachment',$name);
            }
        }


        // STEP 4: Finally send the email


        $action=$this->email->send();
        
        if($action)
        {
            return 1;
        }
        else{
            $description=$this->email->print_debugger();
            return $description;
        }

    }

}
?>
