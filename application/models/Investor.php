<?php
class Investor extends CI_Model
{
    public function sign_up($items)
	{
        $data = array(
            'socialid' => $items['abe3e5_socialid'],
            'first_name' => $items['first_name'],
            'last_name' => $items['last_name'],
            'email' => $items['abe3e5_email'],
            'mobile' => $items['mobile'],
            'role' => $items['role'],
            'user_type' => $items['abe3e5_user_type'],
            'access_type' => $items['abe3e5_access_type'],
            'access_token' => $items['abe3e5_access_token'],
            'location' => $items['location'],
            'sector_expertise' => $items['sector_expertise'],
            'registration_complete' => 'Y',
            'password_hash' => $items['abe3e5_password_hash'],
            'username'=>$items['abe3e5_socialid'],
            'admin_approved'=>0,
            'emailverified'=>$items['email_verified'],
        );

        $data2 = array(
            'socialid' => $items['abe3e5_socialid'],
            'industry' => $items['industry'],
            'company' => $items['company'],
            'working_since' => $items['working_since'],
            'investment_range_start' => $items['investment_range_start'],
            'investment_range_end' => $items['investment_range_end'],
            'designation' => $items['role'],
            'professional_background' => $items['professional_background'],
            'alumni_network' => $items['alumni_network'],
            'investment_network' => $items['investment_network'],
            'members_of_organisation' => $items['members_of_organisation'],
            'sample_companies_invested' => $items['sample_companies_invested'],
            'investor_tags' => $items['investor_tags'],
            'target_sector' => $items['target_sector']
        );

        // Depening upon if he is database or not, perform an insert or an update query

        $this->db->select('count(*)');
        $this->db->from('users');
        $this->db->where('socialid',$data['socialid']);
        $pre_query=$this->db->get();
        $row=$pre_query->row_array();



            if($row['count(*)']>0)
            {
                    $this->db->where('socialid', $data['socialid']);
                    $this->db->update('users', $data);
                    $this->db->update('investors', $data2);
                    return 1;
            }
            else
            {

                foreach($data as $key=>$item)
                {
                    if(is_null($item))
                    {
                        return $key;
                    }

                }

                $this->db->insert('users', $data);
                $this->db->insert('investors', $data2);

                $counter_target_sector = 0;
                foreach ($items['service-target-sectors'] as $target_sector) {
                    if ($counter_target_sector < 10) {
                        $tee= array(
                            'socialid'=>$data['socialid'],
                            'sectorid'=>$target_sector
                        );
                        $action3=$this->db->insert('investor_sectors',$tee);
                        if ($action3) {
                            $counter_target_sector++;
                        }
                    }
                }

                return 1;
            }
	}
    public function get_suggested_ideas($socialid)
    {
        $query="select * from ideas  where ideas.sectorid in ( select sectorid from investor_sectors where investor_sectors.socialid=$socialid)";
        $action=$this->db->query($query);

        $result=$action->result_array();
        if(!$result) {
            $this->db->order_by('created_on','desc');
            $this->db->limit(5);
            $action=$this->db->get('ideas');
            $result=$action->result_array();
            //var_dump($result);die(0);
            return $result;
        }
        return $result;


    }
}