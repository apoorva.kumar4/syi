<?php
class Notification extends CI_Model
{

	public $id;
	public $socialid;
	public $name;
	public $description;

	public function new() {
		$validated_name= $this->db->escape($this->name);
		$validated_socialid= $this->db->escape($this->socialid);
		$validated_description= $this->db->escape($this->description);

		$query="insert into notifications(name,socialid,description) values($validated_name,$validated_socialid,$validated_description)";

		$action= $this->db->query($query);
		if($action) return true; else return false;


	}
	public function read() {
		$id=$this->$id;
		$query="select * from notifications where id=$id";
		$action=$this->db->query($query);
		if($action) {
			$row=$action->result_array();
			return $row[0];
		} else {return false;}

	}
	public function update($tag,$value)
	{
		$id=$this->id;
		$query="update notifications set $tag=$value where id=$id";
		$action=$this->db->query($query);
		if($action) return true; else return false;
	}
	public function delete()
	{
		$id=$this->id;
		$query="delete from notifications where id=$id";
		$action=$this->db->query($query);
		if($action) return true; else return false;

	}
	public function getUnread($socialid) {
		$id=$this->$id;
		$query="select * from notifications where socialid=$socialid and notif_read='no'";
		$action=$this->db->query($query);
		if($action) {
			$row=$action->result_array();
			return $row;
		} else {return false;}

	}
	public function markAllRead($socialid) {
		$query="update notifications set notif_read='yes' where socialid=$socialid";
		$action=$this->db->query($query);
		if($action) return true; else return false;

	}
	public function getAll($socialid) {
		$id=$this->$id;
		$query="select * from notifications where socialid=$socialid";
		$action=$this->db->query($query);
		if($action) {
			$row=$action->result_array();
			return $row;
		} else {return false;}
	}

}
?>
