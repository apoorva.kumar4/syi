<?php
class Event extends CI_Model
{
    const TABLE='events';
    const TABLE_EVENT_COVER_PICS = 'event_cover_pics';
    const TABLE_EVENT_LOGOS = 'event_logos';
    // Variables for this model
    public $id;
    public $ownerid;
    public $name;
    public $slug;
    public $description_short;
    public $description_long;
    public $date_start;
    public $date_end;
    public $approved;
    public $active;
    public $trashed;
    public $locked;
    public $pictures;
    // Functions and subroutines
    protected $properties= array(
        'name'=>null,
        'ownerid'=>null,
        'slug'=>null,
        'description_short'=>null,
        'description_long'=>null,
        'date_start'=>null,
        'date_end'=>null,
        'logo_uploadid'=>null,
        'cover_uploadid'=>null,
    );

    public function __construct()
    {
    }
    public function get_event_list($socialid)
    {
        $query="select name from events where ownerid=$socialid";
        $action=$this->db->query($query);
        if($action)
        {
            return $action->result_array();
        }
        else
        {
            return 0;
        }
    }
    public function set_properties($array)
    {
        foreach($this->properties as $key=>$value)
        {
            if(array_key_exists($key,$array))
            {
                $this->properties[$key]=is_string($array[$key])?strip_tags($array[$key]):$array[$key];
            }

        }
    }
    public function create($par)
    {
        $this->set_properties($par);
        $copied_properties=$this->properties;

        $cover_uploadid=$copied_properties['cover_uploadid']; // Store Cover uploadid so that it can be inserted in events_covers
        unset($copied_properties['cover_uploadid']); //We wont need this in events table

        $logo_uploadid=$copied_properties['logo_uploadid']; // Store Cover uploadid so that it can be inserted in events_logos
        unset($copied_properties['logo_uploadid']); //We wont need this in events table

        $ava=$this->check_slug_availability($copied_properties['slug']);
        if($ava) {
            $this->db->set($copied_properties);
            $action = $this->db->insert('events');
            $retrieved_eventid = $this->db->insert_id();
            if ($action) {
                // We have inserted the event in the table
                // Now use the event id and add the cover and logo

                //Proceed to add cover pic
                $tem_arr_cover=array('eventid'=>$retrieved_eventid, 'uploadid'=>$cover_uploadid);
                $this->add_cover($tem_arr_cover);

                //Proceed to add logo pic
                $tem_arr_logo=array('eventid'=>$retrieved_eventid, 'uploadid'=>$logo_uploadid);
                $this->add_logo($tem_arr_logo);

                // Finally after all done, return the success status and return
                // the newly created event id
                $data['status']="success";
                $data['description']="";
                $data['eventid']=$retrieved_eventid;
                return $data;
            } else {
                return 0;
            }
        }
        else
        {
            $slug=$par['slug'];
            $description="conflict";
            $ret_status=array('status'=>'error','description'=>"event slug not valid or already taken");
            return $ret_status;
        }
    }
    public function update($par)
    {

        $this->set_properties($par);

        $copied_properties=$this->properties;
        unset($copied_properties['cover_uploadid']); //We wont need this in events table
        unset($copied_properties['logo_uploadid']); //We wont need this in events table
        unset($copied_properties['ownerid']); // We wont wish to change/update the owner of the event
        $fetched_event=$this->read($par['id']);
        if($fetched_event['ownerid']==$this->session->socialid){
            if($fetched_event['slug']==$copied_properties['slug'])
            {
                $ava=1;
            }
            else{
                $ava=$this->check_slug_availability($copied_properties['slug']);
            }
            if($ava) {
                $this->db->set($copied_properties);
                $this->db->where('id',$par['id']);
                $action = $this->db->update('events');

                if ($action) {
                    // We have inserted the event in the table
                    // Finally after all done, return the success status and return
                    $data['status']="success";
                    $data['description']="";
                    return $data;
                } else {
                    return 0;
                }
            }
            else
            {
                $slug=$par['slug'];
                $description="conflict";
                $ret_status=array('status'=>'error','description'=>"event slug not valid or already taken");
                return $ret_status;
            }
        }
        else{
            $data['status']="failure";
            $data['description']="unauthorized";
            return $data;
        }

    }
    public function read($id)
    {
        if(is_numeric($id)){
            $action=$this->db->get_where('events',array('id'=>$id));
            $row=$action->result_array();
            if(isset($row[0]))
            return $row[0];
            else
                return 0;
        }
        else{
            return 0;
        }
    }
    public function patch()
    {
        $validated_name= isset($this->name)?$this->db->escape($this->name):NULL;
        $validated_slug= isset($this->slug)?$this->db->escape(_f_validate('description',$this->slug)):NULL;
        $validated_description_short= isset($this->description_short)?$this->db->escape(_f_validate('description',$this->description_short)):NULL;
        $validated_description_long= isset($this->description_long)?$this->db->escape(_f_validate('description',$this->description_long)):NULL;
        $validated_date_start=isset($this->date_start)?$this->db->escape(_f_validate('date',$this->date_start)):NULL;
        $validated_date_end=isset($this->date_end)?$this->db->escape(_f_validate('date',$this->date_end)):NULL;


        $ava=$this->check_slug_availability($validated_slug);

        if($ava)
        {
            $query="Update events set name=$validated_name,slug=$validated_slug,description_short=$validated_description_short,description_long=$validated_description_long,date_start=$validated_date_start,date_end=$validated_date_end where id=$this->id";
            $action=$this->db->query($query);
            $retrieved_eventid=$this->db->insert_id();
            //echo $query;
            if($action)
            {
                // We have inserted the event in the table

                return 1;
            }
            else
            {
                return 0;
            }
            
        }
        else
        {
            $description="Slug $this->unique_url already Taken";
            $ret_status=array('status'=>'error','description'=>$description);
            return $ret_status;
        }
    }
    public function check_slug_availability($unique_url)
    {
        $unique_url=$this->db->escape($unique_url);
        $query="select count(*) from events where slug=$unique_url";
        $action=$this->db->query($query);

        if($action)
        {
            $row=$action->result_array();
            if($row[0]['count(*)']==0)
            {
                return true;
            }
            else
            {
                return false;

            }
        }


    }
    public function delete($id)
    {

        $query="delete from events where id=$id";

        $action=$this->db->query($query);

        if($action)
        {
            return 1;
        }
        else
        {
            return 0;

        }
    }
    public function approve($id){
        $query = "Update events set admin_approved=1 where id=$id";
        $action = $this->db->query($query);
        if ($action) {
            return 1;
        } else {
            return 0;
        }
    }
    public function reject($id){
        $query = "Update events set admin_approved=2 where id=$id";
        $action = $this->db->query($query);
        if ($action) {
            return 1;
        } else {
            return 0;
        }
    }
    public function edit_event($tag,$value)
    {
        $id=$this->id;
        $esc_value=$this->db->escape($value);
        $query="update events set $tag=$esc_value where id=$id and locked=0";

        $action=$this->db->query($query);

        if($action)
        {
            return 1;
        }
        else
        {
            return 0;
        }

    }
    public function get_gallery($event_id)
    {
        if(is_numeric($event_id))
        {
            $query="select * from uploads where id in (select uploadid from event_gallery where eventid=$event_id)";
            $action=$this->db->query($query);
            if($action)
            {
                $row=$action->result_array();
                $list=array();
                foreach($row as $key=>$picture)
                {
                    $path=$this->config->item('upload_path').$picture['url'];
                    $dimension=getimagesize($path);
                    $list[$key]['src']=$path;
                    $list[$key]['w']=$dimension[0];
                    $list[$key]['h']=$dimension[1];
                }
                return $list;
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }

    }
    public function get_event_from_slug()
    {
        $slug=$this->slug;
        $esc_slug=$this->db->escape($slug);
        $query="Select * from events where slug=$esc_slug";
        $action=$this->db->query($query);
        if($action)
        {
            $row=$action->result_array();
            return $row[0];
        }
        else
        {
            return 0;
        }
    }
    public function get_cover_pic_url()
    {
        $id=$this->id;
        $query="select count(url),url from event_cover_pics where id=$id order by timestamp desc limit 1";
        $action=$this->db->query($query);
        if($action)
        {

            $row=$action->result_array();
            if($row[0]["count(url)"]==0)
            {
                return 0;
            }
            else
                return $row[0];
        }
        else
        {
            return 0;
        }
    }
    public function get_cover_pic($event_id) {
        $this->db->limit(1);
        $this->db->where('eventid',$event_id);
        $action=$this->db->get(self::TABLE_EVENT_COVER_PICS);
        if(!$action) return false;
        $row=$action->result_array();
        if(!$row) return false;
        return $row[0]["uploadid"];

    }
    public function get_logo($event_id) {
        $this->db->limit(1);
        $this->db->where('eventid',$event_id);
        $action=$this->db->get(self::TABLE_EVENT_LOGOS);
        if(!$action) return false;
        $row=$action->result_array();
        if(!$row) return false;
        return $row[0]["uploadid"];

    }
    
    public function is_event_public()
    {
        $id=$this->id;
        $query="select admin_approved from events where id=$id";
        $action=$this->db->query($query);
        if($action)
        {

            $row=$action->result_array();

            if($row[0]["admin_approved"]=="1")
            {
                return 1;
                
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }
    public function check_slug_update_availability($eventid,$unique_url)
    {

        $unique_url=$this->db->escape($unique_url);
        $query="select count(*) from events where slug=$unique_url";
        //echo $query; die(0);
        $action=$this->db->query($query);

        if($action)
        {
            $row=$action->result_array();
            if($row[0]['count(*)']==0)
            {

                return true;
            }
            else
            {
                $query="select count(*) from events where slug=$unique_url and ownerid=".$this->session->socialid." and id = $eventid";
                $action=$this->db->query($query);
                $row=$action->result_array();
                if($row[0]['count(*)']==1) return true;
                return false;

            }
        }


    }
    public function add_logo($par)
    {
        // Required parameters
        // eventid
        // uploadid

        if($par['eventid']!="" && $par['uploadid']!="")
        {
            $esc_eventid = $par['eventid'];
            $uploadid= $par['uploadid'];
            $query = "Insert into event_logos(eventid,uploadid) values($esc_eventid,$uploadid)";
            $action = $this->db->query($query);
            if ($action) {
                $ret_par['status'] = "success";
                $ret_par['description'] = "File Uploaded Successfully";
                return $ret_par;
            }
            else {
                $ret_par['status'] = "error";
                $ret_par['description'] = "Temporary error";
                return $ret_par;
            }
        }
    }
    public function add_gallery_pic($par)
    {
        // Required parameters
        // eventid
        // uploadid

        if($par['eventid']!="" && $par['uploadid']!="")
        {
            $esc_eventid = $par['eventid'];
            $uploadid= $par['uploadid'];
            $query = "Insert into event_gallery(eventid,uploadid) values($esc_eventid,$uploadid)";
            $action = $this->db->query($query);
            if ($action) {
                $ret_par['status'] = "success";
                $ret_par['description'] = "File Uploaded Successfully";
                return $ret_par;
            }
            else {
                $ret_par['status'] = "error";
                $ret_par['description'] = "Temporary error";
                return $ret_par;
            }
        }
    }
    public function add_cover($par)
    {
        // Required parameters
        // eventid
        // uploadid

        if($par['eventid']!="" && $par['uploadid']!="")
        {
            $esc_eventid = $par['eventid'];
            $uploadid= $par['uploadid'];
            $query = "Insert into event_cover_pics(eventid,uploadid) values($esc_eventid,$uploadid)";
            $action = $this->db->query($query);
            if ($action) {
                $ret_par['status'] = "success";
                $ret_par['description'] = "File Uploaded Successfully";
                return $ret_par;
            }
            else {
                $ret_par['status'] = "error";
                $ret_par['description'] = "Temporary error";
                return $ret_par;
            }
        }
    }
    public function increment_views($id)
    {
        if(is_numeric($id))
        {
            $action=$this->db->query("update events set views = views + 1 where id=$id");
        }
    }

    public function scan($params=null) {

        if($params != null) {
            if(array_key_exists('id', $params)) {
                $this->db->where('id',$params['id']);
            }

            if(array_key_exists('admin_approved', $params)) {
                $this->db->where('admin_approved',$params['admin_approved']);
            }

            if(array_key_exists('admin_approved', $params)) {
                $this->db->where('admin_approved',$params['admin_approved']);
            }    
        }
        

        $action=$this->db->get(self::TABLE);
        
        if(!$action){
            return array(
                'status'=>'failure',
                'description'=>'DB error while fetching event',
                'error'=>$this->db->error()
            );

        }

        $result=$action->result_array();

        return array(
            'status'=>'success',
            'data'=>$result
        );

    }

    public function get2($params=array()) {

        
        if(array_key_exists('id',$params)) {
            $this->db->where('id',$params['id']);
        }

        if(array_key_exists('order_by_id',$params)) {
            $this->db->order_by('id',$params['order_by_id']);
        }


        if(array_key_exists('order_by_date_start',$params)) {
            $this->db->order_by('date_start',$params['order_by_date_start']);
        }

        if(array_key_exists('limit',$params)) {
            $this->db->limit($params['limit']);
        }



        $action=$this->db->get(self::TABLE);
        if(!$action) {
            return array(
                'status'=>'failure',
                'description'=>'DB Error',
                'error'=>$this->db->error()
            );
        }

        $result=$action->result_array();

        $return_data = array();

        foreach($result as &$row) {
            $temp_id=$row['id'];
            $row['cover_upload_id']=$this->get_cover_pic($row['id']);
            $row['logo_upload_id']=$this->get_logo($row['id']);
            $return_data[$temp_id]=$row;

        }

        return array(
            'status'=>'success',
            'data'=>$return_data
        );

    }


}
?>