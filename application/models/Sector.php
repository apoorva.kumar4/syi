<?php
/**
 * Created by PhpStorm.
 * User: aashay
 * Date: 20/1/17
 * Time: 3:24 PM
 */
class Sector extends CI_Model{
    public function delete($id)
    {
        if(_f_is_admin())
        {
            if(is_numeric($id))
            {
                $this->db->where('id',$id);
                $action=$this->db->delete('sectors');
                if($action)
                {
                    $data['status']="success";
                    $data['description']="sector deleted successfully";
                    return $data;
                }
                if($this->db->_error_message())
                {
                    $data['status']="failure";
                    $data['description']="Sector already in use by Ideas/Users";
                    return $data;
                }
                else{
                    $data['status']="failure";
                    $data['description']="Sector already in use by Ideas/Users";
                    return $data;
                }

            }
            else{
                $data['status']="failure";
                $data['description']="bad request";
                return $data;

            }
        }
        else{
            $data['status']="failure";
            $data['description']="Unauthorized";
            return $data;
        }
    }
    public function create($name)
    {
        if(_f_is_admin())
        {
            $prep_data = array('name'=>$name);
            $this->db->db_debug = FALSE; //disable debugging for queries
            $action=$this->db->set($prep_data)->insert('sectors');
            if($action){
                $id=$this->db->insert_id();
                $data['id']=$id;
                $data['name']=$name;
                $data['status']="success";
                $data['description']="sector added successfully";
                return $data;
            }
            else{
                $data['status']="failure";
                $err=$this->db->error();
                $data['description']=$err['message'];
                return $data;
            }

        }
        else{
            $data['status']="failure";
            $data['description']="Unauthorized";
            return $data;
        }
    }
    public function get_ideas_other_sectors(){
        $query="Select s.socialid,s.first_name as added_by, ideas.id as ideaid,ideas.name as idea_name,ideas.unique_url as idea_url,ideas.sector_other as name from ideas join users s on s.socialid = ideas.ownerid where (ideas.sector_other is not null and ideas.sector_other !='' and ideas.sector_other not in (select name from sectors ))";
        $action=$this->db->query($query);
        if($action){
            $res=$action->result_array();
            $data['status']="success";
            $data['sectors']=$res;
            return $data;

        }
    }
    public function get_users_other_sectors(){
        $query="Select s.socialid,s.first_name as added_by, s.sector_other as name,s.user_type from users s where s.sector_other not in (select name from sectors ) and s.sector_other != ''";
        $action=$this->db->query($query);
        if($action){
            $res=$action->result_array();
            $data['status']="success";
            $data['sectors']=$res;
            return $data;

        }
    }
    public function get()
    {
        $action=$this->db->get('sectors');
        if($action) {
            $result=$action->result_array();
            return $result;
        } else return false;
    }
    public function get_names(){
        $this->db->select('id,name');
        $sect=array();
        $action=$this->db->get('sectors');
        if($action) {
            $result=$action->result_array();
            foreach($result as $sector) {
                $sect[$sector['id']]=$sector['name'];
            }
            return $sect;
        } else return false;   
    }
}
?>