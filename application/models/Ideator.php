<?php

class Ideator extends CI_Model
{
    public function sign_up($items)
	{
        $data = array(
            'socialid' => $items['abe3e5_socialid'],
            'first_name' => $items['first_name'],
            'last_name' => $items['last_name'],
            'email' => $items['abe3e5_email'],
            'mobile' => $items['mobile'],
            'role' => $items['role'],
            'user_type' => $items['abe3e5_user_type'],
            'access_type' => $items['abe3e5_access_type'],
            'access_token' => $items['abe3e5_access_token'],
            'location' => $items['location'],
            'sector_expertise' => $items['sector_expertise'],
            'registration_complete' => 'Y',
            'password_hash' => $items['abe3e5_password_hash'],
            'username'=>$items['abe3e5_socialid'],
            'emailverified'=>$items['email_verified']
        );

        $data2 = array(
            'socialid' => $items['abe3e5_socialid'],
            'institute' => $items['institute'],
            'year_of_graduation' => $items['year_of_graduation'],
            'student_alumni' => $items['student_alumni']
        );

        // Depening upon if he is database or not, perform an insert or an update query

        $this->db->select('count(*)');
        $this->db->from('users');
        $this->db->where('socialid',$data['socialid']);
        $pre_query=$this->db->get();
        $row=$pre_query->row_array();

        if($row['count(*)']>0)
            {
                $this->db->where('socialid', $data['socialid']);
                $this->db->replace('users', $data);
                $this->db->replace('ideators', $data2);
                return 1;
            }
            else
            {

                foreach($data as $key=>$item)
                {
                    if(is_null($item))
                    {
                        return $data;
                    }

                }

                $this->db->insert('users', $data);
                $this->db->insert('ideators', $data2);
                return 1;
            }
	}
    public function _get_list_of_ideas($socialid)
	{

			
		$query="select * from view_ideas where ownerid=$socialid order by id";

		$action=$this->db->query($query);

		if($action)
		{
			return $action->result_array();
		}
	}
    public function _get_list_of_public_ideas($socialid)
    {


        $query="select * from view_ideas where ownerid=$socialid and (visibility like 'public' or visibility like 'visibile') order by id";

        $action=$this->db->query($query);

        if($action)
        {
            return $action->result_array();
        }
    }
    public function _get_list_of_events($socialid)
    {
        $query="select * from events where ownerid=$socialid order by id";

        $action=$this->db->query($query);

        if($action)
        {
            return $action->result_array();
        }
    }
	public function _is_idea_owned($pid)
	{
		$socialid=$this->session->userdata('socialid');
        $query="Select count(*) from ideas where ownerid=$socialid and id=$pid";
		$action = $this->db->query($query);
		
		if($action)
			{
				$row=$action->result_array();
				if($row[0]['count(*)']==1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}	
	}
}