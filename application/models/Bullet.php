<?php
class Bullet extends CI_Model
{
	public $id;
	public $name; // required
	public $ideaid; // required
	public $description;
	public $start_date; // required
	public $end_date;
	public $status;
	public $assigned;
	public $template;
    public function create()
	{
		//createx is used in the visjs timeline and in api v1/ideas/<id>/bullets/add

		$validated_name= isset($this->name)?$this->name:NULL;

		$validated_ideaid=isset($this->description)?$this->ideaid:NULL;
		
		$validated_description= isset($this->description)?_f_validate('description',$this->description):NULL;

		$validated_start_date=isset($this->start_date)?_f_validate('date',$this->start_date):NULL;

        $ins_array =array(
            'name'=>$validated_name,
            'ideaid'=>$validated_ideaid,
            'description'=>$validated_description,
            'start_date'=>$validated_start_date
        );
        $action=$this->db->insert('bullets',$ins_array);

		if($action)
		{	
			return $this->db->insert_id();	
		}
		else
		{
			return false;
		}


	}
	public function update()
	{
		$id=$this->id;
		$validated_name= isset($this->name)?$this->db->escape($this->name):NULL;

		$validated_ideaid=isset($this->description)?$this->db->escape($this->ideaid):NULL;	
		
		$validated_description= isset($this->description)?$this->db->escape(_f_validate('description',$this->description)):NULL;	

		$validated_start_date=isset($this->start_date)?$this->db->escape(_f_validate('date',$this->start_date)):NULL;	

		//$validated_end_date=isset($this->$end_date)?$this->db->escape(_f_validate('date',$this->$end_date)):NULL;

		//$validated_status=isset($this->status)?$this->db->escape(_f_validate('description',$this->status)):NULL;

		//$validated_assigned=isset($this->$assigned)?$this->db->escape($this->$assigned):NULL;
	
		$query="Update bullets set name=$validated_name,description=$validated_description,start_date=$validated_start_date where id=$id";

		$action=$this->db->query($query);

		if($action)
		{	
			return true;
		}
		else
		{
			return false;
		}


	}
    public function read()
	{
		if(isset($this->id) && !empty($this->id))
		{
			$query="Select * from bullets where id=$this->id";

			$action=$this->db->query($query);

			if($action)
			{
				$row=$action->result_array();
				$this->ideaid=$row[0]['ideaid'];
				$this->name=$row[0]['name'];
				$this->description=$row[0]['description'];
				$this->start_date=$row[0]['start_date'];
				$this->end_date=$row[0]['end_date'];
				$this->status=$row[0]['status'];
				$this->assigned=$row[0]['assigned'];
				$this->template=$row[0]['template'];
			}
			else
			{

			}
		}
		
	}
	public function get_bullet_from_id($id)
	{
		if(isset($id) && !empty($id))
		{
			$query="Select id,ideaid,description,start_date,name from bullets where id=$id";
			$action=$this->db->query($query);

			if($action)
			{
				$row=$action->result_array();

				return $row[0];
			}
			else
			{
				return 0;
			}
		}
	}
	public function delete()
	{
		$query="delete from bullets where id=$this->id and template<>1";

		$action=$this->db->query($query);

		if($action)
		{
			if($this->db->affected_rows()==0)
			{
				return 0;
			}
			else
			{
				return 1;
			}

		}
		

	}

}
?>
