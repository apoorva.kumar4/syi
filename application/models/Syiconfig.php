<?php
class Syiconfig extends CI_Model {
	public function __construct() {
		$conf = $this->get_all();
		$CI = &get_instance();
		$CI->config->config += $conf;
	}
	public function get($key) {
		$action=$this->db->get_where('config',array('item'=>$key));
		
		if(!$action) {
			return false;
		}

		if(!$action->result_array())
		{
			return false;
		}
		
		$result=$action->result_array();
		return $result[0]['value'];
	}

	public function get_all() {
		$action=$this->db->get('config');
		$result=$action->result_array();
		$syi_config=array();
		foreach ($result as $inner_array) {
			$syi_config[$inner_array['item']]=$inner_array['value'];
		}
		return $syi_config;
	}
}
?>
