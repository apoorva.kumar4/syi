<?php
class About_us extends CI_Model{
    protected $properties = array(
        'section'=>null,
        'content'=>null
    );
    public function set_properties($array)
    {
        foreach($this->properties as $key=>$value)
        {
            $this->properties[$key]=isset($array[$key])?$array[$key]:null;
        }
    }
    public function update($array)
    {
        $this->set_properties($array);
        $copied_properties=$this->properties;
        $this->db->set($copied_properties);
        $action=$this->db->replace('about_us');
        if($action)
        {
            return 1;
        }
        else{
            return 0;
        }
    }
    public function read($section)
    {
        $this->db->select('section,content');
        $action=$this->db->get_where('about_us',array('section'=>$section));
        if($action)
        {
            $result=$action->result_array();
            return $result[0]['content'];
        }
    }
}
?>