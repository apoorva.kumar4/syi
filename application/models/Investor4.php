<?php
class Investor4 extends CI_Model {
	const TABLE = 'investors';
	
	public function get($key,$value) {
		$table=self::TABLE;
		$this->db->from($table);
		$this->db->where($key,$value);
		$action=$this->db->get();
		if(!$action) {
			return false;
		} else {
			$result=$action->result_array();
			if(!$result) return false;
			else return $result;
		}
	}

	public function update($socialid, $data) {
		$table=self::TABLE;
		$updateable_properties = array(
	        'industry',
        	'company',
        	'working_since',
        	'investment_range_start',
        	'investment_range_end',
	        'professional_background',
	        'alumni_network',
	        'investment_network',
	        'members_of_organisation',
	        'sample_companies_invested',
	        'investor_tags',
	        'allow_direct_contact',
	        'would_like_to_mentor_startups',
	        'currently_mentoring_any_startups',
	        'companies_associated_with_as_a_mentor',
	        'areas_of_expertise_how_do_i_add_value',
	        'currency'
	    );

		$filtered_array=array();

		foreach($updateable_properties as $key) {
			if(array_key_exists($key, $data)) {
				$filtered_array[$key]=$data[$key];	
			}
			
		}

		$this->db->where('socialid',$socialid);
		$this->db->set($filtered_array);
		$action=$this->db->update(self::TABLE);
		if(!$action) return $this->db->error();
		else return true;
	}

	public function insert($socialid,$data) {
		if($this->get('socialid',$socialid)){
			return "user already exists";
		}

		$insert_properties = array(
			'industry',
        	'company',
        	'working_since',
        	'investment_range_start',
        	'investment_range_end',
	        'professional_background',
	        'alumni_network',
	        'investment_network',
	        'members_of_organisation',
	        'sample_companies_invested',
	        'investor_tags',
	        'allow_direct_contact',
	        'would_like_to_mentor_startups',
	        'currently_mentoring_any_startups',
	        'companies_associated_with_as_a_mentor',
	        'areas_of_expertise_how_do_i_add_value',
	        'currency'
		);

		$filtered_array=array();
		foreach($insert_properties as $key) {
			if(array_key_exists($key, $data)) {
				$filtered_array[$key]=$data[$key];	
			}
			
		}

		$this->db->set('socialid',$socialid);
		$this->db->set($filtered_array);
		$action=$this->db->insert(self::TABLE);
		if(!$action) return $this->db->error();
		else return true;
	}

	public function get_profile_completeness($socialid) {
		$score=0;
		$u= $this->get('socialid',$socialid);

		if(!$u) return false;
        
        $total_properties= array (
	        'industry',
        	'company',
        	'working_since',
        	'investment_range_start',
        	'investment_range_end',
	        'professional_background',
	        'alumni_network',
	        'investment_network',
	        'members_of_organisation',
	        'sample_companies_invested',
	        'investor_tags',
	        'allow_direct_contact',
	        'would_like_to_mentor_startups',
	        'currently_mentoring_any_startups',
	        'companies_associated_with_as_a_mentor',
	        'areas_of_expertise_how_do_i_add_value',
	        'currency'
    	);

    	
        $total=count($total_properties);

        foreach ($total_properties as $key) {
            if($u[0][$key]!="" && $u[0][$key]!=null) {
                $score++;
            }
        }

        return round(($score/$total)*100);
	}

	public function delete($socialid){
		if(!$this->get('socialid',$socialid)) return false;

		$this->db->where('socialid',$socialid);
		$action=$this->db->delete(self::TABLE);
		if($action) return true;
		else return false;
	}

	public function get2($params=array()) {

        if(array_key_exists('socialid',$params)) {
                $this->db->where('socialid',$params['socialid']);
        }

        $action=$this->db->get(self::TABLE);
        if(!$action) {
            return array(
                'status'=>'failure',
                'description'=>'DB Error',
                'error'=>$this->db->error()
            );
        }

        $result=$action->result_array();

        $users = array();

        foreach($result as &$row) {
            $temp_socialid=$row['socialid'];
            $users[$temp_socialid]=$row;
        }

        return array(
            'status'=>'success',
            'data'=>$users
        );

    }	
}
?>