<?php
class Testimonial extends CI_Model{
    protected $properties = array(
        'first_name'=>null,
        'last_name'=>null,
        'company'=>null,
        'position'=>null,
        'description'=>null,
        'active'=>1,
        'profile_picture'=>null,
        'website'=>null

    );

    public function set_properties($array)
    {
        foreach($this->properties as $key=>$value)
        {
            if(array_key_exists($key,$array))
            {
                $this->properties[$key]=is_string($array[$key])?strip_tags($array[$key]):$array[$key];
            }

        }
    }
    public function create($array)
    {
        $this->set_properties($array);
        $insert_data=$this->properties;
        
        $action=$this->db->insert('testimonials',$insert_data);
        if($action)
        {
            $data['status']="success";
            $data['id']=$this->db->insert_id();
            return $data;
        }
    }
    public function read($id = null)
    {
        $action="";
        if($id==null)
        {
            $action=$this->db->get('testimonials');

        }
        else{
            if(is_numeric($id))
            {
                $action=$this->db->get_where('testimonials',array('id'=>$id));
            }
            else{
                $action=$this->db->get('testimonials');
            }
        }
        $result=$action->result_array();
        return $result;
    }
    public function activate($id)
    {
        if(is_numeric($id))
        {
            $query = "Update testimonials set active=1 where id=$id";
            $action = $this->db->query($query);
            if ($action) return 1;
            else return 0;
        }
        else{
            return 0;
        }

    }
    public function deactivate($id)
    {
        if(is_numeric($id)) {
            $query = "Update testimonials set active=0 where id=$id";
            $action = $this->db->query($query);
            if ($action) {
                return 1;
            } else {
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    public function delete($id){
        if(is_numeric($id)) {
            $query = "delete from testimonials where id=$id";
            $action = $this->db->query($query);
            if ($action) {
                return 1;
            } else {
                return 0;
            }
        }
        else{
            return 0;
        }
    }
}
?>