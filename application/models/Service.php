<?php
class Service extends CI_Model
{
    public $id;
    public $socialid;
    public $name;
    public $description;
    public $list_of_areas;
    public $servicetags;
    public $target_phases;
    public $target_sectors;

    public function create()
    {
        $validated_socialid = $this->session->socialid;

        $validated_name = isset($this->name) ? $this->db->escape($this->name) : NULL;

        $validated_description = isset($this->description) ? $this->db->escape($this->description) : NULL;

        $validated_list_of_areas = isset($this->list_of_areas) ? $this->db->escape($this->list_of_areas) : NULL;
        $query = "Insert into services(name,ownerid,description) values($validated_name,$validated_socialid,$validated_description)";

        $action = $this->db->query($query);

        if ($action) {

            $retrieved_serviceid = $this->db->insert_id();

            $counter = 0;
            foreach ($this->servicetags as $tag) {

                $esc_name = $this->db->escape($tag['tag']);
                if ($counter < 10) {
                    $query2 = "Insert into services_tags(serviceid,name) values($retrieved_serviceid,$esc_name)";
                    $action2 = $this->db->query($query2);
                    $counter++;
                } else {
                    $counter++;
                }

            }

            $counter_target_phase=0;
                    $validated_target_phases=$this->validate_target_phase();
                    foreach($validated_target_phases as $target_phase)
                    {
                        if($counter_target_phase < 3) {
                            $esc_target_phase=$this->db->escape($target_phase);
                            $query3="Insert into service_target_phase(serviceid,stage) values($retrieved_serviceid,$esc_target_phase)";

                                $action3=$this->db->query($query3);
                                if($action3)
                                {
                                    $counter_target_phase++;
                                }
                        }
                    }

            $counter_target_sector=0;
                    
                    foreach($this->target_sectors as $target_sector)
                    {
                        if($counter_target_sector < 10) {
                            
                            $query3="Insert into service_target_sector(serviceid,sectorid) values($retrieved_serviceid,$target_sector)";

                                $action3=$this->db->query($query3);
                                if($action3)
                                {
                                    $counter_target_sector++;
                                }
                        }
                    }

                    return 1;
                    
        } else {
            return 0;
        }


    }
    public function validate_target_phase()
    {
        $validated_target_phases;
        foreach($this->target_phases as $target_phase)
        {
            if($target_phase==="Ideation")
            {
                $validated_target_phases[]=$target_phase;
            }
            else if ($target_phase==="Prototype"){
                $validated_target_phases[]=$target_phase;
            }
            else if ($target_phase==="Launching Soon"){
                $validated_target_phases[]=$target_phase;
            }
            else if ($target_phase==="Operational"){
                $validated_target_phases[]=$target_phase;
            }
            else if ($target_phase==="Expansion"){
                $validated_target_phases[]=$target_phase;
            }
        }
        return $validated_target_phases;
    }
    public function read($id)
    {
        $query = "Select * from services where id=$id";

        $action = $this->db->query($query);

        if ($action) {
            $row = $action->result_array();
            if ($row) {
                return $row;    
            } else {
                return false;
            }
            
        } else {
            return 0;
        }

    }
    public function patch()
    {
        $validated_id = $this->id;
        $validated_name = isset($this->name) ? $this->db->escape($this->name) : NULL;
        $validated_description = isset($this->description) ? $this->db->escape($this->description) : NULL;

        $validated_list_of_areas = isset($this->list_of_areas) ? $this->db->escape($this->list_of_areas) : NULL;

        $query = "Update services set name=$validated_name,description=$validated_description where id=$validated_id";

        $action = $this->db->query($query);

        if ($action) {
            $counter = 0;
            $query_del = "Delete from services_tags where serviceid=$this->id";
            $action_del = $this->db->query($query_del);
            foreach ($this->servicetags as $tag) {

                $esc_name = $this->db->escape($tag['tag']);
                if ($counter < 10) {
                    $query2 = "Insert into services_tags(serviceid,name) values($validated_id,$esc_name)";
                    $action2 = $this->db->query($query2);
                    $counter++;
                } else {
                    $counter++;
                }

            }

            $counter_target_phase=0;
                    $validated_target_phases=$this->validate_target_phase();
                    $query_del = "Delete from service_target_phase where serviceid=$this->id";
                    $action_del = $this->db->query($query_del); 
                    foreach($validated_target_phases as $target_phase)
                    {
                        if($counter_target_phase < 3) {
                            $esc_target_phase=$this->db->escape($target_phase);
                            $query3="Insert into service_target_phase(serviceid,stage) values($this->id,$esc_target_phase)";

                                $action3=$this->db->query($query3);
                                if($action3)
                                {
                                    $counter_target_phase++;
                                }
                        }
                    }

            $counter_target_sector=0;
                    $query_del = "Delete from service_target_sector where serviceid=$this->id";
                    $action_del = $this->db->query($query_del); 
                    foreach($this->target_sectors as $target_sector)
                    {
                        if($counter_target_sector < 10) {
                            
                            $query3="Insert into service_target_sector(serviceid,sectorid) values($this->id,$target_sector)";

                                $action3=$this->db->query($query3);
                                if($action3)
                                {
                                    $counter_target_sector++;
                                }
                        }
                    }

            return 1;

        }
    }
    public function delete()
    {
        $query = "delete from servicess where id=$this->id";

        $action = $this->db->query($query);

        if ($action) {

        } else {

        }

    }
    public function get_tags()
    {
        $id = $this->id;
        $query = "select id,name,description from services_tags where serviceid=$id";

        $action = $this->db->query($query);

        $row = $action->result_array();

        return $row;
    }
    public function get_tag_names()
    {
        $id = $this->id;
        $query = "select name from services_tags where serviceid=$id";

        $action = $this->db->query($query);

        $row = $action->result_array();

        return $row;
    }
    public function _is_service_owned($checkid)
    {
        $socialid=$this->session->socialid;
        $id=$checkid;
        $query="Select count(*) from services where id=$id and ownerid=$socialid";
        $action=$this->db->query($query);
        if($action)
        {
            $row=$action->result_array();
            if($row[0]["count(*)"]==1)
            {
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    public function update_tags()
    {
        $id=$this->id;
        $query="Delete from services_tags where id=$id";
        $delete_action=$this->db->query($query);
        if($delete_action)
        {

            foreach($this->servicetags as $tag)
            {
                $esc_name=$this->db->escape($name=$tag['tag']);
                $query="Insert into services_tags(serviceid,name) values($id,$esc_name)";
                $action=$this->query($query);

            }

        }
    }
    public function get_services_for_user($socialid)
    {

        $query="select * from services where ownerid=$socialid order by id";

        $action=$this->db->query($query);

        if($action)
        {
            return $action->result_array();
        }
    }
    
}
?>