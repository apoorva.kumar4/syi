<?php
class Idea4 extends CI_Model {
	const TABLE="ideas";
	const TABLE_IDEAS_FOLLOWERS="ideas_followers";
	const TABLE_IDEA_COVER_PICS="idea_cover_pics";
	const TABLE_IDEA_LOGOS="idea_logos";
	public function get_public_ideas_for_user($socialid) {
		$this->db->where(array('ownerid'=>$socialid,'visibility'=>1));
		$action=$this->db->get(self::TABLE);
		if(!$action) return false;
		$result=$action->result_array();
		if(!$result) return false;
		return $result;
	}

	public function get_ideas_followed_by_user($socialid) {
		$this->db->where(array('socialid'=>$socialid));
		$action=$this->db->get(self::TABLE_IDEAS_FOLLOWERS);
		if(!$action) return false;
		$result=$action->result_array();
		if(!$result) return array();
		return $result;
	}
	public function get_idea_phase($ideaid) {
		$table="view_ideas";
		$this->db->where('id',$ideaid);
		$action=$this->db->get($table);
		if(!$action) return false;
		$result=$action->result_array();
		if(!$result) return false;
		return $result[0]['stage'];

	}
	public function get_top_ideas($limit=3) {
		$table="view_ideas";
		$q_query="select ti.ideaid,ti.rank,i.* from top_ideas ti left join view_ideas i on ti.ideaid=i.id order by ti.rank asc limit $limit";
		$action=$this->db->query($q_query);
		if(!$action) return false;
		$result=$action->result_array();
		if(!$result) return array();
		return $result;

	}

	public function get($params) {
		// Current limititations: Data provided to owner and admin only
		$idea_data=array();

		if(array_key_exists('id',$params)) {
			$this->db->where('id',$params['id']);
		}

		$action=$this->db->get(self::TABLE);
		if($action) {
			$result = $action->result_array();	
			foreach($result as &$idea) {
				$idea['logo_url']=$this->get_logo_url($idea['id']);
				$idea['cover_url']=$this->get_cover_url($idea['id']);
			}
			return $result;
		}
		else return array();
	}

	public function get_logo_url($id) {
		$query = "select url from uploads where id in (Select max(uploadid) from idea_logos where ideaid=$id)";
		$action = $this->db->query($query);
		if ($action) {
			$row = $action->result_array();
				if ($row){
					$remoteImage = $this->config->item('upload_path') . $row[0]['url'];  
					return $remoteImage;
				} 
				else {
					return $this->config->item('upload_path') . "bulb.png";
				}
		}
		else {
			return $this->config->item('upload_path') . "bulb.png";
		}		
	}
	public function get_cover_url($id) {
		$query = "select url from uploads where id in (Select max(uploadid) from idea_cover_pics where ideaid=$id)";
		$action = $this->db->query($query);
		if ($action) {
			$row = $action->result_array();
				if ($row){
					$remoteImage = $this->config->item('upload_path') . $row[0]['url'];  
					return $remoteImage;
				} 
				else {
					return $this->config->item('upload_path') . "image-2.jpg";
				}
		}
		else {
			return $this->config->item('upload_path') . "bulb.png";
		}
	}

	public function get_cover_id($id) {
		$this->db->limit(1);
		$this->db->order_by('id');
		$this->db->where('ideaid',$id);
		$action=$this->db->get(self::TABLE_IDEA_COVER_PICS);

		if (!$action) {
			return array('status'=>'failure','error'=>$this->db->error());
		}
		
		$row = $action->result_array();
		
		if(!$row) return false;
		
		return $row[0]['uploadid'];
	}

	public function get_logo_id($id) {
		$this->db->limit(1);
		$this->db->order_by('id');
		$this->db->where('ideaid',$id);
		$action=$this->db->get(self::TABLE_IDEA_LOGOS);

		if (!$action) {
			return array('status'=>'failure','error'=>$this->db->error());
		}

		$row = $action->result_array();
		if(!$row) return false;
		
		return $row[0]['uploadid'];
	}

	public function update($params) {
		// properties that can be updated
		$properties = array(
			'name',
		    'tagline',
		    'stage',
		    'description',
		    'visibility',
		    'sectorid',
		    'sector_other',
		    'address',
		    'website',
		    'company',
		    'patented',
		    'patent_number',
		    'achievements',
		    'business_plan',
		    'future_prospects',
		    'proof_of_concept',
		    'revenue_model',
		    'facebook_link',
		    'linkedin_link',
		    'twitter_link',
		    'pinterest_link',
		    'youtube_link',
		    'team_overview',
		    'executive_summary',
		    'tags',
		    'market_size_usp',
		    'services_required'
		);

		$required_properties = array(
			'name',
		    'description',
		    'address',
		    'business_plan',
		    'future_prospects',
		    'facebook_link',
		    'linkedin_link',
		   	'market_size_usp',
		    
		);

		$filtered_data = array();

		// if required properties are not set or are null then return
		foreach($properties as $property) {

			if(array_key_exists($property, $required_properties)) {
				if(!array_key_exists($property, $params)) {
					$return_array = array (
						'status'=>'failure',
						'description'=>$property.' is required'
					);
					return $return_array;
				}
				if($params[$property]==null || $params[$property]=="") {
					$return_array = array (
						'status'=>'failure',
						'description'=>$property.' cannot be null'
					);
					return $return_array;	
				}

			}

			if(array_key_exists($property, $params)) {
				if(is_array($params[$property])) {
					$sanitized= json_encode($params[$property]);
				}
				else {
					$sanitized=$params[$property];
				}
				
				$sanitized=strip_tags($sanitized);
				$filtered_data[$property]=$sanitized;
			}
			
		}
		
		$this->db->where('id',$params['id']);
		$this->db->set($filtered_data);
		$action=$this->db->update(self::TABLE);
		if($action) 
			return array('status'=>'success');
		else 
			return array('status'=>'failure','description'=>$this->db->error());
	}

	public function add($params) {
		$properties = array(
			'ownerid',
			'name',
		    'tagline',
		    'stage',
		    'description',
		    'visibility',
		    'sectorid',
		    'sector_other',
		    'address',
		    'website',
		    'company',
		    'patented',
		    'patent_date',
		    'achievements',
		    'business_plan',
		    'future_prospects',
		    'proof_of_concept',
		    'revenue_model',
		    'facebook_link',
		    'linkedin_link',
		    'twitter_link',
		    'pinterest_link',
		    'youtube_link',
		    'team_overview',
		    'executive_summary',
		    'tags',
		    'market_size_usp',
		    'services_required'
		);

		$required_properties = array(
			'name',
		    'description',
		    'address',
		    'business_plan',
		    'future_prospects',
		    'facebook_link',
		    'linkedin_link',
		   	'market_size_usp',
		    
		);

		$filtered_data = array();

		// if required properties are not set or are null then return
		foreach($properties as $property) {

			if(array_key_exists($property, $required_properties)) {
				if(!array_key_exists($property, $params)) {
					$return_array = array (
						'status'=>'failure',
						'description'=>$property.' is required'
					);
					return $return_array;
				}
				if($params[$property]==null || $params[$property]=="") {
					$return_array = array (
						'status'=>'failure',
						'description'=>$property.' cannot be null'
					);
					return $return_array;	
				}

			}

			if(array_key_exists($property, $params)) {
				if(is_array($params[$property])) {
					$sanitized= json_encode($params[$property]);
				}
				else {
					$sanitized=$params[$property];
				}
				
				$sanitized=strip_tags($sanitized);
				$filtered_data[$property]=$sanitized;
			}
			
		}
		
		
		$this->db->set($filtered_data);
		$action=$this->db->insert(self::TABLE);
		if($action) 
			return array('status'=>'success');
		else 
			return array('status'=>'failure','description'=>$this->db->error());
		
	}

	public function get2($params=array()) {

        if(array_key_exists('id',$params)) {
            $this->db->where('id',$params['id']);
        }

        if(array_key_exists('limit',$params)) {
            $this->db->limit($params['limit']);
        }

        if(array_key_exists('order_by_id',$params)) {
            $this->db->order_by('id',$params['order_by_id']);
        }


        $action=$this->db->get(self::TABLE);
        if(!$action) {
            return array(
                'status'=>'failure',
                'description'=>'DB Error',
                'error'=>$this->db->error()
            );
        }

        $result=$action->result_array();

        $ideas = array();

        foreach($result as &$row) {
            $temp_id=$row['id'];
            $row['logo_upload_id']=$this->get_logo_id($row['id']);
			$row['cover_upload_id']=$this->get_cover_id($row['id']);
			$row['cover_upload_id']=$this->get_cover_id($row['id']);
			$row['current_phase']=$this->get_current_phase($row['id']);
			$row['followers']=$this->get_followers($row['id']);
            $ideas[$temp_id]=$row;
        }

        return array(
            'status'=>'success',
            'data'=>$ideas
        );

    }

    public function count() {
    	$this->db->select('count(*)');
    	$action=$this->db->get(self::TABLE);
    	if(!$action) return 0;

    	$result=$action->result_array();
    	if(!$result) return 0;
    	return $result[0]['count(*)'];

    }
	public function get_current_phase($id)
	{
		
		$query="select id,name,start_date from bullets where ideaid=$id and template=1 and start_date <= CURDATE() order by start_date desc limit 1;";
		$action=$this->db->query($query);
		if($action){
			$row=$action->result_array();
			if($row) return $row[0]['name'];
			else return '';
		}
		else{
			return '';
		}
	}
	public function get_followers($id) {
		$q_query="Select count(*) from ideas_followers where ideaid=$id";
		$action = $this->db->query($q_query);
		$row=$action->result_array();
		if($row)
			return $row[0]['count(*)'];
		else return 0;
	}
}
?>
