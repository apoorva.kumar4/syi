<?php
class Idea extends CI_Model
{

    protected $properties = array(
        'name' => null,
        'ownerid'=>null,
        'tagline' => null,
        'stage' => null,
        'description' => null,
        'start_date' => null,
        'end_date' => null,
        'unique_url' => null,
        'visibility' => 0,
        'sectorid' => null,
        'sector_other'=>null,
        'address' => null,
        'website' => null,
        'company' => null,
        'patented' => 'No',
        'patent_number' => null,
        'achievements' => null,
        'competitions' => null,
        'business_plan' => null,
        'future_prospects' => null,
        'proof_of_concept' => null,
        'revenue_model' => null,
        'facebook_link' => null,
        'linkedin_link' => null,
        'twitter_link' => null,
        'pinterest_link' => null,
        'youtube_link' => null,
        'team_overview' => null,
        'executive_summary' => null,
        'team_members' => array (),
        'cover_uploadid'=> null,
        'logo_uploadid' => null,
        'tags'=>null,
        'market_size_usp'=>null,
        'services_required'=>null,
        'link_other'=>null

    );

    // strips html tags from the data and santises it
    public function set_properties($array)
    {
        foreach($this->properties as $key=>$value)
        {
            if(array_key_exists($key,$array))
            {
                if($key=="visibility")
                {
                    if($array[$key]=="Public" || $array[$key]==1)
                        $this->properties[$key]=1;
                    else
                        $this->properties[$key]="0";
                }
                else{
                    $this->properties[$key]=is_string($array[$key])?strip_tags($array[$key]):$array[$key];
                }
            }

        }
    }
    public function create($par)
    {
        $this->set_properties($par);
        $ava=$this->check_url_availability($this->properties['unique_url']);

        //check for start_date and end_date gap of 6 months
        $start_epoch = strtotime($par['start_date']);
        $end_epoch = strtotime($par['end_date']);

        if(!($start_epoch && $end_epoch)) {
            $ret_status=array('status'=>'error','description'=>'Dates are invalid');
            return $ret_status;
        }

        if(($end_epoch - $start_epoch) <= 7776000) {
            $ret_status=array('status'=>'error','description'=>'There should be 6 months gap between start date and end date');
            return $ret_status;
        }

        if(!$ava) {
            $uuu=$par['unique_url'];
            $description="URL $uuu already Taken";
            $ret_status=array('status'=>'error','description'=>$description);
            return $ret_status;
        } else {
            
            $insert_data=$this->properties; // We copy the global variable array into our local copy
            $team_members=$insert_data['team_members']; // assign team members into a temp variable
            $cover_uploadid=$insert_data['cover_uploadid'];
            $logo_uploadid=$insert_data['logo_uploadid'];
            unset($insert_data['team_members']); // Remove the team members array because it goes into a different table
            unset($insert_data['cover_uploadid']); // Remove the cover_uploadid because it goes into a different table
            unset($insert_data['logo_uploadid']); // Remove the logo_uploadid because it goes into a different table

            $action=$this->db->insert('ideas',$insert_data); // Finally perform the insert
            $retrieved_ideaid=$this->db->insert_id(); // Store the insert id
            if($action)
            {

                // STEP 1 : Proceed to add team members for the idea
                $this->add_team_members($retrieved_ideaid,$team_members);

                // STEP 2 : Proceed to create auto bullets for the idea
                $this->create_auto_bullets3($retrieved_ideaid,$insert_data['start_date'],$insert_data['end_date'],$insert_data['stage']);

                // STEP 3 : Proceed to add Cover Photo
                $tem_arr_cover=array('ideaid'=>$retrieved_ideaid, 'uploadid'=>$cover_uploadid);
                $this->add_cover($tem_arr_cover);

                // STEP 4: Proceed to add Logo
                $tem_arr_logo=array('ideaid'=>$retrieved_ideaid, 'uploadid'=>$logo_uploadid);
                $this->add_logo($tem_arr_logo);

                // STEP 5 : Proceed to upload documents


                // Finally after all done, return the success status and return
                // the newly created idea's id
                $data['status']="success";
                $data['description']="";
                $data['ideaid']=$retrieved_ideaid;
                return $data;

            }
            else
            {
                return 0;
            }
        }
    }
	public function read($id = null)
	{
		// Get idea from database and assign all the variables
		$id=is_numeric($id)?$id:null;
        if($id==null)
        {
            $query="select id, name,description,ownerid,sectorid,start_date,tagline from ideas where visibility=1";
            $action=$this->db->query($query);

            if($action)
            {
                $row=$action->result_array();
                if(isset($row)) return $row;
                else return $row;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            $query="select * from ideas where id=$id";
            $action=$this->db->query($query);

            if($action)
            {
                $row=$action->result_array();
                if(isset($row[0])) return $row[0];
                else return $row;
            }
            else
            {
                return 0;
            }
        }

	}
	public function patch($array)
    {
        $this->set_properties($array);
        $fetched_idea=$this->read($array['id']);
        $insert_data=array();
        foreach($this->properties as $key=>$value)
        {
            if(!is_null($this->properties[$key]))
            {
                $insert_data[$key]=$value;
            }
        }; // We copy only the variables which are not null from global variable array into our local copy
        unset($insert_data['team_members']); // Remove the team members array because it goes into a different table
        unset($insert_data['cover_uploadid']); // Remove the cover_uploadid because it goes into a different table
        unset($insert_data['logo_uploadid']); // Remove the logo_uploadid because it goes into a different table

        if(isset($array['unique_url']))
        {
            if($fetched_idea['unique_url']==$array['unique_url'])
            {
                unset($insert_data['unique_url']);
            }
            else
            {
                $ava=$this->check_url_availability($array['unique_url']);
                if(!$ava)
                {
                    $uuu=$array['unique_url'];
                    $description="URL $uuu already Taken";
                    $ret_status=array('status'=>'error','description'=>$description);
                    return $ret_status;
                }
            }
        }

        $this->db->set($insert_data);
        $this->db->where('id',$array['id']);
        $action=$this->db->update('ideas'); // Finally perform the update

        if($action)
        {
            $data['status']="success";
            $data['description']="Idea edited successfully";
            return $data;
        }
        else
        {
            return 0;
        }
    }
	public function update($array)
    {
        $this->set_properties($array);
        $fetched_idea=$this->read($array['id']);
        $insert_data=$this->properties; // We copy the global variable array into our local copy
        unset($insert_data['team_members']); // Remove the team members array because it goes into a different table
        unset($insert_data['cover_uploadid']); // Remove the cover_uploadid because it goes into a different table
        unset($insert_data['logo_uploadid']); // Remove the logo_uploadid because it goes into a different table
        unset($insert_data['ownerid']); // Can't change ownerid

        if(isset($array['unique_url']))
        {
            if($fetched_idea['unique_url']==$array['unique_url'])
            {
                unset($insert_data['unique_url']);
            }
            else
            {
                $ava=$this->check_url_availability($array['unique_url']);
                if(!$ava)
                {
                    $uuu=$array['unique_url'];
                    $description="URL $uuu already Taken";
                    $ret_status=array('status'=>'error','description'=>$description);
                    return $ret_status;
                }
            }
        }

        $this->db->set($insert_data);
        $this->db->where('id',$array['id']);
        $action=$this->db->update('ideas'); // Finally perform the update

        if($action)
        {
            $data['status']="success";
            $data['description']="Idea edited successfully";
            return $data;
        }
        else
        {
            return 0;
        }


    }
    public function delete($id)
    {
        $id=is_numeric($id)?$id:null;
        if($id==null)
        {

        }
        else{
            $query="delete from ideas where id=$id";

            $action=$this->db->query($query);

            if($action)
            {
                return 1;
            }
            else
            {
                return 0;

            }
        }
    }
	public function check_url_availability($unique_url)
	{
		$unique_url=$this->db->escape($unique_url);
		$query="select count(*) from ideas where unique_url=$unique_url";

		$action=$this->db->query($query);
		
		if($action)
		{
			$row=$action->result_array();
			if($row[0]['count(*)']==0)
			{
				return true;
			}
			else
			{
				return false;

			}
		}
		else
        {
            return false;
        }


	}
	public function check_url_update_availability($unique_url)
	{
		$unique_url=$this->db->escape($unique_url);
		$query="select count(*) from ideas where unique_url=$unique_url";

		$action=$this->db->query($query);
		
		if($action)
		{
			$row=$action->result_array();
			if($row[0]['count(*)']==0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		


	}
    public function get_bullets_for_timeline($id)
	{

		$query="select id,name as content, start_date as start,template,description from bullets where ideaid=$id";

		$action=$this->db->query($query);

		$row=$action->result_array();
		
		return $row;
	}
    public function create_auto_bullets3($ideaid,$start_date,$end_date,$phase)
    {
        //echo "entered create_auto_bullets2"; die(0);
        // First get the start date, end date and convert it to DateTime format
        $dt_start=DateTime::createFromFormat("Y-m-d", $start_date);
        $dt_end=DateTime::createFromFormat("Y-m-d", $end_date);
        $d1=$dt_start;
        $d2=$dt_end;
        $dt_pot=$dt_start; // $dt_pot acts as a variable because it is added intervals to add consecutive bullets


        $dt_now=DateTime::createFromFormat("Y-m-d",date('Y-m-d')); // Store the current date in $dt_now
        $index=0;
        $fetch_current_phase_bullet=$this->db->get('default_idea_phases'); // Get the default phases list
        if($fetch_current_phase_bullet)
        {
            $row=$fetch_current_phase_bullet->result_array();
            foreach($row as $key=>$item)
            {
                if($item['name']==$phase) //Check if the selected phase matches the item
                {

                    $index=$key; // it matches to set the index to the $key so that we can split the array using this $index

                }
            }

            // Split phases into 2 parts based on the current phase
            $rest_of_the_stuff=array_slice($row,$index+1);
            $before_stuff=array_slice($row,0,$index);

            // Insert the very first bullet

            $prep_first_bullet= array(
                'name'=>$row[0]['name'],
                'ideaid'=>$ideaid,
                'description'=>$row[0]['description'],
                'start_date'=>$dt_start->format('Y-m-d'),
                'template'=>1
            );

            $sql=$this->db->set($prep_first_bullet)->insert('bullets');
            //$sql=$this->db->set($prep_first_bullet)->get_compiled_insert('bullets');

            //echo $sql."<br>";

            if($index!=0)
            {
                unset($before_stuff[0]);
            }

            // Add the last phase bullet to the database

            $prep_data= array(
                'name'=>$row[sizeof($row)-1]['name'],
                'ideaid'=>$ideaid,
                'description'=>$row[sizeof($row)-1]['description'],
                'start_date'=>$dt_end->format('Y-m-d'),
                'template'=>1
            );
            $sql=$this->db->set($prep_data)->insert('bullets');
            //$sql=$this->db->set($prep_data)->get_compiled_insert('bullets');
            //echo $sql."<br>";
            // Remove the last bullet from the array
            unset($rest_of_the_stuff[sizeof($rest_of_the_stuff)-1]);

            // Add the current phase bullet to the database
            if(($index!=0 && $index!=sizeof($row)-1))
            {
                $prep_data= array(
                    'name'=>$row[$index]['name'],
                    'ideaid'=>$ideaid,
                    'description'=>$row[$index]['description'],
                    'start_date'=>$dt_now->format('Y-m-d'),
                    'template'=>1
                );
                //$sql=$this->db->set($prep_data)->get_compiled_insert('bullets');
                $sql=$this->db->set($prep_data)->insert('bullets');
                //echo $sql."<br>";
            }


            // prepeare to create bullets for 1st half
            // Determine the interval between the bullets

            $count_bf=sizeof($before_stuff);

            //echo "count_bf=".$count_bf."<br>";

            if($count_bf)
            {
                $diff= $d1->diff($dt_now)->format('%a');

                //echo "diff1=".$diff."<br>";

                $interval=intval($diff/($count_bf+1));
                $date_interval= new DateInterval('P'.$interval.'D');
                $dt_pot=date_add($dt_start,$date_interval);
                foreach($before_stuff as $item)
                {
                    $prep_data= array(
                        'name'=>$item['name'],
                        'ideaid'=>$ideaid,
                        'description'=>$item['description'],
                        'start_date'=>$dt_pot->format('Y-m-d'),
                        'template'=>1
                    );

                    //$sql=$this->db->set($prep_data)->get_compiled_insert('bullets');
                    $sql=$this->db->set($prep_data)->insert('bullets');
                    $dt_pot=date_add($dt_pot,$date_interval);
                    //echo $sql."<br>";

                }
            }

            $count_af=sizeof($rest_of_the_stuff);

            //echo "count_af=".$count_af."<br>";

            if($count_af)
            {
                $diff2= $d2->diff($dt_now)->format("%a"); // $d2 is the difference between the end date and currrent date

                //echo "diff2=".$diff2."<br>";

                $interval=intval($diff2/($count_af+1));

                //echo "interval=".$interval."<br>";

                $date_interval= new DateInterval('P'.$interval.'D');
                $dt_pot=date_add($dt_now,$date_interval);
                foreach($rest_of_the_stuff as $item)
                {
                    $prep_data= array(
                        'name'=>$item['name'],
                        'ideaid'=>$ideaid,
                        'description'=>$item['description'],
                        'start_date'=>$dt_pot->format('Y-m-d'),
                        'template'=>1
                    );

                    //$sql=$this->db->set($prep_data)->get_compiled_insert('bullets');
                    $sql=$this->db->set($prep_data)->insert('bullets');
                    //echo $sql."<br>";
                    $dt_pot=date_add($dt_pot,$date_interval);

                }
            }

            return 1;

            // End of adding bullets
        }
        else
        {
            // since no phases were retrieved return 0 indicating error
            return 0;
        }
    }
    public function insert_document($array){
        // Required parameters
        // ideaid
        // uploadid
        // name
        // Returns 1 on success
        if($array['ideaid']!="" && $array['uploadid']!="")
        {
            $ins_array=array(
                'ideaid'=>$array['ideaid'],
                'uploadid'=>$array['uploadid'],
                'name'=>$array['name']
            );
            $this->db->set($ins_array);
            $action = $this->db->insert('idea_uploads');
            if ($action) {
               return 1;
            }
            else {
                $ret_par['status'] = "error";
                $ret_par['description'] = "Temporary error";
                return $ret_par;
            }
        }
    }
    public function delete_document($id){
        // Required parameters
        // id of the document
        // Returns 1 on success
        if(is_numeric($id))
        {

            $this->db->where('id',$id);
            $action = $this->db->delete('idea_uploads');
            if ($action) {
                return 1;
            }
            else {
                $ret_par['status'] = "error";
                $ret_par['description'] = "Temporary error";
                return $ret_par;
            }
        }
    }
    public function get_documents($checkid)
    {

        $query="select idea_uploads.id,idea_uploads.name as upload_name,uploads.* from idea_uploads left join uploads on idea_uploads.uploadid=uploads.id where idea_uploads.ideaid=$checkid";
        $action=$this->db->query($query);
        if($action)
        {
            $row=$action->result_array();
            return $row;
        }
    }
    public function get_notes($id)
    {

        $query="select * from idea_notes where ideaid=$id limit 1";
        $action=$this->db->query($query);
        if($action)
        {
            $row=$action->result_array();
            return $row;
        }
    }
    public function get_note_for_idea($id)
    {

        $query="select * from idea_notes where ideaid=$id order by id desc limit 1";
        $action=$this->db->query($query);
        if($action)
        {
            $row=$action->result_array();
            return $row;
        }
    }
    public function add_logo($par)
    {
        // Required parameters
        // ideaid
        // uploadid

        if($par['ideaid']!="" && $par['uploadid']!="")
        {
            $esc_ideaid = $par['ideaid'];
            $uploadid= $par['uploadid'];
            $query = "Insert into idea_logos(ideaid,uploadid) values($esc_ideaid,$uploadid)";
            $action = $this->db->query($query);
            if ($action) {
                $ret_par['status'] = "success";
                $ret_par['description'] = "File Uploaded Successfully";
                return $ret_par;
            }
            else {
                $ret_par['status'] = "error";
                $ret_par['description'] = "Temporary error";
                return $ret_par;
            }
        }
    }
    public function add_cover($par)
    {
        if ($par['uploadid'] != "" && $par['ideaid'] != ""){
            $esc_ideaid = $par['ideaid'];
            $uploadid= $par['uploadid'];
            $query = "Insert into idea_cover_pics(ideaid,uploadid) values($esc_ideaid,$uploadid)";
            $action = $this->db->query($query);
            if ($action) {
                $ret_par['status'] = "success";
                $ret_par['description'] = "File Uploaded Successfully";
                return $ret_par;
            }
            else {
                $ret_par['status'] = "error";
                $ret_par['description'] = "Temporary error";
                return $ret_par;
            }
        }
        else return 0;
    }
    public function add_team_members($retrieved_ideaid,$team_members)
    {

        // TODO: Define the structure of $team_members array here

        foreach($team_members['name'] as $key=>$member)
        {
            if(!$team_members['name'][$key]=="")
            {
                $cur_member_name=isset($team_members['name'][$key])?$team_members['name'][$key]:null;
                $cur_member_email=isset($team_members['email'][$key])?$team_members['email'][$key]:null;
                $cur_member_linkedin=isset($team_members['linkedin'][$key])?$team_members['linkedin'][$key]:null;
                $cur_member_position=isset($team_members['position'][$key])?$team_members['position'][$key]:null;
                $cur_member_bio=isset($team_members['bio'][$key])?$team_members['bio'][$key]:null;
                $daaa= array(
                    'ideaid'=>$retrieved_ideaid,
                    'name'=>$cur_member_name,
                    'email'=>$cur_member_email,
                    'linkedin_profile'=>$cur_member_linkedin,
                    'position'=>$cur_member_position,
                    'bio'=>$cur_member_bio
                );
                $this->db->insert('idea_team_members',$daaa);
            }
        }
    }
    public function get_suggested_services($id)
    {

        $query="select * from services s where s.id in ( select DISTINCT sts.serviceid from service_target_sector sts , ideas i where sts.sectorid = i.sectorid and i.id = $id ) and s.id in ( select DISTINCT stp.serviceid from service_target_phase stp, ideas ij where stp.stage = (select name from bullets b where b.ideaid=$id and template=1 and start_date < CURDATE() order by start_date desc limit 1 ) and ij.id = $id )";
        $action=$this->db->query($query);
        if($action){
            $row=$action->result_array();
            return $row;
        }
        else{
            return 0;
        }
    }
    function get_suggested_services_for_next_phase($id)
    {
        $query="select * from services s where s.id in ( select DISTINCT sts.serviceid from service_target_sector sts , ideas i where sts.sectorid = i.sectorid and i.id = $id ) and s.id in ( select DISTINCT stp.serviceid from service_target_phase stp, ideas ij where stp.stage = (select name from bullets b where b.ideaid=$id and template=1 and start_date > CURDATE() order by start_date limit 1 ) and ij.id = $id )";
        $action=$this->db->query($query);
        if($action){
            $row=$action->result_array();
            return $row;
        }
        else{
            return 0;
        }
    }
    public function public_details($id = null){
        if(_f_is_admin())
        {
            $query="select c.*,(select count(0) from ideas_followers d where d.ideaid=c.id) as followers from ideas c where c.id=$id";
        }
        else{
            $query="select id, name,description,ownerid,sectorid,start_date,tagline from ideas where visibility=1 and id=$id";
        }

        $action=$this->db->query($query);

        if($action)
        {
            $row=$action->result_array();
            if(isset($row)) return $row;
            else return $row;
        }
        else
        {
            return 0;
        }
    }
    public function follow($ideaid,$socialid)
    {
        if(is_numeric($ideaid) && is_numeric($socialid))
        {
            // We are doing an elementary check on the user and see if he is approved by admin

            if(_f_is_user_approved($socialid))
            {
                $par_replace=array(
                    'ideaid'=>$ideaid,
                    'socialid'=>$socialid
                );
                $action=$this->db->replace('ideas_followers',$par_replace);
                if($action)
                {
                    $data['status']="success";
                    return $data;
                }
                else{
                    $data['status']="failure";
                    $data['description']="Query failed 1902171559";
                    return $data;
                }
            }
            else
            {
                $data['status']="failure";
                $data['description']="User not approved by Admin";
                return $data;
            }

        }
    }
    public function unfollow($ideaid,$socialid)
    {
        if(is_numeric($ideaid) && is_numeric($socialid))
        {
            $this->db->where('ideaid',$ideaid);
            $this->db->where('socialid',$socialid);
            $action=$this->db->delete('ideas_followers');
            if($action)
            {
                return 1;
            }
            else{
                return 0;
            }
        }
    }
    public function top($ideaid)
    {
        if(is_numeric($ideaid))
        {
//            $this->db->where('id',$ideaid);
//            $action=$this->db->update('ideas',array('top_idea'=>1));
//            if($action)
//            {
//                $data['status']="success";
//                return $data;
//            }
//            else{
//                $data['status']="failure";
//                $data['description']="Database query failed";
//                return $data;
//            }
            $this->db->set('ideaid',$ideaid);
            $query="Select max(rank) as rank from top_ideas";
            $action= $this->db->query($query);
            $row=$action->result_array();
            $rank=$row[0]['rank']+1;
            $this->db->set('rank',$rank);
            $action2=$this->db->insert('top_ideas');
            if($action2)
            {
                $data['status']="success";
                return $data;
            }
            else
            {
                $data['status']="failure";
                $data['description']="Database query failed";
                return $data;
            }


        }
        else{
            return 0;
        }

    }
    public function drag($ideaid,$rank)
    {
        // Step 1 check if the idea is in the list of top ideas else add it into the top_ideas table
        // Step 2 drop the current rank from table
        // Step 3 check the current rank and the destination rank and decide if the rank of other ideas need
        // to be incremented or decremented
        // Step 4 Increment / Decrement the idea ranks and reinsert the destination rank

        // Step 1
        $this->db->where('ideaid',$ideaid);
        $action=$this->db->get('top_ideas');
        if($action)
        {
            $row=$action->result_array();
            if($row)
            {
                $destination_rank=$rank;
                $current_rank=$row[0]['rank'];


                // STEP 2:
                $this->db->where('ideaid',$ideaid);
                $this->db->delete('top_ideas');

                // STEP 3:
                if($destination_rank>$current_rank)
                {
                    // we need to decrement other idea ranks
                    $query2="update top_ideas set rank=rank-1 where rank>$current_rank and rank<=$destination_rank";
                    $action2=$this->db->query($query2);
                }
                else if($destination_rank<$current_rank)
                {
                    // we need to increment other idea ranks
                    $query2="update top_ideas set rank=rank+1 where rank<$current_rank and rank>=$destination_rank order by rank desc";
                    $action2=$this->db->query($query2);
                }
                // STEP 4:
                $this->db->set('ideaid',$ideaid);
                $this->db->set('rank',$destination_rank);
                $this->db->insert('top_ideas');

                $ret_data['status']="success";
                $ret_data['description']="idea rank changed to $destination_rank";
                return $ret_data;

            }
            else{
                return $this->top($ideaid);
            }
        }
        else{
            $ret_data['status']="failure";
            $ret_data['description']="idea not found in top ideas";
            return $ret_data;
        }
    }
    public function untop($ideaid)
    {
        if(is_numeric($ideaid))
        {
            $this->db->where('ideaid',$ideaid);
            $action=$this->db->delete('top_ideas');
            if($action)
            {
                // after deleting the idea from top_ideas we need to re-rank the ideas
                $data['status']="success";
                return $data;
            }
            else{
                $data['status']="failure";
                $data['description']="Database query failed";
                return $data;
            }
        }
        else{
            return 0;
        }

    }
    public function get($array)
    {
        //Use this method to retrieve multiple users with filters and/or range

        //Currently reserved idea retrieval list via api2 to admin only
        if(_f_is_admin())
        {
            if(isset($array['filter_location']))
            {
                $this->db->like('i.address',$array['filter_location']);
            }
            if(isset($array['filter_sector']))
            {
                $this->db->where_in('i.sectorid',$array['filter_sector'],false);
            }
            if(isset($array['filter_date_start']) && isset($array['filter_date_end']))
            {
                $date_start=$array['filter_date_start'];
                $date_end=$array['filter_date_end'];
                $where_string="date(i.created_on) between '$date_start' and '$date_end'";
                $this->db->where($where_string,null,false);

            }
            if(isset($array['filter_phase']))
            {
                $this->db->having('phase',$array['filter_phase']);
            }
            $action=0;

                $this->db->select('i.id, i.name,i.description,i.created_on,i.ownerid,i.sectorid,i.start_date,i.end_date,i.tagline,i.address,i.unique_url,(select b.name from bullets b where b.ideaid=i.id and b.template=1 and b.start_date <= CURDATE() order by b.start_date desc limit 1) as phase,(select count(socialid) from ideas_followers i_f where i_f.ideaid=i.id) as followers');
                $this->db->order_by('followers','desc');
                $action=$this->db->get('ideas i');



            if($action)
            {
                $result=$action->result_array();
                return $result;
            }
            else{
                return 0;

            }
        }
        else{

        }

    }
    public function get_ideas_for_user($socialid) {
        
        $this->db->order_by('id');
        $action=$this->db->get_where('view_ideas',array('ownerid'=>$socialid));

        if($action)
        {
            return $action->result_array();
        } else return false;
    }
    public function get_suggested_ideas_for_sp($socialid)
    {
        $query="select * from ideas where sectorid in (select sectorid from service_target_sector where serviceid in (select id from services where ownerid=$socialid)) and visibility=1";
        $action=$this->db->query($query);
        $result=$action->result_array();
        return $result;
    }

}
?>