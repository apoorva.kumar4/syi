<?php
class User_wrapper extends CI_Model {
	public function read($socialid) {
		$this->load->model('User4');
		$user=$this->User4->get('socialid',$socialid);
		if(!$user) {
			$user=$this->User4->get('username',$socialid);
			if(!$user) return false;
		} 
		$user=$user[0];

		if($user['user_type']=="ideator") {
			$this->load->model('Ideator4');
			$ideator=$this->Ideator4->get('socialid',$socialid);

			if($ideator) {
				$ideator=$ideator[0];
				$user += $ideator;
			}

		} else if($user['user_type']=="service_provider") {
			$this->load->model('Service_provider4');
			$sp=$this->Service_provider4->get('socialid',$socialid);
			if($sp) {
				$sp=$sp[0];
				$user += $sp;				
			}
			
		} else if($user['user_type']=="investor") {
			$this->load->model('Investor4');
			$iv=$this->Investor4->get('socialid',$socialid);
			if($iv) {
				$iv=$iv[0];
				$user += $iv;	
			}
			
		}


		$profile_picture_id=$this->User4->get_profile_picture($socialid);

        if($profile_picture_id!=false) {
        	$this->load->model('Supload');
            $profile_picture_url=$this->Supload->get_complete_url($profile_picture_id);
        } else {
            // does not have a profile picture so fetch gravatar
            $profile_picture_url=$this->User4->get_gravatar_url($socialid);
        }

        $user['profile_picture_url']=$profile_picture_url;

        

		return $user;

	}
}
?>