<?php
class Admin_model extends CI_Model {
	const TABLE='admins';
	const PERMISSIONS_TABLE='admin_permissions';
	public function add($data) {
		//check if socaialid exists
		if(!array_key_exists('socialid', $data)) {
			return array(
				'status'=>'failure',
				'description'=>'No socialid provided'
			);
		}

		// business rule that no more than 3 admins
		$max_admins=3;
		$action=$this->db->get(self::TABLE);
		$count=$action->num_rows();
		if($count>=$max_admins) {
			return array(
				'status'=>'failure',
				'description'=>"cant add more than $max_admins admins"
			);
		}
		$this->db->set(array('socialid'=>$data['socialid']));

		$action=$this->db->insert(self::TABLE);
		if(!$action) {
			return array(
				'status'=>'failure',
				'description'=>'DB error',
				'error'=>$this->db->error()
			);
		} 

		return array('status'=>'success');

	}

	public function get($params) {
		$this->load->model('User3');
		if(array_key_exists('socialid',$params)) {
			$this->db->where('socialid',$params['socialid']);
		}

		$action=$this->db->get(self::TABLE);
		if(!$action) {
			return array(
				'status'=>'failure',
				'description'=>'DB Error',
				'error'=>$this->db->error()
			);
		}
		$result=$action->result_array();
		if(!$result) 
			return array(
				'status'=>'failure',
				'description'=>'Empty set'
			);

		foreach($result as &$admin) {
			$this->db->where('socialid',$admin['socialid']);
			$this->db->select('permission');
			$get_permissions= $this->db->get(self::PERMISSIONS_TABLE);
			if(!$get_permissions) {
				$admin['permissions']=array();
			} else {
				$permission_result=$get_permissions->result_array();
				$permissions=array();

				foreach($permission_result as $row) {
					$permissions[]=$row['permission'];
				}
				$admin['permissions']=$permissions;

			}
			$user=$this->User3->read($admin['socialid']);
			$admin+=$user;
		}

		return array(
			'status'=>'success',
			'data'=>$result
		);
	}

	public function get2($params) {
		$this->load->model('User3');
		

		$action=$this->db->get(self::TABLE);
		if(!$action) {
			return array(
				'status'=>'failure',
				'description'=>'DB Error',
				'error'=>$this->db->error()
			);
		}
		$result=$action->result_array();
		$socialids = array();
		if(!$result) 
			return array(
				'status'=>'failure',
				'description'=>'Empty set'
			);

		foreach($result as $row) {
			array_push($socialids, $row['socialid']);
		}

		return array(
			'status'=>'success',
			'data'=>$socialids
		);
	}

	public function delete($params) {
		if(!array_key_exists('socialid',$params)) {
			return array('status'=>'failure','description'=>'no socialid provided');
		}

		$this->db->where('socialid',$params['socialid']);
		$this->db->delete(self::TABLE);

		return array('status'=>'success');

	}
}
?>