<?php

class User extends CI_Model
{
	public $socialid="";
    public $id="";

    public function _update()
	{

        $validated_name=$this->db->escape(_f_validate('name',$_POST['name']));
        $validated_lastname=$this->db->escape(_f_validate('lastname',$_POST['lastname']));
        $validated_mobile=$this->db->escape(_f_validate('mobile',$_POST['mobile']));
        $validated_institute= isset($_POST['institute'])?$this->db->escape(_f_validate('institute',$_POST['institute'])):0;
        $validated_year_of_graduation= isset($_POST['year_of_graduation'])?$this->db->escape(_f_validate('year_of_graduation',$_POST['year_of_graduation'])):0;
        $validated_role=$this->db->escape(_f_validate('role',$_POST['role']));
        $validated_location=$this->db->escape(_f_validate('location',$_POST['location']));
        $validated_sector_expertise=$this->db->escape($_POST['sector_expertise']);
        $validated_student_alumni=isset($_POST['student_alumni'])?$this->db->escape(_f_validate('student_alumni',$_POST['student_alumni'])):0;
        $validated_registration_complete= 'Y';
        $validated_industry=isset($_POST['industry'])?$this->db->escape(_f_validate('institute',$_POST['industry'])):0;
        $validated_working_since=isset($_POST['working_since'])?$this->db->escape(_f_validate('year_of_graduation',$_POST['working_since'])):0;
        $validated_company=isset($_POST['company'])?$this->db->escape(_f_validate('description',$_POST['company'])):0;


        $validated_facebook_username=$this->db->escape($_POST['facebook_username']);
        $validated_linkedin_username=$this->db->escape($_POST['linkedin_username']);
        $validated_twitter_username=$this->db->escape($_POST['twitter_username']);
        $validated_google_username=$this->db->escape($_POST['google_username']);

            if(isset($validated_name,$validated_lastname,$validated_mobile,$validated_year_of_graduation,$validated_institute,$validated_role,$validated_location,$validated_sector_expertise,$validated_student_alumni))
            {
                $query="Update users set first_name=$validated_name,last_name=$validated_lastname,mobile=$validated_mobile,role=$validated_role,location=$validated_location,sector_expertise=$validated_sector_expertise,registration_complete='Y' where id=$this->socialid ";
                $this->db->query($query);
                $query="";
                $query2="";
                if(isset($validated_year_of_graduation,$validated_institute,$validated_student_alumni) && $validated_year_of_graduation)
                {
                    $query="update ideators set institute=$validated_institute,year_of_graduation=$validated_year_of_graduation,student_alumni=$validated_student_alumni,facebook_username=$validated_facebook_username,linkedin_username=$validated_linkedin_username,twitter_username=$validated_twitter_username,google_username=$validated_google_username where socialid=$this->socialid";
                    $query2="Select institute,year_of_graduation, student_alumni,facebook_username,linkedin_username,twitter_username,google_username from ideators where socialid=$this->socialid";
                    $this->db->query($query);
                    $id=$this->id;
                    $query="SELECT id, socialid, first_name, last_name, email, mobile, role, location, sector_expertise FROM users where id=$this->id";
                    $action=$this->db->query($query);
                    $username_updated=$this->_update_username();
                    if($action && $username_updated )
                    {
                        $row=$action->result_array();
                        $action2=$this->db->query($query2);
                        $row2=$action2->result_array();
                        $row[0]['institute']=$row2[0]['institute'];
                        $row[0]['year_of_graduation']=$row2[0]['year_of_graduation'];
                        $row[0]['student_alumni']=$row2[0]['student_alumni'];
                        $row0[0]['facebook_username']=$row2[0]['facebook_username'];
                        $row0[0]['linkedin_username']=$row2[0]['linkedin_username'];
                        $row0[0]['twitter_username']=$row2[0]['twitter_username'];
                        $row0[0]['google_username']=$row2[0]['google_username'];

                        return $row;
                    }
                    else if(!$username_updated)
                    {
                        return -5;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if(isset($validated_industry,$validated_working_since,$validated_company) && $validated_industry)
                {
                    if ($this->session->user_type == "service_provider")
                    {
                        $query = "update service_providers set industry=$validated_industry,working_since=$validated_working_since,company=$validated_company,facebook_username=$validated_facebook_username,linkedin_username=$validated_linkedin_username,twitter_username=$validated_twitter_username,google_username=$validated_google_username where socialid=$validated_socialid";
                        $query2="Select industry,working_since, company,facebook_username,linkedin_username,twitter_username,google_username from service_providers where socialid=$validated_socialid";
                    }

                    else if ($this->session->user_type = "investor")
                    {
                        $query = "update investors set industry=$validated_industry,working_since=$validated_working_since,company=$validated_company,facebook_username=$validated_facebook_username,linkedin_username=$validated_linkedin_username,twitter_username=$validated_twitter_username,google_username=$validated_google_username where socialid=$validated_socialid";
                        $query2="Select industry,working_since, company,facebook_username,linkedin_username,twitter_username,google_username from investors where socialid=$validated_socialid";
                    }

                    $this->db->query($query);
                    $username_updated=$this->_update_username();
                    $id=$this->id;
                    $query="SELECT id, socialid, first_name, last_name, email, mobile, role, location, sector_expertise FROM users where id=$id";
                    $action=$this->db->query($query);
                    if($action && $username_updated==1)
                    {
                        $row=$action->result_array();
                        $action2=$this->db->query($query2);
                        $row2=$action2->result_array();
                        $row[0]['industry']=$row2[0]['industry'];
                        $row[0]['working_since']=$row2[0]['working_since'];
                        $row[0]['company']=$row2[0]['company'];
                        return $row;
                    }
                    else if(!$username_updated)
                    {
                        return -5;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    $query="Select 1";
                    $query2="Select 1";
                    echo "Houstan we have a problem: 765";
                    echo $validated_year_of_graduation."-".$validated_institute."-".$validated_student_alumni;
                }
            }
            else
            {
                return 0;
            }
	}
    public function _update_username()
    {
        
        $validated_socialid=$this->socialid;
        $validated_username=_f_validate('username',$_POST['username']);
        $query_for_cu="Select username from users where socialid=$validated_socialid";
        $action=$this->db->query($query_for_cu);
        $current_username=$action->result_array();
        if($current_username[0]['username']==$_POST['username'])
        {
            return 1;
        }
        else
        {
            $esc_username=$this->db->escape(_f_validate('username',$_POST['username']));
            $ava=$this->check_username_availability();
            
            if($ava && !($validated_username=== NULL))
            {
                $query="Update users set username=$esc_username where socialid=$validated_socialid";
                
                $action=$this->db->query($query);
                if($action)
                {
                    return 1;    
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        



    }
    public function check_username_availability()
    {
        $esc_username=$this->db->escape($_POST['username']);
        $esc_socialid=$this->session->userdata('socialid');
        $query="Select count(*) from users where username=$esc_username and socialid != $esc_socialid";
        $action=$this->db->query($query);
        if($action)
        {
            $row=$action->result_array();
            if($row[0]["count(*)"]==0)
            {
                return 1;

            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }
	public function delete($socialid)
	{
	    // Notice that admin check is made here in the model rather than controller for enhanced security
        if(_f_is_admin()){
            if(is_numeric($socialid)){
                $this->db->where('socialid',$socialid);
                $this->db->delete('users');
                return 200;
            }
            else{
                return 0;
            }
        }
        else{
            return 401;
        }
	}
	public function _read()
	{
	    $id=$this->id;
	    $query="SELECT id, socialid, first_name, last_name, email, mobile, user_type, timestamp, access_type, emailverified, registration_complete, username, role, location, sector_expertise,blocked,admin_approved FROM users where id=$id";

        $action=$this->db->query($query);
        if($action)
        {
            $row=$action->result_array();
            return $row;
        }
        else
        {
            return 0;
        }
	}
    public function block($socialid){
        if(is_numeric($socialid))
        {
            $query="Update users set blocked=1 where socialid=$socialid";
            $action=$this->db->query($query);
            if($action)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else{
            return 0;
        }


    }
    public function approve($socialid){
        if(is_numeric($socialid))
        {
            $query="Update users set admin_approved=1 where socialid=$socialid";
            $action=$this->db->query($query);
            if($action)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else{
            return 0;
        }


    }
    public function reject($socialid){
        if(is_numeric($socialid))
        {
            $query="Update users set admin_approved=2 where socialid=$socialid";
            $action=$this->db->query($query);
            if($action)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }
        else{
            return 0;
        }


    }
    public function unblock($socialid){
        if(is_numeric($socialid))
        {
            $query="Update users set blocked=0 where socialid=$socialid";
            $action=$this->db->query($query);
            if($action)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else{
            return 0;
        }


    }
    public function verify($socialid){
        if(is_numeric($socialid))
        {
            $query="Update users set verified_profile=1 where socialid=$socialid";
            $action=$this->db->query($query);
            if($action)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else{
            return 0;
        }


    }
    public function _unblock()
    {
        $id=$this->id;
        $query="Update users set blocked=0 where id=$id";
        $action=$this->db->query($query);
        if($action)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    public function get_messages()
    {
        $validated_socialid=$this->session->userdata('socialid');
        $query="Select * from messages where receiver=$validated_socialid and admin_approved=1 and trashed=0 order by timestamp desc";
        $action=$this->db->query($query);
        if($action)
        {
            $rows=$action->result_array();
            return $rows;
        }
        else
        {
            return 0;
        }


    }
    public function get_reviews()
    {
        $validated_socialid=$this->session->userdata('socialid');
        $query="Select * from reviews where socialid=$validated_socialid order by date desc";
        $action=$this->db->query($query);
        if($action)
        {
            $rows=$action->result_array();
            return $rows;
        }
        else
        {
            return 0;
        }


    }
    public function get_sent_messages()
    {
        $validated_socialid=$this->session->userdata('socialid');
        $query="Select * from messages where sender=$validated_socialid and sender_trashed=0 order by timestamp desc";
        $action=$this->db->query($query);
        if($action)
        {
            $rows=$action->result_array();
            return $rows;
        }
        else
        {
            return 0;
        }

    }
    
    public function get_notifications()
    {
        $validated_socialid=$this->session->userdata('socialid');
        $query="Select * from notifications where socialid=$validated_socialid order by id desc";
        $action=$this->db->query($query);
        if($action)
        {
            $rows=$action->result_array();
            return $rows;
        }
        else
        {
            return 0;
        }


    }
    public function get_unread_notifications()
    {
        $validated_socialid=$this->session->userdata('socialid');
        $query="Select * from notifications where socialid=$validated_socialid and notif_read='no' order by id desc";
        $action=$this->db->query($query);
        if($action)
        {
            $rows=$action->result_array();
            return $rows;
        }
        else
        {
            return 0;
        }


    }

    public function get_public_visible_data()
    {
        
        if(isset($this->socialid))
        {
            return $this->get_public_visible_data_from_socialid();
        }
        else
        {
            return 0;
        }
    }
    public function get_public_visible_data_from_socialid()
    {   
        $esc_socialid=$this->db->escape($this->socialid);
        $query="Select id,socialid, first_name,last_name,user_type,timestamp,emailverified,username,role,location,sector_expertise,verified_profile,admin_approved,blocked from users where socialid=$esc_socialid";
        $action=$this->db->query($query);
        if($action)
        {
            $row=$action->result_array();
            return $row;
        }
        else
        {
            return 0;
        }
    }
    public function get_email_from_socialid()
    {
        $esc_socialid=$this->db->escape($this->socialid);
        $query="Select email from users where socialid=$esc_socialid";
            $action=$this->db->query($query);
            if($action)
            {
                $row=$action->result_array();
                return $row[0]['email'];
            }
            else
            {
                return 0;
            }

    }
    public function get_profile_picture($dimension)
    {
        $esc_socialid=$this->db->escape($this->socialid);
        $query="Select url from profile_pictures where socialid=$esc_socialid order by timestamp desc";
            $action=$this->db->query($query);
            if($action)
            {
                $row=$action->result_array();
                if($row)

                $remoteImage = $this->config->item('upload_path').$row[0]['url'];
                else
                {
                    $email=$this->get_email_from_socialid();
                    $hashe=hash("md5",$email);
                    $remoteImage="https://www.gravatar.com/avatar/".$hashe."?s=$dimension&d=identicon";
                }

                return $remoteImage;

          }
          else
          {

          }   
    }
    public function get_uploaded_profile_picture($socialid) {

        $esc_socialid=$this->db->escape($socialid);
        $query="Select url from profile_pictures where socialid=$esc_socialid order by timestamp desc";
        $action=$this->db->query($query);
        if($action)
        {
            $row=$action->result_array();
            if($row)
            {
                $remoteImage = $this->config->item('upload_path').$row[0]['url'];
                return $remoteImage;
            }
            else {
                return "assets/img/stack2.png";
            }
        } else {
            return "assets/img/stack2.png";
        }   

    }

    public function get_investment_range($socialid)
    {
        $id=$this->id;
        $query="SELECT socialid, investment_range_start, investment_range_end, professional_background,company,alumni_network,investment_network,members_of_organisation,sample_companies_invested,investor_tags FROM investors where socialid=".$socialid;

        $action=$this->db->query($query);
        if($action)
        {
            $row=$action->result_array();
            return $row;
        }
        else
        {
            return 0;
        }
    }


    public function does_profile_picture_exist()
    {
        $esc_socialid=$this->db->escape($this->socialid);
        $query="Select url from profile_pictures where socialid=$esc_socialid order by timestamp desc";
            $action=$this->db->query($query);
            if($action)
            {
                $row=$action->result_array();
                if($row)
                    return 1;
                else
                {
                    return 0;
                }

          }
          else
          {

          }   
    }
    public function get_profile_picture_key()
    {
        $esc_socialid=$this->db->escape($this->socialid);
        $query="Select url from profile_pictures where socialid=$esc_socialid order by timestamp desc";
            $action=$this->db->query($query);
            if($action)
            {
                $row=$action->result_array();
                if($row)
                    return $row[0]['url'];
                else
                {
                    return 0;
                }

          }
          else
          {

          }   
    }
    public function get_user_from_socialid($socialid)
    {

    }
    public function get_message_sender_suggestions($socialid)
    {
        
        $query="select socialid as id,username as value from users where socialid in (Select distinct receiver from messages where sender=$socialid union select distinct sender from messages where receiver=$socialid)";

        $action=$this->db->query($query);
        $result=$action->result_array();
        return $result;
    }

    public function get_shortlisted_ideas($socialid)
    {
        $this->db->where('ideas_followers.socialid',$socialid);
        $this->db->from('ideas_followers');
        $this->db->join('ideas','ideas_followers.ideaid = ideas.id');
        $action=$this->db->get();
        if($action)
        {
            $row=$action->result_array();
            return $row;
        }
        else
        {
            return 0;
        }

    }
    public function shortlist_idea($ideaid,$socialid)
    {
        $data = array (
            'socialid' => $socialid,
            'ideaid' => $ideaid
        );

        $this->db->replace('idea_shortlisters',$data);
        $action['status'] = "success";
        return $action;
    }
    public function delist_idea($ideaid,$socialid)
    {
        $data = array (
            'socialid' => $socialid,
            'ideaid' => $ideaid
        );

        $this->db->delete('idea_shortlisters',$data);
        $action['status'] = "success";
        return $action;
    }
    public function get($array)
    {
        //Use this method to retrieve multiple users with filters and/or range

        //Currently reserved user retrieval list via api2 to admin only
        if(_f_is_admin())
        {
            if(isset($array['filter_location']))
            {
                $this->db->like('location',$array['filter_location']);
            }
            if(isset($array['filter_user_type']))
            {
                $this->db->where('user_type',$array['filter_user_type']);
            }
            if(isset($array['filter_sector']))
            {
                $this->db->where_in('sector_expertise',$array['filter_sector'],false);
            }
            if(isset($array['filter_date_start']) && isset($array['filter_date_end']))
            {
                $date_start=$array['filter_date_start'];
                $date_end=$array['filter_date_end'];
                $where_string="date(timestamp) between '$date_start' and '$date_end'";
                $this->db->where($where_string,null,false);

            }
            $action=0;
            if(_f_is_admin())
            {
                $this->db->select('id,socialid,first_name,last_name,email,mobile,timestamp,emailverified,blocked,admin_approved,user_type,username,role,location,sector_expertise,verified_profile');
                $action=$this->db->get('users');

            }
            else{
                $this->db->select('id,socialid,first_name,last_name,user_type,username,role,location,sector_expertise,verified_profile');
                $action=$this->db->get('users');
            }

            if($action)
            {
                $result=$action->result_array();
                return $result;
            }
            else{
                return 0;

            }
        }
        else{

        }

    }
}
