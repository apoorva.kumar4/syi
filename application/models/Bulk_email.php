<?php
class Bulk_email extends CI_Model{

    protected $props = array(
        'protocol'=>'smtp',
        'charset'=>'iso-8859-1',
        'wordwrap'=>TRUE,
        'smtp_host'=>"ssl://email-smtp.us-east-1.amazonaws.com",
        'smtp_port'=>465,
		'newline'=>"\r\n",
		'crlf'=>"\r\n",
		'useragent'=>'SaYourIdeas Mailer',
		'smtp_user'=>'AKIAJJKD62P6VIV6V4GQ',
		'smtp_pass'=>'Avj2x1O5jZvlBcklPfsmUwtvt9IrJ5LKmoIfrZs132ZV'
    );

    public function send($from,$to,$subject,$message,$attachments="",$cc="",$bcc="")
    {
        //sleep(5);
        //return 1;

        // $from is a string which can be one of the predefined $senders
        // $to is a comma delimited list of email addresses
        // $subject is a string
        // $message is a string
        
        // Check for predefined senders and set the config file for codeigniters email library

        $this->load->library('email');
        $this->email->initialize($this->props);

        // Add the $to, $cc, $bcc and $subject and $message parameters
		$this->email->from($from,"SYI");
		$this->email->to($to);
        if($cc!="") $this->email->cc($cc);
        if($bcc!="") $this->email->bcc($bcc);
        $this->email->subject($subject);
	    $this->email->message($message);
		$this->email->set_header('X-Mailer','SaYourIdeas');
		
		//check for attachments
        if($attachments=="")
        {
            // No attachments; do nothing
        }
        else{
            // Attachments provided
            foreach($attachments as $attachment)
            {

                $path=$attachment['path'];
                $name=$attachment['name'];
                $this->email->attach($path,'attachment',$name);
            }
        }

        // Finally send the email

        $action=$this->email->send();
        
        if($action)
        {
            return 1;
        }
        else{
            $description=$this->email->print_debugger();
            return $description;
        }

	}
	public function new_batch($from,$subject,$message,$options=null) {
		$this->db->set(
			array(
				'from'=>$from,
				'subject'=>$subject,
				'message'=>$message
			)
		);

		$action=$this->db->insert('email_batch');
		if($action) {
			return $this->db->insert_id();
		}

	}
	public function add_batch_receivers($batch_id,$emails){
    
        $insert_array = array();

        foreach($emails as $email) {
            $insert_data= array(
                'batch'=>$batch_id,
                'to'=>$email,
                'delivered'=>'No'
            );

            array_push($insert_array,$insert_data);

        }

        
        $this->db->insert_batch('email_batch_receivers',$insert_array);



	}
	public function process_batch($batch_id=null,$limit=5) {

        if($batch_id!=null) {
            $this->db->where('batch',$batch_id);
        }

        $this->db->limit($limit);
        $this->db->where('delivered','No');
        $this->db->join('email_batch','email_batch.id=email_batch_receivers.batch','right');

		$act_receiver=$this->db->get('email_batch_receivers');
		$receivers=$act_receiver->result_array();
        
        foreach($receivers as $item) {
            
            $send_email_action=$this->send($item['from'],$item['to'],$item['subject'],$item['message']);
            if($send_email_action==1) {
                //email sent so mark as delivered
                $this->db->where('batch',$item['batch']);
                $this->db->where('to',$item['to']);
                $this->db->set('delivered','Yes');
                $this->db->update('email_batch_receivers');
            }
        }
    }

}
?>
