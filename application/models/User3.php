<?php
class User3 extends CI_Model
{
    const TABLE="users";

    protected $properties = array (
        'id'=>'',
        'socialid'=>'',
        'first_name'=>'',
        'last_name'=>'',
        'email'=>'',
        'mobile'=>'',
        'password_hash'=>'',
        'user_type'=>'',
        'access_type'=>'',
        'emailverified'=>'',
        'username'=>'',
        'timestamp'=>'',
        'role'=>'',
        'location'=>'',
        'sector_expertise'=>'',
        'sector_other'=>'',
        'blocked'=>'',
        'admin_approved'=>'',
        'verified_profile'=>'',
        'facebook_username'=>'',
        'twitter_username'=>'',
        'linkedin_username'=>'',
        'google_username'=>''
    );

    protected $ideator_completeness_properties = array (
        'first_name',
        'last_name',
        'email',
        'mobile',
        'institute',
        'year_of_graduation',
        'role',
        'location',
        'sector_expertise',
        'student_alumni',
        'linkedin_username',
        'facebook_username',
        'twitter_username',
        'google_username'
    );
    protected $ideator_properties = array (
        'institute'=>'',
        'year_of_graduation'=>'',
        'student_alumni'=>''
    );

    protected $service_provider_properties = array (
        'industry'=>'',
        'company'=>'',
        'working_since'=>'',
        'show_contact_details'=>''
    );

    protected $investor_properties = array(
        'industry'=>'',
        'company'=>'',
        'working_since'=>'',
        'investment_range_start'=>'',
        'investment_range_end'=>'',
        'professional_background'=>'',
        'alumni_network'=>'',
        'investment_network'=>'',
        'members_of_organisation'=>'',
        'sample_companies_invested'=>'',
        'investor_tags'=>'',
        'allow_direct_contact'=>'',
        'would_like_to_mentor_startups'=>'',
        'currently_mentoring_any_startups'=>'',
        'companies_associated_with_as_a_mentor'=>'',
        'areas_of_expertise_how_do_i_add_value'=>'',
        'currency'=>'',
        'target_sector_other'=>''
    );

    protected function socialify_link($link,$social_platform) {
        if($social_platform=="facebook")
        {
            if(strpos($link,"facebook.com")===false) {
                return "https://facebook.com/".$link;
            }
            else return add_protocol($link);
        }
        else if($social_platform=="twitter") {
            if(strpos($link,"twitter")===false) {
                return "https://twitter.com/".$link;
            }
            else return add_protocol($link);
        }
        else if($social_platform=="google") {
            if(strpos($link,"google.com")===false) {
                return "https://plus.google.com/+".$link;
            }
            else return add_protocol($link);
        }
        else if($social_platform=="linkedin") {
            if(strpos($link,"linkedin.com")===false) {
                return "https://linkedin.com/user/".$link;
            }
            else return add_protocol($link);
        }
    }

    protected function add_protocol($link) {
        if(strpos($link,"http")===false) return "https://".$link;
        else return $link;
    }
    
    public function generate_socialid()
    {
        $random_number=rand(1000000,9999999);
        $this->db->where('socialid',$random_number);
        $this->db->select('count(*)');
        $action=$this->db->get('users');
        $row=$action->result_array();
        if($row) {
            if($row[0]['count(*)']==0)
            {
                return $random_number;
            } else $this->generate_socialid();
        } else return false;
    } 

    protected function is_socialid_present($socialid)
    {
        if(is_numeric($socialid)){
            $this->db->where('socialid',$socialid);
            $this->db->select('count(*)');
            $action=$this->db->get('users');
            $row=$action->result_array();
            if($row[0]['count(*)']==1)
            {
                return true;
            }
            else
            {
                return false;
            }
        } else return false;       
    }

    public function is_user_present($id)
    {
        $this->load->model('Dac');

        $where=array();

        // check if email or mobile is present in db
        if(filter_var($id,FILTER_VALIDATE_EMAIL)) {
            $where['email']=$id;
            
        } else if(is_numeric($id)) {
            $where['mobile']=$id;
        }
        
        
        $action=$this->Dac->read('users',$where);
        if($action) {
            return $action[0];
        }
        else return false;
    }

    protected function filter_and_sanitize_data($reference,$array){
        $filter_array = array();

        foreach($this->$reference as $key=>$value) {
            
            if(array_key_exists($key, $array)){
                $filter_array[$key]=is_string($array[$key])?strip_tags($array[$key]):$array[$key];
            }
        }

        // foreach ($filter_array as $key=>$value) {
        //    if($value===null) {
        //         return false;
        //    }
        // }

        return $filter_array;
    }

    public function create($array)
    {

        // sanitized data goes into database
        $sanitized=$this->filter_and_sanitize_data('properties',$array); 
        
        $required_properties=array(
            'first_name',
            'last_name',
            'mobile',
            'password_hash'
        );

        foreach ($required_properties as $key) {
            if(!(array_key_exists($key,$sanitized))) {
                log_message('debug','required properties not met');
                return false;
            }
        }

        // check if mobile number is numeric
        if(!is_numeric($sanitized['mobile'])) {
            log_message('debug','invalid mobile');
            return false; // invalid mobile
        }

        if($this->is_user_present($sanitized['mobile'])) {
            log_message('debug','user already present');
            return false; // user already present

        }

        $temp_socialid=$this->generate_socialid();

        if($temp_socialid) {
            $sanitized['socialid']=$temp_socialid;   
        } else {
            log_message('debug','Could not generate socialid');
            return false;
        }

        if(array_key_exists('username',$sanitized))
        {   
            $available=$this->is_username_available($sanitized['username']);
            if(!$available) $sanitized['username']=$sanitized['socialid'];
            else return false; 
        } else {
            $sanitized['username']=$sanitized['socialid'];
        }

        if(array_key_exists('blocked', $sanitized) && ($sanitized['blocked']==0 || $sanitized['blocked'] == 1)){
            $sanitized['blocked']=$sanitized['blocked']; 
        } else $sanitized['blocked']=0;

        if(array_key_exists('admin_approved', $sanitized) && ($sanitized['admin_approved']==0 || $sanitized['admin_approved'] == 1)){
            $sanitized['admin_approved']=$sanitized['admin_approved']; 
        } else $sanitized['admin_approved']=1;

        if(array_key_exists('verified_profile', $sanitized) && ($sanitized['verified_profile']==0 || $sanitized['verified_profile'] == 1)){
            $sanitized['verified_profile']=$sanitized['verified_profile']; 
        } else $sanitized['verified_profile']=0;

        $this->load->model('Dac');
        $action=$this->Dac->insert('users',$sanitized);
        if(!$action) return false;

        $extra_data=array();
        $action2=false;
        
        if(!array_key_exists('user_type', $array)) {
            
            return $sanitized['socialid'];
        }

        if($sanitized['user_type']==="ideator") {
            $extra_data=$this->filter_and_sanitize_data('ideator_properties',$sanitized);
            $extra_data['socialid']=$sanitized['socialid'];
            $action2=$this->db->insert('ideators',$extra_data);
            
        } else if($sanitized['user_type']==="service_provider") {
            $extra_data=$this->filter_and_sanitize_data('service_provider_properties',$sanitized);
            $extra_data['socialid']=$sanitized['socialid'];
            $action2=$this->db->insert('service_providers',$extra_data);
        } else if($sanitized['user_type']==="investor") {
            $extra_data=$this->filter_and_sanitize_data('investor_properties',$array);
            $extra_data['socialid']=$sanitized['socialid'];
            $action2=$this->db->insert('investors',$extra_data);
        }

        if($action2) return $sanitized['socialid'];
        else return false;
    }

    protected function dac_create_user($data) {

        $action=$this->db->set($data)->insert('users');

        if($action) return true;
        else {
            log_message('error',$this->db->error());
            return false; 
        }
    }
    public function is_username_available($username) {

        if(is_numeric($username)){
            return false;
        }
        
        $this->db->select('count(*)');
        $this->db->where('username',$username);
        $action=$this->db->get('users');
        $row=$action->result_array();
        if($row[0]['count(*)']>0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public function read($socialid=null)
    {
        $this->load->model('Investor_sectors');
        $user = array();
        $action=false;

        if (is_numeric($socialid))
        {
            $action=$this->db->get_where('users',array('socialid'=>$socialid));
            
        }
        else {
            if($socialid==null) {
                $action=$this->db->get('users');
            }
            else {
                $action=$this->db->get_where('users',array('username'=>$socialid));
            }
        }

        if(!$action) return false;
        $result=$action->result_array();
        if(!$result) return false;
        
        $this->load->model('Dac');

        foreach($this->properties as $key=>$property) {
            $user[$key]=$result[0][$key];
        }

        $uploadid=$this->get_profile_picture($socialid);

        if($uploadid){
           $user['profile_picture_url']=$this->get_profile_picture_url($uploadid);
            
        } else {
            
            $user['profile_picture_url']=$this->get_gravatar_url($user['email']);
        }

        if($user['user_type']=="ideator") {
            $ret_ideator=$this->Dac->read('ideators',array('socialid'=>$socialid));
            if(!$ret_ideator) {
                $ret_ideator=$this->ideator_properties;
                log_message('debug',print_r($ret_ideator,true));
            } else $user+=$ret_ideator[0];


        } else if($user['user_type']=="service_provider") {
            $ret_sp=$this->Dac->read('service_providers',array('socialid'=>$socialid));
            $rating_data=$this->get_rating($socialid);
            if($ret_sp) $user+=$ret_sp[0];
            if($rating_data) $user+=$rating_data;

        } else if($user['user_type']=="investor") {
            $this->db->where('socialid',$socialid);
            
            $ret_action=$this->db->get('investors');
            
            $ret_result=$ret_action->result_array();
            if(!$ret_result) {
                $empty_vc=array();
                foreach($this->investor_properties as $key=>$value) {
                    $ret_vc[$key]=$value;      
                }
                $user+=$empty_vc;
            }
            else {
                $user+=$ret_result[0];
            }
                
            $sectors=$this->Investor_sectors->get_sectors($user['socialid']);
            $user['investor_sectors']=$sectors;
            
        } else {

        }
        
        return $user;
    }

    public function update($array)
    {
        $this->load->model('Investor_sectors');
        $socialid=$array['socialid'];

        $update_properties=array(
            'first_name',
            'last_name',
            'email',
            'password_hash',
            'username',
            'role',
            'location',
            'user_type',
            'sector_expertise',
            'sector_other',
            'facebook_username',
            'twitter_username',
            'linkedin_username',
            'google_username'
        );

        $sanitized = array();

        foreach($update_properties as $key) {
            
            if(array_key_exists($key, $array)){
                $sanitized[$key]=is_string($array[$key])?strip_tags($array[$key]):$array[$key];
            }
        }

        if(array_key_exists('username', $array)) {
            if($this->is_username_available($array['username'])) {

            } else return false;
        }
        

        $this->db->where('socialid',$socialid);
        $this->db->set($sanitized);
        $action=$this->db->update('users');
        
        if(!$action) return false;
        
        $user=$this->read($socialid);
        
        $sub_user= array('socialid'=>$socialid);

        if($user['user_type']=="ideator") {
            $sub_user+=$this->filter_and_sanitize_data('ideator_properties',$array);
            $action=$this->db->replace('ideators',$sub_user); 
        } else if($user['user_type']=="service_provider") {
            
            $sub_user+=$this->filter_and_sanitize_data('service_provider_properties',$array);
            
            if(array_key_exists('show_contact_details',$sub_user)) {
                if($array['show_contact_details']=='on') $sub_user['show_contact_details']='Yes';
                else $array['show_contact_details']='No';
            }
            $action=$this->db->replace('service_providers',$sub_user);
             
        } else if($user['user_type']=="investor") {
            $sub_user+=$this->filter_and_sanitize_data('investor_properties',$array);
            $action=$this->db->replace('investors',$sub_user); 
            if(array_key_exists('service-target-sectors',$array)) {
                $action_update_sectors=$this->Investor_sectors->update_sectors($user['socialid'],$array['service-target-sectors']);
                
            }  
        }

        if(!$action) return false;
        else return true;         
    }

    public function delete($socialid)
    {
        $this->load->model('Dac');
        $action=$this->Dac->delete('users',array('socialid'=>$socialid));
        if($action) return true;
        else return false;
    }

    public function add_profile_picture($data){
        // Required parameters
        // socialid
        // uploadid
        // name
        // Returns 1 on success
        if($data['socialid']!="" && $data['uploadid']!="")
        {
            $data=array(
                'socialid'=>$data['socialid'],
                'uploadid'=>$data['uploadid']
            );

            $this->load->model('Dac');
            $action=$this->Dac->insert('profile_pictures',$data);
            if($action) return true;
            else return false;
        }
    }
    public function remove_profile_picture($socialid) {
        if(!is_numeric($socialid)) return false;
        $this->db->where('socialid',$socialid);
        $action=$this->db->delete('profile_pictures');
        if(!$action) return false;
        return true;
    }
    //returns uploadid for profile picture
    public function get_profile_picture($socialid)
    {
        $this->db->where('socialid',$socialid);
        $action=$this->db->get('profile_pictures');
        $row=$action->last_row('array');

        if($row) {
            return $row['uploadid']; 
        }
        else return false;
    }

    // returns url for upload
    public function get_profile_picture_url($uploadid) {
        $this->load->model('Supload');
        $action=$this->db->get_where('profile_pictures',array('uploadid'=>$uploadid));
        $row=$action->result_array();
        $remoteImageUrl="";
        if($row) {
            $uploadid=$row[0]['uploadid'];
            $upload=$this->Supload->get_upload($uploadid);
                if(!$upload) {
                    die(0);
                }
                $remoteImageUrl=$this->config->item('upload_path').$upload['url'];
        }
        return $remoteImageUrl;
    }

    // returns gravatar url for email
    public function get_gravatar_url($email) {
            $hashe=hash("md5",$email);
            $dimension=300;
            $remoteImageUrl="https://www.gravatar.com/avatar/".$hashe."?s=$dimension&d=identicon";
            return $remoteImageUrl;
    }

    public function get_followed_ideas($socialid){
        $this->load->model('Dac');
        $where = array('socialid' =>$socialid);
        $result=$this->Dac->read('ideas_followers',$where);
        return $result;
    }
    public function get_list_of_ideas($socialid)
    {

        $this->load->model('Dac');
        $result=$this->Dac->read('ideas',array('ownerid'=>$socialid));

        if($result)
        {
            return $result;
        } else return false;
    }
    public function get_public_ideas($socialid) {
        $this->load->model('Dac');
        $result=$this->Dac->read('ideas',array('ownerid'=>$socialid,'visibility'=>'1'));

        if($result)
        {
            return $result;
        } else return false;
    }
    public function get_shortlisted_ideas($socialid)
    {
        $this->db->where('ideas_followers.socialid',$socialid);
        $this->db->from('ideas_followers');
        $this->db->join('ideas','ideas_followers.ideaid = ideas.id');
        $action=$this->db->get();
        if($action)
        {
            $row=$action->result_array();
            return $row;
        }
        else
        {
            return 0;
        }

    }
    public function get_list_of_services($socialid)
    {
        $this->db->where('ownerid',$socialid);
        $this->db->order_by('id');
        $action=$this->db->get('services');

        if($action)
        {
            $rows=$action->result_array();
            return $rows;
        }
        else
        {
            return 0;
        }
    }
    public function get_rate_cards($socialid)
    {

        $this->db->where('socialid',$socialid);
        $this->db->order_by('timestamp','desc');
        $action=$this->db->get('rate_cards');

        if($action)
        {
            $rows=$action->result_array();
            return $rows;
        }
        else
        {
            return 0;
        }
    }
    function get_rating($socialid)
    {

        $this->db->select("round(avg(rating),1) as rating,count(*) as ratings_count");

        $action=$this->db->get_where('reviews',array('socialid'=>$socialid));

        if($action)
        {
            $result=$action->result_array();
            return $result[0];
        } else return false;
    }
    public function get_email($socialid) {
        $this->db->where('socialid',$socialid);
        $this->db->select('email');
        $action=$this->db->get('users');
        if(!$action) return false;

        if(!$action->result_array()) return false;

        $result=$action->result_array();

        return $result[0]['email'];


    }
    public function get_completeness_score($socialid) {
        $u= $this->read($socialid);
        $score=0;
        $total_properties=array();
        if($u['user_type']=="ideator")
            $total_properties=$this->ideator_completeness_properties;
        //else if($u['user_type']=="service_provider") 
          //  $total_properties=$this->ideator_completeness_properties;
        //else if($u['user_type']=="investor")
         //   $total_properties=$this->ideator_completeness_properties;

        $total=count($total_properties);

        //log_message('debug',print_r($total_properties,true));
        //log_message('debug',print_r($u,true));
        foreach ($total_properties as $key) {
            if(array_key_exists($key, $u))
            {
                if($u[$key]!="" && $u[$key]!=null) {
                    $score++;
                }
                
            }
            if(array_key_exists('extra_data', $u)) {
                if(array_key_exists($key, $u['extra_data'][0])) {
                if($u['extra_data'][0]!="" && $u['extra_data'][0][$key]!=null) {
                    $score++;
                }
            }    
            }
    
        }
        return round(($score/$total)*100);
		}

		public function scan($params=array()) {
			$this->load->model('Dac');
			
			if(array_key_exists('user_type',$params)) {
				$this->db->where('user_type',$params['user_type']);
			}	
			
			if(array_key_exists('socialid',$params)) {
				$this->db->where('socialid',$params['socialid']);
			}
			
			if(array_key_exists('location',$params)) {
				$this->db->like('location',$params['location']);
			}
			
			if(array_key_exists('sector',$params)) {
                $this->db->where_in('sector_expertise',$array['sector'],false);
			}
			
			if(array_key_exists('order_by_timestamp',$params)) {
				$this->db->order_by('timestamp',$params['order_by_timestamp']);
			}
			
			if(array_key_exists('order_by_id',$params)) {
				$this->db->order_by('id',$params['order_by_id']);
			}
			
			if(array_key_exists('limit',$params)) {
				$this->db->limit($params['limit']);
			}

			if(array_key_exists('between_timestamp',$params)) {
				$start=$params['between_timestamp'][0];
				$end=$params['between_timestamp'][1];

				$this->db->where("date(timestamp) between '$start' and '$end'");
			}
			
			$action=$this->db->get('users');

			if(!$action) return array('status'=>'failure','description'=>'DB Error');
	
			$result = $action->result_array();

			if(!$result) return array('status'=>'success','description'=>'No records');

			foreach($result as &$user) {
				$socialid=$user['socialid'];
				
				$uploadid=$this->get_profile_picture($user['socialid']);

                if($uploadid){
        					$user['profile_picture_url']=$this->get_profile_picture_url($uploadid);
                            
                } else {
        					$user['profile_picture_url']=$this->get_gravatar_url($user['email']);
                }

                $user['facebook_username']=socialify_link($user['facebook_username'],'facebook');
                $user['twitter_username']=socialify_link($user['twitter_username'],'twitter');
                $user['linkedin_username']=socialify_link($user['linkedin_username'],'linkedin');
                $user['google_username']=socialify_link($user['google_username'],'google');

				if($user['user_type']=="ideator") {
					$ret_ideator=$this->Dac->read('ideators',array('socialid'=>$socialid));
					if(!$ret_ideator) {
						$ret_ideator=$this->ideator_properties;
					} else $user+=$ret_ideator[0];
				} else if($user['user_type']=="service_provider") {
					$ret_sp=$this->Dac->read('service_providers',array('socialid'=>$socialid));
					$rating_data=$this->get_rating($socialid);
					if($ret_sp) $user+=$ret_sp[0];
					if($rating_data) $user+=$rating_data;
				} else if($user['user_type']=="investor") {
					$ret_vc=$this->Dac->read('investors',array('socialid'=>$socialid));
					if($ret_vc) {
                        $user+=$ret_vc[0];
                        $user['investment_range_start']=_f_number_to_words($user['investment_range_start']);
                        $user['investment_range_end']=_f_number_to_words($user['investment_range_end']);
                        if($user['would_like_to_mentor_startups']==1) $user['would_like_to_mentor_startups']="Yes";
                        else $user['would_like_to_mentor_startups']="No";
                    }
					else {
						foreach($this->investor_properties as $key=>$value) {
							$ret_vc[$key]=$value;
						}
						$ret_vc['investment_range_start']=_f_number_to_words($ret_vc['investment_range_start']);
                        $ret_vc['investment_range_end']=_f_number_to_words($ret_vc['investment_range_end']);
                        if($ret_vc['would_like_to_mentor_startups']==1) $ret_vc['would_like_to_mentor_startups']="Yes";
                        else $ret_vc['would_like_to_mentor_startups']="No";
						$user+=$ret_vc;
					}
				} else {
				
				}			
			} //end  of main foreach
		
			return $result;	
		}
    

    public function get2($params=array()) {

        if(array_key_exists('socialid',$params)) {
                $this->db->where('socialid',$params['socialid']);
        }

        if(array_key_exists('user_type',$params)) {
            $this->db->where('user_type',$params['user_type']);
        }

        if(array_key_exists('order_by_timestamp',$params)) {
            $this->db->order_by('timestamp',$params['order_by_timestamp']);
            

        }

        $action=$this->db->get(self::TABLE);
        if(!$action) {
            return array(
                'status'=>'failure',
                'description'=>'DB Error',
                'error'=>$this->db->error()
            );
        }

        $result=$action->result_array();

        $users = array();

        foreach($result as &$row) {
            $temp_socialid=$row['socialid'];
            $row['gravatar_url']=$this->get_gravatar_url($row['email']);
            $row['profile_picture_upload_id']=$this->get_profile_picture($row['socialid']);
            $row['facebook_link']=$this->socialify_link($row['facebook_username'],'facebook');
            $row['twitter_link']=$this->socialify_link($row['twitter_username'],'twitter');
            $row['linkedin_link']=$this->socialify_link($row['linkedin_username'],'linkedin');
            $row['google_link']=$this->socialify_link($row['google_username'],'google');
            $row['user_type_human']=$this->user_type_human($row['user_type']);
            $row['last_active']=$this->get_last_active($row['socialid']);

            $users[$temp_socialid]=$row;

        }

        return array(
            'status'=>'success',
            'data'=>$users
        );

    }

    protected function user_type_human($user_type) {
        if($user_type=="ideator") return 'Ideator';
        if($user_type=="service_provider") return 'Service Provider';
        if($user_type=="investor") return "Investor";
        if($user_type=="admin") return "Admin";
    }
    public function count($params=array()) {
        if(array_key_exists('user_type',$params)) {
            $this->db->where('user_type',$params['user_type']);

        }

        $this->db->select('count(*)');
        $action=$this->db->get(self::TABLE);
        if(!$action) return 0;

        $result=$action->result_array();
        return $result[0]['count(*)'];
        
    }
    
    public function get_last_active($socialid) {
        $this->db->limit(1);
        $this->db->select('timestamp');
        $this->db->order_by('id','desc');
        $this->db->where('socialid',$socialid);
        $action=$this->db->get(self::TABLE);
        if(!$action) return null;

        $result=$action->result_array();
        if(!$result) return null;

        $t = strtotime($result[0]['timestamp']);
        return $t;

    }

    

}
?>
