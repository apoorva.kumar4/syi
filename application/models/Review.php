<?php
class Review extends CI_Model
{
	public $id;
	public $socialid;
	public $rating;
	public $description;
	public $long_description;
	public $date;


	public function __construct()
	{

	}

	public function create()
	{
		
		$validated_socialid=isset($this->$socialid)?$this->db->escape($this->$socialid):NULL;	
		
		$validated_rating=isset($this->$rating)?$this->db->escape(_f_validate('intnumber',$this->$rating)):NULL;

		$validated_description= isset($this->$description)?$this->db->escape(_f_validate('description',$this->$description)):NULL;	
		
		$validated_long_description= isset($this->$long_description)?$this->db->escape(_f_validate('description',$this->$long_description)):NULL;

		$validated_date= isset($this->$date)?$this->db->escape(_f_validate('date',$this->$date)):NULL;

	
		$query="Insert into reviews(socialid,rating,description,long_description,date) values($validated_socialid,$validated_rating,$validated_description,$validated_long_description,$validated_date)";

		$action=$this->db->query($query);

		if($action)
		{

		}
		else
		{

		}


	}
	public function read()
	{
		$query="Select * from reviews where id=$this->$id";

		$action=$this->db->query($query);

		if($action)
		{
			$row=$action->result_array();
			$this->$socialid=$row['socialid'];
			$this->$rating=$row['rating'];
			$this->$description=$row['description'];
			$this->$long_description=$row['long_description'];
			$this->$date=$row['date'];
			
		}
		else
		{

		}

	}
	public function update()
	{
		$id=$this->id;

		$query="update reviews set $tag=$value where id=$id";

		$action=$this->db->query($query);

		if($action)
		{

		}
		else
		{
			
		}
	}
	public function delete()
	{
		$query="delete from reviews where id=$this->$id";

		$action=$this->db->query($query);

		if($action)
		{

		}
		else
		{

		}

	}

}
?>

