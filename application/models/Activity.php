<?php
class Activity extends CI_Model{
    public function get($filters=null){
        // Confidential information so adding one more step to check if the logged in user is admin
        if(!_f_is_admin()) return false;
            
        $query="select * from activity where socialid not in (select socialid from admins) order by timestamp desc limit 50";
        $action=$this->db->query($query);
        if($action) {
            $row=$action->result_array();
            
            return $row;
        }
        else
        {
            return array();
        }
        
    }

    public function get2($filters=array()){
        // Confidential information so adding one more step to check if the logged in user is admin
        if(!_f_is_admin()) return false;

        $this->db->where('users.socialid not in (select socialid from admins)');
        $this->db->order_by('activity.timestamp','desc');
        
    
        if(array_key_exists('date_start',$filters)) {
            $this->db->where("activity.timestamp >= '".$filters['date_start']."'");
        }

        if(array_key_exists('date_end',$filters)) {
            $this->db->where("activity.timestamp <= '".$filters['date_end']."'");
        }
        

        $this->db->select('activity.id,activity.socialid,users.first_name, users.email, users.mobile, activity.event_value as Page, users.user_type, activity.description,activity.timestamp');
        
        $this->db->join('users','activity.socialid=users.socialid','left');

        $act = $this->db->get('activity');
//        return $this->db->get_compiled_select();
        $result= $act->result_array();
        return $result;

    }
    public function get_inactive_users($filters=null){
        if(_f_is_admin()){
            $fetched_array=array();
            $this->load->model('user3');
            
            
            
            $action=$this->user3->get2(array('order_by_timestamp'=>'desc'));

            if($action)
            {
                $row=$action['data'];
                $inactive_users=array();
                foreach($row as $user) {
                    if($user['last_active'] < (time() - 7890000)) {
                        array_push($inactive_users,$user);
                    }
                }
                $ret_data['status']="success";
                $ret_data['description']="fetched records successfully";
                $ret_data['inactive_users']=$inactive_users;
                return $ret_data;

            }
            else{
                return false;
            }
        }
    }

    public function query() {
        $query="select activity.id,activity.socialid,users.first_name, activity.description,activity.timestamp from activity left join users on activity.socialid=users.socialid where users.socialid not in (select socialid from admins) order by activity.timestamp desc";
        $action=$this->db->query($query);
        if($action) {
            $row=$action->result_array();
            
            return $row;
        }
        else
        {
            return array();
        }
    }
}
?>
