<?php
class Subscribe_newsletter extends CI_Model {
	const TABLE='subscribe_newsletter';
	public function add_email($email)
	{
		$data = array(
			'email'=>$email
		);

		$action=$this->db->replace(self::TABLE,$data);
		if($action) return true;
		return false;

	}

	public function get_emails()
	{
		$this->db->select('email');
		$action=$this->db->get(self::TABLE);
		if(!$action) return false;
		$result=$action->result_array();
		if(!$result) return false;
		return $result;
	}

	public function delete_email($email)
	{
		$this->db->where('email',$email);
		$action=$this->db->delete(self::TABLE);
		if(!$action) return false;
		return true;

	}
	public function is_email_subscribed($email) {
		$this->db->where('email',$email);
		$action=$this->db->get(self::TABLE);
		if(!$action) {
			$ret_data['status']="failure";
			$ret_data['description']="Database error";
			$ret_data['db_error']=$this->db->error();
			return $ret_data;
		}

		$result=$action->result_array();

		if(!$result) {
			$ret_data['status']="success";
			$ret_data['is_subscribed']=false;
			return $ret_data;
		}
		$ret_data['status']="success";
		$ret_data['is_subscribed']=true;
		return $ret_data;

	}
}
?>
