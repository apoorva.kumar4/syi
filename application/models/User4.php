<?php
class User4 extends CI_Model {
	const TABLE = 'users';
	const TABLE_PROFILE_PICTURES='profile_pictures';

	
	public function get($key,$value) {
		$table=self::TABLE;
		$this->db->from($table);
		$this->db->where($key,$value);
		$action=$this->db->get();
		if(!$action) {
			return false;
		} else {
			$result=$action->result_array();
			if(!$result) return false;
			else return $result;
		}
	}

	public function update($socialid, $data) {
		$table=self::TABLE;
		$updateable_properties = array(
	        'first_name',
	        'last_name',
	        'email',
            'role',
	        'location',
            'username',
	        'sector_expertise',
	        'sector_other',
	        'facebook_username',
	        'twitter_username',
	        'linkedin_username',
	        'google_username'
	    );

		$filtered_array=array();

		foreach($updateable_properties as $key) {
			if(array_key_exists($key, $data)) {
				$filtered_array[$key]=$data[$key];	
			}
			
		}

		$this->db->where('socialid',$socialid);
		$this->db->set($filtered_array);
		$action=$this->db->update(self::TABLE);
		if(!$action) return $this->db->error();
		else return true;
	}

	public function update_username($socialid,$username) {
		//update the username
		if(!$this->check_username_availability($username)){
			return 'username not available';
		}

		$this->db->set('username',$username);
		$this->db->where('socialid',$socialid);
		$action=$this->db->update(self::TABLE);
		
		if($action) return true;
		else return $this->db->error();
	}

	public function check_username_availability($username) {
		//check for username availablility
		$this->db->select('count(*)');
		$this->db->from(self::TABLE);
		$this->db->where('username',$username);
		$action=$this->db->get();
		if($action) {
			$result=$action->result_array();
			if($result[0]['count(*)']==0) {
				return true; //available
			} else return false;
		} else return false;
	}

	public function update_password($socialid,$new_password_hash) {
		$this->db->where('socialid',$socialid);
		$this->db->set('password_hash',$new_password_hash);
		$action=$this->db->update(self::TABLE);

		if($action) return true;
		else return $this->db->error();
	}

	public function get_profile_completeness($socialid) {
		$u= $this->get('socialid',$socialid);
        $score=0;
        $total_properties= array(
	        'first_name',
	        'last_name',
	        'email',
            'role',
	        'location',
	        'sector_expertise',
	        'facebook_username',
	        'twitter_username',
	        'linkedin_username',
	        'google_username'
	    );

        $total=count($total_properties);

        foreach ($total_properties as $key) {
            if($u[0][$key]!="" && $u[0][$key]!=null) {
                $score++;
            }
        }

        $profile_picture_exists=$this->get_profile_picture($socialid);

        if($profile_picture_exists) $score++;
        $total++;

        
        return round(($score/$total)*100);
	}

	public function get_gravatar_url($socialid) {
		$user=$this->get('socialid',$socialid);
		if(!$user) return "https://www.gravatar.com/avatar/2da6c458eb6a3ecc77dfacb7d84f3d51?d=identicon";
        $hashe=hash("md5",$user[0]['email']);
        $dimension=300;
        $remoteImageUrl="https://www.gravatar.com/avatar/".$hashe."?s=$dimension&d=identicon";
        return $remoteImageUrl;
    }

    public function get_profile_picture($socialid) {
        $user=$this->get('socialid',$socialid);
        if(!$user) return false;
        $this->db->limit(1);
        $this->db->order_by('id','DESC');
        $action=$this->db->get_where('profile_pictures',array('socialid'=>$socialid));
        if(!$action) return false;
        $row=$action->result_array();
        if(!$row) return false; 
        return $row[0]['uploadid'];
    }

    public function remove_profile_picture($socialid,$uploadid) {
        if(!is_numeric($socialid)) return false;
        $this->db->where(array('socialid'=>$socialid,'uploadid'=>$uploadid));
        $action=$this->db->delete('profile_pictures');
        if(!$action) return false;
        return true;
    }

    public function remove_profile_picures($socialid) {
    	if(!is_numeric($socialid)) return false;
        $this->db->where(array('socialid'=>$socialid));
        $action=$this->db->delete('profile_pictures');
        if(!$action) return false;
        return true;
    }

    public function add_profile_picture($socialid,$uploadid){
        $data=array(
            'socialid'=>$socialid,
            'uploadid'=>$uploadid
        );
        
        $action=$this->db->insert(self::TABLE_PROFILE_PICTURES,$data);
        if($action) return true;
        else return false;
    }

    public function delete($socialid){
        if(!$this->get('socialid',$socialid)) return false;

        $this->db->where('socialid',$socialid);
        $action=$this->db->delete(self::TABLE);
        if($action) return true;
        else return false;
    }

    protected function generate_socialid()
    {
        $random_number=rand(1000000,9999999);
        $this->db->where('socialid',$random_number);
        $this->db->select('count(*)');
        $action=$this->db->get(self::TABLE);
        $row=$action->result_array();
        if($row) {
            if($row[0]['count(*)']==0)
            {
                return $random_number;
            } else $this->generate_socialid();
        } else return false;
    }

    public function insert($data)
    {
        //validate the data
    	if(!$this->check_insert_data($data)) {
    		return false;
    	}
        
        // properties that are accepted from user
        $properties = array(
	        'first_name',
	        'last_name',
	        'email',
	        'mobile',
	        'password_hash',
	        'user_type',
	        'access_type',
	        'username',
	        'role',
	        'location',
	        'sector_expertise',
	        'sector_other',
	        'facebook_username',
	        'twitter_username',
	        'linkedin_username',
	        'google_username'
        );
        
        //filter the properties that can be inserted in db
        $filtered = $this->filter_data($properties,$data);

        $sanitized=$filtered;

        //generate random socialid
        $temp_socialid=$this->generate_socialid();
		if(!$temp_socialid) {
            log_message('debug','Could not generate socialid');
            return false;
        } 

        //assign socialid
		$sanitized['socialid']=$temp_socialid;

        //default profile is not verified
        $sanitized['verified_profile']=0; 
        
        //default username to socialid
        if(!array_key_exists('username',$sanitized))
        {   
        	$sanitized['username']=$sanitized['socialid'];
        }
        
        // Investor needs admin_approval
        if($sanitized['user_type']=="investor") $sanitized['admin_approved']=0;

        $this->db->set($sanitized);
        $action=$this->db->insert(self::TABLE,$sanitized);
        if(!$action) return false;
        return true;
    }

    public function check_insert_data($data) {
    	if(!is_numeric($data['mobile'])) {
            log_message('debug','invalid mobile');
            return "Invalid Mobile"; // invalid mobile
        }

    	if($this->get('mobile',$data['mobile'])) {
            log_message('debug','user already present');
            return "User already present";
        }

        if(array_key_exists('username',$data))
        {   
            $available=$this->check_username_availability($data['username']);
            if(!$available) return "Username not available";
        }

    	$required_properties=array(
            'first_name',
            'last_name',
            'mobile',
            'password_hash'
        );

        foreach ($required_properties as $key) {
            if(!(array_key_exists($key,$data))) {
                log_message('debug','required properties not met');
                return "Required property $key not provided";
            }
        }

        return true;
    }

    public function check_update_data($data) {
        if(array_key_exists('username',$data))
        {   
            $available=$this->check_username_availability($data['username']);
            if(!$available) return "Username not available";
        }

        if(array_key_exists('email', $data)) {
            $user_present=$this->get('email',$data['email']);
            if($user_present) {
                if($user_present[0]['socialid']!=$this->session->socialid){
                    return "Email Address already taken";
                }
            }
        }

        return true;
    }

    protected function filter_data($properties,$data) {
    	$filtered = array();

        foreach($properties as $key) {
        	if(array_key_exists($key,$data)) {
        		$filtered[$key]=$data[$key];
        	}

        }

        return $filtered;
    } 

}
?>
