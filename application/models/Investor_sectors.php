<?php
class Investor_sectors extends CI_Model {
	const TABLE = 'investor_sectors';

	public function update_sectors($socialid,$sectors) {
		if(sizeof($sectors) > 10 ){
			return array(
				'status'=>'failure',
				'description'=>'Only 10 sectors allowed'
			);
		}

		//remove all sectors first
		$this->db->where('socialid',$socialid);
		$delete_action=$this->db->delete(self::TABLE);

		$batch_data=array();

		// add updated sectors
		foreach($sectors as $sector) {
			$batch_item=array(
				'socialid'=>$socialid,
				'sectorid'=>$sector
			);
			array_push($batch_data,$batch_item);
		}

		$batch_action=$this->db->insert_batch(self::TABLE,$batch_data);

		if(!$batch_data) {
			return array(
				'status'=>'failure',
				'description'=>$this->db->error()
			);

		}

		return array('status'=>'success');

		
	}

	public function get_sectors($socialid) {
		$this->db->where('socialid',$socialid);
		$this->db->select('sectorid');
		$action=$this->db->get(self::TABLE);
		$sectors=array();
		if($action) {
			$result= $action->result_array();
			foreach($result as $sector) {
				array_push($sectors,$sector['sectorid']);
			}
			return $sectors;
		}
		else return array();
	}
}
?>