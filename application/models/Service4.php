<?php
class Service4 extends CI_Model {
	const TABLE="services";
	public function get_services_for_user($socialid) {
		$this->db->where('ownerid',$socialid);
		$action=$this->db->get(self::TABLE);
		if(!$action) return false;
		$result=$action->result_array();
		if(!$result) return array();
		return $result;
	}	
}
?>