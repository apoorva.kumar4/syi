<?php
class Otp extends CI_Model {
	public function insert_otp($mobile,$otp) {
		
		$this->delete_otp($mobile);
		
		$data = array(
			'mobile'=>$mobile,
			'otp'=>$otp
		);
		$action=$this->db->insert('mobile_otps',$data);
		if(!$action)
			return false;
		else return true;

	}

	public function verify_otp($mobile,$otp) {
		$data = array(
			'mobile'=>$mobile
		);
		$this->db->limit(1);
		$action=$this->db->get_where('mobile_otps',$data);
		
		if(!$action) return false;

		$result=$action->result_array();

		if(!$result) return false;

		//http_response_code(500);
		//var_dump($result[0]["wrong_attempts"]);die(0);
		// check if wrong attempts exceeded
		if($result[0]["wrong_attempts"] > 3){
			$this->delete_otp($mobile);
			return false;
		}

		if($result[0]['otp']!=$otp) 
		{
			//invalid otp, increase wrong attempt count
			$update_data = array (
				"wrong_attempts"=>$result[0]["wrong_attempts"]+1
			);

			$this->db->set($update_data);
			$this->db->where('mobile',$mobile);
			$action=$this->db->update('mobile_otps');
			return false;
		}
		
		date_default_timezone_set("UTC"); 

		$database_time=strtotime($result[0]['timestamp']);
		$now = time();

		$expiry=$database_time+600; // otp expires after 10 minutes

		if($now > $expiry) {
			return false;
		}

		if($result[0]['otp']==$otp)
		{
			return true;
		}
	}

	public function delete_otp($mobile) {
		$this->db->where('mobile',$mobile);
		$action=$this->db->delete('mobile_otps');
		if($action) return true;
		else return false;

	}

	public function generate_otp(){
		$digits = 6;
        $otp = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
        return $otp;
	}
	public function send_sms($mobile, $sms){

		
		$sms_sending=$this->Syiconfig->get('sms_sending');
		$sms_sending_test=$this->Syiconfig->get('sms_sending_test');
		if(!$sms_sending=="true") {
			return false;
		}

		if($sms_sending_test=="true") {
			return true;
		}


		$api_url=$this->config->item('sms_api_url');
		$user=$this->config->item('sms_api_user');
		$password=$this->config->item('sms_api_password');
		$senderid=$this->config->item('sms_api_senderid');

		$ch= curl_init();
		$query='?gwid=2&fl=0&user='.$user.'&password='.$password.'&sid='.$senderid.'&msisdn='.$mobile.'&msg='.curl_escape($ch,$sms);

		//var_dump($query);die(0);
		
		$complete_url=$api_url.$query;
		curl_setopt($ch, CURLOPT_URL, $complete_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		
		$result= curl_exec($ch);

		log_message('debug',print_r($result,true));
		log_message('debug',print_r(curl_getinfo($ch),true));

		curl_close($ch);

		if (strpos($result, 'error') !== false) {
    		return false;
		} else return true;

	}
}
?>
