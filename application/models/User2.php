<?php
class User2 extends CI_Model
{
    protected function sanitize($array)
	{
		// This function safely sanitizes data, removed any html tags and scripts
		$san=array();
		foreach($array as $key=>$value)
		{
            $san[$key]=is_string($array[$key])?strip_tags($array[$key]):$array[$key];
        }
        return $san;
	}
    
    public function create($array)
	{
        // Step 0 : sanitize data
        $sanitized=$this->sanitize($array); // array of sanitized values

        // Step 1 : check wheather the user email is in the table or not; if exists then produce error
        $present=_f_is_user_present($sanitized['email']);
        if($present)
        {
            $data['status']="failure";
            $data['description']="user already exists";
            return $data;
        }

        $prep_data=array(); // $prep_data finally goes into database


		// Step 2: check for username availability, return back if not availaible
        // if not set then give default username as socialid
        if(isset($sanitized['username']))
        {

            // username is provided so check for its availability
            $username_av=_f_username_availability($sanitized['username']);
            if(!$username_av)
            {
                $data['status']="failure";
                $data['description']="username taken";
                return $data;
            }
            $prep_data['username']=$sanitized['username'];
        }
        else{
            // Username is not provided so assign socialid as default username
            $prep_data['username']=_f_generate_socialid($sanitized['email']);
        }

        // STEP 3: Prepare data

        $properties=array(
            'first_name',
            'last_name',
            'email',
            'mobile',
            'role',
            'location',
            'sector_expertise',
            'emailverified',
            'user_type',
            'access_type',
            'admin_approved',
            'sector_other',
            'facebook_username',
            'twitter_username',
            'linkedin_username',
            'google_username'
        );

        $required_properties=array(
            'email'=>"Email",
            'first_name'=>"First name",
            'last_name'=>"Last name",
            'mobile'=>"Mobile",
            'role'=>"Role",
            'location'=>"Location",
            'sector_expertise'=>"Sector Expertise",
            'password'=>"Password"
        );
        // if any of the required property is found null then return error

        foreach($properties as $property)
        {
            $prep_data[$property]=isset($sanitized[$property])?$sanitized[$property]:null;
            if(array_key_exists($property,$required_properties)){
                if($prep_data[$property]==null)
                {
                    $data['status']="failure";
                    $property_label=$required_properties[$property];
                    $data['description']="$property_label is required";

                    return $data;
                }
            }
        }

        // Generate and assign unique socialid
        $prep_data['socialid']=_f_generate_socialid($sanitized['email']);

        // If access_type is not set then set it default to sayourideas
        if(!isset($sanitized['access_type']))
        {
            $prep_data['access_type']="sayourideas";
        }

        // Check for access type if it is sayourideas then hash the password
        if($sanitized['access_type']=="sayourideas"){
            if(isset($sanitized['password']))
            $prep_data['password_hash']=password_hash($sanitized['password'],PASSWORD_DEFAULT);
            else
                $prep_data['password_hash']=1;
        }
        else $prep_data['password_hash']=1; // access_type is not sayourideas so just set 1

        //Generate access token for the user
        $prep_data['access_token']=password_hash($sanitized['email'].$sanitized['password'],PASSWORD_DEFAULT);

        // Mark Registration as complete
        $prep_data['registration_complete']="Y";

        // if he is going to login via facebook/google/linkedin then his email is pre-verified
        if($sanitized['access_type']=="facebook" || $sanitized['access_type']=="google" || $sanitized['access_type']=="linkedin" )
        {

            $prep_data['emailverified']=1;
        }

        $action=$this->db->set($prep_data)->insert('users');

        if(!$action)
        {
            return $this->db->error();
        }
        else
        {
            $ret_data['status']="success";
            $ret_data['description']="user created successfully";
            $ret_data['socialid']=$prep_data['socialid'];
            return $ret_data;
        }
		
	}
	public function update($array){

        if(_f_is_admin() || $this->session->socialid==$array['socialid'])
        {
            // User is authorized to do so
        }
        else{
            //Unauthorized, return back
            $data['status']="failure";
            $data['description']="unauthorized access";
            return $data;
        }

        // Step 0 : sanitize data
        $sanitized=$this->sanitize($array); // array of sanitized values

        // Step 1 : check wheather the user email is in the table or not; if does not in database then produce error
        $this->db->where('socialid',$sanitized['socialid']);
        $this->db->select('count(*)');
        $ac_count=$this->db->get('users');
        if($ac_count) {
            $count = $ac_count->result_array();
            if ($count[0]['count(*)'] == 1)
            {

            }
            else{
                $data['status']="failure";
                $data['description']="user not present";
                return $data;
            }
        }

            $fetched_user=$this->read($sanitized['socialid']);
            $prep_data=array(); // $prep_data finally goes into database

            //check if the username is requested for change, then check its availability
            if(isset($sanitized['username']) && $fetched_user['user'][0]['username']!=$sanitized['username'])
            {
                // check for username availability, return back if not availaible

                    // username is provided so check for its availability
                    $username_av=_f_username_availability($sanitized['username']);
                    if(!$username_av)
                    {
                        $data['status']="failure";
                        $data['description']="username taken";
                        return $data;
                    }
                    $prep_data['username']=$sanitized['username'];
            }

            $properties=array(
                'first_name',
                'last_name',
                'mobile',
                'role',
                'location',
                'sector_expertise',
                'sector_other'
            );

            $required_properties=array(
                'first_name'=>"First name",
                'last_name'=>"Last name",
                'mobile'=>"Mobile",
                'role'=>"Role",
                'location'=>"Location",
                'sector_expertise'=>"Sector Expertise",
            );
            // if any of the required property is found null then return error

            foreach($properties as $property)
            {
                $prep_data[$property]=isset($sanitized[$property])?$sanitized[$property]:null;
                if(array_key_exists($property,$required_properties)){
                    if($prep_data[$property]==null)
                    {
                        $data['status']="failure";
                        $property_label=$required_properties[$property];
                        $data['description']="$property_label is required";

                        return $data;
                    }
                }
            }
            $this->db->where('socialid',$sanitized['socialid']);
            $action=$this->db->set($prep_data)->update('users');

            if(!$action)
            {
                return $this->db->error();
            }
            else
            {
                $ret_data['status']="success";
                $ret_data['description']="user updated successfully";
                return $ret_data;
            }
    }

	public function read($socialid)
    {
        if (is_numeric($socialid))
        {
            $this->db->where('socialid',$socialid);
            if(_f_is_admin() || $this->session->socialid==$socialid)
            {
                $this->db->select('id,socialid,first_name,last_name,email,mobile,user_type,timestamp,emailverified,username,role,location,sector_expertise,sector_other,blocked,admin_approved,verified_profile');
            }
            else{
                $this->db->select('id,socialid,first_name,last_name,user_type,timestamp,username,role,location,sector_expertise,sector_other,verified_profile');
                $this->db->where('blocked',0);
            }
            $action=$this->db->get('users');
            if($action)
            {
                $result=$action->result_array();
                if($result)
                {
                    $data['status']="success";
                    $data['description']="user fetched";
                    $data['user']=$result;
                    return $data;
                }
                else{
                    $data['status']="failure";
                    $data['description']="user not found";
                    return $data;
                }
            }
            else{
                $data['status']="failure";
                $data['description']="query could not be performed";
                return $data;
            }

        }
        else{
            $data['status']="failure";
            $data['description']="bad request";
            return $data;
        }
    }
}
?>