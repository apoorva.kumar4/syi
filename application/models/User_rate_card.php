<?php
class User_rate_card extends CI_Model {
	const TABLE='user_rate_cards';	
	public function create($data) {
		$ins_data = array(
			'socialid'=>$data['socialid'],
			'uploadid'=>$data['uploadid'],
			'name'=>$data['name']
		);
		$action=$this->db->insert(self::TABLE,$ins_data);
		if($action) {
			$ret_data=array(
				'status'=>'success',
				'id'=>$this->db->error()
			);

		}
		else {
			$ret_data=array(
				'status'=>'failure',
				'description'=>$this->db->error()
			);
		}
		
		return $ret_data;
	}
	public function get($socialid) {
		$this->db->where('socialid',$socialid);
		$action=$this->db->get(self::TABLE);
		if(!$action) return $this->db->error();
		else {
			$result=$action->result_array();
			if(!$result) return array();
			else return $result;
		}
	}
	public function delete($id) {
		$this->db->where('id',$id);
		$action=$this->db->delete(self::TABLE);
		$ret_data = array(
			'status'=>'succes'
		);
		return $ret_data;
	}
	public function get_id($id) {
		$this->db->where('id',$id);
		$action=$this->db->get(self::TABLE);
		if(!$action) {
			$ret_data = array(
				'status'=>'failure',
				'description'=>$this->db->error()
			);
		} else {
			return $action->result_array();
		}

	}
}
?>
