<?php
class Dac extends CI_Model {
	public function insert($table,$data) {
		$action=$this->db->insert($table,$data);
		if($action) {
			return true;
		}
		else {
			log_message('erorr',$this->db->error());
			return false;
		}

	}

	public function update($table,$data,$where){
		$this->db->where($where);
		$this->db->set($data);
		$action=$this->db->update($table);
		if($action) {
			return true;
		}
		else {
			log_message('erorr',$this->db->error());
			return false;
		}
	}

	public function read($table,$where) {
		$this->db->from($table);
		$this->db->where($where);
		$action=$this->db->get();
		if($action) {
			$row=$action->result_array();
			return $row;
		}
		else {
			log_message('erorr',$this->db->error());
			return false;
		}
	}

	public function delete($table,$where) {
		$this->db->where($where);
		$action=$this->db->delete($table);
		if($action) {
			return true;
		}
		else {
			log_message('erorr',$this->db->error());
			return false;
		}
	}

}
?>