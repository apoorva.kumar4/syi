<?php
class Service_provider extends CI_Model {
    public function sign_up($items){
        $data = array(
            'socialid' => $items['abe3e5_socialid'],
            'first_name' => $items['first_name'],
            'last_name' => $items['last_name'],
            'email' => $items['abe3e5_email'],
            'mobile' => $items['mobile'],
            'role' => $items['role'],
            'user_type' => $items['abe3e5_user_type'],
            'access_type' => $items['abe3e5_access_type'],
            'access_token' => $items['abe3e5_access_token'],
            'location' => $items['location'],
            'sector_expertise' => $items['sector_expertise'],
            'registration_complete' => 'Y',
            'password_hash' => $items['abe3e5_password_hash'],
            'username'=>$items['abe3e5_socialid'],
            'emailverified'=>$items['email_verified']
        );

        $data2 = array(
            'socialid' => $items['abe3e5_socialid'],
            'industry' => $items['industry'],
            'company' => $items['company'],
            'working_since' => $items['working_since']

        );

        // Depening upon if he is database or not, perform an insert or an update query

        $this->db->select('count(*)');
        $this->db->from('users');
        $this->db->where('socialid',$data['socialid']);
        $pre_query=$this->db->get();
        $row=$pre_query->row_array();



            if($row['count(*)']>0)
            {
                $this->db->where('socialid', $data['socialid']);
                $this->db->update('users', $data);
                $this->db->update('service_providers', $data2);
                return 1;
            }
            else
            {

                foreach($data as $key=>$item)
                {
                    if(is_null($item))
                    {
                        return $data;
                    }

                }

                $this->db->insert('users', $data);
                $this->db->insert('service_providers', $data2);
                return 1;
            }
	}
	public function get_list_of_services($socialid)
    {

        $query="select * from services where ownerid=$socialid order by id";

        $action=$this->db->query($query);

        if($action)
        {
            return $action->result_array();
        }
    }
    public function is_service_owned($pid)
    {
        $socialid=$this->session->userdata('socialid');
        $esc_pid=$pid;
        $query="Select count(*) from services where ownerid=$socialid and id=$esc_pid";
        $action = $this->db->query($query);

        if($action)
        {
            $row=$action->result_array();
            if($row[0]['count(*)']==1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public function get_rate_cards($socialid)
    {

        $query="Select * from rate_cards where socialid=$socialid order by timestamp desc";
        $action=$this->db->query($query);
        if($action)
        {
            $row=$action->result_array();
            return $row;
        }
    }
    public function get_suggested_ideas($socialid)
    {
        $query="select * from ideas where sectorid in (select sectorid from service_target_sector where serviceid in (select id from services where ownerid=$socialid)) and visibility=1";
        $action=$this->db->query($query);
        $result=$action->result_array();
        return $result;


    }
}