<?php
class Idea_note extends CI_Model
{
	public function create($par)
	{
		$data= array(
			'name'=> $par['name'],
			'text' => $par['text'],
			'ideaid' => $par['ideaid']
			);

		$action=$this->db->insert('idea_notes',$data);
		if($action)
		{
			return 1;
		}
		else 
		{
			return 0;
		}

	}
	public function read($par)
	{
	    $data = array (
	        'id' => $par
        );
	    $action=$this->db->get_where('idea_notes',$data);
	    $row=$action->result_array();
	    return $row[0];
	}
	public function update()
	{

	}
	public function delete()
	{

	}

}
?>