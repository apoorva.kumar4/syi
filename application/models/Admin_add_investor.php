<?php
class Admin_add_investor extends CI_Model {
	public function new_investor($data) {
		$required=['email','first_name','last_name','password'];
		foreach($required as $mandatory) {
			if(!array_key_exists($mandatory, $data)) {
				return ['status'=>'failure','description'=>'$mandatory is required'];
			}
		}

		// preliminary check if user exists with mobile
		if(array_key_exists('mobile',$data)) {
			$this->db->where('mobile',$data['mobile']);
			$action=$this->db->get('users');

			if(!$action) return ['status'=>'failure','description'=>'Unknown Error'];

			$result= $action->result_array();
			if($result) {
				return ['status'=>'failure','description'=>'User already exists'];	
			}
		}

		// preliminary check if user exists with email
		$this->db->where('email',$data['email']);
		$action=$this->db->get('users');

		if(!$action) return ['status'=>'failure','description'=>'Unknown Error'];

		$result= $action->result_array();
		if($result) {
			return ['status'=>'failure','description'=>'User already exists'];	
		}

		//proceed to add the user
		$socialid=$this->User3->generate_socialid();

		$insert_user_array = array(
			'first_name'=>$data['first_name'],
			'last_name'=>$data['last_name'],
			'email'=>$data['email'],
			'password_hash'=>password_hash($data['password'],PASSWORD_DEFAULT),
			'socialid'=>$socialid,
			'admin_approved'=>1,
			'user_type'=>'investor'
		);


		if(array_key_exists('mobile',$data)) {
			$insert_user_array['mobile']=$data['mobile'];
		} else {
			$insert_user_array['mobile']="Na";
		}

		$this->db->set($insert_user_array);
		$action=$this->db->insert('users');
		if(!$action) return ['status'=>'failure','description'=>'Unknown Error'];
		
		$this->load->model('User3');

		$data['socialid']=$socialid;

		$action=$this->User3->update($data);
		if($action) {
			$ret_data['status']="success";
			$ret_data['description']="data updated successfully";
			return $ret_data;
		}
		else {
			$ret_data['status']="failure";
			$ret_data['description']="ERR: 1519904239. There was an unknown error, please try again.";
			return $ret_data;
		}
	}
}
?>