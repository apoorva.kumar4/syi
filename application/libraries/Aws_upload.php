<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Aws\S3\S3Client;

class Aws_upload {
	protected $CI;
	protected $file_name;

    protected $s3_region;
    protected $s3_key;
    protected $s3_secret;
    protected $s3_bucket;
    protected $s3_client;

	// We'll use a constructor, as you can't directly call a function
	// from a property definition.
	public function __construct()
    {
    	$this->CI =& get_instance();
        $this->CI->load->model('Syiconfig');
        $syi_config= $this->CI->Syiconfig->get_all(); 
        //var_dump($syi_config);die(0);
        $this->s3_region=$syi_config['aws_s3_region'];
        $this->s3_key=$syi_config['aws_s3_key'];
        $this->s3_secret=$syi_config['aws_s3_secret'];
        $this->s3_bucket=$syi_config['aws_s3_bucket'];

        $this->s3_client= new S3Client([
            'version'     => 'latest',
            'region'      => $this->s3_region,
            'credentials' => [
                'key'    => $this->s3_key,
                'secret' => $this->s3_secret,
            ],
        ]);

    }

    public function do_upload($par)
    {
        
        $file_name=$par['targetfilename'];
        $file_path=$par['file']['tmp_name'];
        $file_access=$par['access'];
        $file_type=$par['file']["type"];
    	try {
            $this->s3_client->putObject([
                'ACL'=>$file_access,
                'Bucket' => $this->s3_bucket,
                'Key'=>$file_name,
                'SourceFile'=>$file_path,
                'ContentType'=>$file_type
            ]);
            
        }
        catch(AwsException $e) {
            return $e->get_message();
            
        }
    	return true;
	}
    public function get_upload($par)
    {
        
        $file_name=$par['targetfilename'];
        try {
            $result=$this->s3_client->getObject([
                'Bucket' => $this->s3_bucket,
                'Key'=>$file_name,
                'ResponseContentDisposition'=>$par['content-disposition']
            ]);
            return $result;
        }
        catch(AwsException $e) {
            echo "exception:" . $e->get_message();
            
        }
        return true;
    }
    public function delete($key){

        log_message('debug','In Aws_upload:delete($key)');

        $result=$this->s3_client->deleteObject([
            'Bucket' => $this->s3_bucket,
            'Key'=>$key
        ]);

    }
}
?>