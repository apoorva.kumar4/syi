<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sanitize {


function filter_and_sanitize($reference,$array) {
	$filter_array = array();

    foreach($reference as $key) {
        
        if(array_key_exists($key, $array)){
            $filter_array[$key]=is_string($array[$key])?strip_tags($array[$key]):$array[$key];
        }
    }
    return $filter_array;
}
}
?>