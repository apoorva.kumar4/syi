<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Recaptcha {


function verify_captcha($response,$secret,$recaptcha_api_url) {
	// response = received g-recaptcha-response from POST request
	// secret = site secret
	// recaptcha_api_url = google's recaptcha verify url

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $recaptcha_api_url);
	curl_setopt($ch,CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "secret=$secret&response=$response");
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

	$server_output = curl_exec($ch);

	$server_output_array = json_decode($server_output,true);
	//var_dump(curl_getinfo($ch));die(0);
	//var_dump($server_output_array);die(0);

	if($server_output_array['success']==true) {
		return true;
	}
	else{
		log_message('debug',print_r($server_output_array,true));
		return false;
			
	} 
}
}
?>