var pathArray = window.location.pathname.split('/');
var attr_socialid; // user in user_info/u/
var cities = [
    "Adilabad",
    "Agra",
    "Ahmedabad",
    "Ahmednagar",
    "Aizawl",
    "Ajitgarh (Mohali)",
    "Ajmer",
    "Akola",
    "Alappuzha",
    "Aligarh",
    "Alirajpur",
    "Allahabad",
    "Almora",
    "Alwar",
    "Ambala",
    "Ambedkar Nagar",
    "Amravati",
    "Amreli district",
    "Amritsar",
    "Anand",
    "Anantapur",
    "Anantnag",
    "Angul",
    "Anjaw",
    "Anuppur",
    "Araria",
    "Ariyalur",
    "Arwal",
    "Ashok Nagar",
    "Auraiya",
    "Aurangabad",
    "Aurangabad",
    "Azamgarh",
    "Badgam",
    "Bagalkot",
    "Bageshwar",
    "Bagpat",
    "Bahraich",
    "Baksa",
    "Balaghat",
    "Balangir",
    "Balasore",
    "Ballia",
    "Balrampur",
    "Banaskantha",
    "Banda",
    "Bandipora",
    "Bangalore Rural",
    "Bangalore Urban",
    "Banka",
    "Bankura",
    "Banswara",
    "Barabanki",
    "Baramulla",
    "Baran",
    "Bardhaman",
    "Bareilly",
    "Bargarh (Baragarh)",
    "Barmer",
    "Barnala",
    "Barpeta",
    "Barwani",
    "Bastar",
    "Basti",
    "Bathinda",
    "Beed",
    "Begusarai",
    "Belgaum",
    "Bellary",
    "Betul",
    "Bhadrak",
    "Bhagalpur",
    "Bhandara",
    "Bharatpur",
    "Bharuch",
    "Bhavnagar",
    "Bhilwara",
    "Bhind",
    "Bhiwani",
    "Bhojpur",
    "Bhopal",
    "Bidar",
    "Bijapur",
    "Bijapur",
    "Bijnor",
    "Bikaner",
    "Bilaspur",
    "Bilaspur",
    "Birbhum",
    "Bishnupur",
    "Bokaro",
    "Bongaigaon",
    "Boudh (Bauda)",
    "Budaun",
    "Bulandshahr",
    "Buldhana",
    "Bundi",
    "Burhanpur",
    "Buxar",
    "Cachar",
    "Central Delhi",
    "Chamarajnagar",
    "Chamba",
    "Chamoli",
    "Champawat",
    "Champhai",
    "Chandauli",
    "Chandel",
    "Chandigarh",
    "Chandrapur",
    "Changlang",
    "Chatra",
    "Chennai",
    "Chhatarpur",
    "Chhatrapati Shahuji Maharaj Nagar",
    "Chhindwara",
    "Chikkaballapur",
    "Chikkamagaluru",
    "Chirang",
    "Chitradurga",
    "Chitrakoot",
    "Chittoor",
    "Chittorgarh",
    "Churachandpur",
    "Churu",
    "Coimbatore",
    "Cooch Behar",
    "Cuddalore",
    "Cuttack",
    "Dadra and Nagar Haveli",
    "Dahod",
    "Dakshin Dinajpur",
    "Dakshina Kannada",
    "Daman",
    "Damoh",
    "Dantewada",
    "Darbhanga",
    "Darjeeling",
    "Darrang",
    "Datia",
    "Dausa",
    "Davanagere",
    "Debagarh (Deogarh)",
    "Dehradun",
    "Deoghar",
    "Deoria",
    "Dewas",
    "Dhalai",
    "Dhamtari",
    "Dhanbad",
    "Dhar",
    "Dharmapuri",
    "Dharwad",
    "Dhemaji",
    "Dhenkanal",
    "Dholpur",
    "Dhubri",
    "Dhule",
    "Dibang Valley",
    "Dibrugarh",
    "Dima Hasao",
    "Dimapur",
    "Dindigul",
    "Dindori",
    "Diu",
    "Doda",
    "Dumka",
    "Dungapur",
    "Durg",
    "East Champaran",
    "East Delhi",
    "East Garo Hills",
    "East Khasi Hills",
    "East Siang",
    "East Sikkim",
    "East Singhbhum",
    "Eluru",
    "Ernakulam",
    "Erode",
    "Etah",
    "Etawah",
    "Faizabad",
    "Faridabad",
    "Faridkot",
    "Farrukhabad",
    "Fatehabad",
    "Fatehgarh Sahib",
    "Fatehpur",
    "Fazilka",
    "Firozabad",
    "Firozpur",
    "Gadag",
    "Gadchiroli",
    "Gajapati",
    "Ganderbal",
    "Gandhinagar",
    "Ganganagar",
    "Ganjam",
    "Garhwa",
    "Gautam Buddh Nagar",
    "Gaya",
    "Ghaziabad",
    "Ghazipur",
    "Giridih",
    "Goalpara",
    "Godda",
    "Golaghat",
    "Gonda",
    "Gondia",
    "Gopalganj",
    "Gorakhpur",
    "Gulbarga",
    "Gumla",
    "Guna",
    "Guntur",
    "Gurdaspur",
    "Gurgaon",
    "Gwalior",
    "Hailakandi",
    "Hamirpur",
    "Hamirpur",
    "Hanumangarh",
    "Harda",
    "Hardoi",
    "Haridwar",
    "Hassan",
    "Haveri district",
    "Hazaribag",
    "Hingoli",
    "Hissar",
    "Hooghly",
    "Hoshangabad",
    "Hoshiarpur",
    "Howrah",
    "Hyderabad",
    "Hyderabad",
    "Idukki",
    "Imphal East",
    "Imphal West",
    "Indore",
    "Jabalpur",
    "Jagatsinghpur",
    "Jaintia Hills",
    "Jaipur",
    "Jaisalmer",
    "Jajpur",
    "Jalandhar",
    "Jalaun",
    "Jalgaon",
    "Jalna",
    "Jalore",
    "Jalpaiguri",
    "Jammu",
    "Jamnagar",
    "Jamtara",
    "Jamui",
    "Janjgir-Champa",
    "Jashpur",
    "Jaunpur district",
    "Jehanabad",
    "Jhabua",
    "Jhajjar",
    "Jhalawar",
    "Jhansi",
    "Jharsuguda",
    "Jhunjhunu",
    "Jind",
    "Jodhpur",
    "Jorhat",
    "Junagadh",
    "Jyotiba Phule Nagar",
    "Kabirdham (formerly Kawardha)",
    "Kadapa",
    "Kaimur",
    "Kaithal",
    "Kakinada",
    "Kalahandi",
    "Kamrup",
    "Kamrup Metropolitan",
    "Kanchipuram",
    "Kandhamal",
    "Kangra",
    "Kanker",
    "Kannauj",
    "Kannur",
    "Kanpur",
    "Kanshi Ram Nagar",
    "Kanyakumari",
    "Kapurthala",
    "Karaikal",
    "Karauli",
    "Karbi Anglong",
    "Kargil",
    "Karimganj",
    "Karimnagar",
    "Karnal",
    "Karur",
    "Kasaragod",
    "Kathua",
    "Katihar",
    "Katni",
    "Kaushambi",
    "Kendrapara",
    "Kendujhar (Keonjhar)",
    "Khagaria",
    "Khammam",
    "Khandwa (East Nimar)",
    "Khargone (West Nimar)",
    "Kheda",
    "Khordha",
    "Khowai",
    "Khunti",
    "Kinnaur",
    "Kishanganj",
    "Kishtwar",
    "Kodagu",
    "Koderma",
    "Kohima",
    "Kokrajhar",
    "Kolar",
    "Kolasib",
    "Kolhapur",
    "Kolkata",
    "Kollam",
    "Koppal",
    "Koraput",
    "Korba",
    "Koriya",
    "Kota",
    "Kottayam",
    "Kozhikode",
    "Krishna",
    "Kulgam",
    "Kullu",
    "Kupwara",
    "Kurnool",
    "Kurukshetra",
    "Kurung Kumey",
    "Kushinagar",
    "Kutch",
    "Lahaul and Spiti",
    "Lakhimpur",
    "Lakhimpur Kheri",
    "Lakhisarai",
    "Lalitpur",
    "Latehar",
    "Latur",
    "Lawngtlai",
    "Leh",
    "Lohardaga",
    "Lohit",
    "Lower Dibang Valley",
    "Lower Subansiri",
    "Lucknow",
    "Ludhiana",
    "Lunglei",
    "Madhepura",
    "Madhubani",
    "Madurai",
    "Mahamaya Nagar",
    "Maharajganj",
    "Mahasamund",
    "Mahbubnagar",
    "Mahe",
    "Mahendragarh",
    "Mahoba",
    "Mainpuri",
    "Malappuram",
    "Maldah",
    "Malkangiri",
    "Mamit",
    "Mandi",
    "Mandla",
    "Mandsaur",
    "Mandya",
    "Mansa",
    "Marigaon",
    "Mathura",
    "Mau",
    "Mayurbhanj",
    "Medak",
    "Meerut",
    "Mehsana",
    "Mewat",
    "Mirzapur",
    "Moga",
    "Mokokchung",
    "Mon",
    "Moradabad",
    "Morena",
    "Mumbai City",
    "Mumbai suburban",
    "Munger",
    "Murshidabad",
    "Muzaffarnagar",
    "Muzaffarpur",
    "Mysore",
    "Nabarangpur",
    "Nadia",
    "Nagaon",
    "Nagapattinam",
    "Nagaur",
    "Nagpur",
    "Nainital",
    "Nalanda",
    "Nalbari",
    "Nalgonda",
    "Namakkal",
    "Nanded",
    "Nandurbar",
    "Narayanpur",
    "Narmada",
    "Narsinghpur",
    "Nashik",
    "Navsari",
    "Nawada",
    "Nawanshahr",
    "Nayagarh",
    "Neemuch",
    "Nellore",
    "New Delhi",
    "Nilgiris",
    "Nizamabad",
    "North 24 Parganas",
    "North Delhi",
    "North East Delhi",
    "North Goa",
    "North Sikkim",
    "North Tripura",
    "North West Delhi",
    "Nuapada",
    "Ongole",
    "Osmanabad",
    "Pakur",
    "Palakkad",
    "Palamu",
    "Pali",
    "Palwal",
    "Panchkula",
    "Panchmahal",
    "Panchsheel Nagar district (Hapur)",
    "Panipat",
    "Panna",
    "Papum Pare",
    "Parbhani",
    "Paschim Medinipur",
    "Patan",
    "Pathanamthitta",
    "Pathankot",
    "Patiala",
    "Patna",
    "Pauri Garhwal",
    "Perambalur",
    "Phek",
    "Pilibhit",
    "Pithoragarh",
    "Pondicherry",
    "Poonch",
    "Porbandar",
    "Pratapgarh",
    "Pratapgarh",
    "Pudukkottai",
    "Pulwama",
    "Pune",
    "Purba Medinipur",
    "Puri",
    "Purnia",
    "Purulia",
    "Raebareli",
    "Raichur",
    "Raigad",
    "Raigarh",
    "Raipur",
    "Raisen",
    "Rajauri",
    "Rajgarh",
    "Rajkot",
    "Rajnandgaon",
    "Rajsamand",
    "Ramabai Nagar (Kanpur Dehat)",
    "Ramanagara",
    "Ramanathapuram",
    "Ramban",
    "Ramgarh",
    "Rampur",
    "Ranchi",
    "Ratlam",
    "Ratnagiri",
    "Rayagada",
    "Reasi",
    "Rewa",
    "Rewari",
    "Ri Bhoi",
    "Rohtak",
    "Rohtas",
    "Rudraprayag",
    "Rupnagar",
    "Sabarkantha",
    "Sagar",
    "Saharanpur",
    "Saharsa",
    "Sahibganj",
    "Saiha",
    "Salem",
    "Samastipur",
    "Samba",
    "Sambalpur",
    "Sangli",
    "Sangrur",
    "Sant Kabir Nagar",
    "Sant Ravidas Nagar",
    "Saran",
    "Satara",
    "Satna",
    "Sawai Madhopur",
    "Sehore",
    "Senapati",
    "Seoni",
    "Seraikela Kharsawan",
    "Serchhip",
    "Shahdol",
    "Shahjahanpur",
    "Shajapur",
    "Shamli",
    "Sheikhpura",
    "Sheohar",
    "Sheopur",
    "Shimla",
    "Shimoga",
    "Shivpuri",
    "Shopian",
    "Shravasti",
    "Sibsagar",
    "Siddharthnagar",
    "Sidhi",
    "Sikar",
    "Simdega",
    "Sindhudurg",
    "Singrauli",
    "Sirmaur",
    "Sirohi",
    "Sirsa",
    "Sitamarhi",
    "Sitapur",
    "Sivaganga",
    "Siwan",
    "Solan",
    "Solapur",
    "Sonbhadra",
    "Sonipat",
    "Sonitpur",
    "South 24 Parganas",
    "South Delhi",
    "South Garo Hills",
    "South Goa",
    "South Sikkim",
    "South Tripura",
    "South West Delhi",
    "Sri Muktsar Sahib",
    "Srikakulam",
    "Srinagar",
    "Subarnapur (Sonepur)",
    "Sultanpur",
    "Sundergarh",
    "Supaul",
    "Surat",
    "Surendranagar",
    "Surguja",
    "Tamenglong",
    "Tarn Taran",
    "Tawang",
    "Tehri Garhwal",
    "Thane",
    "Thanjavur",
    "The Dangs",
    "Theni",
    "Thiruvananthapuram",
    "Thoothukudi",
    "Thoubal",
    "Thrissur",
    "Tikamgarh",
    "Tinsukia",
    "Tirap",
    "Tiruchirappalli",
    "Tirunelveli",
    "Tirupur",
    "Tiruvallur",
    "Tiruvannamalai",
    "Tiruvarur",
    "Tonk",
    "Tuensang",
    "Tumkur",
    "Udaipur",
    "Udalguri",
    "Udham Singh Nagar",
    "Udhampur",
    "Udupi",
    "Ujjain",
    "Ukhrul",
    "Umaria",
    "Una",
    "Unnao",
    "Upper Siang",
    "Upper Subansiri",
    "Uttar Dinajpur",
    "Uttara Kannada",
    "Uttarkashi",
    "Vadodara",
    "Vaishali",
    "Valsad",
    "Varanasi",
    "Vellore",
    "Vidisha",
    "Viluppuram",
    "Virudhunagar",
    "Visakhapatnam",
    "Vizianagaram",
    "Vyara",
    "Warangal",
    "Wardha",
    "Washim",
    "Wayanad",
    "West Champaran",
    "West Delhi",
    "West Garo Hills",
    "West Kameng",
    "West Khasi Hills",
    "West Siang",
    "West Sikkim",
    "West Singhbhum",
    "West Tripura",
    "Wokha",
    "Yadgir",
    "Yamuna Nagar",
    "Yanam",
    "Yavatmal",
    "Zunheboto"

];

serialize = function(obj, prefix) {
    var str = [], p;
    for(p in obj) {
        if (obj.hasOwnProperty(p)) {
            var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
            str.push((v !== null && typeof v === "object") ?
                serialize(v, k) :
            encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
    }
    return str.join("&");
};

var JSONtoCSV = function(JSONData, ReportTitle, ShowLabel=true) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    //Set Report title in first row or line
    
    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    var fileName = "SYI_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    
    
    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
};

function downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV file
    csvFile = new Blob([csv], {type: "text/csv"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Hide download link
    downloadLink.style.display = "none";

    // Add the link to DOM
    document.body.appendChild(downloadLink);

    // Click download link
    downloadLink.click();
};

function exportTableToCSV(table,filename) {
    var csv = [];
    var rows = document.querySelectorAll(table+" tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);
        
        csv.push(row.join(","));        
    }

    // Download CSV file
    downloadCSV(csv.join("\n"), filename);
};

function s2ab(s) {
	if(typeof ArrayBuffer !== 'undefined') {
		var buf = new ArrayBuffer(s.length);
		var view = new Uint8Array(buf);
		for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
		return buf;
	} else {
		var buf = new Array(s.length);
		for (var i=0; i!=s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
		return buf;
	}
}

function export_table_to_excel(id, type, fn) {
	var wb = XLSX.utils.table_to_book(document.getElementById(id), {sheet:"Sheet JS"});
	var wbout = XLSX.write(wb, {bookType:type, bookSST:true, type: 'binary'});
	var fname = fn || 'test.' + type;
	try {
		saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), fname);
	} catch(e) { 
		if(typeof console != 'undefined') console.log(e, wbout); 
	}
	return wbout;
}


function get_events(options) {
	var query_params=serialize(options.query_params);
	$.ajax({
		type:'GET',
		url:'/api2/events?'+query_params,
		success:options.success,
		error: function(response) {
			M.toast({html:'There was an error while fetching events!'});
		}
	});
};

function get_stats(options) {
	$.ajax({
		type:'GET',
		url:'/api3/get_stats',
		success:options.success,
		error: function(response) {
			M.toast({html:'There was an error statistics!'});
		}

	});
};

function get_ideas(options) {
	var query_params=serialize(options);
	$.ajax({
		type:'GET',
		url:'/api2/ideas?'+query_params,
		success:options.success,
		error: function(response) {
			M.toast({html:'There was an error while fetching ideas!'});
		}
	});
};

function get_uploads(options) {
	var query_params=serialize(options);
	$.ajax({
		type:'GET',
		url:'/api2/uploads?'+query_params,
		success:options.success,
		error: function(response) {
			M.toast({html:'There was an error while fetching uploads!'});
		}
	});
};

function get_sectors(options) {
	$.ajax({
		type:'POST',
		url:'/api3/sectors',
		data:JSON.stringify({'action':'get'}),
		success: options.success
	});
};

function get_followed_ideas(options) {
	$.ajax({
		type:'GET',
		url:'/api2/user/followed_ideas',
		success: options.success
	});
}

function add_testimonial(options) {

	$.ajax({

	})
}

function unfollow_idea(options) {
	var idea_id= options.id;
	$.ajax({
		type:'PUT',
		url:'/api2/idea/'+idea_id+'/unfollow',
		success:options.success
	});
}

function get_me(options) {
	$.ajax({
		type:"GET",
		url:'/api2/me',
		success:options.success
	});
}

Vue.component('nav-bar',{
	props:['uploads','users','socialid','unreadmessages'],
	methods: {
		get_profile_picture_url: function(socialid) {
			var def="/assets/ffffff.png";

			//debugger;
			if(socialid=="") {
				return def;
			}
			
            if(typeof this.users == 'undefined') return def;
			if(typeof this.users[socialid] == 'undefined') return def;

			if(this.users[socialid].profile_picture_upload_id == false){
				return this.users[socialid].gravatar_url;
			} else {
				
				var upload_id = this.users[socialid].profile_picture_upload_id;
				var trt = this.uploads[upload_id].complete_url; 
				return trt?trt:def;
			}
			
		}
	}
});

Vue.component('add-admin',{
	props:['users'],
	data:function(){
		return {
			mobile:'',

		}
	},
	methods:{
		add_admin:function(event){
			var request = {
				action:'add',
				data:{
					mobile:this.mobile
				}
			};
			$.ajax({
				type:'POST',
				url:'/api3/admin',
				data:JSON.stringify(request),
				contentType:'application/json',
				success:function(response){
					M.toast({html:"Admin added successfully!"});
				},
				error:function(response) {
					console.log(response);
					
					console.log(response.responseJSON);
					M.toast({html:"Error: "+response.responseJSON.description});
				}
			});

		}
	}
});

Vue.component('manage-admins',{
	props:['admins','uploads','users'],
	data: function() {
		return {
			d_info: {
				first_name:'',
				last_name:'',
				privileges:[]
			},
			delete_user_selection:0
		}
	},
	methods: {
		delete_admin: function(i) {
			this.delete_user_selection=i;
			var elem = document.querySelector('#modal_admin_modify');
			var modify_admin_modal_instance = M.Modal.init(elem);
			modify_admin_modal_instance.open();
		},
		delete_admin_confirm: function() {
			var elem = document.querySelector('#modal_admin_modify');
			var modify_admin_modal_instance = M.Modal.init(elem);
			modify_admin_modal_instance.close();
			
			var socialid=this.users[this.admins[this.delete_user_selection]].socialid;
			var index=this.delete_user_selection;
			this.delete_user_selection=0;
			
			var delete_request = {
				action:'delete',
				data: {
					socialid:socialid
				}

			};

			var instance = this;
			
			$.ajax({
				url:'/api3/admin',
				type:'POST',
				data:JSON.stringify(delete_request),
				contentType:'application/json',
				success:function(response) {
					M.toast({html:'Admin removed Successfully'});

					admin_app.admins.splice(index,1);


				},
				error:function(response) {
					var response_json = response.responseJSON;
					M.toast({html:response_json.description});
				} 
			});
		}

	}
});

Vue.component('manage-partners-add',{
	data:function() {
		return {
			partner: {
				name:'',
				file:'',
				website:''
			}
		}
	},
	methods:{
		file_changed: function() {
			this.partner.file=this.$refs.logo_file.files[0];
		},
		submit: function() {
			console.log(this.partner);
			var formData = new FormData();
			formData.append('name',this.partner.name);
			formData.append('website',this.partner.website);
			formData.append('file',this.partner.file);
			M.toast({html:'Please wait...'});
			var instance=this;
			$.ajax('/api2/partners',{
				type:'POST',
				data:formData,
				processData:false,
				contentType: false,
				success:function(data) {
					M.toast({html:'Partner addedd successfully.'});
					//window.location='/admin/dashboard/partners';
				}
			});
		}
	}
});

Vue.component('manage-partners',{
	data: function() {
		return {
			partners:[],
			delete_partner:false
		}
	},
	mounted: function() {
		console.log('manage-partners mounted');
		var instance=this;
		$('.modal').modal();
		$.ajax({
			url:'/api2/partners',
			type:'GET',
			success:function(data) {
				//console.log('received partners',data);
				instance.partners=data;
			}
		});
	},
	methods: {
		do_delete_partner: function(e) {
			
			if(!this.delete_partner) return;
			var instance=this;
			console.log('before ajax',this.partners);
			
			$.ajax({
				url:'/api2/partners/'+this.delete_partner,
				type:'DELETE',
				success: data=> {
					console.log('removed partner',data);
					var temp_partners=this.partners.slice(0);
					
					var removed=_.remove(temp_partners,n=>{
						return n.id==this.delete_partner;
					});
					this.partners=temp_partners;
					$('.modal').modal('close');
					console.log('after ajax',this.partners);
				}
			});
		},
		show_delete_modal: function(id) {
			console.log(id);
			this.delete_partner=id;
			$('.modal').modal('open');

		}
	}
});

Vue.component('admin-profile',{
	props:['socialid','users','uploads','firstName','lastName','username'],
	data: function() {
		return {
			new_profile_picture_url:'',
			new_profile_picture_file:'',
			current_password:'',
			new_password:'',
			repeat_password:''
		}
	},
	methods:{
		open_profile_picture_modal: function(event) {
			$('#change-profile-modal').modal();
			$('#change-profile-modal').modal('open');
		},
		close_profile_picture_modal: function(event) {
			$('#change-profile-modal').modal();
			$('#change-profile-modal').modal('close');
		},
		upload_logo: function(event) {
			var instance=this;
			event.preventDefault();
			console.log('uploaded');
			M.toast({html:"<span class='upload-status'>Uploading...</span>"});
			var url_pur = "/api2/user/profile-picture/";
			var form_data = new FormData();
			form_data.append('file',this.new_profile_picture_file);
			console.log(form_data);
			$.ajax({
				url: url_pur, 
				type: "POST", 
				data: form_data, 
				contentType: false, 
				cache: false, 
				processData: false, 
				success: function(data) 
				{

					if (data.status == "success") {
						console.log(data);
						M.toast({html:"File Uploaded Successfully"});
						instance.$emit('profile_picture_uploaded',{complete_url:data.url,id:data.uploadid});
						instance.close_profile_picture_modal();
						
						
					} else {
						M.toast({html:"Upload Failed: " + data.description});
					}
				}
			});                    
		},
		filechanged: function(event) {
			var instance=this;
			var file=event.target.files[0];
			instance.new_profile_picture_file=file;
			var reader=new FileReader();
			reader.readAsDataURL(file);
			reader.onload=function(e) {
				console.log(e);
				instance.new_profile_picture_url=e.target.result;    
			}                    
		},
		change_password: function(event) {
			event.preventDefault();
			var instance=this;
			if(instance.new_password!=instance.repeat_password)
			{
				M.toast({html:'New password and Repeat Password do not match!'});
				return;
			}
			var request= {
				action:'change_password',
				data:{
					current_password:instance.current_password,
					new_password:instance.new_password
				}

			};

			$.ajax({
				type:"POST",
				url:"/api3/user",
				data:JSON.stringify(request),
				contentType:"application/json",
				success:function(response) {
					M.toast({html:'Password Changed successfully'});
					instance.current_password='';
					instance.new_password='';
					instance.repeat_password='';
				},
				error: function(response) {
					M.toast({html:'Please check your old password again!'});
				}
			});
		},
		update_profile:function(event) {
			event.preventDefault();
			var instance=this;

			var request= {
				action:'check_username_availability',
				data:{
					username:this.username
				}
			};

			$.ajax({
				type:"POST",
				url:"/api3/user",
				data:JSON.stringify(request),
				contentType:"application/json",
				error: function(response) {
					M.toast({html:'That username is not available!'});
					return;
				},
				success:function(response) {
					request= {
						action:'update',
						data:{
							first_name:instance.firstName,
							last_name:instance.lastName,
							username:instance.username,
						}
					};

					$.ajax({
						type:"POST",
						url:"/api3/user",
						data:JSON.stringify(request),
						contentType:"application/json",
						success:function(response) {
							M.toast({html:'Changes Saved Successfully!'});
							instance.$emit('profile_updated',{first_name:instance.firstName,last_name:instance.lastName,username:instance.username});
						},
						error: function(response) {
							M.toast({html:'There was an unknown error!'});
						}
					});
				}
			});

		}
	},
	mounted:function() {
		$('#change-profile-modal').modal();
	}
});

Vue.component('user-activity',{
	props:['users','uploads'],
	data:function(){
		return {
			activity:[],
			date_start:'',
			date_end:''
		}
	},
	methods: {
		localDate: function(param) {
			var d = new Date(param);
			var options={
				timeZone:'Asia/Kolkata',
				year:'numeric',
				month:'short',
				day:'2-digit',
				hour:'numeric',
				minute:'numeric',
			};
			return d.toLocaleDateString('en-us',options);
		},
		download: function(event) {
			this.$http.get('/api2/activityall?date_start='+this.date_start+'&date_end='+this.date_end).then(response=>{
				JSONtoCSV(response.data.activity,"All Activity by Users");	
			});

		}
	},
	mounted:function() {
		this.$http.get('/api2/activity').then(response=>{
			this.activity=response.data.activity;
		});
	}
});

Vue.component('user-view',{
	props:['user','users','sectors','uploads'],
	
	methods:{
		user_approve: function(event) {
			var temp = event.target;
			debugger;
			var request = {
				action:'approve',
				socialid:this.user.socialid
			};
			var instance=this;
			$.ajax({
				url: "/api3/user",
				type: "POST",
				data:JSON.stringify(request),      
				success: function(response)
				{
					M.toast({html:"Invesor Approved Successfuly"});
					instance.user.admin_approved='1';
				},
				error: function(response) {
					M.toast({html:"User Approval Failed"});
				}
			});
		},
		user_delete: function(event) {
			
			var request={
				action:'delete',
				socialid:this.user.socialid
			};
			
			$.ajax({
				url:'/api3/user',
				type:"POST",
				data:JSON.stringify(request),
				success: function (response)
				{
					M.toast({html:"User has been deleted"});   
					setTimeout(function(){
						window.location='/admin';   
					},2000)
				},
				error: function (xhr)
				{
					M.toast({html:xhr.responseText});
					
				}
			});
		},
		user_verify: function(event) {
			var instance=this;
			var request={
				socialid:this.user.socialid,
				action:'verify_badge'
			};
			
			$.ajax({
				url:'/api3/user',
				type:"POST",
				data:JSON.stringify(request),
				
				success: function (response)
				{
					M.toast({html:"User has been Verified"});   
					
					instance.user.verified_profile='1';
				},
				error: function (xhr)
				{
					M.toast({html:xhr.responseText});
				}
			});
		},
		user_block: function(event) {
			var instance= this;
			var request={
				socialid:this.user.socialid,
				action:'block'
			};
			
			$.ajax({
				url:'/api3/user',
				type:"POST",
				data:JSON.stringify(request),
				
				success: function (response)
				{
					M.toast({html:"User has been blocked successfully!"});   
					instance.user.blocked="1";
				},
				error: function (xhr)
				{
					M.toast({html:xhr.responseText});
				}
			});
		},
		user_unblock: function(event) {
			var instance= this;
			var request={
				socialid:this.user.socialid,
				action:'unblock'
			};
			
			$.ajax({
				url:'/api3/user',
				type:"POST",
				data:JSON.stringify(request),
				
				success: function (response)
				{
					M.toast({html:"User has been unblocked successfully!"});   
					instance.user.blocked="0";
				},
				error: function (xhr)
				{
					M.toast({html:xhr.responseText});
				}
			});
		},
		show_delete_modal:function(event) {
			
			var modal_elem = document.querySelector('#modal_delete_user');
			var modal_instance = M.Modal.init(modal_elem);
			modal_instance.open();

		},
		back_window: function(event) {
			history.back();
    		return false;
		}
	}
});

Vue.component('add-testimonial',{
	data:function() {
		return {
			profile_file:'',
			first_name:'',
			last_name:'',
			company:'',
			position:'',
			description:'',
			website:'',
			active:1
		}
	},
	methods: {
		profile_file_changed:function(event) {
			var file=event.target.files[0];
			this.profile_file=file;  
		},
		submit_data:function(event) {
			let form_data = new FormData();
			form_data.append('profile_file',this.profile_file);
			form_data.append('first_name',this.first_name);
			form_data.append('last_name',this.last_name);
			form_data.append('company',this.company);
			form_data.append('position',this.position);
			form_data.append('website',this.website);
			form_data.append('active',this.active);
			form_data.append('description',this.description);
			this.$http.post('/api2/testimonials',form_data).then(response=>{
				console.log(response);
				M.toast({html:'Testimonail Added Successfully'});
			},response=>{
				console.log(response);
				M.toast({html:response.data.description});
			});

		}
	}
});

Vue.component('testimonials',{
	data: function() {
		return {
			testimonials:[]
		}
	},
	mounted: function() {
		this.fetch_testimonials();
	},
	methods: {
		fetch_testimonials:function() {
			
			this.$http.get('/api2/testimonials').then(response=>{
				this.testimonials=response.data.data;
			});
		},
		activate_testimonial:function(index) {
			let url_pur = "/api2/testimonial/" + this.testimonials[index].id + "/" + 'activate';
			this.$http.patch(url_pur).then(response=>{
				M.toast({html:'Testimonial Activated Successfully!'});
				this.testimonials[index].active='1';
			});
		},
		deactivate_testimonial:function(index) {
			let url_pur = "/api2/testimonial/" + this.testimonials[index].id + "/" + 'deactivate';
			this.$http.patch(url_pur).then(response=>{
				M.toast({html:'Testimonial Deactivated Successfully!'});
				this.testimonials[index].active='0';
			});
		},
		delete_testimonial:function(index) {
			let url_pur = "/api2/testimonial/" + this.testimonials[index].id;
			this.$http.delete(url_pur).then(response=>{
				M.toast({html:'Testimonial Deleted Successfully!'});
				let updated_testimonials = _.filter(this.testimonials,function(o,i){
					
					return i!=index;
				});
				
				this.testimonials=updated_testimonials;
			});
		}
	}
});

var admin_app = new Vue({
	el:'#admin_app',
	data:{
		my_socialid:'',
		user:{},
		admins:[
			{
				socialid:'1',
				first_name:' ',
				last_name:' ',
				username:' ',
				permissions:[],
				profile_picture_url:''
			}
		],
		users:{},
		sectors:[],
		user_activity:[],
		uploads:[],
		ideas:[],
		followed_ideas:[],
		filtered_ideas:[],
		filter_idea_name:'',
		count_ideas:0,
		count_ideators:0,
		count_service_providers:0,
		count_investors:0,
		ideas:[],
		events:[],
		filter_phase:'All',
		filter_user_type:'All',
		filter_sector:'All',
		filter_date_start:'',
		filter_date_end:'',
		filter_location:'',
		filter_user_name:'',
		filter_user_mobile:'',
		checkedIdeas:[],
		checkedUsers:[],
		cities:cities,
		inactive_users_socialids:[],
		count_unread_messages:0
	},
	computed:{
		admin:function() {
			return this.users[this.my_socialid];
		},
        filter_user_type_human: function() {
            if(this.filter_user_type=="ideator") return "Ideator";
            else if(this.filter_user_type=="service_provider") return "Service Provider";
            else if(this.filter_user_type=="investor") return "Investor";    
        },
		current_admin:function() {
			for (var i=0; i < this.admins.length; i++) {
				if (this.admins[i] === this.my_socialid) {
					return i;

				}
				
			}
			return 0;
		},
		recent_registrations:function(){
			var registrations_by_order= _.orderBy(this.users,['timestamp'],['desc']);
			return registrations_by_order.splice(0,5);
		},
		recent_registrations_all:function(){
			return _.orderBy(this.filter_users(),['timestamp'],['desc']);
		},
		recent_ideas:function() {
			var ideas_by_order= _.orderBy(this.filter_ideas(),['created_on'],['desc']);
			return ideas_by_order;
		},
		recent_events: function() {
			var events_by_order= _.orderBy(this.events,['date_start'],['desc']);
			return events_by_order;
		},
		recent_ideas_five:function() {
			var ideas_by_order= _.orderBy(this.filter_ideas(),['created_on'],['desc']);
			return ideas_by_order.splice(0,5);
		},
		email_suggestions:function() {
			var email_suggestions=[];
			for(u in this.users) {
				
				email_suggestions.push({id:this.users[u].socialid,value:this.users[u].email,label:this.users[u].email});
			}
			return email_suggestions;
		},
		num_ideators:function(){
			
			return  _.filter(this.users, { 'user_type': 'ideator' }).length;
		},
		num_service_providers:function(){
			return  _.filter(this.users, { 'user_type': 'service_provider' }).length;
		},
		num_investors:function(){
			return  _.filter(this.users, { 'user_type': 'investor' }).length;
		}

	},
	methods:{
		profilePictureUploaded:function(upload) {
			
			this.uploads[upload.id]=upload;
			this.users[this.my_socialid].profile_picture_upload_id=upload.id;
			

		},
		profile_updated:function(updated_data) {
			console.log(updated_data);
			
			this.users[this.my_socialid].first_name=updated_data.first_name;
			this.admins[this.my_socialid].last_name=updated_data.last_name;
			this.admins[this.my_socialid].username=updated_data.username;

		},
		localDate: function(param) {
			var d = new Date(param);
			var options={
				timeZone:'Asia/Kolkata',
				year:'numeric',
				month:'short',
				day:'2-digit',
				
			};
			return d.toLocaleDateString('en-us',options);
		},
		filter_ideas:function() {
			var instance = this;

			var filters={};
			if(this.filter_phase!=='' && this.filter_phase !== "All"){
				filters.stage=this.filter_phase;
			}

			var filter_stage_0 = this.ideas;

			if(this.filter_location!=='') {
				filter_stage_0= _.filter(filter_stage_0,o=>{
					if(o.address != null) {
						var reg = new RegExp(this.filter_location.toLowerCase());
						//console.log(this.filter_location);
						//console.log(o.address.toLowerCase());
						if(reg.test(o.address.toLowerCase())) {
							return true;
						}
					}
					
				});

			}

			if(this.filter_idea_name!=='') {
				filter_stage_0= _.filter(filter_stage_0,o=>{
					if(o.name != null) {
						if(o.name.toLowerCase().includes(this.filter_idea_name.toLowerCase())) return true;
					}
					
				});

			}

			if(this.filter_sector!=='' && this.filter_sector !== "All"){
				filters.sectorid=this.filter_sector;
			}


			
			if(this.filter_date_start!=="" && this.filter_date_end!=="") {
				var filter_stage_1 = filter_stage_0;
				if(filters) {
					console.log('yes filters');
					filter_stage_1= _.filter(this.ideas,filters);
				}
				
				var filtered_by_date = _.filter(filter_stage_1,function(o){
					//debugger;
					if(moment(o.created_on).isAfter(instance.filter_date_start) && moment(o.created_on).isBefore(instance.filter_date_end)) {
						return true;
					}
				});

				return filtered_by_date;
			} else {
				return _.filter(filter_stage_0,filters);
			}
		},
		filter_users:function() {
			var instance = this;
			var filter_stage_0 = this.users;

			var filters={};
			if(this.filter_location!=='' && this.filter_location !== "All"){
				filters.location=this.filter_location;
				console.log(filters);
			}

			if(this.filter_user_name!==''){
				let s = _.filter(filter_stage_0, o => {
					if(o.first_name.toLowerCase().includes(this.filter_user_name.toLowerCase())) return true;
					if(o.last_name.toLowerCase().includes(this.filter_user_name.toLowerCase())) return true;
					if((o.first_name.toLowerCase()+" "+o.last_name.toLowerCase()).includes(this.filter_user_name.toLowerCase())) return true;
				});
				filter_stage_0=s;
			}

			if(this.filter_user_mobile!==''){
				let s = _.filter(filter_stage_0, o => {
					if(o.mobile.includes(this.filter_user_mobile)) return true;
				});
				filter_stage_0=s;
			}

			if(this.filter_user_type!=='' && this.filter_user_type !== "All"){
				filters.user_type=this.filter_user_type;
				console.log(filters);
			}

			if(this.filter_sector!=='' && this.filter_sector !== "All"){
				filters.sector_expertise=this.filter_sector;
			}
			
			if(this.filter_date_start!=="" && this.filter_date_end!=="") {
				var filter_stage_1 = filter_stage_0;
				if(filters) {
					console.log(filters);
					filter_stage_1= _.filter(this.users,filters);
				}
				
				var filtered_by_date = _.filter(filter_stage_1,function(o){
					//debugger;
					if(moment(o.timestamp).isAfter(instance.filter_date_start) && moment(o.timestamp).isBefore(instance.filter_date_end)) {
						return true;
					}
				});

				return filtered_by_date;
			} else {
				return _.filter(filter_stage_0,filters);
			}
		},
		checkAllIdeas:function() {
			this.checkedIdeas=[];
			var idea_keys = Object.keys(this.recent_ideas);

			for(var i=0;i<idea_keys.length;i++) {
				this.checkedIdeas.push(idea_keys[i]);
			}
		},
		uncheckAllIdeas:function() {
			this.checkedIdeas=[];
		},
		download_selected_ideas:function(event) {
			
			export_table_to_excel('table_selected_ideas','xlsx','ideas.xlsx');
		},
		download_selected_users:function(event) {
			
			export_table_to_excel('table_selected_users','xlsx','users.xlsx');
		},
		checkAllUsers:function() {
			
			this.checkedUsers=[];
			var dense_keys = [...this.recent_registrations_all.keys()];

			for(var i=0;i<dense_keys.length;i++) {
				this.checkedUsers.push(dense_keys[i]);
			}
		},
		uncheckAllUsers:function() {
			this.checkedUsers=[];
		},
		unfollowIdea:function(id) {
			var instance = this;
			console.log(id);
			unfollow_idea({
				id:id,
				success:function(response) {
					M.toast({html:'Idea Unfollowed Successfully!'});

					let followed_ideas=_.filter(instance.followed_ideas,function(o) {
						return o!=id;
					});
		
					instance.followed_ideas=followed_ideas;
				}
			});
			
		},
		download_inactive_users() {
			export_table_to_excel('table_inactive_users','xlsx','inactive-users.xlsx');
		}
		
		
	},
	watch:{
		ideas:function(value){
			this.filter_ideas();
		}
	},
	mounted:function() {
        if(pathArray.includes("registrations") && pathArray.includes("ideators"))
            this.filter_user_type="ideator";
        if(pathArray.includes("registrations") && pathArray.includes("service_providers"))
            this.filter_user_type="service_provider";
        if(pathArray.includes("registrations") && pathArray.includes("investors"))
			this.filter_user_type="investor";
			
		if(pathArray.includes("top_ideas")) {
			drake = dragula([document.getElementById('dragula-cont')]);
			drake.on('drop',function(el){
				var rank=indexInParent(el)+1;
				var ideaid=el.getAttribute('data-ideaid');
				var ms= new XMLHttpRequest();
				ms.open('PUT','/api2/top_idea_drag',true);
				ms.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				ms.send("rank="+rank+"&ideaid="+ideaid);

				ms.onreadystatechange = function(){
					if(this.readyState==4 && this.status==200)
					{
						M.toast({html:'Changes Saved'});
					}
				};
			});
		}

		//get me
		this.$http.get('/api2/me').then(response=>{
			this.my_socialid=response.data.socialid;
		});
		
		//get all uploads
		this.$http.get('/api2/uploads').then(response=>{
			this.uploads=response.data.data;
		});

		// get all admins
		$request_get_admins={
			action:'get',
			data:{}
		};

		this.$http.post('/api3/admin',$request_get_admins).then(response=>{
			this.admins=response.data.data;
		},response=>{
			M.toast({html:'There was an error fetching the details for your profile'});
		});

		//get all users
		$request_get_users={
			action:'get2',
			data:{}
		};

		this.$http.post('/api3/user',$request_get_users).then(response=>{
			this.users=response.data.data;
			
			$(".receiversx").selectize({
				delimiter:',',
				persist:false,
				create: function(input) {
					return {
						value: input,
						text: input,
						label: input
					}
				},
				options: admin_app.email_suggestions,
				valueField:'value',
				labelField:'value',
				searchField:'value',
				preload:true,
				openOnFocus: false,
				closeAfterSelect: true,
				addPrecedence: true,
			});

			if(pathArray.includes('user_info')) {
				this.user =this.users[pathArray[3]];				
			}
		},response=>{
			M.toast({html:'There was an error fetching users'});
		});

		//get ideas
		this.$http.get('/api2/ideas').then(response=>{
			this.ideas=response.data.data;
		});

		//get inactive users socialid
		this.$http.get('/api2/users/inactive').then(response=>{
			this.inactive_users_socialids=response.data.data;
		});

		//get unread message count
		this.$http.get('/api2/unapproved_messages_count').then(response=>{
			this.count_unread_messages= parseInt(response.data.count);
		});
	}

});




if(pathArray.includes('recent_activity')) {

	
} 
else if((pathArray.length==2 && pathArray.includes('admin')) || (pathArray.length==3 && pathArray.includes('admin') && pathArray[2]==="")) {
	// we are at dashboard
	
	get_stats({
		success:function(response){
			admin_app.count_ideas=response.data.ideas;
			admin_app.count_ideators=response.data.ideators;
			admin_app.count_service_providers=response.data.service_providers;
			admin_app.count_investors=response.data.investors;	
		},
	});

	get_events({
		query_params:{
			limit:5,
			order_by_id:'desc',
			order_by_date_start:'desc'
		},
		success:function(response) {
			admin_app.events=response.data;
		},
	});

} else if(pathArray.includes('followed_ideas')) {
	get_followed_ideas({
		success:function(response) {
			admin_app.followed_ideas=response.data
		}
	});
}

get_sectors({
	success:function(response){
		admin_app.sectors=response.data;	
	},
});	

var drake; //for dragula on top_ideas
jQuery(document).ready(function() {
	$('.sidenav').sidenav();
	
	$("#reg-user-top-idea").click(function(e) {
		e.preventDefault();
		var url_pur = "/api2/idea/" + $(this).attr('data-ideaid') + "/" + $(this).attr("data-action");
		var pare = $(this);
		var after_action;
		var text_action;
		if ($(this).attr("data-action") == "top") {
			after_action = "untop";
			text_action = "Unmark as Top Idea";
		} else {
			after_action = "top";
			text_action = "Mark as Top Idea";
		}
		$.ajax({
			url: url_pur, // Url to which the request is send
			type: "PATCH", // Type of request to be send, called as method
			contentType: false, // The content type used when sending data to the server.
			cache: false, // To unable request pages to be cached
			processData: false, // To send DOMDocument or non processed data file it is set to false
			success: function(data) // A function to be called if request succeeds
			{
				var obj = JSON.parse(data);

				if (obj.status == "success") {
					M.toast({html:"Success"});
					pare.attr("data-action", after_action);
					pare.text(text_action);

				} else {
					console.log(data);
					M.toast({html:"Removed from top"});
				}
			}
		});
	});

	$(".approvemessage").on('click', (function(e) {
		e.preventDefault();
		M.toast({html:"<span class='upload-status'>Please wait...</span>"});
		var url_pur = "/api2/message/" + $(this).attr('data-messageid') + "/approve";
		var pare = $(this);
		$.ajax({
			url: url_pur, // Url to which the request is send
			type: "PATCH", // Type of request to be send, called as method
			contentType: false, // The content type used when sending data to the server.
			cache: false, // To unable request pages to be cached
			processData: false, // To send DOMDocument or non processed data file it is set to false
			success: function(data) // A function to be called if request succeeds
			{

				var obj = data;
				if (obj.status == "success") {
					M.toast({html:"Message Approved Successfuly"});
					pare.parent().parent().fadeOut();

				} else {
					console.log(data);
					M.toast({html:"Message Approve Failed"});
				}
			}
		});
	}));

	$(".rejectmessage").on('click', (function(e) {
		e.preventDefault();
		M.toast({html:"<span class='upload-status'>Please wait...</span>"});
		var url_pur = "/api2/message/" + $(this).attr('data-messageid') + "/reject";
		var pare = $(this);
		$.ajax({
			url: url_pur, // Url to which the request is send
			type: "PATCH", // Type of request to be send, called as method
			contentType: false, // The content type used when sending data to the server.
			cache: false, // To unable request pages to be cached
			processData: false, // To send DOMDocument or non processed data file it is set to false
			success: function(data) // A function to be called if request succeeds
			{

				obj = data;
				if (obj.status == "success") {
					M.toast({html:"Message Rejected Successfuly"});
					pare.parent().parent().fadeOut();
				} else {
					console.log(data);
					M.toast({html:"Message Rejection Failed"});
				}
			}
		});
	}));

	$(".deletesector").click(function(e) {
		e.preventDefault();
		var url_pur = "/api2/sector/" + $(this).attr('data-sectorid');
		var pare = $(this);
		$.ajax({
			url: url_pur, // Url to which the request is send
			type: "DELETE", // Type of request to be send, called as method
			contentType: false, // The content type used when sending data to the server.
			cache: false, // To unable request pages to be cached
			processData: false, // To send DOMDocument or non processed data file it is set to false
			success: function(data) // A function to be called if request succeeds
			{
				var obj = data;
				if (obj.status == "success") {
					M.toast({html:"Sector Deleted Successfuly"});
					pare.parent().parent().parent().fadeOut();

				} else {
					console.log(data);
					M.toast({html:"Sector Delete Failed"});
				}
			},
			error: function(xhr) {
				console.log(xhr);
				M.toast({html:"Sector Delete Failed"});
			}

		});
	});

	$(".approveevent").on('click', (function(e) {
		e.preventDefault();
		var url_pur = "/api2/event/" + $(this).attr('data-event_id') + "/approve";
		var pare = $(this);
		$.ajax({
			url: url_pur, // Url to which the request is send
			type: "PATCH", // Type of request to be send, called as method
			contentType: false, // The content type used when sending data to the server.
			cache: false, // To unable request pages to be cached
			processData: false, // To send DOMDocument or non processed data file it is set to false
			success: function(data) // A function to be called if request succeeds
			{

				var obj = data;
				if (obj.status == "success") {
					M.toast({html:"Event Approved Successfuly"});
					pare.text("Approved");
					pare.fadeOut();
					$("#modal-event").closeModal();
				} else {
					console.log(data);
					M.toast({html:"Event Approve Failed"});
				}
			}
		});
	}));

	$(".rejectevent").on('click', (function(e) {
		e.preventDefault();
		M.toast({html:"<span class='upload-status'>Please wait...</span>"});
		var url_pur = "/api2/event/" + $(this).attr('data-event_id') + "/reject";
		var pare = $(this);
		$.ajax({
			url: url_pur, // Url to which the request is send
			type: "PATCH", // Type of request to be send, called as method
			contentType: false, // The content type used when sending data to the server.
			cache: false, // To unable request pages to be cached
			processData: false, // To send DOMDocument or non processed data file it is set to false
			success: function(data) // A function to be called if request succeeds
			{

				var obj = data;
				if (obj.status == "success") {
					M.toast({html:"Event Rejected Successfuly"});
					pare.parent().fadeOut();

				} else {
					console.log(data);
					M.toast({html:"Event Rejection Failed"});
				}
			}
		});
	}));

	var save_note = function() {
		M.toast({html:'Please Wait...'});
		var $ccc = document.getElementById("#how_we_do_it").innerHTML;
		var data = {
			section: 'how_we_do_it',
			content: $ccc
		};

		$.ajax({
			type: "PUT",
			url: "/api2/about_us",
			data: data,
			success: function(resp) {
				if (resp.status == "success") {
					M.toast({html:"Saved 'How we do it'"});
					var $ccc = document.getElementById("#what_do_we_do").innerHTML;
					$new_data = {
						section: 'what_do_we_do',
						content: $ccc
					};
					$.ajax({
						type: "PUT",
						url: "/api2/about_us",
						data: $new_data,
						success: function(resp2) {
							if (resp2.status == "success") {
								M.toast({html:"Saved 'What do we do'"});
							} else {
								M.toast({html:"An error occured while saving 'What do we do'"});
							}
						}
					});
				} else {
					M.toast({html:"An error occured while saving 'How do we do it'"});
				}
			}
		})
	};
	tinymce.init({
		selector: '.myeditablediv',
		menubar: 'file edit view',
		toolbar: 'save | bold | italic | underline | strikethrough | alignleft | aligncenter | alignright | alignjustify | styleselect | formatselect | cut | copy | paste | bullist | numlist | undo | redo',
		inline: true,
		plugins: 'save advlist autolink link image lists charmap print preview',
		save_onsavecallback: save_note
	});

	$("#add_video").on('submit', function(e) {
		e.preventDefault();
		var $ccc = $("#video_link").val();

		var $new_data = {
			section: 'youtube_link',
			content: $ccc
		};
		$.ajax({
			type: "PUT",
			url: "/api2/about_us",
			data: $new_data,
			success: function(resp2) {
				if (resp2.status == "success") {
					M.toast({html:"Youtube Link Updated"});
				} else {
					M.toast("An error occured while saving 'What do we do'", 4000);
				}
			}
		});
	});

	$("#add_faq").on('submit', function(e) {
		e.preventDefault();
		var parent_form = $(this);
		$.ajax({
			type: "POST",
			url: "/api2/faqs",
			data: parent_form.serialize(),
			contentType: "application/x-www-form-urlencoded",
			success: function(response) {
				M.toast({html:"Faq Added!"});
				setTimeout(3000, function() {
					window.location = "/admin/faqs";
				});
			},
			error: function(xhr) {
				alert('error');
			}
		});
	});

	$("#reg-user-top-idea").click(function(e) {
		e.preventDefault();
		var url_pur = "/api2/idea/" + $(this).attr('data-ideaid') + "/" + $(this).attr("data-action");
		var pare = $(this);
		var after_action;
		var text_action;
		if ($(this).attr("data-action") == "top") {
			after_action = "untop";
			text_action = "Unmark as Top Idea";
		} else {
			after_action = "top";
			text_action = "Mark as Top Idea";
		}
		$.ajax({
			url: url_pur,
			type: "PATCH",
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				var obj = JSON.parse(data);

				if (obj.status == "success") {
					M.toast({html:"Success"});
					pare.attr("data-action", after_action);
					pare.text(text_action);

				} else {
					console.log(data);
					M.toast({html:"Removed from top"});
				}
			}
		});
	});

	$("#admin_send_new_email").on('submit',function(e){
		e.preventDefault();

		$.ajax({
			url:"/api2/email",
			type:"POST",
			data: new FormData(this),
			contentType: false,
			cache: false,             // To unable request pages to be cached
			processData:false,
			beforeSend: function(){
				M.toast({html:"<span class='please_wait'>Please wait<span class='pprog'></span></span>",displayLength:15000});


			},
			success:function(data){
			
					$(".please_wait").text("Sent Successfully!");
					
					setTimeout(function(){
						$(".please_wait").parent().fadeOut(function(){
							$(".please_wait").text("");
						});
					},4000);
			}
		});

	});

	$("#admin_send_bulk_email").on('submit',function(e){
		e.preventDefault();

		$.ajax({
			url:"/api2/bulk_email",
			type:"POST",
			data: new FormData(this),
			contentType: false,
			cache: false,             // To unable request pages to be cached
			processData:false,
			beforeSend: function(){
				M.toast({html:"<span class='please_wait'>Please wait<span class='pprog'></span></span>",displayLength:15000});


			},
			success:function(data){
			
					$(".please_wait").text("Sent Successfully!");
					
					setTimeout(function(){
						$(".please_wait").parent().fadeOut(function(){
							$(".please_wait").text("");
						});
					},4000);
			}
		});

	});

	$('.chips-receivers').on('chip.delete', function(e, chip){
		$("#receivers").val(JSON.stringify($('.chips-receivers').material_chip('data')));
		console.log($("#service-tags").val());
	});

	$('.chips-receivers').on('chip.add', function(e, chip){
		$("#receivers").val(JSON.stringify($('.chips-receivers').material_chip('data')));
		console.log($("#service-tags").val());
	});

	$(".btn_add_sector").click(function(e){
		e.preventDefault();
		var ar={name:$(this).attr('data-name')};
		$.ajax({
			type:"POST",
			url:'/api2/sectors',
			data:$.param(ar),
			contentType: 'application/x-www-form-urlencoded',       
			cache: false,
			processData:false,
			success:function(data)
			{
				 if(data.status=="success")
				 {
					 M.toast({html:'Sector added successfully'});
					 setTimeout(function(){
						 location.reload();
					 },2000);
				 }
			},
			error: function(xhr)
			{
				M.toast({html:'An error occurred'});
				var obj=JSON.parse(xhr.responseText);
				M.toast(obj.description,3000);
			}
		});
	});

	$("#edit_faq").on('submit',function(e){
		e.preventDefault();
		var parent_form=$(this);
		$.ajax({
			type:"PUT",
			url:"/api2/faq",
			data:parent_form.serialize(),
			contentType:"application/json",
			success:function(response){
				M.toast({html:"Changes Saved!"});
			},
			error: function(xhr)
			{
				alert('error');
			}
		});
	});

	$("#delete_faq").on('click',function(e){
		e.preventDefault();
		var faq_id=$(this).attr('data-post_id');
		$.ajax({
			type:"Delete",
			url:"/api2/faq",
			data:"post_id="+faq_id,
			contentType:"application/json",
			success:function(response){
				M.toast({html:"Deleting..."});
				setTimeout(function(){
					window.location="/admin/faq";
				},1000);
			},
			error: function(xhr)
			{
				alert('error');
			}
		});
	});

	$("#sign_up_sp").on('submit',function(e){
		e.preventDefault();
		M.toast({html:"<span class='xhr_pw'>Please Wait...</span>"});

		$.ajax({
			type:"POST",
			url:"/api2/"+"service_providers",
			data:new FormData(this),
			contentType:false,
			processData:false,
			success: function(data)
			{
				debugger;

				$(".xhr_pw").text("Sign Up Success");
				$(".xhr_pw").parent().addClass("toastgreen");
				
			},
			error: function(xhr)
			{
				debugger;
				var jsObj=JSON.parse(xhr.responseText)
				$(".xhr_pw").text(jsObj.description);
				$(".xhr_pw").parent().addClass("toastred");
			}
		});
	});

	M.updateTextFields();
	

});

function progress(e){

	if(e.lengthComputable){
		var max = e.total;
		var current = e.loaded;

		var Percentage = (current * 100)/max;
		$(".please_wait").text($(".please_wait").text()+" "+Percentage+" %");


		if(Percentage >= 100)
		{
			// process completed
		}
	}
}

$('.list-topidea').click(function(e){
    e.preventDefault();
    var url_pur="/api2/idea/"+$(this).attr('data-ideaid')+"/untop";
    var pare=$(this);
    $.ajax({
        url: url_pur, // Url to which the request is send
        type: "PATCH",             // Type of request to be send, called as method
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData: false,        // To send DOMDocument or non processed data file it is set to false
        success: function (data)   // A function to be called if request succeeds
        {
            var obj=JSON.parse(data);
            if(obj.status=="success")
            {
                M.toast({html:"Removed from top"});
                pare.parent().parent().fadeOut();
            }
            else{
                console.log(data);
                M.toast({html:"Removed Failed"});
            }

        }
    });
});

function indexInParent(node) {
    var children = node.parentNode.childNodes;
    var num = 0;
    for (var i=0; i<children.length; i++) {
        if (children[i]==node) return num;
        if (children[i].nodeType==1) num++;
    }
    return -1;
}

$("#sign_up_ideator").on('submit',function(e){
	e.preventDefault();
	M.toast({html:"<span class='xhr_pw'>Please Wait...</span>"});

	$.ajax({
		type:"POST",
		url:"/api2/"+"ideators",
		data:new FormData(this),
		contentType:false,
		processData:false,
		success: function(data)
		{
			$(".xhr_pw").text("Sign Up Success");
		},
		error: function(xhr)
		{
			console.log(xhr);
			$(".xhr_pw").text(xhr.responseJSON.description);
			
		}
	});
});

$("#sign_up_vc").on('submit',function(e){
	e.preventDefault();
	M.toast({html:"<span class='xhr_pw'>Please Wait...</span>"});

	$.ajax({
		type:"POST",
		url:"/api3/admin_add_investors",
		data:new FormData(this),
		contentType:false,
		processData:false,
		success: function(data)
		{
			$(".xhr_pw").text("Sign Up Success");
		},
		error: function(xhr)
		{
			console.log(xhr);
			$(".xhr_pw").text(xhr.responseJSON.description);
			
		}
	});
});

$(".back_window").click(function(e){
    e.preventDefault()
    history.back();
    return false;
});


var all_collapsibles=document.getElementsByClassName('collapsible');
var collapsibles=[];
for(collapsible in all_collapsibles) {
	collapsibles.push(M.Collapsible.init(all_collapsibles[collapsible]));
}