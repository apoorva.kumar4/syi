var status = 0;
var selectedVal = "";
$(document).ready(function() {
	if ($("input.service-target-sectors[value='30']").prop("checked") == true) {
		$("input[name='target_sector_other']").parent().removeClass('hide');
	}
	$("input.service-target-sectors[value='30']").change(function() {

		if ($("input.service-target-sectors[value='30']").prop("checked") == true) {
			$("input[name='target_sector_other']").parent().removeClass('hide');
		} else {
			$("input[name='target_sector_other']").parent().addClass('hide');
		}
	});

	$('.faq_quetion').on('click', function(e) {
		e.preventDefault();
		$(this).parent().parent().parent().children('.panel-collapse').slideToggle();
	});
	$('input.character-counter').characterCounter();
	if (!Modernizr.inputtypes.date) {
		// If not native HTML5 support, fallback to jQuery datePicker
		$('input[type=date]').pickadate({
			// Consistent format with the HTML5 picker
			format: 'yyyy-mm-dd'
		});
	}
	$('.modal-trigger').leanModal();
	$('.modal-registration').leanModal();

	$('select').material_select();
	$('.carousel.carousel-slider').carousel({
		full_width: true
	});

	var limit = 3;
	$('input.service-target-phases').on('change', function(evt) {
		if ($(this).parent().parent().find('.service-target-phases:checked').length > limit) {
			this.checked = false;
		}
	});

	var limit_sector = 10;
	$('input.service-target-sectors').on('change', function(evt) {

		if ($('.service-target-sectors:checked').length > limit_sector) {
			this.checked = false;
		}
	});

	$('.start-datepicker').pickadate({
		format: 'yyyy-mm-dd',
		selectMonths: true, // Creates a dropdown to control month
		selectYears: 15 // Creates a dropdown of 15 years to control year
	});

	$('.chips-service-tags').on('chip.add', function(e, chip) {
		$("#service-tags").val(JSON.stringify($('.chips-initial').material_chip('data')));
		console.log($("#service-tags").val());
	});


	$('.chips-service-tags').material_chip({
		placeholder: '+tag',
		secondaryPlaceholder: 'Press enter to add multiple tags',
	});

	$('.chips-service-tags').on('chip.delete', function(e, chip) {
		$("#service-tags").val(JSON.stringify($('.chips-initial').material_chip('data')));
		console.log($("#service-tags").val());
	});

	$('.chips-service-tags').on('chip.add', function(e, chip) {
		$("#service-tags").val(JSON.stringify($('.chips-initial').material_chip('data')));
		console.log($("#service-tags").val());
	});


	$('.chips-service-tags-edit').on('chip.add', function(e, chip) {
		$("#service-tags").val(JSON.stringify($('.chips-service-tags-edit').material_chip('data')));
		console.log($("#service-tags").val());
	});

	$('.chips-service-tags-edit').on('chip.delete', function(e, chip) {
		$("#service-tags").val(JSON.stringify($('.chips-service-tags-edit').material_chip('data')));
		console.log($("#service-tags").val());
	});

	$('.chips-idea-tags-edit').on('chip.add', function(e, chip) {
		$('#idea_tags').val(JSON.stringify($(this).material_chip('data')));
		//console.log($("#service-tags").val());
	});

	$('.chips-idea-tags-edit').on('chip.delete', function(e, chip) {
		$('#idea_tags').val(JSON.stringify($(this).material_chip('data')));
		//console.log($("#service-tags").val());
	});

	$("#service-tags").val(JSON.stringify($('.chips-initial').material_chip('data')));

	$('.milestone-datepicker').pickadate({
		format: 'yyyy-mm-dd',
		selectMonths: true, // Creates a dropdown to control month
		selectYears: 15 // Creates a dropdown of 15 years to control year
	});

	$(".button-collapse").sideNav();


	$("#action-save-edit-bullet-modal").click(function(e) {

		e.preventDefault();
		console.log(JSON.stringify($("#form-edit-bullet-modal").serializeArray()));
		$.ajax({
			type: 'POST',
			url: '/api/v1/ideas/' + $(this).attr('data-ideaid') + '/bullets/' + $(this).attr('data-bulletid') + '/update',
			data: {
				'data': JSON.stringify($("#form-edit-bullet-modal").serializeArray())
			},
			success: function(resp) {
				console.log(resp);
				Materialize.toast('Changes saved!', 4000);
				location.reload();
			},
			error: function(resp) {
				console.log(resp);
				Materialize.toast('Error Occured, Try Again', 4000);
			}
		});
	});

	$("#action-delete-edit-bullet-modal").click(function(e) {

		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: '/api/v1/ideas/' + $(this).attr('data-ideaid') + '/bullets/' + $(this).attr('data-bulletid') + '/delete',
			success: function(resp) {
				console.log(resp);
				var resp_arr = JSON.parse(resp);
				if (resp_arr.status === "success") {
					Materialize.toast('Bullet Deleted', 4000);
					location.reload();
				} else {
					Materialize.toast('Default bullets cannot be deleted', 4000);
				}
			},
			error: function(resp) {
				console.log(resp);
				Materialize.toast('Error Occured, Try Again', 4000);
			}
		});
	});


	$(".year-datepicker").pickadate({
		format: 'yyyy',
		selectYears: true,
		selectMonths: false,
		selectDate: false,
		closeOnSelect: true
	});
	$(".followidea").click(function(e) {
		e.preventDefault();
		var action = $(this).attr("data-action") ? $(this).attr("data-action") : "follow";
		//var url="/api2/idea/"+$(this).attr("data-ideaid")+action;
		var url = $(this).attr("data-hrefx");

		var misa = $(this);
		var misa_text = $(this).text();
		var idea_id = $(this).attr("data-ideaid");
		var placeholder_idea_followers = $("#placeholder_" + idea_id + "_followers");
		$.ajax({
			url: url,
			type: 'PUT',
			beforeSend: function() {
				misa.text("Please wait");
				misa.addClass("disabled");
			},
			success: function(data, status) {
				var obj = data;
				if (obj.status == "success") {
					if (obj.description.toUpperCase() == "IDEA FOLLOWED SUCCESSFULLY") {
						misa.removeClass("green").addClass("red");
						misa.text("Unfollow Idea");
						Materialize.toast(obj.description, 4000);
						misa.removeClass("disabled");
						console.log(data);
						var update_url = "/api2/idea/" + misa.attr("data-ideaid") + "/unfollow";
						misa.attr("data-hrefx", update_url);
						misa.attr("data-action", "unfollow");
						placeholder_idea_followers.text(obj.followers + " followers");

					} else if (obj.description.toUpperCase() == "IDEA UNFOLLOWED SUCCESSFULLY") {
						misa.removeClass("red").addClass("green");
						misa.text("Follow Idea");
						Materialize.toast(obj.description, 4000);
						misa.removeClass("disabled");
						console.log(data);
						var update_url = "/api2/idea/" + misa.attr("data-ideaid") + "/follow";
						misa.attr("data-hrefx", update_url);
						misa.attr("data-action", "follow");
						placeholder_idea_followers.text(obj.followers + " followers");
					}

				} else {
					Materialize.toast(obj.description, 4000);
					misa.removeClass("disabled");
					misa.text(misa_text);

				}

			},
			error: function(xhr) {
				var obj = JSON.parse(xhr.responseText);
				Materialize.toast(obj.description, 4000);
				misa.removeClass("disabled");
				misa.text(misa_text);
			}
		});
	});

	$(".cc_visibility").mouseup(function(e) {
		//e.preventDefault();
		var ideaid = $(this).attr('data-ideaid');
		var toggle_url = '/api/v1/ideas/' + ideaid + '/visibility/toggle';
		console.log(toggle_url);
		$.get(toggle_url, function(data, status) {


			if (status === "success") {
				if (data) {
					var obj = JSON.parse(data);

					if (obj.status === "success") {
						$.get('/api/v1/ideas/' + ideaid, function(data1, status1) {
							if (status1 === "success") {
								var idea = JSON.parse(data1);
								if (idea.visibility == 1)
									Materialize.toast(idea.name + ' is now public', 4000);
								else
									Materialize.toast(idea.name + ' is now private', 4000);
							} else {}
						});
					}
				}

			}


		});
	});

	$("form.getin").submit(function(e) {
		e.preventDefault();
		Materialize.toast('<span id="status_getin">Please Wait...</span>');
		
		var data = $(this).serialize();
		var jqxhr = $.post('/social/sayourideas', data);

		jqxhr.done(function(result) {
			if (result) {

				var obj = JSON.parse(result);
				if (obj.status === "success") {
					$("#status_getin").html('<span id="status_getin">Login Success, Redirecting...</span>');
					location.reload();
				} else if (obj.status === "refresh") {
					location.reload();
				} else
					//Materialize.toast(obj.description, 4000);
					$("#status_getin").html('<span id="status_getin">'+obj.description+'</span>');
					setTimeout(()=>{
						$("#status_getin").parent().parent().fadeOut(400,function(){
							this.remove();
						});
					},3000);
			} else {
				alert("Noresult");
			}


		});
	});


	$('#student_alumni').material_select();

	$(".input-field label").click(function() {
		$(this).siblings('input').focus();
		$(this).siblings('textarea').focus();
	});

	$("#file").change(function() {
		var file = this.files[0];
		var imagefile = file.type;
		var match = ["image/jpeg", "image/png", "image/jpg"];
		if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
			$('#previewing').attr('src', 'noimage.png');
			Materialize.toast("Invalid image", 4000)
			return false;
		} else {
			var reader = new FileReader();
			reader.onload = imageIsLoaded;
			reader.readAsDataURL(this.files[0]);
		}
	});

	$(".messagedelete").click(function(e) {
		e.preventDefault();
		var flag = $(this);

		var url = $(this).attr('data-hrefx');
		$.ajax({
			url: url, // Url to which the request is send
			type: "GET", // Type of request to be send, called as method
			contentType: false, // The content type used when sending data to the server.
			cache: false, // To unable request pages to be cached
			processData: false, // To send DOMDocument or non processed data file it is set to false
			success: function(data) // A function to be called if request succeeds
			{

				var obj = JSON.parse(data);
				if (obj.status == "success") {
					Materialize.toast(obj.description, 4000);
					flag.parent().parent().parent().fadeOut();
				} else {
					Materialize.toast(obj.description, 4000);
				}
			}
		});
	});

	$("#rate").change(function() {
		var file = this.files[0];
		var ratefile = file.type;
		var match = ["image/jpeg", "image/png", "image/jpg", "application/pdf"];
		if (!((ratefile == match[0]) || (ratefile == match[1]) || (ratefile == match[2]) || (ratefile == match[3]))) {

			Materialize.toast("Invalid image", 4000)
			return false;
		} else {
			var reader = new FileReader();
			reader.onload = imageIsLoaded;
			reader.readAsDataURL(this.files[0]);
		}
	});

	$("#uploadRateCard").on('submit', (function(e) {
		e.preventDefault();
		Materialize.toast("<span class='upload-status'>Uploading Price Card, Please wait...</span>");
		var url_pur = "/api3/add_rate_card/" + $("#profile-picture").attr('data-userid') + "/rate-cards/upload";
		$.ajax({
			url: url_pur, // Url to which the request is send
			type: "POST", // Type of request to be send, called as method
			data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false, // The content type used when sending data to the server.
			cache: false, // To unable request pages to be cached
			processData: false, // To send DOMDocument or non processed data file it is set to false
			success: function(obj) // A function to be called if request succeeds
			{

				if (obj.status == "success") {
					Materialize.toast("File Uploaded Successfully", 4000);
					window.location = '/dashboard';

				} else {
					Materialize.toast("Upload Failed: " + obj.description, 4000);
				}
			}
		});
	}));
	$("#update-image-submit").click(function() {
		$("#uploadImage").submit();
	});

	$(".view_event_gallery").click(function(e) {
		e.preventDefault();
		pswpElement = document.querySelectorAll('.pswp')[0];

		if (gallery_items != null) {
			event_gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, gallery_items);
			event_gallery.init();
			return;
		}

		var url = "/api2/event/" + $(this).attr('data-event_id') + "/gallery";
		Materialize.toast("<span id='toast_loading_gallery'>Loading Gallery...</span>");



		$.ajax({
			url: url,
			type: "GET",
			success: function(data) {
				$("#toast_loading_gallery").parent().fadeOut();


				// build items array
				gallery_items = data;
				if (data == null) {
					$("#toast_loading_gallery").html("No images for this event yet!");
					setTimeout(function() {
						$("#toast_loading_gallery").parent().fadeOut();
					}, 4000)
				}
				// Initializes and opens PhotoSwipe
				event_gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, gallery_items);
				event_gallery.init();
			},
			error: function(xhr) {
				$("#toast_loading_gallery").html("No images for this event yet!");
				setTimeout(function() {
					$("#toast_loading_gallery").parent().fadeOut();
				}, 4000);

			}
		});
	});
});

var event_gallery = null;
var pswpElement;
var gallery_items = null;


function imageIsLoaded(e) {
	$("#file").css("color", "green");
	$('#image_preview').css("display", "block");
	$('#previewing').attr('src', e.target.result);
	$('#previewing').attr('width', '250px');
	$('#previewing').attr('height', '230px');
};

function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}
String.prototype.capitalizeFirstLetter = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
}