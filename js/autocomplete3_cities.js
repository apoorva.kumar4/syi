jQuery(document).ready(function($){
	
	var cities= [
	{
    data: "Mumbai",
    value: "Mumbai"
  },
  {
    data: "Delhi",
    value: "Delhi"
  },
  {
    data: "Bengaluru",
    value: "Bengaluru"
  },
  {
    data: "Ahmedabad",
    value: "Ahmedabad"
  },
  {
    data: "Hyderabad",
    value: "Hyderabad"
  },
  {
    data: "Chennai",
    value: "Chennai"
  },
  {
    data: "Kolkata",
    value: "Kolkata"
  },
  {
    data: "Pune",
    value: "Pune"
  }  
];
$("#autocomplete_cities").autocomplete({
	lookup:cities
});

});
